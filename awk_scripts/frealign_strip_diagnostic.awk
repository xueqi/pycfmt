
tolower($0) ~ "time" || tolower($0) ~ "err" || tolower($0) ~ "stop" {
  print tolower($0);
  fflush();
}

tolower($0) ~ "write" && tolower($0) !~ "write out" {
  print;
  fflush();
}

tolower($0) ~ "read" && tolower($0) !~ "spread" {
  print;
  fflush();
}

tolower($0) ~ "cc for particle" {
  print;
  fflush();
}

