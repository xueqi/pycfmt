BEGIN {
  if(method"a" == "a") 
    method = "exact";
  else
    method = "wade";      # published method by chretien and wade has geometric approximations--
                          #  these are insignificant for the MT's likely to exist, however

  if(num_pfs"a" == "a") num_pfs = 13;
  if(num_starts"a" == "a") num_starts = 3;
  if(repeat_dist13pf"a" == "a") repeat_dist13pf = 80;                 # protofilament axial repeat distance, in angstroms

  if(r13pf"a" == "a") r13pf = 112 #  106.6;                      # 13-pf MT radius, in angstroms

  if(pdb2dxtal == 1)  {
    if(thetaoffset"a" == "a") thetaoffset = 60;           # axially rotate tubulin by this amount before assembling
    xyz_permute = 1;
  } else {
    thetaoffset = 0;
    xyz_permute = 0;
  }

  if(generate"a" == "a") generate=1;

  pi = atan2(0,-1);

  theta_pitch13 = atan2(3/2*repeat_dist13pf, 2*pi*r13pf);
  w_pf = 2*pi*r13pf/(13*cos(theta_pitch13));
  theta_pf = pi/2 - theta_pitch13;

  print "13-pf MT: r = " r13pf ", theta_pitch = " deg(theta_pitch13) ", theta_pf = " deg(theta_pf)", dimer repeat = " repeat_dist13pf ;

#  x = (num_starts/2) * sin(theta_pf) * repeat_dist13pf/(num_pfs * w_pf);

#  y1 = sqrt((1 - sqrt(1 - 4 * x * x))/2);
#  y2 = sqrt((1 + sqrt(1 - 4 * x * x))/2);

#  theta_pitch = -atan2(y1, sqrt(1 - y1*y1));
#  soln2 = atan2(y2, sqrt(1 - y2*y2));

#  mt_radius = num_pfs * w_pf * cos(theta_pitch)/(2*pi);
#  print "r = " mt_radius ", theta_pitch = " deg(theta_pitch)", offset pf angle = " deg(theta_pf + theta_pitch) - 90;

  best_error = 10000;
  for(theta_pitch = pi/2 - theta_pf - rad(9); theta_pitch <= pi/2 - theta_pf + rad(9); theta_pitch += rad(.001)) {
    d = num_starts/2*repeat_dist13pf * sin(theta_pf)/cos(theta_pitch);
    mt_radius = 1/(2*pi) * d * cos(theta_pitch)/sin(theta_pitch);

    delta = d * sin(theta_pitch - (pi/2 - theta_pf))/cos(theta_pitch);
#    print delta, d * sin(theta_pitch - (pi/2 - theta_pf))/sin(theta_pf);
#    exit(0);
    error = abs(2*pi*mt_radius/cos(theta_pitch) - delta - num_pfs*w_pf)
    if(error < best_error) {
      best_theta_pitch = theta_pitch;
      best_error = error;
    }
#    print num_pfs "- protofilament MT (" num_starts" -start): theta_pitch = "deg(theta_pitch)" r = " mt_radius ", error = " error " angstroms";
  }

  if(method == "exact") {
    theta_pitch = best_theta_pitch;
    d = num_starts/2 * repeat_dist13pf * sin(theta_pf)/cos(theta_pitch);
    mt_radius = 1/(2*pi) * d * cos(theta_pitch)/sin(theta_pitch);

  }  else {

    delta_n = 3/2 * repeat_dist13pf * num_pfs / 13 - num_starts/2 * repeat_dist13pf;
    theta_pitch = theta_pitch13 - atan2(delta_n, num_pfs * w_pf * cos(theta_pitch13));
    mt_radius = num_pfs * w_pf * cos(theta_pitch13) / (2*pi);

    d = num_starts/2 * repeat_dist13pf * sin(theta_pf)/cos(theta_pitch);
  }

  print "protofilament separation: " w_pf * cos(theta_pitch13)

  twist_per_subunit = w_pf * cos(theta_pitch) / mt_radius;
  rise_per_subunit = w_pf * sin(theta_pitch);
  protofilament_twist = repeat_dist13pf*sin(theta_pitch - theta_pitch13) / mt_radius;

  print num_pfs "- protofilament MT (" num_starts" -start): theta_pitch = "deg(theta_pitch)" r = " mt_radius ", error = " best_error " angstroms";
  print "Rise per subunit: " rise_per_subunit, "twist_per_subunit: " deg(twist_per_subunit) " protofilament twist: " deg(protofilament_twist);

  if(theta_pitch != theta_pitch13) {
    print "Characteristic length: ", abs(w_pf * cos(theta_pitch) / (sin(theta_pitch-theta_pitch13)/cos(theta_pitch-theta_pitch13)));
    if(3/2*num_pfs != num_starts/2*13)
      print " (wade value: " abs(13*num_pfs*(w_pf * cos(theta_pitch13))*(w_pf * cos(theta_pitch13))/(repeat_dist13pf*(3/2*num_pfs - num_starts/2*13))) ")";
    else
      print " (wade value: infinity)";
  } else
    print "Characteristic length: infinity";

  if(generate == 0)
    exit(0);
}

NR == 1 {
}

{
  if($1 == "ATOM") {
    if(! inited) {
      inited = 1;
      for(i = 1; i <= num_pfs; ++i) {
	outfile[i]=sprintf(substr(FILENAME,1,length(FILENAME) - 4)"_pf%02d.pdb", i);
	printf "" > outfile[i];
      }
    }

    pdbfield1 = substr($0,0,6);
    pdbfield2 = substr($0,7,5);
    pdbfield3 = substr($0,13,4);
    pdbfield4 = substr($0,17,1);
    pdbfield5 = substr($0,18,3);
    pdbfield6 = substr($0,22,1);
    pdbfield7 = substr($0,23,4);
    pdbfield8 = substr($0,27,1);
    pdbfield9 = substr($0,31,8);
    pdbfield10 = substr($0,39,8);
    pdbfield11 = substr($0,47,8);
    pdbfield12 = substr($0,55,6);
    pdbfield13 = substr($0,61,6);
    pdbfield14 = substr($0,67,6);
    pdbfield15 = substr($0,73,4);
    pdbfield16 = substr($0,77,2);
    pdbfield17 = substr($0,79,2);

# NB in the PDB structure, the protofilament axis is along x; 
# here we swap coordinates to make z be the protofilament axis.
# (NBNB: cyclically shifting the coordinates as done here avoids changing the 
# handedness)

    if(xyz_permute == 1)  {
      z = pdbfield9 - xoffset;
      x = pdbfield10 - yoffset;
      y = pdbfield11 - zoffset;
    } else {
      x = pdbfield9 - xoffset;
      y = pdbfield10 - yoffset;
      z = pdbfield11 - zoffset;
    }

    xc = x*cos(thetaoffset * pi/180) - y*sin(thetaoffset * pi/180);
    yc = x*sin(thetaoffset * pi/180) + y*cos(thetaoffset * pi/180);
    zc = z;

    for(i = 1; i <= num_pfs; ++i) {
      cos_i = cos((i-1) * twist_per_subunit);
      sin_i = sin((i-1) * twist_per_subunit);
      x_out = (xc + mt_radius)*cos_i - yc*sin_i;
      y_out = (xc + mt_radius)*sin_i + yc*cos_i;
      z_out = zc - (i - int(num_pfs/2)) * rise_per_subunit;

      printf "%-6s%5d %4s%s%3s %s%4d%s   %8.3f%8.3f%8.3f%6.2f%6.2f\n",pdbfield1,pdbfield2,pdbfield3,pdbfield4,pdbfield5,pdbfield6,pdbfield7,pdbfield8,x_out,y_out,z_out,pdbfield12,pdbfield13 >> outfile[i];

#      printf "%6s%4s%2s%2s\n",pdbfield14,pdbfield15,pdbfield16,pdbfield17;
    }

  } else if(NF > 0 && inited) {
    for(i = 1; i <= num_pfs; ++i) {
      print $0 >> outfile[i];
    }
  }
}

function rad(x) {return(x*pi/180)}
function deg(x) {return(x*180/pi)}
function abs(x) {return(x >= 0 ? x : -x)}
