BEGIN {
  num_script_args = 0;
  parse_parameter_files();
}

############
# First line given to this script is the command line the user typed
############
NR == 1 {
  if($0 ~ "^-")
    command_line = "";
  else
    command_line = $0;
}
  
############
# Next lines that start with "chuff_args" specify which variables can be assigned
############
NR > 1 && $1 == "#" && $2 == "chuff_args" {
  parse_script_args(1);
}

############
# Next lines that start with "chuff_required_variables" specify which variables 
#  NEED to be assigned
############

NR > 1 && $1 == "#" && $2 == "chuff_required_variables" {
  parse_script_args(0);
}

NR > 1 && $1 == "#" && $2 == "chuff_required_executables" {
  parse_script_args(0);
}

END {
  if(!error)  {

    add_option("help", -1);
    add_option("cfdebug", -1);

    parse_command_line();

    command_csh_command = command_csh_command "set script_args = (" 
    for(i = 1; i <= num_script_args; ++i)
      command_csh_command = command_csh_command " " script_args[i];
    command_csh_command = command_csh_command ");";

    command_csh_command = command_csh_command "set chuff_args = (" 
    for(i = 1; i <= n_option_list["chuff_args"]; ++i)
      command_csh_command = command_csh_command " " option_list["chuff_args "i];
    command_csh_command = command_csh_command ");";

    command_csh_command = command_csh_command "set chuff_required_variables = (" 
    for(i = 1; i <= n_option_list["chuff_required_variables"]; ++i)
      command_csh_command = command_csh_command " " option_list["chuff_required_variables "i];
    command_csh_command = command_csh_command ");";

    command_csh_command = command_csh_command "set chuff_required_executables = (" 
    for(i = 1; i <= n_option_list["chuff_required_executables"]; ++i)
      command_csh_command = command_csh_command " " option_list["chuff_required_executables "i];
    command_csh_command = command_csh_command ");";

    print script_csh_command;
    print parameter_csh_command;
    print command_csh_command;
  }
}

function parse_script_args(add_to_option_library) {
  first_word = 3;

  option_list_name = $2;

  gsub("[ ]*[,][ ]*", ",");
  gsub("[=]", " = ");
  i = first_word;
  while(i <= NF)  {
    delta_i = 1;

    n_option_list[option_list_name] += 1;
    option_list[option_list_name" "n_option_list[option_list_name]] = $i;

    if(add_to_option_library) {
      n_option_library += 1;
      option_library[n_option_library] = $i;
      num_args[n_option_library] = -1;
    }

    if(i < NF)  {
      if($(i+1) == "=")  {
        delta_i = 3;
        if(i + 2 <= NF && add_to_option_library)  {
          val = $(i+2);
          num_args[n_option_library] = split(val, vals, ",");
          gsub("," , " ", val);
#          if(assigned[option_library[n_option_library]])
#            val = "$" option_library[n_option_library] " " val;

          script_csh_command = script_csh_command "set " option_library[n_option_library] " = (" val ");";
          assigned[option_library[n_option_library]] = 1;
        }
      }
    }
    i += delta_i;
  }
}

function parse_command_line()  {
  gsub("[ ]*[,][ ]*", ",", command_line);
  gsub("[=]", " = ", command_line);
  n = split(command_line, command_options);

  if(command_options[1] == "-help") {
    print "set help = T";
    exit(0);
  }

  i = 1;
  num_script_args = 0;
  reading_options = 0;
  chuff_extra_args = "";
  while(i <= n)  {
    option_candidate = command_options[i];

    if(i < n && command_options[i+1] == "=")
      reading_options = 1;
    if(command_options[i] ~ "^[-]")
      reading_options = 1;

    if(reading_options) {
#############
# Find which option the user specified
#############
      option_index = -1;
      found_option = "";
      for(j = 1; j <= n_option_library; ++j) {
        if(option_library[j] ~ "^" option_candidate) {
          option_index = j;
          if(found_option != "") {
	    error_msg = "ERROR: ambiguous option " option_candidate " specified";
            error_msg = error_msg "\n" "#   (" found_option ", " option_library[j] ")";
            exit_error(error_msg);
          }
          found_option = option_library[j];
        }
      }

#############
# Detect errors
#############
      if(option_index != -1 && command_assigned[found_option]) {
        error_msg = "ERROR: option \""found_option"\" specified more than once";
        exit_error(error_msg);
      }
      command_assigned[found_option] = 1;

      if(option_index != -1)  {
        if(i == n)  {
          error_msg = "ERROR: no argument found for option \"" command_options[i] "\"";    
          exit_error(error_msg);
        } else {
          if(command_options[i+1] != "=") {
            error_msg = "ERROR: no argument found for option \"" command_options[i] "\"";    
            exit_error(error_msg);
          }
          if(i > n-2)  {
            error_msg = "ERROR: no argument found for option \"" command_options[i] "\"";    
            exit_error(error_msg);
          }
        }
      }
########
# If chuff_get_extra_args is non-zero, we allow other arguments to come through
#  that weren't in the "chuff_args" specifications
########
      if(option_index == -1 && chuff_get_extra_args == 0) {
        error_msg = "ERROR: unrecognized option \"" option_candidate "\"";
        exit_error(error_msg);
      }
    } else {
      if(command_options[i] ~ "[=]")  {
        if(num_script_args == 0)  {
          error_msg = "ERROR: can't parse command line arguments:";
          error_msg = error_msg "\n" "#   " command_line;
          exit_error(error_msg);
        } else {
########
# If chuff_get_extra_args is non-zero, we allow other arguments to come through
#  that weren't in the "chuff_args" specifications
########
          if(chuff_get_extra_args == 0)  {
            error_msg = "ERROR: unrecognized option \"" script_args[num_script_args] "\"";
            exit_error(error_msg);
          }
        }
      }  else {
        num_script_args += 1;
        script_args[num_script_args] = command_options[i];
      }
    }

    if(reading_options)  {
#########
# If no option is found (option_index == -1) that means we just pass through the word.
#  NB  This is only allowed if "chuff_get_extra_args" is non-zero
########
      if(option_index == -1)  {
        delta_i = 1;
        chuff_extra_args = chuff_extra_args " " option_candidate;
      } else {
#########
# Assign specified values (or detect error)
#########
        delta_i = 3;
        val = command_options[i+2];
        cur_n_args = split(val, vals, ",");
#        if(num_args[option_index] != -1)  {
#          if(cur_n_args != num_args[option_index]) {
#            error_msg = "ERROR: Wrong number of arguments (" cur_n_args ") for option \"" option_candidate "\"";
#            exit_error(error_msg);
#          }
#        }
        gsub("," , " ", val);
        command_csh_command = command_csh_command "set " option_library[option_index] " = (" val ");"
      }
    } else
      delta_i = 1;

    i = i + delta_i;
  }

  command_csh_command = command_csh_command "set chuff_extra_args = ('" chuff_extra_args "'); ";
}

function parse_parameter_files()  {
  if(parameter_file == "")
    return;
  n_parameter_files = split(parameter_file,parameter_files,",");

  line_extension_spec = "\\[ \t]*$";
  var_num = 1;
  
  parameter_csh_command = "";
  for(i_file = 1; i_file <= n_parameter_files; ++i_file)  {
    while( (getline < parameter_files[i_file]) == 1)  {
      line = $0;

      ###########
      # Get rid of comment lines
      ###########
      sub("[#].*$","", line);
      sub("[%].*$","", line);

      ###########
      # Get rid of matlab style symbols "=" ";" "[" "]"
      #  (this script does not need or use them to interpret assignments)
      ###########
      sub("[=]"," ", line);
      sub("[;]","", line);
      sub("[[]","", line);
      sub("[]]","", line);

      n_elts = split(line, line_elts);
      if(n_elts == 0)
	  line_elts[1] = "";

      if(line_elts[1] ~ "^[A-Za-z0-9_]*$")  {
        ###########
        # Merge continued lines ( where previous line ended with "\" )
        ###########
        while($0 ~ line_extension_spec)  {
          sub(line_extension_spec, "", line);
          if( (getline < parameter_files[i_file]) != 1)
            break;
          line = line $0;
          if($0 !~ line_extension_spec)
            break;
        }
        num_elts = split(line, args);

        ###########
        # Need at least 2 words to indicate an assignment
        ###########
        if(num_elts >= 2)  {
          ###########
          # Make a shell script command that assigns the appropriate variable, i.e. 
          #  "set xxxx = (yyyy zzzz)"
          ###########
          parameter_csh_command = parameter_csh_command "set " args[1] " = (";

          ###########
          # If the shell variable already exists, merge the old values into a list with the new values
          ###########
#          if(assigned[args[1]])
#            parameter_csh_command = parameter_csh_command "$" args[1]" ";

          ###########
          # Add new values to the list
          ###########
          for(i = 2; i <= num_elts; ++i)
            parameter_csh_command = parameter_csh_command args[i] " ";

          parameter_csh_command = parameter_csh_command ") ; ";
          var_num = var_num + 1;
          assigned[args[1]] = 1;
        }
      }
    }
  }
}

function add_option(option, default_val)  {
  n_option_library += 1;
  option_library[n_option_library] = option;
  num_args[n_option_library] = default_val;
}
  
function exit_error(error_msg)
{
  if(diagnose != 1) {
    print "set chuff_error\n";
  } else
    print error_msg;

  exit(2);
}

