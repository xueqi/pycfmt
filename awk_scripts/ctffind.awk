
BEGIN {
  if(bin "a" == "a") {
    bin = 1;
  }
}

$0 ~ "Final Values" {

# First extract micrograph number from micrograph name:

  match(micrograph_name, "[0-9]+[.]...$"); 

  if(RLENGTH > 0) 
    micrograph_number = substr(micrograph_name, RSTART, RLENGTH -4); 
  else 
    micrograph_number = -1;

  if(micrograph_number != -1)  {
    ctf_info[1] = micrograph_number + 1;          # document indices must be >= 1, image number could be 0
    ctf_info[12] = micrograph_number + 0;
  }  else  {
    ctf_info[1] = 1;
    ctf_info[12] = -1;
  }

  if(!for_spider) {
    defocus1 = $1;
    defocus2 = $2;
    angle_astig = $3;

    ctf_info[2] = defocus1;
    ctf_info[3] = defocus2;
    ctf_info[4] = angle_astig;

  } else {
    defocus = ($1 + $2)/2;
    astig = ($2 - $1);
    angle_astig = $3 - 45;
    if(astig < 0) {
      astig = -astig;
      angle_astig = angle_astig + 90;
    }
    ctf_info[2] = defocus;
    ctf_info[3] = astig;
    ctf_info[4] = angle_astig;
  }

  ctf_info[5] = $5

  if(!ctftilt)
    no_error = 1;
}

$1 == "CX" {
  ctf_info[6] = $5;
}
$1 == "CY" {
  ctf_info[7] = $5;
}

# $1 == "PSIZE" {
#   ctf_info[8] = $7;
# }
$1 == "CS[mm]" && ctftilt {
  getline;
  ctf_info[8] = $5*10000/$4;
}

$1 == "N1" && ctftilt {
  ctf_info[9] = $5;
}

$1 == "N2" && ctftilt {
  ctf_info[10] = $5;
  ctf_info[11] = magnification;
  no_error = 1;
}

END {
  if(no_error) {
    if(output_text " " == " ")
      print_doc_line(ctf_info, 13);
    else {
      if(micrograph_name " " == " ")
	printf "" > output_text;
      print_doc_to_file(ctf_info, 13,output_text);
    }

  } else {
    exit(2);
  }
}

function print_doc_line(info, num_infos)
{
  printf("%5d%3d", info[1], num_infos - 1);
  for(j = 2; j <= num_infos; ++j)
    printf(" %11.4f ", info[j]);
  printf "\n";
}

function print_doc_to_file(info, num_infos, filename)
{
  printf("%5d%3d", info[1], num_infos - 1) >> filename;
  for(j = 2; j <= num_infos; ++j)
    printf(" %11.4f ", info[j]) >> filename;
  printf "\n" >> filename;
}
