#!/usr/bin/env python


from chuff_parser import ChuffParser, ChuffGooey
import read_chuff_parameters
from chuff.util.command import run_command, build_commands
from chuff.util.box import Box
from chuff.util import get_next_workdir
import relion
import math
import os
import glob


def setup_arg_parser():
   
    parser = ChuffParser(description="Import coordinates")
    parser.add_argument('--input_star', required=True, widget="FileChooser",
            help="Input file, e.g. from CFCtf/job001/micrographs_ctf.star")
    parser.add_argument('--output_star', 
            help="Output star file")
    parser.add_argument('--helical', action="store_true",
            help="import helical coordinates.")
    parser.add_argument('--helical_rise', type=float, 
            help="leave blank if do not segment the filament")
    parser.add_argument('--continuous_filaments', action='store_true',
            help='Use to connect boxes of curved filaments.')
    parser.add_argument('--ext', default='mrc')
    return parser

@ChuffGooey
def my_func():
    parser = setup_arg_parser()
    par = read_chuff_parameters.read_chuff_parameters(parser)
    par.pixel_size = par.micrograph_pixel_size
    istar = relion.Metadata.read_file(par.input_star)
    # prepare the workdir
    wd = get_next_workdir('CFImport')
    if par.helical_rise is not None and par.helical_rise <= 0:
        par.helical_rise = None
    if par.helical_rise is not None:
        par.helical = True
    if par.helical:
        print("Boxing helical filaments")
    if par.helical_rise > 1:
        print("Segment the filament with distance: %d A" % par.helical_rise)
    if par.output_star is None:
        par.output_star = os.path.join(wd, "particles.star")
    ostar = relion.Metadata()
    basename = os.path.basename 
    print "_______________________________"
    for mg in istar:
        micrograph = mg['rlnMicrographName']
        output_box = "%s.box" % os.path.splitext(micrograph)[0]
        print micrograph
        mg_dir = os.path.dirname(micrograph)
        # check if there is any ctf info 
        if not 'rlnVoltage' in mg.keys():
            try:
                mg['rlnVoltage'] = par.accelerating_voltage
                mg['rlnAmplitudeContrast'] = par.amplitude_contrast_ratio
                mg['rlnDetectorPixelSize'] = par.scanner_pixel_size
                mg['rlnMagnification'] = par.target_magnification
                mg['rlnSphericalAberration'] = par.spherical_aberration_constant
            except:
                raise Exception("key doese not found. Please make sure the cf_init_project has been run, and the cf-parameters.m contains the  accelerating_voltage, amplitude_contrast_ratio, scanner_pixel_size, target_magnification and spherical_aberration_constant")
        if not 'rlnDefocusU' in mg.keys():
            ctf_doc = "%s_ctf_doc.spi" % os.path.splitext(micrograph)[0]
            if os.path.exists(ctf_doc):
                ad = open(ctf_doc).readlines()
                line = None
                for line in ad:
                    if not line.strip() or line.strip().startswith(";"): continue
                    break
                if line is not None:
                    d1, d2, ang = map(float, line.split()[2:5])
                    mg['rlnDefocusU'] = d1
                    mg['rlnDefocusV'] = d2
                    mg['rlnDefocusAngle'] = ang
            else:
                raise Exception("Please run ctf first")
                # check ctf_files dir
        cwd = os.getcwd()
        if not os.path.exists(output_box):
            # Remove the EMAN2 parts
            if os.path.exists(os.path.join(mg_dir, "EMAN2DB22222")):
                # try to get box from E2helixboxer
                os.chdir(mg_dir)
                cmd = build_commands('e2helixboxer.py', basename(micrograph), **{
                    "helix-coords"  : basename(output_box), 
                    "helix-width"   : 64,
                    'prefix'        : '--',
                    'sep'           : ' ' })
                run_command(cmd, join = True)
                os.chdir(cwd)
            # This one is used for compatibility with old chuff
            elif len(glob.glob("%s_MT*_1.box" % os.path.splitext(micrograph)[0])) > 0:
                # there is segmented box. Used for the test data, don't know if there is any in the future
                new_boxes = []
                for fboxname in sorted(glob.glob("%s_MT*_1.box" % os.path.splitext(micrograph)[0])):
                    fboxes = Box.read_from_file(fboxname)
                    for b3 in fboxes: b3.tag = 0
                    fboxes[0].tag = -1
                    fboxes[-1].tag = -2
                    new_boxes.extend(fboxes)
                Box.write_box_file(output_box, new_boxes)
            else:
                print "%s do not have any box" % micrograph
                continue
        boxes = Box.read_from_file(output_box)
        if len(boxes) == 0:
            print "There is no box in", output_box
            continue
        # segment filaments
        if par.helical_rise is not None:
            filaments = []
            for box in boxes:
                if box.tag == -1: 
                    # start of filament
                    if len(filaments) == 0 or len(filaments[-1]) == 2:
                        filaments.append([box])
                elif len(filaments) > 0 and len(filaments[-1]) == 1 and box.tag == -2:
                    filaments[-1].append(box)
            # remove malformed coordinates
            while len(filaments) > 0 and len(filaments[-1]) != 2:
                filaments.pop()
            # segment filament
            if par.continuous_filaments:
                tubeid = 1
                cont=False
                oldX,oldY=-1111111,-1111111
                for box_start, box_end in filaments:
                    print box_start._box_size.x, math.sqrt((oldX-box_start.center.x)**2+(oldY-box_start.center.y)**2) < box_start._box_size.x*0.5
                    if math.sqrt((oldX-box_start.center.x)**2+(oldY-box_start.center.y)**2)< \
                                               box_start._box_size.x*0.5:
                        tubeid -= 1
                    seg_boxes = segment_filament(box_start, box_end, helical_rise = par.helical_rise, pixel_size = par.pixel_size)
                    angle_psi = -math.atan2(box_end.center.y - box_start.center.y, box_end.center.x - box_start.center.x) * 180 / math.pi
                    for i, box in enumerate(seg_boxes):
                        b1 = {'rlnCoordinateX'  : box.center.x,
                            'rlnCoordinateY'    : box.center.y,
                            'rlnAnglePsi'       : angle_psi,
                            'rlnHelicalTrackLength' : 0,
                            'rlnOriginX'        : 0,
                            'rlnOriginY'        : 0,
                            'rlnHelicalTubeID'  : tubeid,
                            'rlnAngleTilt'      : 90,
                            }
                        b1.update(mg)
                        if len(ostar.labels) == 0:
                            ostar.add_labels(b1.keys())
                        ostar.append(b1)
                    oldX,oldY=box_end.center.tolist()
                    tubeid += 1
            else:
                tubeid = 1
                for box_start, box_end in filaments:
                    seg_boxes = segment_filament(box_start, box_end, helical_rise = par.helical_rise, pixel_size = par.pixel_size)
                    angle_psi = -math.atan2(box_end.center.y - box_start.center.y, box_end.center.x - box_start.center.x) * 180 / math.pi
                    for i, box in enumerate(seg_boxes):
                        b1 = {'rlnCoordinateX'  : box.center.x,
                            'rlnCoordinateY'    : box.center.y,
                            'rlnAnglePsi'       : angle_psi,
                            'rlnHelicalTrackLength' : 0,
                            'rlnOriginX'        : 0,
                            'rlnOriginY'        : 0,
                            'rlnHelicalTubeID'  : tubeid,
                            'rlnAngleTilt'      : 90,
                            }
                        if i > 0:
                            b1['rlnHelicalTrackLength'] = box.center.dist(seg_boxes[0].center) * par.pixel_size
                        b1.update(mg)
                        if len(ostar.labels) == 0:
                            ostar.add_labels(b1.keys())
                        ostar.append(b1)
                    tubeid += 1
        else:
            helical = False
            # This box the helical boxes without segmentation.
            if boxes[0].tag == -1:
                helical = True
            tubeid = 0
            for i, box in enumerate(boxes):
                b1 = {
                        'rlnCoordinateX'  : box.center.x,
                        'rlnCoordinateY'    : box.center.y,
                        'rlnOriginX'        : 0,
                        'rlnOriginY'        : 0,
                        }
                if helical:
                    if i > 0:
                        p1 = boxes[i]
                        p0 = boxes[i-1]
                    else:
                        p1 = boxes[1]
                        p0 = boxes[0]
                    x1, y1 = p1.center.x, p1.center.y
                    x0, y0 = p0.center.x, p0.center.y
                    psi = -math.atan2(y1-y0, x1-x0) * 180. / math.pi
                    b1['rlnAnglePsi'] = psi
                    b1['rlnAngleTilt'] = 90.
                    b1['rlnAnglePsiPrior'] = psi
                    b1['rlnAngleTiltPrior'] = 90.
                    if box.tag == -1:
                        tubeid += 1
                        helical_track_length = 0
                    else:
                        helical_track_length += math.sqrt((x1-x0) * (x1-x0) + (y1-y0) * (y1-y0))
                    b1['rlnHelicalTrackLength'] = helical_track_length
                    b1['rlnHelicalTubeID'] = tubeid
                    b1['rlnAnglePsiFlipRatio'] = 0.5

                b1.update(mg)
                if len(ostar.labels) == 0:
                    ostar.add_labels(b1.keys())
                ostar.append(b1)

    ostar.write(par.output_star)
    print("Particles written to %s" % par.output_star)

def segment_filament(box_start, box_end, helical_rise = 1, pixel_size = 1):
    helical_rise_pix = helical_rise / pixel_size
    sx, sy = box_start.center.tolist()
    ex,ey = box_end.center.tolist()
    # get the angle
    angle = math.atan2(ey-sy, ex-sx)
    dx, dy = helical_rise_pix * math.cos(angle), helical_rise_pix * math.sin(angle)
    n_boxes = int(box_start.center.dist(box_end.center) / helical_rise_pix)
    return [Box(sx + dx * i, sy + dy * i) for i in range(n_boxes)]

def continuous_filaments(filaments,helical_rise,pixel_size):
#    helical_rise_pix=helical_rise/pixel_size
    distance_tolerance=filaments[0][0]._box_size*0.25
    new_filaments=[]
    for i in range(len(filaments)-1):
        first_end=filaments[i][1]
        new_start=filaments[i+1][0]
        sx, sy = new_start.center.tolist()
        ex,ey = first_end.center.tolist()
        distance=math.sqrt((sx-ex)**2+(sy-ey)**2)
        if distance<distance_tolerance:
            new_filaments.append([filaments[i][0]])
            new_filaments[-1].append(filaments[i][1])
            new_filaments[-1].append(filaments[i+1][1])
        else:
            new_filaments.append(filament[i])
    return filaments

if __name__ == "__main__":
    my_func()

