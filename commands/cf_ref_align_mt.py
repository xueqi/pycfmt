#!/usr/bin/env python
''' cf_ref_align_mt.py
usages:

# print help
cf_ref_align_mt.py -h
# run with default arguments
cf_ref_align_mt.py --i path/to/input.star 
# run parallel
cf_ref_align_mt.py --i path/to/input.star --cores 16
# run in SLURM batch submit
cf_ref_align_mt.py --i path/to/input.star --cores 16 --qsub
# run with different number of protofilaments
cf_ref_align_mt.py --i path/to/input.star --num_pfs 12 13
# run with existing volume
cf_ref_align_mt.py --i path/to/input.star --num_pfs 12 13 --ref_vol \
    vol_12.mrc vol_13.mrc --voxel 1.34
# run with fitted pdb (fit to 13 pf) (kin_fit is optional)
cf_ref_align_mt.py --i path/to/input.star --pdb tub_fit_13.pdb kin_fit_13.pdb
# run with no decoration microtubule
cf_ref_align_mt.py --i path/to/input.star --no_dec
# run with ctf information
cf_ref_align_mt.py --i path/to/input.star --ctf
'''
import logging

from cryofilia.helix.ref_align import RefAlign
from chuff_parser import ChuffGooey

import logging
import warnings
warnings.simplefilter("error")

@ChuffGooey()
def my_func():
    refAlign = RefAlign.new_from_command_line()
    refAlign.start()

if __name__ == "__main__":
  my_func()
  
