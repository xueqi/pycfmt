#!/usr/bin/env python
''' Reference alignment with single cpu.
'''
import os
import logging
import sys
import heapq
import cProfile
import argparse

import read_chuff_parameters, cf_utilities
from chuff_parser import ChuffParser, ChuffGooey
from chuff.util.project import get_next_workdir
import relion

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
handler = logging.StreamHandler(sys.stdout)
handler.setLevel(logging.DEBUG)
logger.addHandler(handler)

def setup_arg_parser():
    parser = ChuffParser(\
        description="""cfref_align_mt mpi version""",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    io = parser.add_argument_group("Input/Output")
    io.add_argument("--input_star",  required = True, widget = "FileChooser",
                    help = "input star for ref align. This file contains the coordinates of the boxes")
    io.add_argument("--output_star", widget = "FileChooser",
                    help = "output result for refalign")
    io.add_argument("--ref_volume", nargs = "*",
                    help = "reference volume used for ref align. Use space to separate more than one volume")
    io.add_argument('--scratch_dir', 
                    help = 'scratch dir to cache projections')
    io.add_argument("--workdir", 
                    help = "work directory")
    fila = parser.add_argument_group("Filament Options")
    fila.add_argument("--num_pfs", type = int, nargs = "*",
                    help = "number of protofilamentm. use space to seperate more than one configs")
    fila.add_argument("--num_starts", type = int, default = 3,
                    help = "number of starts. Only support one config start now")
    fila.add_argument("--box_size",type = int,
                    help = "Box size for refinement")
    fila.add_argument("--bin_factor",type = int, default = 1,
                    help = "bin factor for ref align. This is relative bin compare to micrograph")
    fila.add_argument("--pixel_size", type = float,
                    help = "pixel size of particles. Do not use this currently")
    fila.add_argument("--voxel_size", type = float,
                    help = "voxel_size. default to pixel size if not provided")
    calc = fila.add_argument_group("Calculation")
    calc.add_argument('--ncpus', type = int, default = 1,
            help = "total cores running the job")
    calc.add_argument("cpuid", nargs="?", type = int,
            help = "The cpu index, count from 1")
    calc.add_argument("--nuke", action = "store_true",
            help = "Redo the alignment")
    return parser

def get_subjob_workdir(workdir, cpuid):
        return os.path.join(workdir, 'node%03d' % cpuid)

def split_star(m, cpuid, ncpus, by_micrograph = True, by_filament = False):
    
    from collections import OrderedDict
    
    if by_filament: 
        mgs = m.group_by("MicrographName", "HelicalTubeID")
    elif by_micrograph:
        mgs = m.group_by("MicrographName")

    ptcls_for_node = [[0,None] for i in range(ncpus)]
    for mg_ptcls in mgs.values():
        tmp = heapq.heappop(ptcls_for_node)
        tmp[0] += len(mg_ptcls)
        if tmp[1] is None: tmp[1] = mg_ptcls.copy()
        else: tmp[1] += mg_ptcls
        heapq.heappush(ptcls_for_node, tmp)
    ptcls_for_node.sort(reverse = True)
    logger.debug("ptcls on each procs: %s" %  str([x[0]  for x in ptcls_for_node]))
    return ptcls_for_node[cpuid - 1][1]
        
@ChuffGooey()
def my_func():
    from cryofilia.helix.ref_align_mt import ref_align_mt_star
    
    # profiling
    pr = cProfile.Profile()
    pr.enable()

    parser = setup_arg_parser()
    par = read_chuff_parameters.read_chuff_parameters(parser)
    if not par.workdir:
        par.workdir = get_next_workdir('RefAlign')
        if not os.path.exists(par.workdir):
            os.mkdir(par.workdir)
    if not par.output_star:
        par.output_star = "%s_out%s" % os.path.splitext(par.input_star)

    # this is part of parallel job
    if par.cpuid is not None:
        cpuid = par.cpuid
        par.workdir = get_subjob_workdir(par.workdir, cpuid)
        if not os.path.exists(par.workdir):
            os.mkdir(par.workdir)
    
        m = relion.Metadata.readFile(par.input_star)
        m1 = split_star(m, cpuid, par.ncpus)
        star_bname = os.path.basename(par.input_star)
        par.input_star = os.path.join(par.workdir, "%s_%d.star" % (os.path.splitext(star_bname)[0], cpuid))
        m1.write(par.input_star)
        par.output_star = os.path.join(par.workdir, "%s_%d.star" % (os.path.splitext(os.path.basename(par.output_star))[0], cpuid))
#   import pickle
#   pickle.dump(par, open(os.path.join(par.workdir, 'par.pkl'),'w'))

    if not os.path.exists(par.output_star) or par.nuke:
        ref_align_mt_star(par)

    # profiling code
    pr.disable()
    import pstats
    p = pstats.Stats(pr, stream = open('profiling_out.txt', 'w'))
    p.sort_stats('cumtime')
    p.print_stats()

def main():
      my_func()
if __name__ == "__main__":
  main()
