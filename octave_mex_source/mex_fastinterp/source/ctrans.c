/* 
   DESCRIPTION: Various ISDFT(1/2,0), cosine transform and spectrum 
   shaping routines.

   Copyright(C) 2002 Antti Happonen
   email: happonea@cs.tut.fi
   You may use, copy, modify this code for any purpose and 
   without fee. You may distribute this ORIGINAL package.

*/
#include<math.h>
#ifndef PI
#define PI 3.14159265358979323846
#endif
#include"ctrans.h"
#include"fft2f.h"
#include"utils.h"

/* DESCRIPTION of isdft:
   Kernel of the sinc-interpolation in DCT domain.
   isdft performs inverse SDFT(1/2,0).
   INPUT:
   result, transformed data vector
   cosspec, modified spectrum "real" part
   sinspec, modified spectrum "imaginary" part
   length, length of the vector
   OUTPUT:
   result, signal in spatial domain
*/
void isdft(double *result, double *cosspec, double *sinspec, 
	   unsigned long length) {

  /* Definition of inverse SDFT(1/2, 0): 

   b = IDCT(2*Re(eta).*alpha) + IDST(2*Im(eta).*alpha), where

   alpha is a DCT spectrum
   eta is a interpolation function
   Re == Real part
   Im == imaginary part
   IDCT == inverse cosine transform
   IDST == inverse sine transform          
*/

  unsigned long k;
  /* Cosine transform: */
  cosspec[0] *= 0.5;
  ddct(length, cos(PI/length/2), sin(PI/length/2), cosspec);
  /* Sine transform: */
  sinspec[0] *= 0;
  ddst(length, cos(PI/length/2), sin(PI/length/2), sinspec); 

  for (k=0; k < length; k++) {

    result[k] = (cosspec[k] + sinspec[k])/length;
  }
}
/* DESCRIPTION of dctarb:
   Performs cosine transform of arbitrary length.
   INPUTS:
   signal, signal to be transformed
   spectrum, transformed signal
   N, length of the signal
   OUTPUT:
   spectrum, transformed signal
*/
void dctarb(double *signal, double *spectrum, unsigned int N) {

  unsigned int k, r;
  double spec = 0.0;

  for ( r=0; r < N; r++ ) {

    for (k=0; k < N; k++) {
      
      spec = signal[k]*cos((2*k + 1)*r*PI/(2*N)) + spec;
    }
    spectrum[r] = spec;
    spec = 0.0;
  }
  return;
}
/* DESCRIPTION of kthdctarb:
   Performs inverse cosine transform of arbitrary length at one point.
   INPUTS:
   spectrum, cosine transform spectrum
   N, length of the spectrum
   kth, point to be transformed
   OUTPUT:
   sig, signal of spectrum at point kth
*/
double kthdctarb(double *spectrum, unsigned int N, int kth) {

  unsigned int k;
  double sig = 0.0;
  kth = 1*kth; /* :) */
  /* for ( k=1; k < N; k++ ) {
      
    sig = spectrum[k]*cos((2*kth + 1)*k*PI/(2*N)) + sig;
  }  */
  /* Faster version: */ 
  for ( k=2; k < N; k = k + 2) { 

    sig = spectrum[k]*cos(PI*k/2) + sig;
  } 
  sig = (2*sig + spectrum[0])/N;
  return sig;
}
/* DESCRIPTION of kthdstarb:
   Performs inverse sine transform of arbitrary length at one point.
   INPUTS:
   spectrum, sine transform spectrum
   N, length of the spectrum
   kth, point to be transformed
   OUTPUT:
   sig, signal of spectrum at point kth
*/
double kthdstarb(double *spectrum, unsigned int N, int kth) {

  int k;
  double sig = 0.0;
  /* for ( k=1; k < N; k++ ) {
      
    sig = spectrum[k]*sin((2*kth + 1)*k*PI/(2*N)) + sig;
  } */
  /* Faster version: */ 

  for( k=0; k < kth; k++) {

    sig = spectrum[2*k+1]*cos(PI*k) + sig;
  } 
  sig = 2*sig/N;
  return sig;
}
/* DESCRIPTION of invdctarb:
   Performs inverse cosine transform of arbitrary length.
   INPUTS:
   signal, transformed signal
   spectrum, spectrum to be transformed
   N, length of the signal
   OUTPUT:
   signal, transformed signal
*/
void invdctarb(double *signal, double *spectrum, unsigned int N) {

  unsigned int k, r;
  double sig = 0.0;

  for ( r=0; r < N; r++ ) {

    for (k=1; k < N; k++) {
      
      sig = spectrum[k]*cos((2*r + 1)*k*PI/(2*N)) + sig;
    }
    signal[r] = (2*sig + spectrum[0])/N;
    sig = 0.0;
  }
  return;
}

/* DESCRIPTION of recdct:
   Recursive (like a IIR filter) DCT. 
   INPUTS:
   sig, data signal
   speccopy, spectrum at point
   spec1, spectrum at point - 1
   spec2, spectrum at point - 2
   point, point at present time
   L, length of the data
   N, length of the window
   mid, floor(N/2)
   what, hat to do?
   OUTPUTS:
   speccopy, spectrum at point
   spec1, spectrum at point - 1
   spec2, spectrum at point - 2
   what, what to do?
*/
void recdct(double *sig, double *speccopy, double *spec1, double *spec2, 
	    long point, unsigned long L, unsigned int N, int mid, int *what) { 

  unsigned int k;
  double help;

  if ( *what == 1 ) {

    getwin(sig, spec1, point, mid, L); 
    dctarb(spec1, spec2, N);
    
    for ( k=0; k < N; k++ ) {

      speccopy[k] = spec2[k];
      spec1[k] = 0.0;
    } 
    *what = 2;
  }
  else if ( *what == 2 ) {

    getwin(sig, speccopy, point, mid, L); 
    dctarb(speccopy, spec1, N);
    
    for ( k=0; k < N; k++ ) {

      speccopy[k] = spec1[k];
    } 
    *what = 3;
  }
  else {

    for ( k=0; k < N; k++ ) {

      spec2[k] = 
      
	2*( spec1[k] - (sig[ind(point-mid-1, L)] + 
			
	cos(PI*k)*sig[ind(point+mid-1, L)])*cos(PI*k/(2*N)))*cos(PI*k/N)   
	
	- spec2[k] + (sig[ind(point-mid-2, L)] + 

        cos(PI*k)*sig[ind(point+mid, L)])*cos(PI*k/(2*N)) + 

	(sig[ind(point-mid-1, L)] + cos(PI*k)*

	 sig[ind(point+mid-1, L)])*cos(3*PI*k/(2*N));
      
    }
    
    for ( k=0; k < N; k++ ) {
      
      help = spec2[k];
      spec2[k] = spec1[k];
      spec1[k] = help;
      speccopy[k] = help;
    }
  }
  return;
}

/* Gets right and mirrored indices for recdct */
long ind(long point, unsigned long length) {


  return (labs( (long)length - 1 -labs( (long)length - 1 - point)));

}

/* DESCRIPTION of threshold:
   Performs denoising of input spectrum.
   INPUTS:
   spectrum, spectrum to be denoised
   N, length of the spectrum
   thresh, threshold value
   method, wanted denoising method
   OUTPUT:
   spectrum, denoised spectrum
*/
int threshold(double *spectrum, unsigned int N, double thresh, int method) {

  int k;

  /* Hard threshold */
  if ( method == 1 ) {

    for ( k=2; k < (int)N; k++ ) {

      if ( fabs(spectrum[k]) < N*thresh ) {

	spectrum[k] = 0.0;
      }
    }
  }
  /* Soft threshold */
  else if ( method == 2 ) {

    for ( k=2; k < (int)N; k++ ) {

      if ( spectrum[k] >= N*thresh ) {

	spectrum[k] = spectrum[k] - (int)N*thresh;
      }
      else if ( spectrum[k] <= - (int)N*thresh ) {

	spectrum[k] = spectrum[k] + N*thresh;
      }
      else {

	spectrum[k] = 0;
      }
    }
  }
  /* Detection for nearest neighbor interpolation */
  else if ( method == 3 ) {
    
    for ( k=(int)ceil(N/2); k < (int)N; k++ ) {

      if ( fabs(spectrum[k]) > N*thresh ) {

	return 1;
      }
    }
  }
  /* Wiener filter */
  else if ( method == 4 ) {

    if ( thresh > 0 ) {

      for ( k=2; k < (int)N; k++ ) {
	
	spectrum[k] = spectrum[k]*spectrum[k]/
	  (spectrum[k]*spectrum[k] + N*N*thresh*thresh)*spectrum[k];
      }
    }
  }
  return 0;
}
/*
   DESCRIPTION of make_eta:
   Makes interpolation function
   INPUTS:
   data, resulting interpolation function
   length, length of the vector, half of the actual length
   p, shift parameter
   M, upsampling parameter
   OUTPUT:
   data, interpolation function
*/
void make_eta(double *data, unsigned long length, double p, int M) {

  /* data[2*j] = Re(x[j]), data[2*j+1] = Im(x[j]), 0<=j<n */
  unsigned long r;
  for (r=0; r < length; r++) {

    data[2*r] = cos(-1*PI*2*p*r/(2*M*length));
    data[2*r+1] = sin(-1*PI*2*p*r/(2*M*length));
  }
  return;
}
/* 
   DESCRIPTION of make_coef:
   Makes modified spectrum, which is used in function isdft
   INPUTS:
   coscoef, "real" part of the modified spectrum
   sincoef, "imaginary" part of the modified spectrum (not really imaginary)
   spec, DCT spectrum
   eta, interpolation function
   length, length of spectrum
   OUTPUT:
   coscoef, "real" part of the modified spectrum
   sincoef, "imaginary" part of the modified spectrum (not really imaginary)
*/
void make_coef(double *coscoef, double *sincoef, double *spec, 
	       double *eta, unsigned long length) {

  /* data[2*j] = Re(x[j]), data[2*j+1] = Im(x[j]), 0<=j<n */
  unsigned long r;
  for (r=0; r < length; r++) {
    
    coscoef[r] = 2*eta[2*r]*spec[r];
    sincoef[r] = 2*eta[2*r+1]*spec[r];
  }
  return;
}

/*
   DESCRIPTION of eta_shift:
   Modifies interpolation function so that in spatial domain signal 
   is shifted 1/M.
   INPUTS:
   eta, interpolation function
   length, half of the length of eta (real and imaginary part)
   M, upsampling coefficient, signal is shifted 1/M.
   OUTPUT:
   eta, modified interpolation function
   NOTE:
   eta is complex
*/
void eta_shift(double *eta, unsigned long length, int M) {

  unsigned long k;
  double a, b, c, d;
  /* complex multiplication: (a  + i*b)(c + i*d) */
  for (k=0; k < length; k++) {

    a = eta[2*k];
    b = eta[2*k+1];
    c = cos(-1*PI*2*k/(2*M*length));
    d = sin(-1*PI*2*k/(2*M*length));

    eta[2*k] = a*c - b*d;
    eta[2*k+1] = a*d + b*c;
  }
  return;
}    

/* FOR TESTING 
   out = fastinterp(sig2, 1, 1, 1, 9, 11, 0, 0); 
int transform(double *input, double *output, unsigned long rows, 
	      unsigned long cols, unsigned long  win) {
   
   unsigned long i, j; 
   int mid;
   double *spectrum, *signal, *signal2, *winsig, *winspec, *spec1, *spec2;
   double *speccopy;

   winspec  = (double*)calloc(win, sizeof(double));
   spec1    = (double*)calloc(win, sizeof(double));
   spec2    = (double*)calloc(win, sizeof(double));
   speccopy  = (double*)calloc(win, sizeof(double));
   signal    = (double*)calloc(rows, sizeof(double));
   signal2   = (double*)calloc(rows, sizeof(double));
   winsig    = (double*)calloc(win, sizeof(double));

   for (i=0; i < rows; i++) { signal2[i] = 0.0; }

   mid = (int)floor(win/2);
   
   for (i=0; i < cols; i++) {
     
     getcolumn(input, signal, rows, i);
     
     for (j=0; j < rows; j++ ) { 

       getwin(signal, winsig, j, mid, rows); 
       dctarb(winsig, speccopy, win);   
       recdct(signal, speccopy, spec1, spec2, (long)j, rows, win, mid );
       signal2[j] = kthdctarb(speccopy, win, mid); 
     }
 
     putcolumn(output, signal2, rows, i);
   }
   free(spectrum); free(spec1); free(spec2); free(speccopy); 
   free(signal); free(signal2); free(winsig); free(winspec);
   return 1;
}
*/
