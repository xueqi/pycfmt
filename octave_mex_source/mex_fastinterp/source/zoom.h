/* 
   Copyright(C) 2002 Antti Happonen
   email: happonea@cs.tut.fi
   You may use, copy, modify this code for any purpose and 
   without fee. You may distribute this ORIGINAL package.

*/

int zoom(double *input, double *output, unsigned long rows, 
	 unsigned long cols, unsigned long  Lx, unsigned long  Ly);

int interp1d(double *input, double *output, unsigned long rows,
	     unsigned long cols, unsigned long  Ly, double p);

int winzoom(double *input, double *output, unsigned long rows, 
	    unsigned long cols, unsigned long  Lx, unsigned long  Ly,
	    int wlength, double thrsh, int method);

int winint1d(double *input, double *output, unsigned long rows,
	     unsigned long cols, unsigned long  Ly, double p,
	     int wlength, double thrsh, int method);
