/*
   DESCRIPTION: Contains general utility functions.
  
   Copyright(C) 2002 Antti Happonen
   email: happonea@cs.tut.fi
   You may use, copy, modify this code for any purpose and 
   without fee. You may distribute this ORIGINAL package.

*/ 
void getcolumn(double *data, double *column, unsigned long rows, 
	       unsigned long jth);

void getrow(double *data, double *row, unsigned long rows, 
	    unsigned long cols, unsigned long ith);

void putcolumn(double *data, double *column, unsigned long rows, 
	       unsigned long jth);

void putrow(double *data, double *row, unsigned long  rows, 
	    unsigned long cols, unsigned long ith);

void getwin(double *signal, double *window, unsigned long point, 
	    int Nmid, unsigned long length);
