/* 
   This is the source code of the main program of FASTINTERP MEX function.  
   The program performs the sinc-interpolation in DCT domain. Image 
   rotation, zooming, 1-D resampling and 1-D shifting are performed 
   by the sinc-interpolation in DCT domain with and without sliding window.  

   Based on the L. Yaroslavsky's article:
   "Fast signal sinc-interpolation and its applications in 
   signal and image processing", 
   IS&T/SPIE's 14th Annual Symposium Electronic Imaging 2002,
   Science and Technology, Conference 4667 "Image Processing: 
   Algorithms and Systems", San Jose, CA, 21-23 January 2002. 
   Proceedings of SPIE vol. 4667

   Source codes:

   fastinterp.c     - main program
   zoom.{c,h}       - zoom routines
   rotate.{c,h}     - rotation routines
   ctrans.{c,h}     - SDFT(1/2,0), cosine transforms etc. routines
   utils.{c,h}      - utility routines
   fft2f.{c,h}      - FFT and DCT etc. routines from  
                    http://momonga.t.u-tokyo.ac.jp/~ooura/fft.html

   Copyright(C) 2002 Antti Happonen
   email: happonea@cs.tut.fi
   You may use, copy, modify this code for any purpose and 
   without fee. You may distribute this ORIGINAL package.

   FUNCTION fastinterp:
   out = fastinterp(input, Lx, Ly, angle, index, wlngh, trsh, method)
   INPUTS:
   input, a matrix of input data, size power of two
   Lx, an integer of upsampling in the x-direction
   Ly, an integer of upsampling in the y-direction
   angle, a scalar of rotation angle OR a scalar of signal shift
   index, an integer to choose a function
   wlngth, length of sliding window
   thrsh, threshold value
   method, threshold method
   OUTPUT:
   output, a matrix of output data
 
*/

#include<math.h>
#include"mex.h" /* Copyright (c) 1984-98 by The MathWorks, Inc. */
#include"zoom.h"
#include"rotate.h"

/* the gateway function */
void mexFunction(int nlhs,mxArray *plhs[],int nrhs,const mxArray *prhs[]) {

  double *input, *output;
  double  angle, thrsh;
  unsigned long  Lx, Ly, mrows, ncols;
  int index, wlngth, method;

  if(nrhs!=8) {
    mexErrMsgTxt("Five inputs required.");
  }
  if(nlhs>1) {
    mexErrMsgTxt("Too many output arguments.");
  }
  if( !mxIsNumeric(prhs[1]) || !mxIsDouble(prhs[1]) ||
      mxIsEmpty(prhs[1])    || mxIsComplex(prhs[1]) ||
      mxGetN(prhs[1])*mxGetM(prhs[1])!=1 ) {

    mexErrMsgTxt("Input Lx must be a scalar.");
  }
  if( !mxIsNumeric(prhs[2]) || !mxIsDouble(prhs[2]) ||
      mxIsEmpty(prhs[2])    || mxIsComplex(prhs[2]) ||
      mxGetN(prhs[2])*mxGetM(prhs[2])!=1 ) {

    mexErrMsgTxt("Input Ly must be a scalar.");
  }
  if( !mxIsNumeric(prhs[3]) || !mxIsDouble(prhs[3]) ||
      mxIsEmpty(prhs[3])    || mxIsComplex(prhs[3]) ||
      mxGetN(prhs[3])*mxGetM(prhs[3])!=1 ) {

    mexErrMsgTxt("Input angle/p must be a scalar.");
  }
  if( !mxIsNumeric(prhs[4]) || !mxIsDouble(prhs[4]) ||
      mxIsEmpty(prhs[4])    || mxIsComplex(prhs[4]) ||
      mxGetN(prhs[4])*mxGetM(prhs[4])!=1 ) {

    mexErrMsgTxt("Input index must be a scalar.");
  }
  if( !mxIsNumeric(prhs[5]) || !mxIsDouble(prhs[5]) ||
      mxIsEmpty(prhs[5])    || mxIsComplex(prhs[5]) ||
      mxGetN(prhs[5])*mxGetM(prhs[5])!=1 ) {

    mexErrMsgTxt("Input length of the window must be a scalar.");
  }
   if( !mxIsNumeric(prhs[6]) || !mxIsDouble(prhs[6]) ||
      mxIsEmpty(prhs[6])    || mxIsComplex(prhs[6]) ||
      mxGetN(prhs[6])*mxGetM(prhs[6])!=1 ) {

    mexErrMsgTxt("Input threshold must be a scalar.");
  }
   if( !mxIsNumeric(prhs[7]) || !mxIsDouble(prhs[7]) ||
      mxIsEmpty(prhs[7])    || mxIsComplex(prhs[7]) ||
      mxGetN(prhs[7])*mxGetM(prhs[7])!=1 ) {

    mexErrMsgTxt("Input threshold method must be a scalar.");
  }
  
  
  Lx = (unsigned long)mxGetScalar(prhs[1]);
  Ly = (unsigned long)mxGetScalar(prhs[2]);
  angle = mxGetScalar(prhs[3]);
  index = mxGetScalar(prhs[4]);
  wlngth = mxGetScalar(prhs[5]);
  thrsh = mxGetScalar(prhs[6]);
  method = mxGetScalar(prhs[7]);

  input = mxGetPr(prhs[0]);

  mrows = mxGetM(prhs[0]);
  ncols = mxGetN(prhs[0]);

  /*  call the C subroutine zoom */
  if ( index == 1 ) {
    
    if ( ceil(Ly) != floor(Ly) || ceil(Lx) != floor(Lx) 
	 || Ly < 1 || Lx < 1 ) {

      mexErrMsgTxt("Inputs Lx and Ly must be integers greater than zero."); 
    }
    if ( ceil(log(mrows)/log(2)) != floor(log(mrows)/log(2)) ||
	 ceil(log(ncols)/log(2)) != floor(log(ncols)/log(2)) ) {

      mexErrMsgTxt("Size of the input data must be a power of two.");
    }
    plhs[0] = mxCreateDoubleMatrix(Ly*mrows, Lx*ncols, mxREAL);
    output = mxGetPr(plhs[0]);
    /* zoom routine */
    if ( zoom(input, output, mrows, ncols, Lx, Ly) == -1 ) {

      mexErrMsgTxt("Out of memory.");
    }
  }
  /* call the C subroutine rotate */
  else if ( index == 2 ) { 
  
    if ( fmod(angle, 90) == 0 ) {

      mexErrMsgTxt("Input angle cannot be n*90, n = 0,1,2,...");
    }
    if ( ceil(log(mrows)/log(2)) != floor(log(mrows)/log(2)) ||
	 ceil(log(ncols)/log(2)) != floor(log(ncols)/log(2)) ||
	 mrows != ncols ) {

      mexErrMsgTxt("Input must be a square and a size of it power of two.");
    }
    plhs[0] = mxCreateDoubleMatrix(mrows, ncols, mxREAL);
    output = mxGetPr(plhs[0]);    
    if ( rotate(input, output, mrows, ncols, angle) == -1 ) {

      mexErrMsgTxt("Out of memory.");
    }
  }
  /* call the C subroutine interp1d for shifting */
  else if ( index == 3 ) {
    
    if ( ceil(log(mrows)/log(2)) != floor(log(mrows)/log(2)) ) {

      mexErrMsgTxt("Length of the columns must be a power of two.");
    }
    plhs[0] = mxCreateDoubleMatrix(mrows, ncols, mxREAL);
    output = mxGetPr(plhs[0]);
    /* angle == p, shift */
    if ( interp1d(input, output, mrows, ncols, 1, angle) == -1 ) {
      
      mexErrMsgTxt("Out of memory.");
    }
  }
  /* call the C subroutine interp1d for resampling */ 
  else if ( index == 4 ) {

    if ( ceil(Ly) != floor(Ly) || Ly < 1) {

      mexErrMsgTxt("Input Ly must be an integer greater than zero.");
    }
    if ( ceil(log(mrows)/log(2)) != floor(log(mrows)/log(2)) ) {

      mexErrMsgTxt("Length of the columns must be a power of two.");
    }
    plhs[0] = mxCreateDoubleMatrix(Ly*mrows, ncols, mxREAL);
    output = mxGetPr(plhs[0]);
    /* 1-D upsamling routine */
    if ( interp1d(input, output, mrows, ncols, Ly, 1) == -1 ) {
      
      mexErrMsgTxt("Out of memory.");
    }
  }
  /*  call the C subroutine zoom with sliding window*/
  else if ( index == 5 ) {
    
    if ( ceil(Ly) != floor(Ly) || ceil(Lx) != floor(Lx) 
	 || Ly < 1 || Lx < 1 ) {

      mexErrMsgTxt("Inputs Lx and Ly must be integers greater than zero."); 
    }
    if ( wlngth < 3 || fmod(wlngth+1,2) != 0 ) {

      mexErrMsgTxt("Length of the window must be odd and greater than two.");
    }
    if ( (unsigned long)ceil(wlngth/2) >= mrows || 
	 (unsigned long)ceil(wlngth/2) >= ncols ) {
      
      mexErrMsgTxt("Length of the window is too long.");
    }
    plhs[0] = mxCreateDoubleMatrix(Ly*mrows, Lx*ncols, mxREAL);
    output = mxGetPr(plhs[0]);
    /* zoom routine with sliding window */
    if (winzoom(input,output,mrows,ncols,Lx,Ly,wlngth,thrsh,method) == -1) {

      mexErrMsgTxt("Out of memory.");
    }
  }
  /* call the C subroutine rotate with sliding window*/
  else if ( index == 6 ) { 
  
    if ( fmod(angle, 90) == 0 ) {

      mexErrMsgTxt("Input angle cannot be n*90, n = 0,1,2,...");
    }
    if ( wlngth < 3 || fmod(wlngth+1,2) != 0 ) {

      mexErrMsgTxt("Length of the window must be odd and greater than two.");
    }
    if ( mrows != ncols ) {

      mexErrMsgTxt("Input must be a square.");
    }
    if ( (unsigned long)ceil(wlngth/2) >= mrows ) {
      
      mexErrMsgTxt("Length of the window is too long.");
    }
    plhs[0] = mxCreateDoubleMatrix(mrows, ncols, mxREAL);
    output = mxGetPr(plhs[0]);    
    if ( winrot(input,output,mrows,ncols,angle,wlngth,thrsh,method) == -1 ) {

      mexErrMsgTxt("Out of memory.");
    }
  }
   /* call the C subroutine interp1d for shifting with sliding window */
  else if ( index == 7 ) {

    if ( wlngth < 3 || fmod(wlngth+1,2) != 0 ) {

      mexErrMsgTxt("Length of the window must be odd and greater than two.");
    }
    if ( (unsigned long)ceil(wlngth/2) >= mrows ) {
      
      mexErrMsgTxt("Length of the window is too long.");
    }
    plhs[0] = mxCreateDoubleMatrix(mrows, ncols, mxREAL);
    output = mxGetPr(plhs[0]);
    /* angle == p, shift */
    if ( winint1d(input,output,mrows,ncols,1,angle,wlngth,thrsh,method)==-1) {
      
      mexErrMsgTxt("Out of memory.");
    }
  }
  /* call the C subroutine interp1d for resampling with sliding window */ 
  else if ( index == 8 ) {

    if ( ceil(Ly) != floor(Ly) || Ly < 1) {

      mexErrMsgTxt("Input Ly must be an integer greater than zero.");
    }
    if ( wlngth < 3 || fmod(wlngth+1,2) != 0 ) {

      mexErrMsgTxt("Length of the window must be odd and greater than two.");
    }
    if ( (unsigned long)ceil(wlngth/2) >= mrows ) {
      
      mexErrMsgTxt("Length of the window is too long.");
    }
    plhs[0] = mxCreateDoubleMatrix(Ly*mrows, ncols, mxREAL);
    output = mxGetPr(plhs[0]);
    /* 1-D upsamling routine */
    if (winint1d(input,output,mrows,ncols,Ly,1,wlngth,thrsh,method) == -1) {
      
      mexErrMsgTxt("Out of memory.");
    }
  }
  else {

    mexPrintf(" Usage: \n"); 
    mexPrintf(" out=fastinterp(input,Lx,Ly,angle,index,wlngth,thrsh,mthd)\n");
    mexPrintf(" where \n");
    mexPrintf(" Lx is upsampling in the x-direction \n");
    mexPrintf(" Ly is upsampling in the y-direction \n");
    mexPrintf(" angle is an rotation angle \n");
    mexPrintf(" index tells which function is used: \n");
    mexPrintf(" 1 == zoom \n 2 == rotation \n 3 == shift \n");
    mexPrintf(" 4 == upsampling in the y-direction \n");
    mexPrintf(" 5 == zoom with sliding window, 6 == rotation with \n");
    mexPrintf(" sliding window, 7 == shift with sliding window, \n");
    mexPrintf(" 8 == upsampling in y direction with sliding window\n");
    mexPrintf(" wlngth is the length of the sliding window \n");
    mexPrintf(" thrsh is the threshold value for interpolation \n");
    mexPrintf(" mthd is the denoising method 0, 1, 2, 3 or 4\n"); 
    mexErrMsgTxt("Unknown index.");
  } 
}




