setenv CC /usr/bin/gcc_disable
setenv CXX /usr/bin/g++

cd source
if(! -d OSX) mkdir OSX32
cd OSX32

mkoctfile --mex --output fastinterp ../fastinterp.c ../zoom.c \
    ../rotate.c ../fft2f.c ../utils.c ../ctrans.c

if(! -d ../../../../octave_mex_OSX) mkdir ../../../../octave_mex_OSX32
/bin/mv fastinterp.mex ../../../../octave_mex_OSX32/

cd ../../
