cd source
if(! -d linux) mkdir linux
cd linux

mkoctfile --mex --output fastinterp ../fastinterp.c ../zoom.c \
    ../rotate.c ../fft2f.c ../utils.c ../ctrans.c

if(! -d ../../../../octave_mex_linux) mkdir ../../../../octave_mex_linux
/bin/mv fastinterp.mex ../../../../octave_mex_linux/

cd ../../
