'''
Created on Jun 8, 2018

@author: xueqi
'''
from __future__ import absolute_import, unicode_literals

from apps.celery import app

@app.task
def smooth_filament(filament, **kwargs):
    from cryofilia.helix.smooth_function import SmoothCoords
    smooth = SmoothCoords(**kwargs)
    print(smooth)
    result = smooth.smooth_filament_m(filament)
    return result

