""" Applications. This is where all the public commands locates.

An application is a single command that group multiple tasks.

Each task has an input and output

for instance, for smooth filament:
SmoothTask:
    input: smooth_param
    output: smooth_result

All tasks should share the same file system, 
which means all tasks would finding the same file with same path on disk.

Use in script for one task:
result_async = smooth.delay(**input)
result = result_async.get()

For an app:
result_async = app.delay(**input)

App should run in app queue, not the task queue.

Backend should distinguish these.

For django, there should be separate worker/queue for apps.
"""

import threading

def start_task():
    pas