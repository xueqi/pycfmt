'''
Created on Jun 8, 2018

@author: xueqi
'''
from __future__ import absolute_import, unicode_literals

from celery import Celery

app = Celery("apps")
app.config_from_object("apps.celeryconfig")
app.conf.update(result_expires=3600,)
app.conf.update(include=["apps.smooth_app"])