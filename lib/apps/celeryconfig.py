'''
Created on Mar 30, 2018

@author: xueqi
'''
broker_url = "redis://"
result_backend = "rpc://"
task_serializer = "pickle"
result_serializer = "pickle"
accept_content = ['pickle']
enable_utc = True
