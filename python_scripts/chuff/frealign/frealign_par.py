import logging
import os
import re
import math
from chuff.util.box import Point
from relion import Metadata, Container, add_label
logger = logging.getLogger(__name__)
labels = {"index"           : ["index", int, "Index in this stack"],
          "phi"             : ["AngleRot"],
          "theta"           : ["AngleTilt"],
          "psi"             : ["AnglePsi"],
          "magnification"   : ["Magnification"],
          "shx"             : ["OriginX"],
          "shy"             : ["OriginY"],
          "filament_id"     : ["HelicalTubeID"],
          "mg_id"           : ["micrographID", int, "Micrograph ID"],
          "defocus1"        : ["DefocusU"],
          "defocus2"        : ["DefocusV"],
          "astigmatism_angle"    : ["DefocusAngle"],
          "phase_residual"  : ["phaseResidual", float, "phase residual"],
          "phase_residual_change"   : ["phaseResidualChange", float, "phase residual change"],
          "logp"            : ["logp", float, "log p"],
          "occ"             : ["occ", float, "occupancy"],
          "sigma"           : ["sigma", float, "sigma"],
          "score"           : ["score", float, "score"],
          "score_change"    : ["scoreChange", float, "score change from last iteration"],
          "x"               : ["CoordinateX"],
          "y"               : ["CoordinateY"],
          "starts"          : ["starts", float, "number of starts"],
          "stack"      : ["stack", str, "the stack file that the particle located in"]
        }
# add custom labels
for key, value in labels.items():
    if len(value) > 1:
        add_label(*value)

class FrealignParticle(Container):
    def __init__(self, d = None, version = 9):
        super(FrealignParticle, self).__init__(d)

        # the version is controlling the str representation of frealign. Reading from a file will automatically set the version. If another version is needed to output, just change the version here before output
        self.version = version
        
        # pixel size is set by user. This is used in between converting frealign 8 and 9 if the shx or shy is not 0
        self.pixel_size = 1

        self.prerotate = 0  # the angle of the particle is prerotated. EM convention. So if prerotated CC, prerotate is positive, which means the original particles/filament is in CCC positive angle 

        # the stack is the parent container of this particlem, which is FrealignParameter instance. 
        self.stack = None
        self.occ = 1.

    @property
    def center(self):
        return self.x - self.shx, self.y - self.shy
    
    def recenter(self):
        self.x -= self.shx
        self.y -= self.shy
        self.shx = self.shy = 0.

    def reset(self):
        '''
            reset refinement status
        '''
        for k in ['logp', 'sigma', 'score', 'score_change', 'phase_residual_change']:
            setattr(self, k, 0)
        self.phase_residual = 90
        self.occ = 1.

    def string_as_v9(self):
        if self.version >= 9: return str(self)
        # for v8, we set default  logp, occ, sigma, score, change
        self.logp = 0
        self.occ = 1
        self.sigma =  0
        self.score = 0
        self.score_change = 0
        ov = self.version
        self.version = 9
        s = str(self)
        self.version = ov
        return s
    def string_as_v8(self):
        if self.version < 9: return str(self)
        # for v9, we set default phase_residual, phase_residual_change
        self.phase_residual = 90
        self.phase_residual_change = 0
        ov = self.version
        self.version = 8
        s = str(self)
        self.version = ov
        return s
    
    def get_fields(self):
        if self.version < 9:
            return ["index", "psi", "theata", "phi", "shx", "shy",
                    "magnification", "mg_id", "defocus1", "defocus2", "astigmatism_angle", 
                    "phase_residual", "phase_residual_change"]
        else:
            return ["index", "psi", "theata", "phi", "shx", "shy",
                    "magnification", "mg_id", "defocus1", "defocus2", "astigmatism_angle", 
                    "occ", "logp", "sigma", "score", "score_change"]
    def __str__(self):
        if self.version < 9:
            self.format = "%7d%8.2f%8.2f%8.2f%8.2f%8.2f%7.0f.%6d%9.1f%9.1f%8.2f%7.2f%8.2f"
        else:
            self.format = "%7d%8.2f%8.2f%8.2f%10.2f%10.2f%8d%6d%9.1f%9.1f%8.2f%8.2f%10i%11.4f%8.2f%8.2f"

        if self.version < 9:
            return self.format % (self.index, self.psi, self.theta, self.phi, 
                                  -self.shx, -self.shy, self.magnification, 
                                  self.mg_id, self.defocus1, self.defocus2, 
                                  self.astigmatism_angle, self.phase_residual, 
                                  self.phase_residual_change)
        else:
            return self.format % (self.index, self.psi, self.theta, self.phi, 
                                  -self.shx * self.pixel_size, -self.shy * self.pixel_size, self.magnification, 
                                  self.mg_id, self.defocus1, self.defocus2, 
                                  self.astigmatism_angle, self.occ, self.logp, 
                                  self.sigma, self.score, self.score_change)
    @staticmethod
    def create_from_line(line, pixel_size = 1.):
        '''
            load a line in frealing par file, 

            :param str line: line from frealign par file
            :param int version: version of frealign par file, 8 or 9
        '''
        s = line.strip().split()
        fp = FrealignParticle()
        fp.pixel_size = pixel_size
        if len(line) < 104: # frealign 8
            fp.index, fp.psi, fp.theta, fp.phi, fp.shx, fp.shy, fp.magnification, fp.mg_id, fp.defocus1, fp.defocus2, fp.astigmatism_angle, fp.phase_residual, fp.phase_residual_change = [float(ss) for ss in s]
            fp.version = 8
        else:
            if len(line) < 128:
                fp.index, fp.psi, fp.theta, fp.phi, fp.shx, fp.shy, fp.magnification, fp.mg_id, fp.defocus1, fp.defocus2, fp.astigmatism_angle, fp.occ, fp.logp, fp.phase_residual, fp.phase_residual_change = [float(ss) for ss in s]
                fp.version = 9
            elif len(line) < 140:
                fp.index, fp.psi, fp.theta, fp.phi, fp.shx, fp.shy, fp.magnification, fp.mg_id, fp.defocus1, fp.defocus2, fp.astigmatism_angle, fp.occ, fp.logp, fp.sigma, fp.score, fp.score_change =  [float(ss) for ss in s]
                fp.version = 9
            else:
                raise Exception, "The line is in wrong format."
            fp.shx /= fp.pixel_size
            fp.shy /= fp.pixel_size
        fp.shx, fp.shy = -fp.shx, -fp.shy
        fp.index, fp.mg_id = int(fp.index), int(fp.mg_id)
        return fp
    
class FrealignParameter(Metadata):
    ContainerClass = FrealignParticle
    _labels = labels
    def __init__(self, name = "", par_file = None):
        '''
            par_file is the parameter_file location, usually assign new if copied
            stack_file is the particle stacks. usually do not change if parameter file copied. Only changes if rebox the particles using same parameters.
        '''
        super(FrealignParameter, self).__init__(name)
        for label in labels:
            self.add_label(labels[label][0])
        self.header = ""
        self.footer = ""
        self.particles = []
        self.stack_file = None
        if par_file is not None:
            self.load_param_from_file(par_file)
        self.par_file = par_file
        self.current = 0 # iteration
    def copy(self):
        fp = super(FrealignParameter, self).copy()
        fp.par_file = self.par_file
        fp.header= self.header
        fp.footer = self.footer
        fp.stack_file = self.stack_file
        return fp

    def renumber(self):
        i = 1
        for ptcl in self:
            ptcl.index = i
            i += 1
    @staticmethod
    def create_from_file(frealign_par_file):
        par = FrealignParameter(frealign_par_file)
        return par

    def write_param_to_file(self, fname):
        '''write parameter to file
        '''
        f = open(fname, 'w')
        f.write(self.header)
        if len(self) > 0:
            f.write("C %s\n" % (" ".join(self[0].get_fields())))
        f.write("\n".join([str(p) for p in self]))
        f.write("\n")
        f.write(self.footer)
        f.close()

    def load_param_from_file(self, fname, pixel_size = 1.):
        ''' load parameter from file. 

        :param str fname: parameter file name
        :param float pixel_size: Set pixel_size. Par file does not keep pixel size. 
        :return: no return, set parameters to instance

        '''
        header = ''
        footer = ''
        f = open(fname)
        lines = f.readlines()
        if len(lines) == 0:
            print fname
            raise Exception("%s is empty" % fname)

        i = 0
        while i < len(lines) and (lines[i].strip().startswith("C") or len(lines[i].strip()) == 0):
            header += lines[i]
            i += 1
        if i == len(lines):
            print fname
        while i < len(lines) and lines[i].strip() and not lines[i].strip().startswith("C"):
            self.append(FrealignParticle.create_from_line(lines[i].strip("\n"), pixel_size = pixel_size))
            i += 1
        while i < len(lines):
            footer += lines[i]
            i += 1
        self.header = header
        self.footer = footer
        self.renumber()
        self.par_file = fname
    
    def recenter(self):
        for ptcl in self:
            ptcl.recenter()
    def add_particle(self, ptcl):
        '''
            @param ptcl: the particle to add
        '''
        if len(self.particles) == 0:
            ptcl.index = 1
        else:
            ptcl.index = self.particles[-1].index + 1
        self.particles.append(ptcl)
        ptcl.stack = self
    @staticmethod
    def create_random_parameter_file(par_file):
        fp  = FrealignParameter.create_random_parameter()
        fp.write_param_to_file(par_file)
    @staticmethod
    def create_random_parameter():
        fp = FrealignParameter()
        from random import uniform as ru, randint  as ri, shuffle as rs
        nfilaments = ri(20,40)
        filaments = [(i, ri(40,80)) for i in range(nfilaments)]
        rs(filaments)

        for mg_id, filament_num in filaments:
            d= ru(10000, 50000)
            dd = ru(100, 10000)
            d1, d2 = d - dd, d + dd
            astigangle = ru(0, 360)
            for _ in xrange(filament_num):
                ptcl = FrealignParticle()
                ptcl.phi = ru(0, 360)
                ptcl.psi = ru(0, 360)
                ptcl.theta = ru(75, 105)

                ptcl.magnification = 100000
                ptcl.shx = ru(-5, 5)
                ptcl.shy = ru(-5, 5)
                ptcl.mg_id = mg_id
                ptcl.defocus1 = d1
                ptcl.defocus2 = d2
                ptcl.astigmatism_angle = astigangle 
                fp.add_particle(ptcl)
        return fp

    def __str__(self):
        s = '''Frealign Parameter File:'''
        s += "Total %d Particles" % len(self)
        return s
    
    def write_box_file(self, box_filename, box_size = 64, prerotate=False, fmt = 'eman', filament = False):
        from chuff.util.box import Box
        boxes = []
        for i in range(len(self)):
            ptcl = self[i]
            boxes.append(Box(ptcl.x, ptcl.y, box_size, box_size, 0))

        if fmt == 'eman':
            Box.write_box_file(box_filename, boxes, fmt)
            return 
        with open(box_filename,'w') as f:
            for ptcl in self.particles:
                if prerotate:
                    psi = ptcl.psi
                else:
                    psi = 0
                f.write("%.2f %.2f %d %.2f\n" % (ptcl.x, ptcl.y, box_size, psi))
        f.close()

    def to_list(self):
        alis = []
        for ptcl in self.particles:
            center = ptcl.get_center()
            alis.append([center[0], center[1], ptcl.phi, ptcl.theta, ptcl.psi + ptcl.prerotate])

        return alis
    
if __name__ == "__main__":
    fp = FrealignParticle.create_from_line('  46418    0.00   97.94  219.14    0.13   -4.74 128778.   410  20875.9  19976.1 -141.31  76.51    2.77')
    assert str(fp) == '  46418    0.00   97.94  219.14    0.13   -4.74 128778.   410  20875.9  19976.1 -141.31  76.51    2.77'
