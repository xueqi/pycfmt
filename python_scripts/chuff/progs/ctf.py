'''CtfFind program wrapper

@author: xueqi
'''
import pexpect
import os
from chuff import chuff_dir
from relion.parameter import MPIParameter
from relion import Metadata
from cryofilia.project.project import CFProject
from cryofilia.ctf import CTFMeta

class CtfFind(MPIParameter):
    '''
    CTF Find program wrapper
    '''

    def __init__(self):
        '''
        '''
        self.exec_ = os.path.join(chuff_dir, "linux", "ctffind3")
        self.exec_ = self.exec_.replace(" ", "\ ")
        super(CtfFind, self).__init__()
        self.timeout = 20 * 60

    def add_arguments(self):
        
        self.add_argument("input_star", type=str, required=True,
                help="Input micrograph star file")
        self.add_argument("cs", type=float, default=2.0,
                help="Spherical Abberation")
        self.add_argument("voltage", type=float, default=-1.,
                help="Volatage in kV")
        self.add_argument("pixel_size", type=float, default=-1.,
                help="pixel_size of micrograph")
        self.add_argument("box_size", default=256, type=int,
                help="Tile size")
        self.add_argument("min_defocus", type=float, default=5000.,
                help="min defocus")
        self.add_argument("max_defocus", type=float, default=40000.,
                help="max defocus")
        self.add_argument("min_resolution", type=float, default=100.,
                help="min resolution")
        self.add_argument("max_resolution", type=float, default=10.,
                help="max resolution")
        self.add_argument("defocus_step", type=float, default=1000.,
                help="step for finding defocus")
        self.add_argument("astigmatism_step", type=float, default=100.,
                help="Astigmatism finding step")
        self.add_argument("amplitude_contrast", type=float, default=0.1,
                help="Amplitude contrast.")
        
        super(CtfFind, self).add_arguments()
        
    def run(self):
        if self.is_master():
            if not os.path.exists(os.path.join(self.workdir, "ctfs")):
                self.mkdir(os.path.join(self.workdir, "ctfs"))
        self.barrier()
        m_mg = Metadata.read_file(self.input_star)
        results = self.parallel_job("self.find_one", self.generate_args(m_mg))
        if self.is_master():
            meta = CTFMeta()
            for result in results:
                meta.append(result)
            meta.write(os.path.join(self.workdir, "micrographs_ctf.star"))
            self.logger.info("CtfFind Finished. Result in %s" % self.workdir)
    def generate_args(self, m_mg):
        for mg in m_mg:
            yield (mg, ), {}
    
    def find_one(self, mg):
        """ # return CTFMeta 
        """
        
        ctf_meta = CTFMeta()
        obj = ctf_meta.new_object()
        output_diag_file = os.path.join(self.workdir, "ctfs", "%s.ctf" % os.path.splitext(mg["MicrographName"])[0])
        dname = os.path.dirname(output_diag_file)
        if self.pixel_size < 0:
            proj = CFProject()
            self.pixel_size = proj.micrograph_pixel_size
        if self.voltage < 0:
            proj = CFProject()
            self.voltage = proj.accelerating_voltage
        if self.pixel_size < 0:
            raise Exception("Could not get pixel size. Please specify --pixel_size or create a project using cf_init_project")
        if not os.path.exists(dname):
            try:
                os.makedirs(dname)
            except:
                pass
        def1, def2, defangle, cc = self.find(mg["MicrographName"], output_diag_file)
        obj.fom = cc
        obj.def1 = def1
        obj.def2 = def2
        obj.defangle = defangle
        obj.ctf_image = output_diag_file
        obj.cs = self.cs
        obj.voltage = self.voltage
        obj.amplitude_contrast = self.amplitude_contrast
        obj.dstep = self.pixel_size
        obj.mag = 10000
        obj.mg_name = mg['MicrographName']
        return obj

    def find(self, micrograph_name, output_diag_name=None, logfile=None, mode="w"):
        """ Find ctf parameters
        
        Args:
            micrograph_name: A `str`. The path to the micrograph
        """
        proc = pexpect.spawn(self.exec_, timeout=10)
        if logfile is not None:
            proc.logfile = open(logfile, mode)
        proc.expect("Input image file name")
        proc.sendline(micrograph_name)
        proc.expect("Output diagnostic file name")
        proc.sendline(output_diag_name)
        proc.expect("AmpCnst")
        xmag = 10000.
        # pixel_size = dstep * 10000 / xmag
        dstep = xmag * self.pixel_size / 10000
        proc.sendline(", ".join([str(i) for i in [self.cs,
                                                  self.voltage, 
                                                  self.amplitude_contrast,
                                                  xmag,
                                                  dstep]]))
        proc.sendline(", ".join([str(i) for i in [
            self.box_size, self.min_resolution, self.max_resolution,
            self.min_defocus, self.max_defocus, self.defocus_step, self.astigmatism_step
            ]]))
        proc.expect("Final Values", timeout=self.timeout)
        output = proc.before
        line = output.split("\n")[-1]
        def1, def2, angst, cc = [float(i) for i in line.split()[:4]]
        proc.expect(pexpect.EOF, timeout=None)
        return (def1, def2, angst, cc)

if __name__ == "__main__":
    ctf = CtfFind()
    ctf.pixel_size=0.857
    ctf.voltage = 200
    ctf.input_star = "CFImport/job001/micrographs.star"
    os.chdir("/home/xueqi/Documents/py_cf_mt/python_scripts/testCF/test_data/TestCtfFind/test1")
    ctf.run()
    