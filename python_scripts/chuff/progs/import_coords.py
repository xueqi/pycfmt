#!/usr/bin/env python


from chuff.util.box import Box
import math
import os
from cryofilia.project.parameter import MPIParameter
from cryofilia.project.project import CFProject
from relion.metadata import Metadata

from cryofilia.ctf import CTFMeta

class Coordinate(Metadata):
    _labels = CTFMeta._labels.copy()
    _labels.update({
        "x" : ["CoordinateX"],
        "y" : ["CoordinateY"],
        "shx" : ["OriginX"],
        "shy" : ["OriginY"],
        "phi" : ["AngleRot"],
        "theta" : ["AngleTilt"],
        "psi" : ["AnglePsi"]
        })

class HelicalCoordinate(Metadata):
    _labels = Coordinate._labels.copy()
    _labels.update({"helical_track_length" : ["HelicalTrackLength"],
                    "fila_id" : ["HelicalTubeID"],
                    "psi_prior" : ["AnglePsiPrior"],
                    "theta_prior" : ['AngleTiltPrior'],
                    "psi_flip_ratio" : ["AnglePsiFlipRatio"] })
                   
class ImportCoords(MPIParameter):
    
    def __init__(self, *args, **kwargs):
        super(ImportCoords, self).__init__(*args, **kwargs)
        
    def add_arguments(self):
        self.add_argument('input_star', required=True, widget="FileChooser",
            help="Input file, e.g. from CFCtf/job001/micrographs_ctf.star")
        self.add_argument('output_star', help="Output star file")
        self.add_argument("micrograph_pixel_size", type=float, default=-1.,
            help="Micrograph pixel size. Required for helical segmentation")
        self.add_argument('helical', action="store_true",
            help="import helical coordinates.")
        self.add_argument("helical_rise", type=float, default=-1.,
            help="Helical rise. In Angstrom")
        self.add_argument('continuous_filaments', action='store_true',
            help='Use to connect boxes of curved filaments.')
        self.add_argument("continious_threshould", type=float,
            help="helical join threshold. In Angstrom")
        
        self.add_argument("no_segment", action="store_true",
            help="Do not segment filament. (Already segmented)")
        self.add_argument('ext', default='mrc')
        super(ImportCoords, self).add_arguments()
    
    def run(self):
        
        if self.helical:
            if self.helical_rise < 0:
                try:
                    proj = CFProject()
                    self.helical_rise = proj.helical_repeat_distance
                except AttributeError:
                    pass
            if self.micrograph_pixel_size < 0:
                try:
                    proj = CFProject()
                    self.micrograph_pixel_size = proj.micrograph_pixel_size
                except AttributeError:
                    pass
        self.save()
        mgs = Metadata.read_file(self.input_star)
        if self.helical:
            result = HelicalCoordinate()
        else:
            result = Coordinate()
            
        results = self.parallel_job("self.run_one", self.generate_args(mgs))
        if self.is_master():
            for r in results:
                result += r
            result.write(os.path.join(self.workdir, "particles.star"))
            self.logger.info("Coordinate imported to %s" % os.path.join(self.workdir, "particles.star"))
    def generate_args(self, mgs):
        for mg in mgs:
            if self.helical:
                cls = HelicalCoordinate
            else:
                cls = Coordinate
            yield (mg,cls), {}
    
    def run_one(self, mg, cls):
        """ one micrograph
        """
        mg_name = mg.mg_name
        box_name = "%s.box" % os.path.splitext(mg_name)[0]
        boxes = Box.read_from_file(box_name)
        result = cls()
        tpl = result.new_object()
        tpl.update(mg)
        if not self.helical:
            # read normal box
            results = self.parse_normal_boxes(boxes, tpl)
        else:
            results = self.parse_helical_boxes(boxes, self.helical_rise, tpl)
        for r in results:
            result.append(r)
        return result

    def parse_normal_boxes(self, boxes, tpl):
        """convert boxes to metadata
        
        Args:
            boxes: A `list` of `Box`. The boxes to convert
            tpl: A `Container`. This is used to generate new object
        
        Returns:
            A `list` of cls instance. 
        """
        results = []
        for i, box in enumerate(boxes):
            b = tpl.copy()
            b.x = box.center.x
            b.y = box.center.y
            results.append(b)
        return results
    
    def parse_helical_boxes(self, boxes, helical_rise, tpl):
        """ Convert boxes to helical boxes
        """
        # get boxes from one filaments
        filaments = []
        if boxes[0].tag != -1:
            raise Exception("Not a valid helical boxes")
        for i in range(len(boxes)):
            if boxes[i].tag == -1:
                filaments.append([boxes[i]])
            else:
                filaments[-1].append(boxes[i])
        filas = []
        for i, fila in enumerate(filaments):
            seg_fila = self.segment(fila, tpl)
            if i > 0 and filaments[i-1][-1].dist(fila[0]) < self.continious_threshold:
                last_track = filas[-1][-1].helical_track_length
                for ptcl in seg_fila:
                    ptcl.helical_track_length += last_track
                    filas[-1].append(ptcl)
            else:
                filas.append(seg_fila)
        fila_id = 1
        result = []
        for fila in filas:
            for ptcl in fila:
                ptcl.fila_id = fila_id
            fila_id += 1
            result.extend(fila)
        return result

    def segment(self, fila, tpl):
        """
        """
        if not self.no_segment and self.helical_rise > 0:
            fila = segment_filament(fila[0], fila[-1], self.helical_rise,
                                    self.micrograph_pixel_size)
        
        results = []
        last_box = None
        for box in fila:
            obj = tpl.copy()
            obj.x = box.center.x
            obj.y = box.center.y
            if last_box is not None:
                p1, p0 = box, last_box
                x1, y1 = p1.center.x, p1.center.y
                x0, y0 = p0.center.x, p0.center.y
                psi = -math.atan2(y1-y0, x1-x0) * 180. / math.pi
                obj.psi = psi
                obj.psi_prior = psi
                obj.helical_track_length = results[-1].helical_track_length + p0.dist(p1)
            
            obj.theta = 90
            obj.theta_prior = 90
            obj.psi_flip_ratio = 0.5
            results.append(obj)
            last_box = box
        results[0].psi = results[1].psi
        results[0].psi_prior = results[1].psi_prior
        return results

        print("Particles written to %s" % self.output_star)

def segment_filament(box_start, box_end, helical_rise = 1, pixel_size = 1):
    helical_rise_pix = helical_rise / pixel_size
    sx, sy = box_start.center.tolist()
    ex,ey = box_end.center.tolist()
    # get the angle
    angle = math.atan2(ey-sy, ex-sx)
    dx, dy = helical_rise_pix * math.cos(angle), helical_rise_pix * math.sin(angle)
    n_boxes = int(box_start.center.dist(box_end.center) / helical_rise_pix)
    return [Box(sx + dx * i, sy + dy * i) for i in range(n_boxes)]

def continuous_filaments(filaments,helical_rise,pixel_size):
#    helical_rise_pix=helical_rise/pixel_size
    distance_tolerance=filaments[0][0]._box_size*0.25
    new_filaments=[]
    for i in range(len(filaments)-1):
        first_end=filaments[i][1]
        new_start=filaments[i+1][0]
        sx, sy = new_start.center.tolist()
        ex,ey = first_end.center.tolist()
        distance=math.sqrt((sx-ex)**2+(sy-ey)**2)
        if distance<distance_tolerance:
            new_filaments.append([filaments[i][0]])
            new_filaments[-1].append(filaments[i][1])
            new_filaments[-1].append(filaments[i+1][1])
        else:
            new_filaments.append(filament[i])
    return filaments

