""" The chuff enviroment.
"""
import os
import logging
import warnings

# get The chuff dir. this file is $chuff_dir/python_scripts/chuff/__init__.py
chuff_dir = os.path.join(os.path.dirname(os.path.realpath(__file__)),'..','..')
chuff_dir = os.path.realpath(chuff_dir)
chuff_dir = os.path.dirname(os.path.dirname(os.path.dirname(os.path.realpath(__file__))))

exec_dir = 'linux'

CHUFF_MACHINE_FILE = os.path.join(chuff_dir, '..', 'chuff_machine_parameters.m')
CHUFF_OLD_PARAMETER_FILE = "chuff_parameters.m"
CHUFF_PARAMETER_FILE = "cf-parameters.m"

DEFAULT_TUBULIN_PDB = os.path.join(chuff_dir, "data", "tubulin_13.pdb")
DEFAULT_KINESIN_PDB = os.path.join(chuff_dir, "data", "kinesin_13.pdb")
DEFAULT_MICROTUBULE_PF = 13
DEFAULT_MICROTUBULE_START = 1.5

logger = logging.getLogger(__name__)

class ChuffParameter(object):
    """ Old style chuff parameter
    """
    def __init__(self, proj_dir="."):
        self.micrograph_pixel_size = 0.
        self.pixel_size = 0.
        self._project_dir = proj_dir
        self._read_parameter_file()

    def _read_parameter_file(self):
        # The global machine file
        if not os.path.exists(CHUFF_MACHINE_FILE):
            pass
            # warnings.warn("No global machine parameter file found.")
        elif not self.read(CHUFF_MACHINE_FILE):
            warnings.warn("Could not read global machine parameter file.")

        # read new style chuff parameter file cf-parameters.m
        param_loaded = False
        
        if os.path.exists(os.path.join(self._project_dir, CHUFF_PARAMETER_FILE)):
            if self.read(os.path.join(self._project_dir, CHUFF_PARAMETER_FILE)):
                param_loaded = True
        # if not new style not found, read old styled chuff_parameters.m
        elif os.path.exists(os.path.join(self._project_dir, CHUFF_OLD_PARAMETER_FILE)):
            if self.read(os.path.join(self._project_dir, CHUFF_OLD_PARAMETER_FILE)):
                param_loaded = True
        if not param_loaded:
            if not os.path.exists(os.path.join(self._project_dir, CHUFF_PARAMETER_FILE)):
                raise NoChuffParameterException("No cf parameter file found."
                        + "Working directory: %s" % os.getcwd())
            else:
                raise ChuffParameterFormatWrongException()
        
        if hasattr(self, 'scanner_pixel_size') and hasattr(self, 'target_magnification'):
            self.pixel_size = 10000.0 * self.scanner_pixel_size / self.target_magnification
            self.micrograph_pixel_size = self.pixel_size

    def update(self, new_param):
        '''update dictionary with new dictionary.
        
        Args:
            new_param: A `dict`. The values used to update.
        '''
        self.__dict__.update(new_param)

    def __str__(self):
        rs = ""
        for key, value in self.__dict__.items():
            if key.startswith("_"): continue
            if key in ['update', 'write']: continue
            rs += "%s = %s\n" % (key, value)
        return rs
    
    def write(self, parameter_file):
        """ Write parameter to file.
        
        Args:
            parameter_file: A `str`. The file name used to write.
        """
        with open(parameter_file, 'w') as f_param:
            for key, value in self.__dict__.items():
                if type(value) is str:
                    value = "\"%s\"" % value
                elif value is True: value = 1
                elif value is False: value = 0
                f_param.write("%-25s = %s;\n" % (key, str(value)))

    def _parse_value(self, str_value, value_type=None):
        """ Parse value to a predefined format
        
        Args:
            str_value: A `str`. The string value to parse.
        Returns:
            A parse type value.
        """
        import re
        pat_float =  r"[-+]?\d*\.\d+|\d+$"
        pat_int = r"[-+]?\d+$"
        # string
        value = str_value
        if len(str_value) > 1 and ((str_value[0] == "'" and str_value[-1] == "'") 
                               or (str_value[0]=='"' and str_value[-1]== '"')):
            value = str_value[1:-1]
        # int
        elif re.match(pat_int, str_value):
            value = int(str_value)
        # float/double
        elif re.match(pat_float, str_value):
            value = float(str_value)
        return value

    def read(self, parameter_file):
        """ read parameter from file
        
        Args:
            parameter_file: A `str`. Where the parameter is read from.
        """
        if not os.path.exists(parameter_file):
            warnings.warn("parameter file %s does not exist" % parameter_file)
            return False

        with open(parameter_file) as f_param:
            import re
            for line in f_param:
                ends = "#%;"
                for end_char in ends:
                    if end_char in line:
                        line = line[:line.index(end_char)]
                line = line.strip()
                if not line:
                    continue
                
                sep = '='
                if not sep in line:
                    sep = ' '
    
                idx = line.index(sep)
                varname = line[:idx].strip()
                value = line[idx+1:].strip()
                value = self._parse_value(value)
                # this is old chuff styled program
                if isinstance(value, str) and varname.endswith("_prog"):
                    # check program
                    value = value.replace("_prog", "")
                    progname = os.path.join(chuff_dir, exec_dir, value)
                    if os.path.exists(progname):
                        value = progname
                    else:
                        pg = self.getExecutable(value)
                        if pg is not None:
                            logger.warn("Could not find executable %s" % value)
                setattr(self, varname, value)
        return True

    def getExecutable(self, prog_path):
        ''' Get executable from path
        Args:
            prog: A `str`. The program name to find
        Returns:
            The full path of the executable if found, otherwise None
        '''
        paths = os.environ.get("PATH", "")
        sep = os.path.sep
        paths = paths.split(sep).extends(os.environ.get("path"), "").split(sep)
        for path in paths:
            if os.access(os.path.join(path, prog_path), os.X_OK):
                return os.path.join(path, prog_path)
        return None
    
    def __getitem__(self, key):
        return getattr(self, key)

    def __setitem__(self, key, value):
        setattr(self, key, value)

class NoChuffParameterException(RuntimeError):
    pass
class ChuffParameterFormatWrongException(RuntimeError):
    pass
