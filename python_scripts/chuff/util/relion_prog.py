'''
Created on Feb 28, 2017

@author: xl256
'''

import os
import logging

from command import build_commands, run_command
from functools import partial
from relion import Metadata, add_label
from cryofilia.EMImage import EMImage, TestImage
import shutil
from matplotlib.backends.backend_pdf import PdfPages
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
stdout = logging.StreamHandler()
stdout.setLevel(logging.DEBUG)
logger.addHandler(stdout)
def qsub_run(cmd, cores = 1, nodes = 1, workdir = None, logger = None, join = True):
    '''
        use slurm to run the job
    '''
    CPU_PER_NODE = 20
    max_job_per_nodes = CPU_PER_NODE / cores
    nodes_needed = nodes / max_job_per_nodes
    if nodes_needed * max_job_per_nodes < nodes: nodes_needed+=1 
    from chuff.util.batch.slurm import SlurmBatch
    sb = SlurmBatch()
    sb.cores = cores
    sb.ntasks = nodes
    sb.mem_per_cpu = 7500   

    script_file = "%s.script" % os.path.basename(cmd[0])
    cmd =  " ".join(map(str, cmd))
    sb.submit(cmd, script_file = script_file)
#    run_command(cmd, workdir, logger, join)

def relion_command(cmd_name, logfile = None, errfile = None, *args, **kwargs):
    '''
        Default parameters: 
        logger: None, no log out
        workdir: None, current workdir
        join: True, wait the command to finish
        @param build_command_only: only build the command line and return the command line
    '''
    logger = kwargs.pop('logger', None)
    workdir = kwargs.pop('workdir', None)
    join = kwargs.pop('join', True)
    cores = kwargs.pop('cores', 1)
    nodes = kwargs.pop('nodes', 1)
    qsub = kwargs.pop('qsub', False)
    if logger is None:
        logger = logging.getLogger(__name__)
    if (logger is None) and logfile:
        fh = logging.FileHandler(logfile)
        logger.setLevel(logging.DEBUG)
        logger.addHandler(fh)
    if logger:
        logger.info("Running command: %s, with parameter : %s" % (cmd_name, str(kwargs)))
    if cores > 1:
        kwargs['j'] = cores
    build_command_only = kwargs.pop("build_command_only", False)
    cmd = build_commands(cmd_name, sep = ' ', prefix = '--', *args, **kwargs)
    if nodes > 1:

        cmd = build_commands('mpirun', "--oversubscribe", '--map-by', 'node', '--np', str(nodes), *cmd)
    if qsub:
        qsub_run(cmd, workdir = workdir, logger = logger, join = join, cores=cores, nodes=nodes)
        return
    
    if build_command_only:
        return cmd
    import os
    print " ".join(cmd)
    run_command(cmd, workdir = workdir, logger = logger, join = join, env = os.environ.copy())

relion_preprocess = partial(relion_command, 'relion_preprocess')
relion_refine = partial(relion_command, 'relion_refine')
relion_refine_mpi = partial(relion_command, 'relion_refine_mpi')
relion_reconstruct = partial(relion_command, 'relion_reconstruct')
relion_reconstruct_mp = partial(relion_command, 'relion_reconstruct_mp')
relion_postprocess = partial(relion_command, 'relion_post_process')
relion_mask_create = partial(relion_command, 'relion_mask_create')

# `which relion_preprocess` --i /ysm-gpfs/pi/sindelar/mike//krios_session_analysis_first500/CFCTFFind/job001/micrographs_ctf.star --reextract_data_star CFRefAlign/job075/particles_out_pf12_relion.star --recenter --part_star Extract/job029/particles.star --part_dir Extract/job029/ --extract --extract_size 480 --scale 240 --norm --bg_radius 119 --white_dust -1 --black_dust -1 --invert_contrast  --helix --helical_outer_diameter 380 --helical_bimodal_angular_priors

relion_extract = partial(relion_preprocess,  )

refine_mt_opt = {
        'sigma_ang' : 0.5,
        'dont_check_norm' : True,
        'auto_local_healpix_order':0,
        'healpix_order':6
        }
relion_refine_mt = partial(relion_refine, *refine_mt_opt)
relion_refine_mt_mpi = partial(relion_refine_mpi, *refine_mt_opt)


lbls = {
    'helical_rise' : ['HelicalRise'],
    'helical_twist' : ['HelicalTwist'],
    'num_pfs'       : ['numPfs', int, 'Number of protofilament'],
    'starts'        : ['numStarts', float, "Number of starts"],
    'inner_radius'  : ['innerRadius', float, 'Inner radius'],
    'outer_radius'  : ['outerRadius', float, 'Outer Radius'],
    'max_iter'      : ['NumberOfIterations'],
    'particle_diameter':['ParticleDiameter'],
    'box_size'      : ['ImageSize'],
    'mask_file'     : ['MaskName'],
    'current_iteration' : ["CurrentIteration"],
    'helix'         : ['IsHelix'],
    'sigma_tilt'    : ['SigmaPriorTiltAngle'],
    'sigma_psi'    : ['SigmaPriorPsiAngle'],
    'ignore_helical_symmetry' : ['IgnoreHelicalSymmetry'],
    'current_resolution' : ['CurrentResolution'],
    'use_gpu'       : ['useGPU', bool, 'Use gpu acceleration'],
    'gpu'           : ['gpu', str, "Gpu allocation"],
    'gold'          : ['gold', bool, 'Good standard refine'],
    }

for label in lbls.values():
    if len(label) == 1: continue
    add_label(*label)

class RefinementParameter(Metadata):
    def __init__(self):
        super(RefinementParameter, self).__init__(isList = True)
        # add paramters
        for lbl in lbls:
            self.add_label(lbls[lbl][0])
#  add properties
def getx(x, self):
    return self[x]
def setx(x, self, v):
    self[x] = v
for lbl, value in lbls.items():
    v1 = value[0]
    setattr(RefinementParameter, lbl, property(partial(getx, v1), partial(setx, v1)))
    

class MicrotubuleRefinement(RefinementParameter):
    def __init__(self, helical_rise, helical_twist, num_pfs, num_starts = 1.5):
        super(MicrotubuleRefinement, self).__init__()
        self.helical_rise = helical_rise
        self.helical_twist = helical_twist
        self.num_pfs = num_pfs
        self.num_starts = num_starts
        self.workdir = None
        # microtubule mask
        self.inner_radius = 80
        self.outer_radius = 200
        self.max_iter = 10
        self.particle_diameter = 600 #TODO
        self.sigma_tilt = 5
        self.sigma_psi = 5
        self.helix = True
        self.ignore_helical_symemtry = True
        
        self.nodes = 1
        self.cores = 4
        self.qsub = False
        self.logfile = "log.log"
        self.voxel_size = None
        self._box_size = None
        self._search_helical_symmetry = True
        self._local_refine = True
        self._helix = True
        self._current_iteration = 0
        self._mask = None
        
    def set_initial_star(self, star_file):
        shutil.copy(star_file, self.star_file(0))    
        if self.voxel_size is None:
            m = Metadata.readFile(star_file)
            self.voxel_size = m[0]['DetectorPixelSize'] / m[0]['Magnification'] * 10000
            
    def run(self):
        # load existing run
        if os.path.exists(self.parameter_file):
            self.load()
        if not self.mask_file:
            self.mask_file = self.volume_file(0, "mask")
            self.mask.write_image(self.mask_file)
        self.save()
        # loop through iterations
        while self.current_iteration <= self.max_iter:
            if self.current_iteration == 0:
                self.current_resolution = self.voxel_size * 2

            self.refine_one(self.current_iteration)
            self.current_iteration += 1
            # save iteration
            self.save()
    @property
    def parameter_file(self):
        return os.path.join(self.workdir, "parameter.star")
    
    def save(self):
        self.write(self.parameter_file)
    
    def load(self):
        self.read(self.parameter_file)
    
    def refine_one(self, iteration):
        print iteration, 'iteration'
        # common args
        refine_kwargs = {
            'o'                         : self.output_root(iteration),
            'iter'                      : self.current_iteration,
            'particle_diameter'         : self.particle_diameter,
            }
        if iteration <= 1: 
            refine_kwargs.update({
                'i'                         : self.star_file(iteration - 1),
                'ref'                       : self.volume_file(iteration - 1),

                'ctf'                       : True,
                'ctf_corrected_ref'         : True,
                'flatten_solvent'           : True,
                'zero_mask'                 : True,
    
                # local refinement
                'oversampling'              : 1,
                'sigma_ang'                 : 0.2,
                'offset_range'              : 3,
                'offset_step'               : 1,
                'sym'                       : 'C1',
                'norm'                      : True,
                'scale'                     : True,
                'dont_check_norm'           : True, 
                # only write star file . Do not use now, because it does not write optimiser file
                #'skip_maximize'             : True,
                })
            
            if self.local_refine:
                refine_kwargs.update({
                    'auto_local_healpix_order'  : 3,
                    'healpix_order'             : 4,
                    })
            if self.helix:  # always true in MT
                refine_kwargs.update({
                        'helix'                     : True,
                        'helical_outer_diameter'    : self.outer_radius * 2,
                        'helical_inner_diameter'    : self.inner_radius * 2,
                    })
                if not self.ignore_helical_symmetry:
                    refine_kwargs.update({
                        'helical_z_percentage'      : 0.3,
                        'helical_nr_asu'            : 1,
                        'helical_rise_initial'      : self.helical_rise,
                        'helical_twist_initial'     : self.helical_twist,                
                        })
                else:
                    refine_kwargs['ignore_helical_symmetry'] = True
                if self.search_helical_symmetry:
                    refine_kwargs.update({
                        'helical_symmetry_search'   : True,
                        'helical_twist_min'         : self.helical_twist - 5,
                        'helical_twist_max'         : self.helical_twist + 5,
                        'helical_rise_min'          : self.helical_rise - 5,
                        'helical_rise_max'          : self.helical_rise + 5,                
                        })
                if self.local_refine:
                    refine_kwargs.update({
                        'sigma_tilt'                : self.sigma_tilt,
                        'sigma_psi'                 : self.sigma_psi,
                        'helical_sigma_distance'    : 0.33333,
                        'helical_offset_step'       : 1,
                        'psi_step'                  : 2,
                    })
            if self.mask:
                refine_kwargs.update({
                    'flatten_solvent' : True,
                    'solvent_mask'  : self.mask_file
                    })
            if self.gold and iteration > 0:
                refine_kwargs.update({
                    "split_random_halves" : True
                    })
        else: # this is a continious run from last
            refine_kwargs.update({
                "continue"  : self.star_file(iteration - 1, "it%03d_optimiser" % (iteration-1)), # run_it003_optimiser.star
                })
            
            if self.mask_file:
                refine_kwargs['solvent_mask'] = self.mask_file
        
            
        refine_kwargs.update({
            'nodes' : self.nodes,
            'cores' : self.cores,
            'qsub'  : self.qsub,
            'logfile' : self.logfile,
            })
        if self.use_gpu:
            refine_kwargs.update({
                'gpu' : self.gpu,
                })

        if not os.path.exists(self.star_file(iteration)):
            if self.nodes > 1:
                output = relion_refine_mpi(**refine_kwargs)
            else:
                output = relion_refine(**refine_kwargs)

            # get the refined star file
            os.symlink(os.path.realpath(self.star_file(iteration, "it%03d_data" % iteration)),
                       self.star_file(iteration))
        # get helical parameter from model.star
        model_star_file = self.star_file(iteration, "it%03d_model" % iteration)
        if os.path.exists(model_star_file):
            logger.info("Updating helical prameters")
            m_model = Metadata("model_classes")
            m_model.read(model_star_file)
            if m_model[0].hasKey('HelicalRise'):
                self.helical_rise = m_model[0]['HelicalRise']
                self.helical_twist = m_model[0]['HelicalTwist']
                logger.info("New helical parameter: rise: %.4f, twist: %.4f" 
                            % (self.helical_rise, self.helical_twist))
            else:
                logger.warn("Could not find helical information")
                logger.debug("labels: %s" % " ".join(m_model.getActiveLabels()))
            m_model = Metadata("model_general")
            m_model.read(model_star_file)
            if m_model[0].hasKey('CurrentResolution'):
                self.current_resolution = m_model[0]['CurrentResolution']
                logger.info("Current Resolution: %.4f" % self.current_resolution)
        # symmetrize the volume
        if iteration == 0:
            # for first iteration, only symmetrize the whole volume
            self.postprocess(iteration)
        else:
            self.postprocess(iteration, half=1)
            self.postprocess(iteration, half=2)
        
        # Need to replace the last mrc file with new symmetrized mrc file
        self.plot_result(iteration)
    def postprocess(self, iteration, half = -1):
        ''' Post process the volume to apply microtubule symmetry
        '''
        # remove the original reconstruction
        try:
            os.remove(self.volume_file(iteration))
        except OSError:
            pass
            
        from cryofilia.helix.filament.filament import symm_mt_star
        
        logger.info("Symmetrizing star file")
        # symmetrize the star file
        tag= ""
        if half > 0: tag = "_%d" % half
        symmed_star_file = self.star_file(iteration, "symmed%s" % tag)
        if not os.path.exists(symmed_star_file):
            m = Metadata.read_file(self.star_file(iteration))
            if tag:
                m = m.filter(lambda x: x['RandomSubset'] == half)
            symm_m = symm_mt_star(m, self.num_pfs, self.num_starts, 
                              -self.helical_twist / self.voxel_size, 
                              self.helical_rise / self.voxel_size
                              )
            symm_m.write(symmed_star_file)
        symmed_vol_file = self.volume_file(iteration, "noseam%s" % tag)
        logger.info("Reconstructing symmetrized volume")
        if not os.path.exists(symmed_vol_file):
            # reconstruct using the symmed star file
            relion_reconstruct_mp(i = symmed_star_file, ctf = True, o=symmed_vol_file,
                           j = self.cores)
        # rebuild seam
        rebuild_file = self.volume_file(iteration, "rebuild%s" % tag)
        logger.info("Rebuild microtubule seam")
        if not os.path.exists(rebuild_file):
            # from cryofilia.helix.filament.filament import rebuild_seam_mt
            symmed_vol = EMImage(symmed_vol_file)
#            rebuild_vol = rebuild_seam_mt(symmed_vol, self.helical_rise, 
#                                      -self.helical_twist, 
#                                      num_pfs = self.num_pfs, 
#                                      num_starts = self.num_starts, 
#                                      voxel_size = self.voxel_size,
#                                      inner_radius = self.inner_radius, 
#                                      outer_radius = self.outer_radius)
            from cryofilia.helix.basics import symmetrize_filament
            rebuild_vol = symmed_vol
            rebuild_vol.data = symmetrize_filament(rebuild_vol.data.T, self.voxel_size,
                    self.helical_rise / self.num_pfs * self.num_starts,
                    (-self.helical_twist*1.5 + 360.) / self.num_pfs,
                    num_pfs = self.num_pfs,
                    num_starts = self.num_starts,
                    outer_radius = self.outer_radius).T

            rebuild_vol.write_image(rebuild_file)
        # mask and filter the volume
        mask_vol_file = self.volume_file(iteration, "masked%s" % tag)
        logger.info("Filter and masking volume")
        if not os.path.exists(mask_vol_file):
            v = EMImage(rebuild_file)
            self.gaussian_filter(v, resolution = self.current_resolution)
            if self.mask:
                v *= self.mask
            else:
                self.mask_vol(v)
            v.write_image(mask_vol_file)
            if iteration == 0:
                os.symlink(os.path.realpath(mask_vol_file), self.volume_file(iteration))
        half_root = 'it%03d_half%d_class001' % (iteration, half)
        if iteration >= 1 and not os.path.islink(self.volume_file(iteration, half_root)):
            if os.path.exists(self.volume_file(iteration, half_root)):
                os.rename(self.volume_file(iteration, half_root),
                      self.volume_file(iteration, "%s_old" % half_root))
            os.symlink(os.path.realpath(self.volume_file(iteration, "masked_%d" % half)),
                       self.volume_file(iteration, half_root))
        
    def gaussian_filter(self, v, resolution = None):
        import numpy as np
        N = v.xsize 
        r = self.voxel_size * N / resolution
        mask = TestImage.gaussian(v.size, r, 4)
        v.fftdata = v.fftdata * np.fft.ifftshift(mask.data)
        return v
    
    
    def plot_result(self, iteration):
        ''' Plot refinement result to pdf
        '''
        import numpy as np
        import matplotlib.pyplot as plt
        this_star = self.star_file(iteration)
        ts = Metadata.read_file(this_star)
        i_phis, i_thetas, i_psis = ts.get_values(['AngleRot', 'AngleTilt', 'AnglePsi'])
        i_shxs, i_shys = ts.getValues(["OriginX", "OriginY"])
        # plot angle distributions )
        angles = zip(i_phis, i_thetas)
        counts = {}
        for angle in angles:
            if angle not in counts:
                counts[angle] = 0
            counts[angle] += 1
        angles, counts = zip(*(counts.items()))
        phis, thetas = zip(*angles)
        last_star = None
        if iteration > 0:
            last_star = self.star_file(iteration - 1)
            js = Metadata.read_file(last_star)
            j_phis, j_thetas, j_psis = js.get_values(['AngleRot', 'AngleTilt', 'AnglePsi'])
            j_shxs, j_shys = js.getValues(["OriginX", "OriginY"])
            d_shxs = [j_shxs[i] - i_shxs[i] for i in range(len(j_shxs))]
            d_shys = [j_shys[i] - i_shys[i] for i in range(len(j_shys))]
            d_psis = (np.array([j_psis[i] - i_psis[i] for i in range(len(j_psis))]) + 180)  % 360 - 180
            d_phis = (np.array([j_phis[i] - i_phis[i] for i in range(len(j_phis))]) + 180)  % 360 - 180
            d_thetas = (np.array([j_thetas[i] - i_thetas[i] for i in range(len(j_thetas))]) + 180)  % 360 - 180
        with PdfPages(self.pdf_File(iteration)) as pdf:
            plt.figure()
            plt.scatter(phis, thetas, s = counts, alpha = 0.5)
            plt.title("Angle distribution")
            plt.xlabel("phi")
            plt.ylabel("theta")
            pdf.savefig()
            plt.close()
            if iteration > 0:
                plt.figure()
                plt.hist(d_shxs, bins=100, label="x change", alpha=0.5)
                plt.hist(d_shys, bins=100, label="y change", alpha=0.5)
            
                plt.legend()
                pdf.savefig()
                plt.close()
                plt.figure()
                plt.hist(d_phis, bins=100, label = "phi chages", alpha=0.5)
                plt.hist(d_thetas, bins=100, label = "theta chages", alpha=0.5)
                plt.hist(d_psis, bins=100, label = "psi chages", alpha=0.5)
                plt.legend()
                pdf.savefig()
                plt.close()
    
    def output_root(self, iteration = None):
        if iteration is None:
            return os.path.join(self.workdir, "run")
        else:
            return os.path.join(self.workdir, "run_%d" % iteration)
    
    
    @property
    def mask(self):
        if self._mask is None:
            if self.mask_file and os.path.exists(self.mask_file):
                self._mask = EMImage(self.mask_file, mmap=True)
            else:
                self._mask = TestImage.cylinder(self.box_size, 
                                             self.outer_radius / self.voxel_size, 
                                             self.inner_radius / self.voxel_size)
        return self._mask
    @property
    def box_size(self):
        if self._box_size is None:
            s = self.star_file(0)
            m = Metadata.read_file(s)
            stk = m[0]["ImageName"].split("@")[-1]
            m = EMImage(stk, mmap = True)
            self._box_size = m.xsize
        return self._box_size
    
    @property
    def helix(self):
        '''property helix
        '''
        return self._helix
    @helix.setter
    def helix(self, value):
        ''' property setter for helix
        '''
        self._helix = value
    
    @property
    def local_refine(self):
        '''property local_refine
        '''
        return self._local_refine
    @local_refine.setter
    def local_refine(self, value):
        ''' property setter for local_refine
        '''
        self._local_refine = value
    @property
    def search_helical_symmetry(self):
        '''property search_helical_symmetry
        '''
        return self._search_helical_symmetry
    
    @search_helical_symmetry.setter
    def search_helical_symmetry(self, value):
        ''' property setter for search_helical_symmetry
        '''
        self._search_helical_symmetry = value
    
    def mask_vol(self, v):
        return v
    
    def volume_file(self, iteration, extension = None):
        if extension:
            return "%s_%s.mrc" % (self.output_root(iteration), extension)
        else:
            return "%s.mrc" % (self.output_root(iteration))
    def star_file(self, iteration, extension = "data"):
        return "%s_%s.star" % (self.output_root(iteration), extension)
    
    def pdf_File(self, iteration, extension = None):
        if extension:
            return "%s_%s.pdf" % (self.output_root(iteration), extension)
        else:
            return "%s.pdf" % (self.output_root(iteration))
    
    
    
    
    
