""" Base class for multiprocessing protocol.

Parallelization is achieved by running on all process with same code,
    rank 0 / cpu 0 would distribute job, collect result
    rank 1+ request for job, run job and send back result

Usage:
1. For single machine, use ThreadedParallel or ProcessedParallel
   Only one thread is provided from command line.
   After the Object initialized, the thread/process is created
       and for each thread/process, the Parallel instance is created.
       
    
2. For multiple machine, use MPIParallel
"""
import importlib
import multiprocessing as mp
from collections import OrderedDict
import Queue

DEFAULT_MASTER = 0
class MultiProcess(mp.Process):
    """ Custom Multiprocess
    """
    def __init__(self, error_queue, *args, **kwargs):
        super(MultiProcess, self).__init__(*args, **kwargs)
        self.error_queue = error_queue
    
    

class ParallelBase(object):
    def __init__(self, rank=0, size=1):
        self._rank = rank
        self._active = False
        self.job_queue = None
        self.size = size
        if self.__class__ is ParallelBase:
            raise RuntimeError("ParallelBase should not be initilaized")

    def _master(self):
        """ Master thread/process distribute jobs, collect results, and loging
        """
        
    
    def add_job(self, func, *args, **kwargs):
        """ Add a single job to job queue
        """
        self.job_queue.put((func, args, kwargs))

    def run(self):
        """ Parallel base
        """
        if self.is_master:
            self._master()
        elif self.is_worker:
            self._work()
    
    def _work(self):
        """
        """
        self._active = True
        while self._active:
            try:
                # get a job
                job_func, job_args, job_kwargs = self.get_job()
                if job_func is None:
                    self._active = False
                    break
                # Get the function
                if isinstance(job_func, str):
                    if job_func.startswith("self."):
                        job_func = getattr(self, job_func)
                    else:
                        job_func = importlib.import_module(job_func)
                result = job_func(*job_args, **job_kwargs)
                self.return_result(result)
            except Exception:
                print("Error occurred")
                raise            
    
    def get_job(self):
        """ Get a job from queue. This need to be overrided
        """
        return None, None, None
    
    def return_result(self, result):
        """ Return the result to queue. This need to be overrided
        """
        result = result
    
    @property
    def is_master(self):
        return self._rank == 0
    @property
    def is_worker(self):
        return self._rank > 0
    

class ThreadedParallel(ParallelBase):
    """ Multithread implmentation. Use thread pool design
    """
    
    def __init__(self, *args, **kwargs):
        super(ThreadedParallel, self).__init__(*args, **kwargs)
        self.job_queue = Queue.Queue()
        self.result_queue = Queue.Queue()
        self.error_queue = Queue.Queue()
        self.log_queue = Queue.Queue()
        
        # create threads
        self.threads = OrderedDict()
        for cpu_idx in range(self.size):
            thread_name = "parallel_%d" % cpu_idx
            thread = self.start_worker()
            self.threads[thread_name] = thread
        self.master = self.start_master()
    
    
    def start_master(self):
        """ Master for parallel jobs.
        Master thread distribute jobs, collect result, and log
        """
        import threading
        master = threading.Thread(target=self._master)
        master.start()
        return master

    
    def start_worker(self):
        """ Worker for parallel jobs
        """
        import threading
        thread = threading.Thread(target=self._work, args=(
                                    self.job_queue,
                                    self.result_queue,
                                    self.error_queue,
                                    self.log_queue))
        thread.start()
        return thread
    
    def _work(self, job_queue, result_queue, error_queue, log_queue):
        job = self.get_job()
        func, args, kwargs = job
        
    
class MP(ParallelBase):
    """ Multiprocessing based parallization
    """
    def __init__(self, *args, **kwargs):
        super(MP, self).__init__(*args, **kwargs)
        if self._rank == DEFAULT_MASTER:
            self.job_queue = mp.Queue()
            self.exc_queue = mp.Queue()
            self.result_queue = mp.Queue()
        else:
            self.job_queue = None
            self.exc_queue = None
            
    def get_job(self):
        """ Get a job from job queue
        Returns:
            Tuple of (func_name, func_args, func_kwargs)
        """
        try:
            job = self.job_queue.get(timeout=MP.DEFAULT_TIMEOUT)
            if len(job) != 3:
                raise Exception("Job should be with length of 3")
        except Queue.Empty:
            pass
    
    def return_result(self, result):
        """ Return a result to result queue
        Args:
            result: The result put to the result queue.
        """
        self.job_queue.task_done()
        self.result_queue.put(result)
    
class TestMP(object):
    def __init__(self):
        self.job_queue = mp.Queue()
        self.result_queue = mp.Queue()
        self.exc_queue = mp.Queue()
    
    def tt(self):
        a = [1,2,3,4,5,6,7]
        for _ in a: self.job_queue.put(('self.a', [_], {}))
        self.run()
        