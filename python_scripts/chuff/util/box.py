import math
class Point(object):
    def __init__(self, x = 0, y = 0):
        self.x = x
        self.y = y
    
    def tolist(self):
        return [self.x, self.y]
    def __add__(self, other):
        return Point(self.x + other.x, self.y + other.y)
    
    def dist(self, other):
        return math.sqrt(self.dist2(other))
    
    def dist2(self, other):
        dx, dy = self.x - other.x, self.y - other.y
        return dx * dx + dy * dy
    
class Box(object):
    def __init__(self, centx, centy, sx = 64, sy = None, angle = 0, tag = 0):
        '''
        @param centx:  center of x
        @param centy: center of y
        @param sx: width
        @param sy: height
        @param angle: psi angle
        
        '''
        if sy is None: sy = sx
        self.center = Point(centx, centy)
        self._box_size = Point(sx, sy)
        self.angle = angle
        self.tag = tag
    def copy(self):
        b = Box(self.center.x, self.center.y, self._box_size.x, self._box_size.y, self.angle, self.tag)
        return b
    @property
    def box_size(self):
        return self._box_size
    @box_size.setter
    def box_size(self, xy):
        if type(xy) is int:
            xy = [xy, xy]
        self._box_size.x = xy[0]
        self._box_size.y = xy[1]

    def inside(self, mg_size, box_size = None):
        if box_size is None:
            box_size = [self.box_size.x, self.box_size.y]
        sx2, sy2 = map(lambda x: x/2, box_size)
        return self.center.x >= sx2 and self.center.x + sx2 <mg_size[0] and self.center.y >= sy2 and self.center.y + sy2 < mg_size[1]
    
    def dist(self, other):
        return self.center.dist(other.center)

    @staticmethod
    def read_from_file(box_file, fmt = "eman", **kwargs):
        '''
            read from box file and return a list of boxes
            @param fmt: eman, lmbfgs, lmbfgs_vec
                eman : The tranditional EMAN .box file
                lmbfgs: The Rubinstein's align particle program input
                lmbfgs_vec: The output of Rubinstein's align particle program. This should not be used
        '''
        boxes = []
        if fmt == "eman":
            with open(box_file) as f:
                for line in f:
                    x, y, sx, sy, tag = [int(m) for m in line.strip().split()]
                    b = Box(x + sx / 2, y + sy / 2, sx, sy, 0, tag = tag)
                    boxes.append(b)
        elif fmt == "relion":
            from relion import Metadata
            m = Metadata.read_file(box_file)
            for m1 in m:
                b = Box(m1['rlnCoordinateX'], m1['rlnCoordinateY'], 64, 64)
                boxes.append(b)
        elif fmt == "lmbfgs":
            with open(box_file) as f:
                f.readline()
                for line in f:
                    x,y,z = line.strip().split()
                    x,y,z = int(x), int(y), float(z)
                    b = Box(x, y, 0,0,0)
                    boxes.append(b)
        elif fmt == "lmbfgs_vec":
            with open(box_file) as f:
                lines = f.read().split("\n")
                boxes = [[]]
                for line in lines:
                    if not line.strip():
                        if len(boxes[-1]) != 0:
                            boxes.append([])
                        continue
                    b = line.strip().split()
                    b = [float(mm) for mm in b]
                    boxes[-1].append(Box(b[1], b[2], 0, 0, 0))
                if len(boxes[-1]) == 0:
                    boxes.pop()
                n_movies = len(boxes[0])
                movies = zip(*boxes)
                # for each movies
                min_dif = 100000000
                zero_idx = -1
                for i in range(n_movies):
                    x_c = [b.center.x for b in movies[i]]
                    ints = [int(x_cc) for x_cc in x_c]
                    difs = [abs(x_c[j] - ints[j]) for j in range(len(ints))]
                    sumdifs = sum(difs)
                    if sumdifs < min_dif:
                        min_dif = sumdifs
                        zero_idx = i
                boxes = movies[zero_idx] 
                    
        return boxes
    
    def __str__(self):
        return "%d %d %d %d %.2f" % (self.center.x, self.center.y, self.box_size.x, self.box_size.y, self.angle)

    def __repr__(self):
        return "(%d %d)" %(self.center.x, self.center.y)
    
    def __add__(self, other):
        '''
            add a shift. Ohter is a point instance
        '''
        b = Box(self.center.x + other.x, self.center.y + other.y, self.box_size.x, self.box_size.y, self.angle)
        return b
    @staticmethod
    def write_box_file(box_file, boxes, fmt = 'eman'):
        if fmt == 'eman':
            f = open(box_file, 'w')
            for box in boxes:
                cx = box.center.x
                cy = box.center.y
                w = box.box_size.x
                h = box.box_size.y
                f.write("%d %d %d %d %s\n" % (cx - w / 2, cy - w / 2, w, h, box.tag))
            f.close()
        elif fmt == 'lmbfgs':
            f = open(box_file, 'w')
            f.write(' x y density\n')
            for box in boxes:
                f.write("%d %d 0.0\n" % (box.center.x, box.center.y))
            f.close()

def box_from_relion(star_file, output_dir = None, apply_shift = True, box_size = 64):
    
    from relion import Metadata
    import os
    m = Metadata.read_file(star_file)
    
    boxes = {}
    if output_dir is None:
        output_dir = os.path.join(os.path.dirname(star_file), 'boxes')
    if not os.path.exists(output_dir):
        os.mkdir(output_dir)
    for ptcl in m:
        x = ptcl['rlnCoordinateX']
        y = ptcl['rlnCoordinateY']
        
        if apply_shift:
            xshift = ptcl.get('rlnOriginX',0)
            yshift = ptcl.get('rlnOriginY',0)
            x -= xshift
            y -= yshift

        mgname = ptcl['rlnMicrographName']
        #tubeid = ptcl.get('rlnHelixTubeID', 0)
        
        if not mgname in boxes:
            boxes[mgname] = []
        
        boxes[mgname].append(Box(x,y,box_size, box_size))
    
    for mgname in boxes:
        box_file_name = "%s.box" % os.path.splitext(os.path.basename(mgname))[0]
        box_file_name = os.path.join(output_dir, box_file_name)
        Box.write_box_file(box_file_name, boxes[mgname])
    return boxes  
        
