'''
Batch system is management system to run multiple commands
@author: xueqi
'''
import os
from command import build_commands, run_command

import logging
logger = logging.getLogger(__name__)
import threading

class BatchJob(threading.Thread):
    ''' A BatchJob is one job that is running on its own process
    '''
    JOB_RUNNING = 1
    JOB_PENDING = 2
    JOB_COMPLETED = 3
    JOB_NOTEXIST = 4
    JOB_CREATED = 5
    INVALID_JOB = -1

    def __init__(self, cmd, nodes = 1, cores = 1, name = None,
                 workdir = None, error_file = None, 
                 output_file = None):
        '''
            @param cmd: A list represent a command line using build_commands.
            @param name: if name is None, the first element in cmd will be used as name.
            @param workdir: The job should be started in which dir
            @param error_file: The output of error will goes
            @param output_file: The output file 
            @param nodes: How many tasks to run for the job
            @param cores: How many cores to run for each task 
        '''
        super(BatchJob, self).__init__()
        self.cmd = cmd
        self.job_id = None
        self.queue = None
        self.nodes = nodes
        self.cores = cores
        self.jobname = name
        self.workdir = workdir
        self._status = BatchJob.JOB_CREATED 
        self.error_file = error_file
        self.output_file = output_file
        self.statusChangeCallBack = None
        self.result = None
        
    def build_command(self):
        '''
            current The command should be parsed before passing to job
        '''
        return build_commands(self.cmd)
    @property
    def status(self):
        '''property status
        '''
        return self._status
    @status.setter
    def status(self, value):
        ''' property setter for status.
            emit status change signal
        '''
        if value != self._status:
            self._status = value
            self.statusChanged()
            
    def statusChanged(self):
        ''' Status changed signal
        '''
        if self.statusChangeCallBack is not None:
            if callable(self.statusChangeCallBack):
                self.statusChangeCallBack(self)
    
    def waitForStatus(self, status, timeout = -1):
        ''' Wait the job until the status
        '''
        import time
        startTime = time.time()
        while True:
            if self.status == status:
                break
            if timeout >= 0 and time.time() - startTime > timeout:
                raise Exception("Timeout to wait for status")
            time.sleep(0.1)
    
    def removeFromQueue(self):
        ''' Remove the job from queue
        '''
        if self.queue:
            self.queue.removeJob(self)
        
    def get_output(self):
        ''' Get the output
        '''
        output_file = self._get_output_file()
        if isinstance(output_file, str) and os.path.exists(output_file):
            return open(self._get_output_file()).read()
        else:
            return None
    
    def _get_output_file(self):
        ''' Get output file path
        '''
        return self.output_file

    def __repr__(self):
        ''' Representation of the instance
        '''
        return self.name or "NoName"
    
    def _run_command(self):
        ''' Run the job command
        '''
        import sys
        stdout = sys.stdout
        if self.output_file:
            stdout = open(self.output_file, 'a+')
        proc = run_command(self.cmd, stdout = stdout, join=False)
        return proc
    
    def run(self):
        ''' Override threading.Thread.run
        '''
        self.status = BatchJob.JOB_RUNNING
        proc = self._run_command()
        self.job_id = proc.pid
        proc.join()
        self.status = BatchJob.JOB_COMPLETED
        
    @classmethod
    def new(cls, *args, **kwargs):
        ''' Factory constructor. 
        ''' 
        queue = kwargs.pop("queue", "default")
        job = cls(*args, **kwargs)
        job.queue = queue
        return job

class BatchQueue(object):
    ''' The queuing system for jobs
    '''
    job_class = BatchJob
    def __init__(self, queuename, bs):
        self.name = queuename
        self.bs = bs
        self.jobs = []
        
    def new_job(self, *args, **kwargs):
        job = self.job_class.new(self, *args, **kwargs)
        self.jobs.append(job)
        return job
    
    def getJobList(self):
        return self.jobs
    
    def removeJob(self, job_to_remove):
        for job in self.jobs:
            if job == job_to_remove:
                self.jobs.remove(job)
                break
    @classmethod
    def new(cls, *args, **kwargs):
        return cls(*args, **kwargs)
    
    def __repr__(self):
        return self.name + " with %d jobs" % len(self.jobs)
    
class BatchSystem(object):
    '''
    Batch System class is used to run program on multiple nodes. 
    
    BatchSystem is singleton, which means there is only one BatchSystem instance in the program. 
    
    Use BatchSystem.getInstance(). Use of BatchSystem() in program will give undefined behaviors
    
    The base class of BatchSystem is used for single node without any queuing system. 
    
    to run a command:
    bs = BatchSystem.getInstance()
    job = bs.new_job(['ls', '-l', '*.txt'])
    bs.submitJob(job)
    
    # convinient tools
    
    # run in current process
    BatchSystem().run(cmd, *args, **kwargs)
    
    Subclass this class, override following function:
        BATCH_COMMAND : The class member of command used to submit the job
        queue_class : The Queueing class. 
        get_script_header : The script header
        submitScript : submit the job
        
    '''
    queues = {}
    queue_class = BatchQueue
    instance = None
    BATCH_COMMAND = "bash"
    
    def new_job(self, queuename, *args, **kwargs):
        '''
            create a new job with command
            @param queuename: The queue name of the system
        '''
        if not queuename in self.queues:
            self.queues[queuename] = self.queue_class.new(queuename, self)
        job = self.queues[queuename].new_job(*args, **kwargs)
        return job
    
    @classmethod
    def getInstance(cls):
        ''' Singleton model. Get the instance of Batch system. 
        Only one instance accross this python session
        '''
        if cls.instance is None:
            cls.instance = cls()
        return cls.instance

    def get_script_header(self):
        '''
            get script header. Deafult to bash
        '''
        return '#!/bin/bash'
    
    def submitScript(self, script_file):
        ''' Submit a script in backgroud, and return the job id
        '''
        p = run_command([self.BATCH_COMMAND, script_file], 
                             workdir = None, logger = None, 
                             join = False, env = None)
        return p.jobid
    
    def submitJob(self, job):
        '''
            Submit a job in background
            Use job.start() to run syncly
        '''
        job_script = self.build_job_script(job)
        script_dir = os.getcwd()
        if hasattr(job, 'script_dir'):
            script_dir = job.script_dir
        script_file = os.path.join(script_dir, ".%s.script" % job.name)
        open(script_file, 'w').write(job_script)
        job.job_id = self.submitScript(script_file)
        if job.job_id == job.INVALID_JOB: return False
        return True
    
    def getJobList(self, queues = None):
        ''' Get all job list in all queues
        '''
        jobs = []
        for queue in self.queues:
            if queues is None or queue in queues:
                jobs.extend(queue.getJobList())
        return jobs
    
    def build_job_script(self, job):
        ''' Build the jbo script for submit in background run
        '''
        cmd = job.build_command()
        cmd = " ".join(map(str, cmd))
        return "%s\n\n%s\n" % (self.get_script_header(), cmd)
    
    def run(self, cmd, *args, **kwargs):
        '''
            run a single process job
        '''
        cmd = build_commands(cmd, *args, **kwargs)
        job = self.new_job("noqueue", cmd)
        job.start()
        job.join()
        return job.result


