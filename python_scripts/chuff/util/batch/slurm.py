''' Slurm scheduler wrapper
'''

import os
from relion import Parameter
DEFAULT_QUEUE = "general"
class Slurm(object):
    def __init__(self):
        pass

    def get_config(self):
        ''' Get config from system
        '''
        if not get_command("srun"):
            return
class SlurmBatch(Parameter):
    _labels = {
            "nodes" : ['nodes', int, 'number of nodes'],
            "ntasks" : ['ntasks', int, 'number of tasks'],
            "cores" : ['cores', int, "number of cores"],
            "mem_per_cpu" : ["mem_per_cpu", int, "memory per cpu in MB"],
            "queue" : ['queue', str, "queue"],
            "logerr": ["logError", str, "error logging file"],
            "log": ["logOut", str, "logging file"]
            }
    
    def __init__(self):
        super(SlurmBatch, self).__init__()
        self.nodes = 1
        self.cores = 1
        self.mem_per_cpu = 1024
        self.queue = DEFAULT_QUEUE
        self.batch_command = "sbatch"
    def run_interactive(self, command, mpirun=False):
        ''' interactive run is just a subprocess run
        '''
        if mpirun:
            mr = MPIRun()
            mr.np=self.nodes * self.cores
            mr.oversubscribe = True
            mr.run(command)
        else:
            import subprocess as sp
            p = sp.Popen(command)
            p.communicate()
    
    def submit(self, command, mpirun=False, name=None, script_file=None, env=""):
        if isinstance(command, str):
            command = [command]
        if mpirun:
            mr = MPIRun()
            mr.np = self.cores * self.nodes
        else:
            mr = None
        if name is None:
            name = command[0][:10]
        self._submit_command(command, mr, name=name, script_file=script_file, env=env)
        
    def _build_script_header(self, **kwargs):
        header = "#!/bin/bash\n"
        if self.queue:
            header += "#SBATCH -p %s\n" % self.queue
        if self.cores > 1:
            header += "#SBATCH -c %s\n" % self.cores
        if self.mem_per_cpu > 0:
            header += "#SBATCH --mem-per-cpu=%s\n" % self.mem_per_cpu
        if self.nodes > 1:
            header += "#SBATCH --nodes %s\n" % self.nodes
        if self.ntasks > 1:
            header += "#SBATCH --ntasks %s\n" % self.ntasks
        output = kwargs.get("output", None)
        if output is not None:
            header += "#SBATCH --output=\"%s\"\n" % output
        err_out = kwargs.get("error", None)
        if err_out is not None:
            header += "#SBATCH --error=\"%s\"\n" % err_out
        name=kwargs.get('name', None)
        if name is not None:
            header+= "#SBATCH -J %s\n" % name
        return header

    def _submit_command(self, cmd, mpirun=None, script_file=None, name="job", env=""):
        '''
        '''
        if mpirun is not None:
            cmd = mpirun.build_command_line(cmd)
        if not isinstance(cmd, str):
            cmd = " ".join(cmd)
        script = self._build_script_header(name=name)
        script += "\n"
        script += env + "\n"
        script += cmd
        script += "\n"
        # write script to 
        if script_file is None:
            import tempfile
            script_file_tmp = tempfile.NamedTemporaryFile()
            script_file = script_file_tmp.name
            script_file_tmp.write(script)
            script_file_tmp.flush()
        else:
            open(script_file,'w').write(script)

        # submit
        self.submit_script(script_file)

    def submit_script(self, script_file):
        import subprocess as sp
        p = sp.Popen([self.batch_command, script_file])
        p.communicate()

class MPIRun(Parameter):
    _labels = {
            "np" : ["np", int, "number of processes"],
            "oversubscribe" : ["oversubscribe", bool, "over subscibe"],
            }
    _mpirun_exec = None

    def __init__(self):
        super(MPIRun, self).__init__()
        self.oversubscribe=True

    def run(self, cmd, *args, **kwargs):
        ''' mpirun warpper
            :param cmd: The command to run, list or tuple, or single command,  use build_commands to build the cmd
        '''
        mpi_cmd = self.build_command_line(cmd)
        import subprocess as sp
        p = sp.Popen(mpi_cmd, **kwargs)
        p.communicate()
    def build_mpi_args(self):
        ''' Build mpirun arguments using property
        '''
        args = []
        args.extend(['-np', str(self.np)])
        if self.oversubscribe:
            args.append('--oversubscribe')
        return args
    def build_command_line(self, command):
        if self.mpirun_exec is None:
            logger.error("No mpirun executable found in path")
            return

        mpi_cmd = [self.mpirun_exec]

        if isinstance(command, str):
            command = [command]
        mpi_cmd.extend(self.build_mpi_args())
        mpi_cmd.extend(command)
        return mpi_cmd
    @property
    def mpirun_exec(self):
        ''' Get mpirun executable
        '''
        if MPIRun._mpirun_exec is not None: 
            if not os.path.exists(MPIRun._mpirun_exec):
                MPIRun._mpirun_exec = None
        else:
            MPIRun._mpirun_exec = get_command('mpirun')
        return MPIRun._mpirun_exec

def get_command(cmd):
    import os, stat
    path = os.environ.get("PATH", "").split(os.pathsep)
    path.extend(os.environ.get('path',"").split(os.pathsep))
    found = False
    for pth in path:
        pth = pth.strip('"')
        if not pth: continue
        cmd_full = os.path.join(pth, cmd)
        if os.path.exists(cmd_full) and os.path.isfile(cmd_full):
            st = os.stat(cmd_full)
            if os.access(cmd_full, os.X_OK):
                return cmd_full
    return None
