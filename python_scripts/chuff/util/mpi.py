''' MPI class implementation using mpi4py
First cpu is server, used to distribute job, gathering result
other cpu is worker

'''
from mpi4py import MPI as _MPI
from .mp import MP
import threading
import Queue
import os
import sys
import time
import logging
import tqdm

comm = _MPI.COMM_WORLD

rank = comm.Get_rank()
logger = logging.getLogger(__name__)

SLAVE_LOG_LEVEL = logging.ERROR
MASTER_LOG_LEVEL = logging.INFO

# Remove stdout handler from slave process
# add file handler to slave process

rootLogger = logging.getLogger()
stdoutHandler = None
for handler in rootLogger.handlers:
    if isinstance(handler, logging.StreamHandler):
        if handler.stream == sys.stderr:
            stdoutHandler = handler
            break
if rank > 1:
    rootLogger.setLevel(logging.INFO)
    if stdoutHandler is not None:
        rootLogger.handlers.remove(stdoutHandler)
else:
    rootLogger.setLevel(MASTER_LOG_LEVEL)

REQUEST_TAG = 101
FUNC_TAG = 102
ARGS_TAG = 103
KWARGS_TAG = 104
RETURN_TAG = 105

# prevent exception deadlock
sys_exc_hook = sys.excepthook
def mpi_excepthook(_type, value, tb):
    if _type is not KeyboardInterrupt:
        sys_exc_hook(_type, value,tb)
        comm.Abort(1)

sys.excepthook = mpi_excepthook

class GatherResult(threading.Thread):
    ''' Gathering result from 
    '''
    formatter = logging.Formatter('%(asctime)s - %(message)s')
    def __init__(self, results, nworkers = 0, *args, **kwargs):
        logfile = kwargs.pop("logfile", None)
        super(GatherResult, self).__init__(*args, **kwargs)
        self.results = results
        self.nworkers = nworkers
        self.logger = logging.getLogger(__name__ + "." + self.__class__.__name__)
        if logfile is not None:
            h = logging.FileHandler(logfile)
            h.setFormatter(GatherResult.formatter)
            self.logger.addHandler(h)
            self.logger.propagate = False

    def run(self):
        status = _MPI.Status()
        worker_finished = 0  # keep track how much 
        try:        # catch the system exceptions.
            while worker_finished < self.nworkers:
                result = comm.recv(tag=RETURN_TAG, status=status)
                
                if isinstance(result, JobFinishObject):
                    worker_finished += 1
                    #self.logger.info("worker %d finished" % (status.Get_source()))
                    self.logger.debug("%d/%d worker finished" % (worker_finished, self.nworkers))
                else:
                    self.results.put(result)
                    self.logger.debug("Got result from process %d, result: %s" % (status.Get_source(), result))
                    # self.logger.info("Got result from process %d, result: %s" % (0, result))
                time.sleep(0.01)
            self.logger.debug("Gathering result finished")
        except (KeyboardInterrupt, SystemError, SystemExit):
            self.logger.exception("System exception, exitting")
        except:
            self.logger.exception("Exception occurred")

class MPI(MP):
    ''' Multiprocessing enviroment parallel run job
    '''
    def __init__(self, *args, **kwargs):
        self._size = comm.Get_size()
        self._rank = comm.Get_rank()
        self._master_rank = 1
        super(MPI, self).__init__(self._size, *args, **kwargs)
        logger.debug("Rank %d intialized" % self._rank)
        # supress output for rank more than 1
        self.logfile = None

    def submit(self, cmd, cores=1, nodes=1, queue=None, name=None, **kwargs):
        from chuff.util.batch.slurm import SlurmBatch
        sb = SlurmBatch()
        sb.cores=cores
        sb.ntasks=nodes
        sb.queue=queue or sb.queue
        sb.mem_per_cpu=7500

        if name is None:
            name = self.__class__.__name__
        sb.output=kwargs.get("output", None)
        sb.error=kwargs.get("error", None)
        cmds = " ".join(map(str, cmd))
        env = kwargs.get("env", "")

        script_file = "%s.script" % name

        if hasattr(self, "workdir"):
            import os
            script_file = os.path.join(self.workdir, script_file)
        sb.submit(cmd, script_file=script_file, name=name, env=env)

    def start(self, **kwargs):
        """ wrapper for mpirun if requested.
        """

        if hasattr(self, "cores") and self.cores > 1:
            cmd = ['mpirun', '-np', str(self.cores + 2), '--oversubscribe']
            argv = sys.argv
            qsub = False
            if '--cores' in argv:
                idx = argv.index('--cores')
                argv.pop(idx)
                argv.pop(idx)
            if '--qsub' in argv:
                idx=argv.index('--qsub')
                argv.pop(idx)
                qsub = True
            if hasattr(self, 'workdir') and self.workdir:
                if '--workdir' in argv:
                    idx = argv.index('--workdir')
                    argv[idx+1] = self.workdir
                else:
                    argv.extend(['--workdir', self.workdir])
            cmd.extend(argv)
            if qsub:
                logger.info("Submitting job...")
                try:
                    queue = self.queue
                except:
                    queue = None
                self.submit(cmd, nodes=self.cores, queue=self.queue, **kwargs)
            else:
                import subprocess as sp
                try:
                    p = sp.Popen(cmd)
                except OSError as e:
                    if e.errno == 2:
                        logger.error("command %s not found" % cmd[0])
                        exit(1)
                    raise (e)
                stdout, stderr = p.communicate()
        else:
            try:
                logdir = os.path.join(self.workdir, 'logs')
                self.mkdir(logdir)
                logfile = os.path.join(logdir, "log_%d.log" % self._rank)
                fh = logging.FileHandler(logfile)
                logging.getLogger().addHandler(fh)
            except:
                logger.exception("create log dir failed")
            self.run()

    def parallel_job(self, func, list_of_args, callback=None, progress_bar=True):
        ''' parallel run job with list of args.

        For MPI process, basically list of args will be sent by rank 0, all others receiver from master rank
        func and arguments must be picklable.
        '''
        if self.logfile is None:
            log_dir = os.path.join(self.workdir, "logs")
            if not os.path.exists(log_dir):
                try:
                    os.mkdir(log_dir)
                except:
                    pass
            self.logfile = os.path.join(self.workdir, "logs", "log_%d.log" % self._rank)
            fh = logging.FileHandler(self.logfile)
            fh.setLevel(logging.DEBUG)
            logger.addHandler(fh)
        self.barrier()
        logger.debug("Parallel job started. rank %d" % self._rank)
        results= []
        import types
        def get_func(func_name):
            if isinstance(func_name, types.MethodType):
                cls = func_name.im_class
                if cls is self.__class__:
                    func_name = func_name.im_func.__name__
                    func_name = getattr(self, func_name)
            elif isinstance(func_name, str):
                try:
                    if func_name.startswith("self"):
                        func_name = getattr(self, func_name[5:])
                except:
                    pass
            return func_name
        # Debug code. 
        try:
            if self.debug:
                if self._rank != 0: return
                for arg, kwargs in list_of_args:
                    results.append(get_func(func)(*arg, **kwargs))
                return results
        except:
            import traceback as tb
            tb.print_exc()

        # Do not use MPI. Use MP instead
        if self._size == 1:
            # results = super(MPI, self).parallel_job(func, list_of_args, callback=callback)
            import types
            func_name = func
            if isinstance(func_name, types.MethodType):
                cls = func_name.im_class
                if cls is self.__class__:
                    func_name = func_name.im_func.__name__
                    func_name = getattr(self, func_name)
            elif isinstance(func_name, str):
                try:
                    if func_name.startswith("self"):
                        func_name = getattr(self, func_name[5:])
                except:
                    pass
            func = func_name
            results = []
            for arg, kwargs in list_of_args:
                results.append(func(*arg, **kwargs))
            return results
        if self._rank == 0:
            # start gather process
            rqueue = Queue.Queue()
            gr = GatherResult(rqueue, self._size - 2, logfile="gather.log")
            gr.start()
        
        elif self._rank == self._master_rank:
            # first cpu is used to distributing jobs
            try:
                logger.debug("Parallel section")
                list_of_args = list(list_of_args)
                if progress_bar is True:
                    pg_bar = tqdm.tqdm(total=len(list_of_args))
                elif progress_bar:
                    pg_bar = progress_bar
                else:
                    pg_bar = None
                for args, kwargs in list_of_args:
                    try:
                        req_id = comm.recv(tag=REQUEST_TAG)
                        # func, args, kwargs here must be serializable by pickle
                        comm.send(func, dest=req_id, tag=FUNC_TAG)
                        comm.send(args, dest=req_id, tag=ARGS_TAG)
                        comm.send(kwargs, dest=req_id, tag=KWARGS_TAG)
                        if pg_bar is not None:
                            pg_bar.update(1)
                    except Exception as e:
                        logger.exception(sys.exc_info())
                        raise e
                if progress_bar is True: # I am responsible to close the progress bar
                    try:
                        pg_bar.close()
                    except:
                        pass
                logger.info("Waiting for all process finish")
                for i in range(self._size - 2):
                    req_id = comm.recv(tag=REQUEST_TAG)
                    comm.send(None, dest=req_id, tag=FUNC_TAG)

            except Exception as e:
                open("error4", 'w').write(str(sys.exc_info()))            
        else:
            try:
                logger.debug("rank %d working" % self._rank)
                while True:
                    comm.send(self._rank, dest=self._master_rank, tag=REQUEST_TAG)
                    func_name = comm.recv(tag=FUNC_TAG)
                    logger.debug("Got function %s" % func_name)
                    if func_name is None:
                        comm.send(JobFinishObject(), dest=0,tag=RETURN_TAG)
                        break
                    args = comm.recv(tag=ARGS_TAG)
                    kwargs = comm.recv(tag=KWARGS_TAG)
                    # check if this is method of this class. if so, set the instance to self
                    import types
                    if isinstance(func_name, types.MethodType):
                        cls = func_name.im_class
                        if cls is self.__class__:
                            func_name = func_name.im_func.__name__
                            func_name = getattr(self, func_name)
                    elif isinstance(func_name, str):
                        try:
                            if func_name.startswith("self"):
                                func_name = getattr(self, func_name[5:])
                        except:
                            pass
                    func = func_name
                    try:
                        #if self._rank > 1:
                            #sys.stdout = open(os.devnull, 'w')
                        result = func(*args, **kwargs)
                    except Exception as e:
                        err_msg =  "Error on rank %d, with arguments: %s" % (
                                self._rank, "args: %s, kwargs: %s" % (args, kwargs))
                        open("error1",'a').write(err_msg + str(sys.exc_info()))
                        logger.exception("exception")
    
                        # raise e
                    comm.send(result, dest=0, tag=RETURN_TAG)
            except Exception as e:
                import traceback as tb
                open("error2", 'w').write("error on rank %d\n%s" % (self._rank,
                                tb.format_exc()))
                raise e
        if self._rank == 0:
            gr.join()
            while not rqueue.empty():
                results.append(rqueue.get())
        comm.barrier()
        results = comm.bcast(results, root=0)
        return results
    
    def mkdir_sync(self, path):
        if self._rank == 0:
            try:
                os.mkdir(path)
            except OSError as e:
                if e.errno != 17: # file exists
                    raise e
        self.barrier()

    def is_master(self):
        """ Return the master process
        """
        return self._size == 1 or self._rank == self._master_rank

    def error(self, e_str):
        """ Raise an exception with message.
        
        Args:
            e_str: A `str`. The error message
        Raises:
            Exception. 
        """
        if self._rank == 0:
            raise Exception(e_str)
        else:
            sys.exit(-1)

    def barrier(self):
        comm.barrier()

    def bcast(self, a):
        """ Cast variable a accross the mpi nodes
        No-op if not using mpi, e.g. self._size == 1
        """
        if self._size > 1:
            return comm.bcast(a, root=self._master_rank)
        else:
            return a
    def sync(self):
        ''' Sync parameters of self
        '''
        pass
class JobFinishObject(object):
    pass
