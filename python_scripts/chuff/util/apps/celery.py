'''
Created on Mar 30, 2018

@author: xueqi
'''
from __future__ import absolute_import, unicode_literals

from celery import Celery

app = Celery("chuff.util.apps")
app.config_from_object("chuff.util.apps.celeryconfig")
app.conf.update(result_expires=3600,)
app.conf.update(include=["chuff.util.apps.tasks"])