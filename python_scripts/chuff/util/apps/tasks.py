'''
Created on Mar 30, 2018

@author: xueqi
'''
import os
from chuff.util.app import app
from relion import Metadata

class ModelMeta(Metadata):
    _labels = {
        "file" : ["ReferenceImage"],
        "rise" : ["HelicalRise"],
        "twist" : ["HelicalTwist"]
        }
    def __init__(self):
        super(ModelMeta, self).__init__("model_classes")

@app.task(name="synth_microtubule")
def synth_microtubule(output_file, num_pf=13, ref_pdb=None, ref_num_pf=None,
                      voxel_size=None, box_size=None,
                      nuke=False):
    from cryofilia.helix.filament.microtubule import Microtubule
    mt = Microtubule.new(num_pf, ref_pdb, ref_num_pf)
    if not os.path.exists(output_file) or nuke:
        vol = mt.toVolume(voxel_size, box_size)
        vol.write_image(output_file)
    v = ModelMeta().newObject()
    v.file = output_file
    v.rise = 80
    v.twist = 0
    return v

@app.task(name="parallel")
def parallel_job(self, func_name, *args, **kwargs):
    func = getattr(self, func_name)
    return func(*args, **kwargs)


def build_command_ui(par):
    """ Build command ui using Parameter instance
    """
    