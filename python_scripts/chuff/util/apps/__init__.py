import os

def start_worker():
    # get pid
    worker_pid = None
    if os.path.exists('.worker'):
        worker_pid = open(".worker").read()
        try:
            worker_pid = int(worker_pid)
        except:
            worker_pid = None
        try:
            os.kill(worker_pid, 0)
        except OSError:
            worker_pid = None
    if not worker_pid:
        os.system("cf_start_worker")
