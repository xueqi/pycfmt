'''
Created on Jul 18, 2018

@author: xueqi
'''

import Tkinter as _tk
# import ttk as _ttk
class MetadataViewer(_tk.Frame, object):
    '''
    classdocs
    '''
    def __init__(self, *args, **kwargs):
        '''
        Constructor
        '''
        super(MetadataViewer, self).__init__(*args, **kwargs)
        self._init_menus()
        self._init_ui()
    def _init_menus(self):
        self.menubar = _tk.Menu(self)
        filemenu = _tk.Menu(self.menubar, tearoff=0)
        filemenu.add_command(label="Open", command=self.open_file)
        filemenu.add_separator()
        filemenu.add_command(label="Exit", command=self.quit)
        self.menubar.add_cascade(label="File", menu=filemenu)
        self.master.config(menu=self.menubar)
    
    def _init_ui(self):
        self.main_window = _tk.Frame(self)
        self.main_window.pack(fill=_tk.BOTH)
        tl = _tk.Toplevel(self)
    
    def open_file(self):
        print("OPen file")
if __name__ == "__main__":
    # unittest
    root = _tk.Tk()
    root.geometry("800x600")
    mv = MetadataViewer(root)
    mv.pack(fill=_tk.BOTH)
    root.mainloop()