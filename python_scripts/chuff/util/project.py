'''
Created on Mar 3, 2017

@author: xl256
'''
import os
def get_next_workdir(task_name, create=True):
    if not create:
        return "."
    if not os.path.exists(task_name):
        os.mkdir(task_name)
    for i in range(1,1000):
        if not os.path.exists(os.path.join(task_name, 'job%03d' % i)):
            try:
                os.mkdir(os.path.join(task_name, 'job%03d' % i))
            except OSError as e:
                if e.errno != 17: #  File exists
                    raise e
            break
    return os.path.join(task_name, 'job%03d' % i)


class OldCF(object):
    def __init__(self, *args, **kwargs):
        super(OldCF, self).__init__()
        self.chumps_round = 1
    
    def create_dirs(self):
        if not os.path.exists(self.scan_dir):
            os.mkdir(self.scan_dir)
        if not os.path.exists(self.chumps_dir):
            os.mkdir(self.chumps_dir)
        if not os.path.exists(self.cf_parameter_file):
            try:
                os.symlink(os.path.realpath('cf-parameters.m'), self.cf_parameter_file)
            except:
                import sys
                print sys.exc_info()
                print("Could not create cf-parameter file")
        if not os.path.exists(self.smooth_dir):
            os.mkdir(self.smooth_dir)

    @property
    def scan_dir(self):
        return os.path.join(self.workdir, "scans")

    @property
    def chumps_dir(self):
        return os.path.join(self.workdir, "chumps_round%d" % self.chumps_round)

    @property
    def cf_parameter_file(self):
        return os.path.join(self.workdir, 'cf-parameters.m')
    
    @property
    def smooth_dir(self):
        return os.path.join(self.chumps_dir, "smoothed_coords")

