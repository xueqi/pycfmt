'''
Created on Jul 14, 2017

@author: xueqi
'''
from tqdm import tqdm
class ProgressBar(tqdm):
    '''
    multilevel progress bar
    '''
    def __init__(self, *args, **kwargs):
        '''
        Constructor
        '''
        level = kwargs.pop('level', 0)
        super(ProgressBar, self).__init__(*args, **kwargs)
    def format_meter(self, *args, **kwargs):
        result = super(ProgressBar, self).format_meter(*args, **kwargs)
        return "\x1b[F" * self.level + result