'''
Created on Jun 28, 2017

@author: xueqi
'''
import unittest
from chuff.util.batch.batch import BatchSystem, BatchJob


class TestBatch(unittest.TestCase):


    def setUp(self):
        pass


    def tearDown(self):
        pass


    def testSingleton(self):
        bs = BatchSystem.getInstance()
        bs2 = BatchSystem.getInstance()
        self.assertIs(bs, bs2, "Should return same instance")
        
class TestBatchJob(unittest.TestCase):
    
    def testInit(self):
        # test default parameters
        bj = BatchJob(['cmd'])
        self.assertEqual(['cmd'], bj.cmd)
        self.assertEqual(1, bj.nodes)
        self.assertEqual(1, bj.cores)
        self.assertEqual(None, bj.jobname)
        self.assertEqual(None, bj.workdir)
        self.assertEqual(BatchJob.JOB_CREATED, bj.status)
        self.assertEqual(None, bj.error_file)
        self.assertEqual(None, bj.output_file)
        self.assertEqual(None, bj.statusChangeCallBack)
        self.assertEqual(None, bj.result)
        
        # test parameters
        bj = BatchJob('cmd1', nodes = 2, cores = 4, name = "testcmd1",
                      workdir="testworkdir", error_file = "error_file",
                      
                      output_file = "output_file")
        self.assertEqual('cmd1', bj.cmd)
        self.assertEqual(2, bj.nodes)
        self.assertEqual(4, bj.cores)
        self.assertEqual("testcmd1", bj.jobname)
        self.assertEqual("testworkdir", bj.workdir)
        self.assertEqual(BatchJob.JOB_CREATED, bj.status)
        self.assertEqual("error_file", bj.error_file)
        self.assertEqual("output_file", bj.output_file)
        
    def test_build_command(self):
        '''
        '''
        job = BatchJob.new(['ls', '-l'], error_file = "job1.error", 
                           output_file = "job1.output", queue = "Default")
        self.assertEqual(['ls', '-l'], job.build_command())
        
    def test_run(self):
        job = BatchJob.new(['ls', '-l'], error_file = "job1.error", 
                           output_file = "job1.output")
        
        job.start()
        job.join()
        print job.result
        
if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    #unittest.main()
    def statusChanged(self):
        print "job %s status chagned to %s" % (self.name, self.status)
    bs = BatchSystem()
    job = bs.new_job("testqueue",["ls", "10"])
    job.name = "job1"
    job.statusChangeCallBack = statusChanged
    job.output_file = "job1.output"
    job.error_file = "job1.error"
    job = bs.new_job("testqueue", ['ls', '.'])
    job.name = 'job2'
    job.output_file = "job2.output"
    job.error_file = "job2.error"
    import os
    print os.getcwd()
    job.statusChangeCallBack = statusChanged
    q = bs.queues['testqueue']
    for job in q.getJobList():
        job.start()
        print job.status
    while True:
        nlives = 0
        for job in q.getJobList():
            if job.isAlive():
                nlives += 1
                print job.name, job.job_id
        import time
        time.sleep(1)
        if nlives == 0: break
