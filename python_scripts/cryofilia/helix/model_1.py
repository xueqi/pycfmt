from relion import Metadata, Container

class Microscope(Metadata):
    _labels = {
            'voltage'   : ['Voltage'],
            'cs'        : ['SphericalAberration'],
            }

class Camera(Metadata):
    _labels = {
            'scanner_pixel_size'    : ["DetectorPixelSize"]
            }
class Experiment(Metadata):
    _labels = {
            'magnification' : ["Magnification"],
            'amplitube_contrast' : ["AmplitudeContrast"], 
            }
    _labels.update(Microscope._labels)
    _labels.update(Camera._labels)

class CTF(Metadata):
    _labels = {
            'd1'    : ['DefocusU'],
            'd2'    : ['DefocusV'],
            'dangle': ['DefocusAngle'],
            }
class Micrograph(Metadata):
    _labels = {
            'mgname': ['MicrographName'],
            'mgid'  : ['micrographID', int, "MicrographID"]
            }
    _labels.update(Experiment._labels)
    _labels.update(CTF._labels)

class Filament(Metadata):
    _labels = {
            'mt_id' : ['HelicalTubeID'],
            }
    _labels.update(Micrograph._labels)
    
class Particle(Metadata):
    _labels = {
            'x'     : ['CoordinateX'],
            'y'     : ['CoordinateY'],
            'phi'   : ['AngleRot'],
            'theta' : ['AngleTilt'],
            'psi'   : ['AnglePsi'],
            'dx'    : ['OriginX'],
            'dy'    : ['OriginY'],
            }
    _labels.update(Micrograph._labels)

class FilamentParticleContainer(Container):
    ''' Filament ParticleContainer
    '''
class FilamentParticle(Metadata):
    ContainerClass = FilamentParticleContainer
    _labels = {
            }
    _labels.update(Filament._labels)
    _labels.update(Particle._labels)
