"""
    Smooth Coordinate Module
    Smooth a filament according to its symmetry
    Smoothing is done in following steps for each filament:
1. fix the seam. Optional. 
    For num_pf > 1, the phi angles are bring to the same proto-filament first.
2. index the coordinates.
    Using the xyz coordinates to put each particles in certain index from 0.
    The index of particles looks like:
    0, 1, 1, 3, 3, 4, 5, ...
3. interpolation.
    The extra points are removed and the lacking points are added.
4. smoothing
    All the points are interpolated using fitting order 2 with adjacent points.

"""
from __future__ import print_function
import logging
import os
import numpy as np
from collections import Counter, OrderedDict

import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages

from relion import Metadata, MPIParameter
from cryofilia.alg.fitting import trimmed_nd_lsq_fit,\
    trimmed_lsq_fit_arr
from cryofilia.helix.smoothing.utils import get_values, fix_rollover, get_window
from cryofilia.helix.smoothing.plotting import plot_smooth_result_m
from cryofilia.helix.smoothing.discontinuity import DISCONT_COORD, DISCONT_DIST, DISCONT_TWIST
logger = logging.getLogger(__name__)

SMOOTH_XYZ = "smooth_xyz"
SMOOTH_ALL = "smooth_all"
SMOOTH_ANGLE = "smooth_angle"
SMOOTH_NONE = "smooth_none"
PLOT_EXT = "jpg"


class FilamentInfo(Metadata):
    """ Smoothed filament info
    """
    _labels = {
        "mg_name" : ["MicrographName"],
        "fila_id" : ["HelicalTubeID"],
        "old_fila_id" : ["oldHelicalTubeID", int, "Old tube id it belongs to"],
        "repeats" : ["nrepeats", int, "Number of repeat in this filament"],
        "rise"    : ['rise', float, "rise of the filament"],
        "twist"   : ["twist", float, "twist of the filament"],
        "std_rise" : ['std_rise', float, "standard error of the rise"],
        "std_twist" : ['std_twist', float, "standard error of the twist"]
        }
FilamentInfo()

class SmoothedCoordinates(Metadata):
    """ Smooth Coordinate Output format
    """
    _labels = {
        'mg_name'   : ['MicrographName'],
        'x'         : ['CoordinateX'],
        'y'         : ['CoordinateY'],
        'z'         : ['CoordinateZ'],
        'shx'       : ['OriginX'],
        'shy'       : ['OriginY'],
        'shz'       : ['OriginZ'],
        'phi'       : ['AngleRot'],
        'theta'     : ['AngleTilt'],
        'psi'       : ['AnglePsi'],
        'magnification' : ['Magnification'],
        'scanner_pixel_size'    : ['DetectorPixelSize'],
        'voltage'   : ['Voltage'],
        'd1'        : ['DefocusU'],
        'd2'        : ['DefocusV'],
        'dangle'    : ['DefocusAngle'],
        'amplitude_contrast'    : ['AmplitudeContrast'],
        'cs'        : ['SphericalAberration'],
        'fila_id' : ['HelicalTubeID'],
        'track_length'  :['HelicalTrackLength'],
        'twist' : ['twist', float, "helical twist"],
        'dtheta': ['dtheta', float, 'dtheta'],
        'dpsi'  : ['dpsi', float, 'dpsi'],
        'discontinuity' : ['discontinuity', int, 'discontinuity'],
        'interpolated'  : ['interpolated', bool, 'interpolated'],
        'fitness'   : ['fitness', float, 'fitness'],
        'index' : ['index', int, 'index'],
        'x_index_est' : ['xIndexEstimate', float, "estimated x after indexing"],
        'y_index_est' : ['yIndexEsitmate', float, 'estimated y after indexing'],
        'x_old' : ['oldXCoordinate', float, 'original x coord'],
        'y_old' : ['oldYCoordinate', float, 'original y coord'],
        }

    def get_name(self, num_pf=None):
        """ Get identifier name of the filament
            bname_filaid_pfxx
        """
        bname = os.path.splitext(os.path.basename(self.mg_name))[0]
        if num_pf is not None:
            return "%s_%d_%d" % (bname, self.fila_id, num_pf)
        else:
            return "%s_%d" % (bname, self.fila_id)
# load labels
SmoothedCoordinates()

class SmoothCoordsParameter(MPIParameter):
    """ SmoothCoords parameters. Inherit from MPIParameter
    """
    def __init__(self, *args, **kwargs):
        super(SmoothCoordsParameter, self).__init__()
        
    def add_arguments(self):
        self.add_argument('input_star', required=True,
                help="Required. Input star file", group="Input/Output")
        self.add_argument("output_star_file",
                help="Optional. Output star file", group="Input/Output")
        # experiment
        self.add_argument("micrograph_pixel_size", type=float, required=True,
                help=("Pixel size on micrograph, in which boxing is done.\n"
                      + "Required is no cf-parameters.m"),
                group="Experiment")        
        # filament Parameter
        self.add_argument("helical_twist", type=float, required=True,
                help="Required. Approximately twist per repeat",
                group="Filament Parameter")
        self.add_argument("helical_rise", type=float, required=True,
                help="Required, Approximately rise per repeat",
                group="Filament Parameter")
        self.add_argument("num_pf", type=int, default=1,
                help="Optional. Number of protofilament, default is 1",
                group="Filament Parameter")
        self.add_argument("num_starts", type=float, default=1.0,
                help=("Optional. Num of starts is how many repeat distances "
                      + "when the symmetry turn 360 degree."
                      + "Default is 1. For MT, should be 1.5"),
                group="Filament Parameter")
        self.add_argument("subunits_per_repeat", type=float, default=1,
                help=("Optional. Number of subunit per repeat.\n"
                      + "Not confused by num_pfs. Default is 1."
                      + "This is the number of subunits in one pf of one repeat"
                      + "For MT, use 2 for better result"),
                group="Filament Parameter")
        self.add_argument("rise_per_subunit", type=float,
                help=("Optional. Rise per subunit.\n"
                      + "Used for fixup seam for Microtubule."
                      + "Default is helical_rise * num_starts / num_pfs"),
                group="Filament Parameter")
        self.add_argument("twist_per_subunit", type=float, default=0.0,
                help=("Optional. Twist per subunit.\n"
                      + "Needed for fix seam. "
                      + "Default is (360 + helical_twist) / num_pfs"),
                group="Filament Parameter")

        self.add_argument("phi_error_tolerance", type=float, default=5,
                help="phi error tolerance. Default 5",
                group="Diagnosis/Optional")
        self.add_argument("theta_error_tolerance", type=float, default=5,
                help="Theta error tolerance. Default 5",
                group="Diagnosis/Optional")
        self.add_argument("psi_error_tolerance", type=float, default=5,
                help="Psi error tolerance. Default 5",
                group="Diagnosis/Optional")
        self.add_argument("angle_tolerance", type=float, default=-1,
                help=("Angle tolerance, global for all angles."
                     + "Use negative value to disable. Default -1"),
                group="Diagnosis/Optional")
        self.add_argument("direction_error_tolerance", type=float, default=20,
                help="Direction error tolerance."
                     + "Degree the direction change compared to prev subunit. "
                     + "Default 20",
                group="Diagnosis/Optional")
        self.add_argument("dist_tolerance", type=float, default=5,
                help=("Distance difference between adjacent subunits tolerance. "
                     + "Default 5"),
                group="Diagnosis/Optional")
        self.add_argument("twist_tolerance", type=float, default=7,
                help=("Twist tolerance"
                     + "Twist difference between adjacent subunits. "
                     + "Default 5"),
                group="Diagnosis/Optional")
        
        # smooth parameter
        self.add_argument("win_size", type=int, default=7,
                help="Optional. Window size for smooth. Default 7",
                group="SmoothParameter")
        self.add_argument("fit_order", type=int, default=2,
                help="Optional. Fit order to smooth the data. Default 2",
                group="SmoothParameter")
        self.add_argument("min_segs", type=int, default=3,
                help="Optional. Mininum consecutive boxes to keep. Default 3",
                group="SmoothParameter")
        self.add_argument("output_plot_dir",
                help=("Optional. Output plot directory. Default: %s/jobxxx/plots"
                      % self.__class__.__name__),
                group="Input/Output")
        self.add_argument("output_pdf", action="store_true",
                help="Optional. Output plot in pdf format instead of jpg.",
                group="Input/Output")        
        self.add_argument("interp_discontinuity", action="store_true",
                help=("Optional. Also interpolate discontinuity. Default false"
                      + "Another step to try to fix discontinuity after diagnosis"),
                group="SmoothParameter")
        self.add_argument("keep_filament_id", action="store_true",
                help="Optional. Do not reject the boxes and keep filament id.",
                group="SmoothParameter")
        self.add_argument("only_interpolate", action="store_true",
                help=("Optional. only interpolate, do not smooth. Default false"),
                group="SmoothParameter")
        self.add_argument("separate_filament", type=float, default=-1,
                help=("Optional. Separate filament before smooth, threshold in angstrom."
                    + " Default (-1), means do not smooth"),
                group="SmoothParameter")

        self.add_argument("dont_output_plot", action="store_true",
                help="Optional. Do not output plot",
                group="Input/Output")
        
        super(SmoothCoordsParameter, self).add_arguments()

SmoothCoordsParameter()
class SmoothCoords(SmoothCoordsParameter):
    """ Smooth Coords instance. For command line processing
    """
    def __init__(self, *args, **kwargs):
        """ Constructor. No description
        """
        # attributes. This is only used to make eclipse happy.
        # super class would make all these attributes        
        super(SmoothCoords, self).__init__()
        self._pdf = None
        self.project = None
        # default values. Keep the same as in command line.
        self.init_with_dict(kwargs)
        self.filament_offset = 100
        self.is_2d = True

    def run(self):
        """ Overwrite Parameter.run.
        """
        if self.output_plot_dir == "": #pylint: disable=access-member-before-definition
            self.output_plot_dir = os.path.join(self.workdir, "plots")
        if self.output_plot_dir != ""  and not os.path.exists(self.output_plot_dir):
            self.mkdir(self.output_plot_dir)
        self.workdir = self.workdir or  self.get_next_workdir()
        if self.output_star_file == "": #pylint: disable=access-member-before-definition
            self.output_star_file = os.path.join(self.workdir, "smooth_output.star")

        if abs(self.helical_rise) < 0.0001:
            raise SmoothError("Helical rise must not be 0")

        # interpolation of discontinuity does not work with split filament
        if self.interp_discontinuity:
            self.keep_filament_id = True

        # get default micrograph_pixel_size from config file if there is any.
        if self.micrograph_pixel_size == 0.: #pylint: disable=access-member-before-definition
            try:
                from cryofilia.project import CFProject
                self.project = CFProject(os.getcwd())
            except RuntimeError:
                raise "Please provide micrograph_pixel_size if not in a project dir"
            self.micrograph_pixel_size = self.project.pixel_size

        if self.micrograph_pixel_size == 0.:
            raise RuntimeError("The micrograph pixel size is 0. Please specify"
                               +  "--micrograph_pixel_size in command")
        
        # read the input file. This would take long time if the file is large.
        # not because the file is big, but the parsing is slow.
        m_input = Metadata.read_file(self.input_star)

        if m_input[0].has_key('CoordinateZ'):
            self.is_2d = False
        try:
            filaments = m_input.group_by('MicrographName', 'HelicalTubeID')
        except KeyError: # No Helical Tube ID. This should not happen normally
            # assign the filament id to 1
            filaments = m_input.group_by("MicrographName")
            n_f = OrderedDict()
            for key, value in filaments.items():
                n_f[(key, 1)] = value
                for obj in value:
                    obj.fila_id = 1
            filaments = n_f
        # try to continue. This would not happen because we only write output
        # when all finished
        output_star = SmoothedCoordinates()
        if not os.path.exists(self.output_star_file) or self.nuke:
            output_filaments = {}
        else:
            output_star.read(self.output_star_file)
            output_filaments = output_star.group_by('MicrographName', 'HelicalTubeID')
        # write the parameter to job dir
        self.save()

        # parallel part
        results = self.parallel_job("self.smooth_filament_m",
                        self.generate_args(filaments, output_filaments))
        
        # gather result in main process
        for smoothed_filament in results:
            for ptcl in smoothed_filament:
                if len(output_star) == 0:
                    output_star.add_labels(ptcl.keys())
                output_ptcl = output_star.new_object()
                output_ptcl.update(ptcl)
                output_star.append(output_ptcl)
        if self.is_2d:
            output_star.deactivate_label('CoordinateZ')
            output_star.deactivate_label('OriginZ')
        # output the star file
        output_star.write(self.output_star_file)
        output_star.clean_for_relion().write("%s_relion%s"
             % os.path.splitext(self.output_star_file))
        if self.is_master():
            logger.info("Smooth finished. Result in %s", self.workdir)
    
    def gather_result(self, queue):
        """ Gather result from queue and write to disk
        """
        pass
        
    def generate_args(self, filaments, output_filaments):
        """ Generate arguments for smooth_filament function
        """
        for mgname, fila_id in filaments:
            fila = filaments[(mgname, fila_id)]
            if len(fila) < 2: continue
            tubename = (mgname, fila_id)
            bname = os.path.splitext(os.path.basename(mgname))[0]
            if not self.nuke and tubename in output_filaments:
                continue
            kwargs = {}
            plot_ext = PLOT_EXT
            if self.output_pdf:
                plot_ext = "pdf"
            if not self.dont_output_plot:
                kwargs['output_plot'] = os.path.join(self.output_plot_dir,
                        "%s_%d.%s" % (bname, fila_id, plot_ext))
            yield [fila], kwargs
    
    def smooth_filament_m(self,
                          fila,
                          output_plot=None,
                          keep_original_ptcls=False):
        """ smooth_filament takes a filament as input,
            output the smoothed coordinates`
            The returned filament is a SmoothedCoordinates instance,
            containing all smoothed parameters for each particles.
            This is the main function for parallelization
            
            Args:
                fila: A `Metadata`. The input filament
                output_plot: A `str`. The plot name for output
            Returns:
                The smoothed filament. A `SmoothedCoordinates`
        """
        if keep_original_ptcls:
            self.keep_filament_id = True
        if not isinstance(fila, SmoothedCoordinates):
            fila = SmoothedCoordinates.copy_from(fila, extra=True)

        # create plot dir.
        output_plot_name = output_plot
        if output_plot_name is None:
            if self.output_plot_dir:
                if not os.path.exists(self.output_plot_dir):
                    # this is from MPParameter, dealing with multiprocess
                    self.mkdir(self.output_plot_dir)
                mg_name = os.path.basename(fila.mg_name)
                fila_id = fila.fila_id
                plot_ext = PLOT_EXT
                if self.output_pdf:
                    plot_ext = "pdf"
                output_plot_name = os.path.join(self.output_plot_dir,
                              "%s_%d.%s" % (mg_name, fila_id, plot_ext))
        
        if self.separate_filament < 0:
            return self._smooth_filament_m1(fila,
                                            output_plot_name,
                                            keep_original_ptcls)
        
        # self.separater_filament > 0
        
        filas = separate_filament(fila, self.separate_filament)
        result = None
        for idx, fila in enumerate(filas):
            for ptcl in fila:
                ptcl.fila_id += idx * 10000
            if output_plot_name is not None:
                output_plot_name = self._get_output_plot_name(
                    fila)
            result += self._smooth_filament_m1(fila,
                                               output_plot_name,
                                               keep_original_ptcls)
        return result
    
    def _get_output_plot_name(self, fila):
        """Get output plot name
        """
        if len(fila) == 0:
            return None
        fila_id = fila[0].fila_id
        mg_name = os.path.splitext(os.path.basename(fila[0].mg_name))[0]
        plot_ext = PLOT_EXT
        if self.output_pdf:
            plot_ext = "pdf"
        return os.path.join(self.output_plot_dir,
                              "%s_%d.%s" % (mg_name, fila_id, plot_ext))
    
    def _smooth_filament_m1(self,
                          fila,
                          output_plot_name=None,
                          keep_original_ptcls=False):
        """ smooth_filament takes a filament as input,
            output the smoothed coordinates`
            The returned filament is a SmoothedCoordinates instance,
            containing all smoothed parameters for each particles.
            This is the main function for parallelization
            
            Args:
                fila: A `Metadata`. The input filament
                output_plot_name: A `str`. The plot name for output
            Returns:
                The smoothed filament. A `SmoothedCoordinates`
        """
        logger.debug("smoothing %s:%d", fila.mg_name, fila.fila_id) 
        # fix seam first
        if self.num_pf > 1:
            if self.rise_per_subunit == 0.: #pylint: disable=access-member-before-definition
                if self.helical_rise == 0.:
                    raise SmoothError("Could not determine helical_rise_per_subunit")
                logger.info("Use rise: %.4f, num_pf: %d, num_starts: %.1f",
                             self.helical_rise, self.num_pf, self.num_starts)
                self.rise_per_subunit = self.helical_rise * self.num_starts / self.num_pf

            if self.twist_per_subunit == 0.: #pylint: disable=access-member-before-definition
                # This won't happend for num_pf > 1
                self.twist_per_subunit = (360. + self.helical_twist) / self.num_pf
            fila = fixup_seam_pos_filament_star(fila, self.rise_per_subunit,
                                          self.twist_per_subunit,
                                          self.helical_twist,
                                          self.micrograph_pixel_size)

        spacing = self.helical_rise / self.micrograph_pixel_size
        # return immediately if only 2 box
        if len(fila) <= 2:
            return fila
        polarity = get_polarity_m_1(fila,
                    micrograph_pixel_size=self.micrograph_pixel_size)
        if np.sum(polarity) < 0:
            fila.reverse()
            polarity = -polarity
            polarity = polarity[::-1]
        def _m_smooth(fila, polarity=None):
            fila_est = fila.copy()
            # initialize index
            for ptcl in fila_est:
                ptcl.index = 0
            for _ in xrange(5):
                indices = fila_est.get_values("index")

                index_coords_m(fila_est, spacing,
                               micrograph_pixel_size=self.micrograph_pixel_size,
                               winsize=self.win_size,
                               polarity=polarity)
                if indices == fila_est.get_values("index"):
                    # converged
                    break
            for idx in range(len(fila_est)):
                fila[idx].index = fila_est[idx].index
                fila[idx].fitness = fila_est[idx].fitness
            fila_interp = fila.copy()
            interpolate_coords_m(fila_interp,
                                 micrograph_pixel_size=self.micrograph_pixel_size,
                                 expect_dphi=-self.helical_twist,
                                 winsize=self.win_size,
                                 fit_order=self.fit_order)
            fila_smooth = fila.copy()
            interpolate_coords_m(fila_smooth,
                                 smooth_method=SMOOTH_ALL,
                                 micrograph_pixel_size=self.micrograph_pixel_size,
                                 expect_dphi=-self.helical_twist,
                                 winsize=self.win_size,
                                 fit_order=self.fit_order)
            # re-estimate helical track length
            for i in range(len(fila_smooth)):
                if i == 0:
                    fila_smooth[i].track_length = 0.
                else:
                    fl = fila_smooth[i-1]
                    fc = fila_smooth[i]
                    lx, ly, lz = fl.x, fl.y, fl.z
                    x, y, z = fc.x, fc.y, fc.z
                    x, y, z = x - lx, y - ly, z - lz
                    fc.track_length = fl.track_length + np.sqrt(x*x + y*y + z*z)
                    
            return fila_interp, fila_smooth
        
        fila_interp, fila_smooth = _m_smooth(fila, polarity=polarity)
        for ptcl in fila_smooth:
            ptcl.phi = ptcl.phi % 360
        diagnosis_m(fila_smooth,
                  micrograph_pixel_size=self.micrograph_pixel_size,
                  direction_error_tolerance=self.direction_error_tolerance,
                  phi_error_tolerance=self.phi_error_tolerance,
                  theta_error_tolerance=self.theta_error_tolerance,
                  psi_error_tolerance=self.psi_error_tolerance,
                  twist_error_tolerance=self.twist_tolerance,
                  dist_error_tolerance=self.dist_tolerance/self.micrograph_pixel_size)
        if self.interp_discontinuity:
            logger.debug("Correcting discontinuity")
            interp_discontinuity_m(fila_smooth, micrograph_pixel_size=self.micrograph_pixel_size)
            # fila_smooth_filt = fila_smooth.filter(lambda x: x.discontinuity < 0.5)
            # fila_interp, fila_smooth = _m_smooth(fila_smooth_filt)
            diagnosis_m(fila_smooth,
                  micrograph_pixel_size=self.micrograph_pixel_size,
                  direction_error_tolerance=self.direction_error_tolerance,
                  phi_error_tolerance=self.phi_error_tolerance,
                  theta_error_tolerance=self.theta_error_tolerance,
                  psi_error_tolerance=self.psi_error_tolerance,
                  twist_error_tolerance=self.twist_tolerance,
                  dist_error_tolerance=self.dist_tolerance/self.micrograph_pixel_size)
        # smooth should be done in one threads. This is a fast step.
        # but we need to keep the fila_interp and original fila information.
        fig = plot_smooth_result_m(fila_smooth, self.micrograph_pixel_size,
                            fila_interp=fila_interp,
                            fila_ori=fila,
                           expect_dphi=-self.helical_twist,
                           expect_rise=self.helical_rise,
                           ylim=self.helical_rise * 2,
                           is_3d=not self.is_2d)
        if self._pdf:
            self._pdf.savefig(fig)
        if output_plot_name is not None:
            if output_plot_name.endswith(".pdf"):
                with PdfPages(output_plot_name) as pdf:
                    pdf.savefig(fig)
            else:
                fig.savefig(output_plot_name)
        plt.close(fig)

        if keep_original_ptcls:
            # keep original particles, find the nearest ptcls in original stack
            smooth_x, smooth_y = fila_smooth.get_values(['CoordinateX', 'CoordinateY'])
            smooth_coords = zip(smooth_x, smooth_y, range(len(smooth_x)))
            # O(N^2) 
            scale = 10000.*fila[0]['ScannerPixelSize']/fila[0]['Magnification']
            scale /= self.micrograph_pixel_size
            for ptcl in fila:
                x,y = ptcl.x, ptcl.y
                min_dist2 = 1000000000000
                min_idx = 0
                for smx, smy, idx in smooth_coords:
                    dist2 = (smx-x) * (smx-x) + (smy-y) * (smy-y)
                    if dist2 < min_dist2:
                        min_dist2 = dist2
                        min_idx = idx
                if min_dist2 < 1000000000:
                    min_ptcl = fila_smooth[min_idx]
                    ptcl.x, ptcl.y = min_ptcl.x, min_ptcl.y
                    ptcl.shx = (x - min_ptcl.x) / scale
                    ptcl.shy = (y - min_ptcl.y) / scale
                    ptcl.phi = min_ptcl.phi
                    ptcl.theta = min_ptcl.theta
                    ptcl.psi = min_ptcl.psi
        
        if self.only_interpolate:
            fila_smooth = fila_interp
        
        if not self.keep_filament_id:
            fila_smooth = self.split_filament(fila_smooth)
        return fila_smooth

    def split_filament(self, m_filament):
        """ Split filament by reasigning filament id
        """
        result = Metadata.new_like(m_filament)
        fid = m_filament[0].fila_id
        current = []
        for ptcl in m_filament:
            tmp_ptcl = ptcl.copy()
            if not tmp_ptcl.discontinuity:
                ptcl.fila_id = fid
                current.append(ptcl)
            if len(current) > 0 and (tmp_ptcl.discontinuity
                                     or ptcl is m_filament[-1]):
                if len(current) >= self.min_segs:
                    fid += 100
                    result += current
                current = []
        return result

    def __getstate__(self):
        """ For pickle. Used for mpi communication
        """
        state = super(SmoothCoords, self).__getstate__()
        if 'progress_bar' in state:
            del state['progress_bar']
        return state

class SmoothError(RuntimeError):
    """ Smooth Error
    """
    pass

def diagnosis_m(m_in, *args, **kwargs):
    """ Diagnosis on smoothed coords
    calculate net_twist
    """
    n_ptcls = len(m_in)
    mg_name = m_in[0].mg_name
    fila_id = m_in[0].fila_id
    micrograph_pixel_size = kwargs.pop("micrograph_pixel_size")
    coords = get_values(m_in, micrograph_pixel_size)
    result = diagnosis(coords, *args, **kwargs)
    discontinuities = result['discontinuities']
    for idx in range(n_ptcls):
        m_in[idx].discontinuity = discontinuities[idx]
    helical_twist = result['helical_twist']
    helical_rise = result['helical_rise_pix']
    if helical_rise > 0:
        logger.info("%s:%s  Helical Rise: %.2f, Helical_twist %.2f" % (
            mg_name, fila_id, 
            helical_rise * micrograph_pixel_size, 
            helical_twist))
def diagnosis(coords,
               direction_error_tolerance=0,
               phi_error_tolerance=0,
               theta_error_tolerance=0,
               psi_error_tolerance=0,
               twist_error_tolerance=0,
               dist_error_tolerance=0):
    """Analysis the result.
    
    Args:
        coords: A `np.ndarray`. Nx6 inputs, x,y,z,phi,theta,psi
        direction_error_tolerance: A `float`. How far from the expected direction to consider as outliers.
                In degree.
        phi_error_tolerance: A `float`. How far from the expected phi to consider as outliers
        
    """
    from cryofilia.helix.smoothing.discontinuity import (
        coord_discontinuity, twist_discontinuity, dist_discontinuity)
     
    dist_disc = dist_discontinuity(coords[:, :3], threshold=dist_error_tolerance)
    coord_disc = coord_discontinuity(coords[:, :3], threshold=direction_error_tolerance)
    twist_disc = twist_discontinuity(coords[:, 3], threshold=twist_error_tolerance)
    disc = (coord_disc.astype(np.int32) * DISCONT_COORD 
            + twist_disc.astype(np.int32) * DISCONT_TWIST 
            + dist_disc.astype(np.int32) * DISCONT_DIST)
    
    helical_rise_pix = -1
    helical_twist = 0.
    if coords.shape[0] > 3:
        diff = coords[1:, :] - coords[:-1, :]
        dists = np.linalg.norm(diff[:, :3], axis=1)
        med_dist = np.median(dists)
        std = np.std(dists)
        helical_rise_pix = np.mean(dists[np.abs(dists - med_dist) < 3 * std])
        
        twists = diff[:, 3]
        twists = fix_rollover(twists, 0)
        twists = np.mod(twists + 180, 360) - 180
        med_twist = np.median(twists)
        std = np.std(twists)
        try:
            helical_twist = np.mean(twists[np.abs(twists - med_twist) < 3 * std])
        except:
            pass
    return {'discontinuities' : disc,
            'helical_rise_pix' : helical_rise_pix,
            'helical_twist' : helical_twist
        }
    #return twist_disc

def index_equispaced(coords, unit_vec, spacing, trimmed=None):
    """ Get index with equal spaced,
    The zero index is the center of remaining points after trimmed lsq fit

    Args:
        coords: A `ndarray`. Input coords. Should be vector, not matrix
        unit_vec: A `ndarray`. The direction vector.
        spacing: A `float`. The expected distance between unit point
    Returns:
        A `ndarray`. The indices for each point. np.float type.
        A `float`. The adjusted distance using the indexing
    """
    if len(coords.shape) == 1: # vector change to matrix
        coords = coords.reshape([coords.size, 1])
    all_n = coords.shape[0]
    unit_vec = np.array(unit_vec, dtype=np.float)
    if all_n == 1:
        return np.zeros(1), spacing
    ind = np.arange(all_n)
    if trimmed is not None:
        ind = ind[~trimmed]
    coords_sel = coords[ind, :]
    n_sel = ind.size
    # t_inds = np.round(np.abs(np.arange(n_sel) - (n_sel // 2)))
    # ind_center = np.argmin(t_inds)
    ind_center = n_sel // 2
    # distance = np.matmul(coords_sel - coords_sel[0, :], unit_vec)
    # spacing_adjust = distance[-1] / round(distance[-1] / spacing)
    spacing_adjust = spacing
    point_indices = np.matmul(coords - coords_sel[ind_center, :], unit_vec)
    point_indices = np.around(point_indices / spacing_adjust, 0).flatten()
    return point_indices, spacing_adjust

def get_polarity_m(m_in, micrograph_pixel_size):
    """ Fix polarity of filament\
    
    Args:
        m_in: A `Metadata`. The input Metadata containing data. The get_polarity for Metadata
        micrograph_pixel_size: A `float`. The micrograph pixel size for the data. Used to calculate the coordinates
    Returns:
        A `ndarray`. The polarity of each point. 1 is forward, -1 is backward.
    """
    coords = get_values(m_in, micrograph_pixel_size=micrograph_pixel_size)
    return get_polarity(coords)

def get_polarity_m_1(m_in, micrograph_pixel_size):
    """ Get polarity assignment using original coordinates.
    This is used to assess the polarity before smooth.
    Because the aligned particles might have same center
    """
    coords = get_values(m_in, micrograph_pixel_size)
    coords[:, 0] = m_in.get_values("CoordinateX")
    coords[:, 1] = m_in.get_values("CoordinateY")
    if m_in[0].has_key("CoordianteZ"):
        coords[:, 2] = m_in.get_values("CoordinateZ")
    return get_polarity(coords)

def get_polarity(coords):
    """ Get polarity according to the euler angle and the coordinates.
    Basically compare the vector from last point to this point with the euler angle transformed unit vector [0, 0, 1]
    This suppose the original direction is [0, 0, 1]

    Args:
        coords: A `ndarray`. coords' shape should be Nx6, where first 3 columns are coordinates
            last 3 columns are euler angles
    
    Returns:
        A `ndarray`. polarity for each point. pos = 1, neg = -1
    """
    from cryofilia.euler import Euler
    coord_diff = np.zeros([coords.shape[0], 3])
    coord_diff[1:, :] = coords[1:, :3] - coords[:-1, :3]
    coord_diff[0, :] = coord_diff[1,:]
    dists = np.sqrt(np.sum(coord_diff * coord_diff, axis=1))
    dists[dists==0] = 1 # to avoid two points are at same position.
    coord_diff = (coord_diff.T / dists).T
    coord_from_angle = np.zeros(coord_diff.shape)
    unit_v = np.array([0,0,1])
    for idx in range(coord_diff.shape[0]):
        coord_from_angle[idx, :] = Euler(coords[idx, 3], coords[idx, 4], coords[idx, 5]).transform(unit_v)
    polarity = coord_diff * coord_from_angle
    polarity = np.sum(polarity, axis=1)
    polarity = np.sign(polarity)
    return polarity

def interpolate_coords_m(m_in, smooth_method=SMOOTH_NONE, winsize=7,
                         micrograph_pixel_size=None,
                         pixel_size=None, scale=None,
                         fit_order=2,
                         expect_dphi=0.):
    """ Interpolate coords using metadata input
    """

    coords = get_values(m_in, micrograph_pixel_size=micrograph_pixel_size)
    coord_indices = np.array([ptcl.index for ptcl in m_in])
    fitnesses = np.array([ptcl.fitness for ptcl in m_in])
    coords_interpolated, interpolated, original_indices = interpolate_coords(
        coord_indices, coords, fitnesses, smooth_method=smooth_method,
        winsize=winsize, fit_order=fit_order, expect_dphi=expect_dphi)
    m_result = m_in.new_like(m_in)
    ori_idx = 0
    new_index = 1
    for index in range(len(interpolated)):
        if not interpolated[index]:
            ori_idx = int(original_indices[index])
        ptcl = m_in[ori_idx].copy()

        ptcl.x = coords_interpolated[index][0]
        ptcl.y = coords_interpolated[index][1]
        ptcl.z = coords_interpolated[index][2]
        ptcl.shx = 0
        ptcl.shy = 0
        ptcl.shz = 0
        ptcl.phi = coords_interpolated[index][3]
        ptcl.theta = coords_interpolated[index][4]
        ptcl.psi = coords_interpolated[index][5]
        ptcl.interpolated = interpolated[index]
        ptcl.index = new_index
        new_index += 1
        m_result.append(ptcl)
    m_in.clear()
    m_in += m_result
    return m_in

def interpolate_coords(coord_indices, coords, fitnesses,
                       smooth_method=SMOOTH_XYZ,
                       winsize=7, fit_order=2,
                       expect_dphi=0.,
                       expect_dtheta=0.,
                       expect_dpsi=0.):
    """ Interploate coords using indexed points.
    This will add point if the two indices are not adjacent,
    or remove point is the adjacent points have same index
    Args:
        coord_indices: A `ndarray`. The index for each point. Got from index_coords
        coords: A `ndarray`. The points. Could be xyz or angles
        fitnesses: A `ndarray`. The distances from the point to expected location
        smooth_all: A `str`. Smooth parameters. 
                SMOOTH_NONE: Only do interpolation
                SMOOTH_ANGLE: Only smooth angles
                SMOOTH_XYZ: Only smooth coordinates
        winsize: A `int`. The window size to fit the curve, calculate the trimmed points.
        fit_order: A `int`. The fit order to fit the curve. Default 2, support 1, 2
        expect_dphi: A `float`. The expected dphi, e.g twist.
        expected_dtheta: A `float`. The expected theta change between points
        expected_dpsi: A `float`. The expected psi change between points.
    Returns:
        (coords_interpolated, interpolated, original_indices)
            -1 in original_indices is interplated
        coords_interpolated: Interpolated or smoothed points
        interpolated: If the point is interpolated for each point. bool.
    """
    good_indices = coord_indices >= 0
    coords = coords[good_indices]
    fitnesses = fitnesses[good_indices]
    coord_indices = coord_indices[good_indices]
    ndim = coords.shape[1]
    coord_indices = coord_indices.astype(np.int32)
    ind_sorted = np.lexsort((fitnesses, coord_indices))
    indices, index_of_indices = np.unique(coord_indices[ind_sorted], return_index=True)
    coord_indices_sort = coord_indices[ind_sorted][index_of_indices]
    coords_sort = coords[ind_sorted][index_of_indices]
    original_index_sort = ind_sorted[index_of_indices]
    # The number in interpolated results
    n_result = int(coord_indices_sort[-1] + 1)
    coords_interpolated = np.zeros([n_result, ndim])
    interpolated = np.zeros(n_result, dtype=np.bool)
    original_indices = np.zeros(n_result, dtype=np.int32) - 1
    good_indices = {}
    for idx, index in enumerate(indices):
        good_indices[index] = idx
    n_good_indices = len(good_indices)
    idx_in_ori = 0
    for idx_in_result  in range(n_result):
        if idx_in_result in good_indices:
            idx_in_ori = good_indices[idx_in_result]

        if idx_in_result not in good_indices:
            interpolated[idx_in_result] = True
        else:
            original_indices[idx_in_result] = original_index_sort[idx_in_ori]
        if ((not interpolated[idx_in_result] )
            and smooth_method != SMOOTH_ALL):
            coords_interpolated[idx_in_result] = coords_sort[idx_in_ori]
        else:
            # need interploate
            # idx_in_ori is the first point after idx_in_result
            # we need point before idx_in_ori and after idx_in_ori
            # use data in coords_sorted, where unique indices has length winsize
            min_i, max_i = get_window(idx_in_ori, winsize, n_good_indices)
            if max_i - min_i + 1 < fit_order+1:
                print("Warning: Too less point for interpolate, skipping")
            else:
                idx_tmp = coord_indices_sort[min_i:max_i+1]
                coord_tmp = coords_sort[min_i:max_i+1]
                # interplate
                # do lsq fit on coords
                if max_i - min_i + 1 >= fit_order + 1:
                    coord_params = trimmed_lsq_fit_arr(
                        idx_tmp, coord_tmp[:, :3], fit_order=fit_order, lsq=True)[0]
                poly = np.poly1d(coord_params[0,:])
                coords_interpolated[idx_in_result, 0] = poly(idx_in_result)
                poly = np.poly1d(coord_params[1,:])
                coords_interpolated[idx_in_result, 1] = poly(idx_in_result)
                poly = np.poly1d(coord_params[2,:])
                coords_interpolated[idx_in_result, 2] = poly(idx_in_result)
                # do lsq fit to get all angles
                for dim in xrange(3, ndim):
                    if dim == 3:
                        delta = expect_dphi
                    elif dim == 4:
                        delta = expect_dtheta
                    elif dim == 5:
                        delta = expect_dpsi
                        delta = 0.
                    angle_param, trimmed, best_error = trimmed_lsq_fit_arr(
                        idx_tmp,
                        fix_rollover(coord_tmp[:, dim], delta, indices=idx_tmp),
                        fit_order, lsq=True)
                    poly = np.poly1d(angle_param)
                    coords_interpolated[idx_in_result, dim] = poly(idx_in_result)
            idx_in_result += 1
    return coords_interpolated, interpolated, original_indices

def interp_discontinuity_m(fila_smooth, micrograph_pixel_size, expect_dphi=0.):
    coords = get_values(fila_smooth, micrograph_pixel_size=micrograph_pixel_size)
    discontinuity = np.array(fila_smooth.get_values("discontinuity"))
    
    discontinuity[-1] = False # TO keep the filament the same length as original
    try:
        
        coords_interpolated, interpolated, original_indices = interp_discontinuity(
            coords, discontinuity, expect_dphi)
        for idx in xrange(len(fila_smooth)):
            if idx >= interpolated.size:
                break
            if interpolated[idx]:
                ptcl = fila_smooth[idx]
                coord = coords_interpolated[idx, :]
                ptcl.x = coord[0]
                ptcl.y = coord[1]
                ptcl.z = coord[2]
                ptcl.shx = 0
                ptcl.shy = 0.
                ptcl.shz = 0.
                ptcl.phi = coord[3]
                ptcl.theta = coord[4]
                ptcl.psi = coord[5]
    except Exception as e:
        import traceback as tb
        tb.print_exc()
        logger.warn("Could not inerpolate discontinuity")


def dialate_discontinuity(discontinuity, n_dialate):
    """ Dialate the discontinuity to make the continuious discontinuity 
    
    Args:
        discontinuity: A `ndarray`. Input disctinuity
        n_dialate: A `int`. The number to dialate
    
    Returns:
        A `ndarray`. The dialated discontinuity
    """
    preserve = True
    disc = discontinuity.copy()
    for i in range(n_dialate):
        tmp = disc.copy()
        for j in range(disc.size):
            if disc[j] > 0.5:
                if j > 1 and tmp[j-1] == 0:
                    tmp[j-1] = disc[j]
                if j < disc.size - 1 and tmp[j+1] == 0:
                    tmp[j+1] = disc[j]
        disc = tmp
    if not preserve:
        flag = False
        for i in range(disc.size):
            if not flag:
                if disc[i] > 0.5 and discontinuity[i] > 0.5:
                    flag = True
                else:
                    disc[i] = 0
            elif disc[i] == 0 and discontinuity[i] == 0:
                flag = False
        flag = False
        disc = disc[::-1]
        discontinuity = discontinuity[::-1]
        for i in range(disc.size):
            if not flag:
                if disc[i] > 0.5 and discontinuity[i] > 0.5:
                    flag = True
                else:
                    disc[i] = 0
            elif disc[i] == 0 and discontinuity[i] == 0:
                flag = False
                
        disc = disc[::-1]
    return disc
    
def interp_discontinuity(coords, discontinuity, expect_dphi=0.):
    """ Interpolate discontinuity points
    """
    N_DIALATE = 2
    indices = np.arange(coords.shape[0])
    disc = dialate_discontinuity(discontinuity, N_DIALATE)
    indices = indices[disc < 0.5]
    coords_good = coords[indices]
    # dialate the discontinuities
    coords_interpolated, interpolated, original_indices = interpolate_coords(
        indices, coords_good, np.zeros(coords_good.shape[0]), expect_dphi=expect_dphi)
    return coords_interpolated, interpolated, original_indices

def index_coords_m(m_in, spacing, winsize=7, micrograph_pixel_size=1.,
                   pixel_size=None, scale=None, polarity=None,
                   use_half_repeat=False):
    """ Index coords using metadata input
    """
    coords = get_values(m_in, micrograph_pixel_size)[:, :3]
    need_redo_indexing = False
    if use_half_repeat:
        # index for half spacing first
        coords_indices, fitness, coord_estimates = index_coords(
                coords, spacing/2, winsize, polarity=polarity)
        # check which spacing is better
        coords_indices_parity = coords_indices % 2
        if sum(coords_indices_parity) < coords_indices.size / 2:
            parity = 0
        else:
            parity = 1
        # let move every even coordinates half towards to the odd indices
        for i in range(coords_indices.size):
            if coords_indices_parity[i] % 2 != parity:
                need_redo_indexing = True
                # get direction to move
                amp = 0 
                i1, j1 = i, i - 1
                while abs(amp) < spacing * 3:
                    if j1 < 0:
                        i1 += 1
                        if i1 >= coords_indices.size:
                            print("Warning: Could not get direction")
                            amp = 0
                            break
                        j1 = 0
                    direction = coords[i1,:3] - coords[j1, :3]
                    amp = np.linalg.norm(direction)
                    j1 -= 1
                if amp > 0:
                    direction = direction / amp
                    # move towards direction half spacing
                    coords[i, :3] += direction * (spacing / 2)
        # redo the indicing if any adjustment in coords
    if need_redo_indexing or not use_half_repeat:
        coords_indices, fitness, coord_estimates = index_coords(
            coords, spacing, winsize,
            polarity=polarity)
    else:
        coords_indices/= 2
    # put result into m_in
    for index in range(len(m_in)):
        ptcl = m_in[index]
        ptcl.index = coords_indices[index]
        ptcl.fitness = fitness[index]
        ptcl.x = coord_estimates[index, 0]
        ptcl.y = coord_estimates[index, 1]
        ptcl.shx = 0
        ptcl.shy = 0
    return m_in

def index_coords(coords, spacing, winsize=7, polarity=None):
    """ Get the index of coords with equal space.
    Indexes only use points that is correct polarity, wrong polarity will be assigned as -1 
    :param coords: The input coords. All points should in same R sapce.
    :type coords: ndarray
    :param spacing: The single spacing between points.
    :type spacing: float
    :param winsize: The window size for averaging. Default is 7
    :type winsize: int.
    :return: coord_indices, fitnesses, coord_estimates.
        The index is set to -1 if the polarity is wrong
    :rtype: ndarray, ndarray, ndarray
    """
    shape = coords.shape
    return_vec = False
    if len(shape) == 1:
        return_vec = True
        coords = np.reshape(coords, [shape[0], 1])
    ori_coords = coords
    if polarity is None:
        polarity = np.ones(ori_coords.shape[0])
    coords = ori_coords[polarity>0]
    n_coords, ndim = coords.shape
    coord_indices = np.zeros(n_coords)
    coord_estimates = np.zeros([n_coords, ndim])
    fitnesses = np.zeros(n_coords)
    coord_trimmed = np.zeros(n_coords)
    last_trimmed = np.array([False] * n_coords)
    first_ind = 0
    last_ind = n_coords - 1
    if n_coords < winsize:
        winsize = n_coords
    last_max_i, last_coord_index = None, []
    for ind in range(first_ind, last_ind + 1):
        min_i, max_i = get_window(ind, winsize, n_coords)
        ind_local = ind - min_i
        unit_vec, origin, trimmed = trimmed_nd_lsq_fit(coords[min_i:max_i+1, :3])[:3]
        coord_index, spacing_adjust = index_equispaced(
            coords[min_i:max_i+1, :3] - origin, unit_vec, spacing, trimmed)
        coord_trimmed[ind] = trimmed[ind_local]
        ind2 = np.arange(winsize)[~trimmed]
        index_centroid = sum(coord_index[ind2].astype(np.float)) / ind2.size
        adjusted_origin = origin - index_centroid * spacing_adjust * unit_vec
        offset = round(coord_index[ind_local] - 0.00001) * spacing_adjust * unit_vec
        coord_estimates[ind, :3] = adjusted_origin + offset
        fitnesses[ind] = np.linalg.norm(coords[ind, :] - coord_estimates[ind, :])
        # print(ind, ind_local, unit_vec, origin, adjusted_origin, ind2, index_centroid, coord_index[ind2])
        # exit()
        if ind == first_ind:
            coord_index_offset = 0
        else:
            current_coord_index = np.zeros(n_coords)
            current_coord_index[min_i:max_i+1] = coord_index
            last_trimmed[min_i:max_i+1] |= trimmed
            match_min_i = min_i
            match_max_i = last_max_i
            last_coord_index = last_coord_index[match_min_i:match_max_i+1]
            last_coord_index = last_coord_index[~last_trimmed[match_min_i:match_max_i+1]]
            current_coord_index = current_coord_index[match_min_i:match_max_i+1]
            current_coord_index = current_coord_index[~last_trimmed[match_min_i:match_max_i+1]]
            cnt = Counter(np.round(last_coord_index - current_coord_index, 0))
            offset = cnt.most_common(1)[0][0]
            coord_index_offset += offset
        coord_indices[ind] = coord_index[ind_local] + coord_index_offset
        last_coord_index = np.zeros(n_coords)
        last_coord_index[min_i:max_i+1] = coord_index
        last_trimmed = np.array([False] * n_coords)
        last_trimmed[min_i:max_i+1] = trimmed
        last_max_i = max_i
    coord_indices -= coord_indices[0]
    coord_indices_good = coord_indices
    coord_indices = -np.ones(ori_coords.shape[0])
    coord_indices[polarity>0] = coord_indices_good
    fitnesses_good = fitnesses
    fitnesses = np.ones(ori_coords.shape[0]) * 100000000
    fitnesses[polarity>0] = fitnesses_good
    coord_estimates_good = coord_estimates
    coord_estimates = ori_coords.copy()
    coord_estimates[polarity>0, :] = coord_estimates_good
    coord_indices = np.rint(coord_indices + 0.00000001)
    if return_vec:
        coord_estimates = coord_estimates.flatten()
    return coord_indices, fitnesses, coord_estimates

def fixup_seam_pos_star(m_star, rise_per_subunit, twist_per_subunit,
                        twist_per_repeat, micrograph_pixel_size=None,
                        pixel_size=None):
    """ Fixup the seam position in the metadata
        :param m_star: The metadata input
        :param twist_per_subunit: The twist angle between subunit. CC positive
        :param rise_per_repeat: The distance along helical axis between repeats
        :param twist_per_repeat: The twist angle between repeat. CC positive
        :param micrograph_pixel_size: The micrograph pixel size in Angstrom
                    to calculate the rise in pixel
                    and to calculate the coordinates
        :type m_star: Metadata
        :type rise_per_subunit: float
        :type twist_per_subunit: float
        :type twist_per_repeat: float
        :type micrograph_pixel_size: float
        :type pixel_size: float
        :return: The fixed metadata
        :rtype: Metadata
    """
    tubs = m_star.group_by("MicrographName", "HelicalTubeID")
    new_tubs = m_star.new_like(m_star)
    for mgname, tubid in tubs.iterkeys():
        tub = tubs[(mgname, tubid)]
        tub = fixup_seam_pos_filament_star(tub, rise_per_subunit,
                                           twist_per_subunit, twist_per_repeat,
                                           micrograph_pixel_size, pixel_size)
        new_tubs += tub
    return new_tubs

def fixup_seam_pos_filament_star(fila_star, rise_per_subunit, twist_per_subunit,
                        twist_per_repeat, micrograph_pixel_size=None,
                        pixel_size=None):
    """ Fixup the seam position in the filament with metadata
        The origin(shift) in relion is reported as pixels.
        :param fila_star: The metadata input for one filament
        :param twist_per_subunit: The twist angle between subunit. CC positive
        :param rise_per_repeat: The distance along helical axis between repeats
        :param twist_per_repeat: The twist angle between repeat. CC positive
        :param micrograph_pixel_size: The micrograph pixel size in Angstrom
                    to calculate the rise in pixel
                    and to calculate the coordinates
        :type fila_star: Metadata
        :type rise_per_subunit: float
        :type twist_per_subunit: float
        :type twist_per_repeat: float
        :type micrograph_pixel_size: float
        :type pixel_size: float
        :return: The fixed metadata
        :rtype: Metadata
    """
    if len(fila_star) == 0:
        logger.warn("No need to fixup for only one ptcl")
        return fila_star.copy()
    if micrograph_pixel_size is None or micrograph_pixel_size <= 0:
        raise Exception("micrograph_pixel_size must be provided")
    if pixel_size is None or pixel_size <= 0:
        if ((not fila_star.has_label("Magnification"))
            or (not fila_star.has_label("DetectorPixelSize"))):
            raise RuntimeError("input should contain both Magnificaiton "
                               + "and DetectorPixelSzie")
        pixel_size = 10000. * fila_star[0]['DetectorPixelSize'] / fila_star[0]['Magnification']
    scale = pixel_size / micrograph_pixel_size
    xs0, ys0 = fila_star.get_values(["CoordinateX", "CoordinateY"])
    phis, thetas, psis = fila_star.get_values(["AngleRot", "AngleTilt",
                                             "AnglePsi"])
    coords = get_values(fila_star, micrograph_pixel_size)
    center_xs = coords[:, 0].tolist()
    center_ys = coords[:, 1].tolist()
    # fix psis
    psis_model = fix_rollover(psis, 0, 180)
    #psis_model = smooth_angle(psis, 4, 1, 20)[0]
    for index in xrange(len(psis_model)):
        if abs(psis[index] - psis_model[index]) > 10:
            if abs((psis[index] - psis_model[index] + 540) % 360 - 180) > 90:
                # reverse the ptcl
                thetas[index] = 180 - thetas[index]
                phis[index] = (phis[index] + 180) % 360
            psis[index] = psis_model[index]
    xs1, ys1, phis1, thetas1, psis1 = fixup_seam_pos(
        center_xs, center_ys,
        phis, thetas, psis,
        rise_per_subunit/micrograph_pixel_size,
        twist_per_subunit, twist_per_repeat)
    # reconstruct the shx, shy and phi
    m_result = fila_star.new_like(fila_star)
    for index in xrange(len(xs0)):
        ptcl = fila_star[index].copy()
        ptcl['OriginX'] = (xs0[index] - xs1[index]) / scale
        ptcl['OriginY'] = (ys0[index] - ys1[index]) / scale
        ptcl['AngleRot'] = phis1[index]
        ptcl['AnglePsi'] = psis1[index]
        ptcl['AngleTilt'] = thetas1[index]
        m_result.append(ptcl)
    return m_result

def separate_filament(fila, threshold, filament_offset=100):
    """Separate filament by distance between adjacent particles.
    
    Args:
        fila: A `Metadata`. Should have HelicalTubeID label
    Returns:
        list of `Metadata`. The filament id is not renumbered.
    """
    
    values = get_values(fila, 1.0)
    if values.size <=3:
        return fila.copy()
    dists = np.zeros(values.shape[0])
    dists[1:] = np.linalg.norm(values[1:, :3] - values[:-1, :3], axis=1)
    result = fila.new_like(fila)
    results = []
    for idx in range(dists.size):
        if dists[idx] > threshold:
            results.append(result)
            result = fila.new_like(fila)
        result.append(fila[idx])
    results.append(result)
    return results

def fixup_seam_pos(coord_xs, coord_ys, phis, thetas, psis,
                       rise_per_subunit, twist_per_subunit,
                       twist_per_repeat):
    """ Bring the phi angle to same protofilimant
        Do not change theta and psi.
        :param coord_xs: The input x coordinates
        :param coord_ys: The input y coordinates
        :param phis: The input phi angles. Used to calculate the pf
        :param thetas: The input theta angles. Used to calculate the shift
        :param psis: The input psi angles. Used to calculate the shift
        :param twist_per_subunit: The twist angle between subunit. CC positive
        :param rise_per_repeat: The distance along helical axis between repeats
        :param twist_per_repeat: The twist angle between repeat. CC positive
        :type coord_xs: list[float]
        :type coord_ys: list[float]
        :type phis: list[float]
        :type thetas: list[float]
        :type psis: list[float]
        :type rise_per_subunit: float
        :type twist_per_subunit: float
        :type twist_per_repeat: float
        :return: The fixed xs, ys, phis
    """

    if len(set([len(coord_xs), len(coord_ys), len(phis)])) != 1:
        raise RuntimeError("The input data are not same length")
    if len(coord_xs) == 1:
        return coord_xs[:], coord_ys[:], phis[:], thetas[:], psis[:]
    result_xs1 = coord_xs[:]
    result_ys1 = coord_ys[:]
    phis = phis[:]
    # bring phi to same protofilament
    new_phis = fix_rollover(phis, twist_per_repeat, twist_per_subunit)
    # shift x, y
    for index in xrange(len(result_xs1)):
        phi_diff = new_phis[index] - phis[index]
        psi = psis[index] * np.pi / 180.
        # calculate how much we need to shift in z
        # to match original volume.
        rise_diff = phi_diff / twist_per_subunit * rise_per_subunit
        # calculate how much we need to shift x, y after projection
        # to match original projection
        dxy = rise_diff * np.sin(-thetas[index] * np.pi / 180.)
        result_xs1[index] += dxy * np.cos(-psi)
        result_ys1[index] += dxy * np.sin(-psi)
    return result_xs1, result_ys1, new_phis, thetas, psis

