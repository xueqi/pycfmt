'''
Created on Jun 19, 2017

@author: xueqi
'''
import matplotlib.pyplot as plt
class MetadataPlot(object):
    '''
    Plot a metadata
    '''
    def __init__(self, params):
        '''
        Constructor
        '''
        self.params = params
        self.output = None
    
    def plot(self, m, *keys):
        if len(keys) == 0: return
        values = []
        for key in keys:
            values.append(m.getValue(key))
        x = range(len(values[0]))
        for lbl, y in keys.items():
            plt.plot(x, y, label = lbl)
        
        plt.legend()
    
    def plotXY(self, m, xlbl, ylbl):
        x = m.getValues(xlbl)
        y = m.getValues(ylbl)
        plt.plot(x, y)
        plt.xlabel(xlbl)
        plt.ylabel(ylbl)
        
        if self.output:
            pass