from relion import Metadata, Container

def strSeamResult(sr):
    return "%6.2f %6.2f %6.2f %6.2f %6.2f %6.2f %2d %5s" % (
            sr.phi, sr.proto_phi, sr.proto_theta, sr.proto_psi, sr.seam_phi, 
            sr.seam_psi, sr.seam_index, sr.selected)

class SeamResultContainer(Container):
    pass

class SeamResult(Metadata):
    ContainerClass = SeamResultContainer
    _labels = {
            "phi"       : ['originPhi', float, "original phi before select seam"],
            "proto_phi" : ['protoPhi', float, 'proto phi'],
            "proto_theta" : ["protoTheta", float, "proto theta"],
            "proto_psi" : ["protoPsi", float, "proto psi"],
            "seam_index" : ["seamIndex", int, " seam index"],
            "seam_phi" : ["seamPhi", int, " seam Phi"],
            "seam_psi" : ["seamPsi", int, " seam Psi"],
            "selected" : ["selected", bool, " is selected"],
            }
SeamResult()
SeamResult.ContainerClass.__str__ = strSeamResult
class FindSeam(object):
    def __init__(self, num_starts, num_pfs, helical_repeat_distance, seam_psi=None,
            pixel_size=1, min_num_seam_boxes=1, min_frac_seam_boxes=0, 
            phi_tolerance=360, num_repeat_per_box=1):
        self.seam_theta = 0
        self.seam_psi = 0
        self._twist = None

        self.radius13pf = 112
        self.num_pfs = num_pfs
        self.num_starts = num_starts
        if self.num_pfs > 1 and self.num_starts > 1:
            self.num_starts /= 2.
        self.d_repeat = helical_repeat_distance
        self.true_num_starts = num_starts / 2.
        self.phi_tolerance = phi_tolerance
        self.seam_psi = seam_psi
        self.num_repeat_per_box=num_repeat_per_box
    def guess_seam(self, boxes):
        ''' guess seam with boxes. box should have phi, theta, psi
        '''
        sResult = SeamResult()
        for i in range(len(boxes)):
            obj = sResult.new_object()
            sResult.append(obj)

        if self.seam_psi is None:
            b0, b180 = 0,0
            for box in boxes:
                if abs(box.psi) < 90: b0+=1
                elif abs(box.psi -180) <90: b180+=1
            if b0 >= b180: self.seam_psi = 0
            else: self.seam_psi = 180


        self.guess_proto_phi(boxes, sResult, self.num_repeat_per_box)
        self.fix_proto_phi(boxes, sResult)
        best_seam_index = self.guess_seam_1(boxes, sResult)

        self.fix_proto_phi2(boxes, sResult, best_seam_index)
        self.guess_seam_1(boxes, sResult)
        return sResult

    def guess_proto_phi(self, boxes, sResult,  num_repeat_per_box=1):
        '''
        '''
        found_good_psi = False
        twist_change = 0
        for i, box in enumerate(boxes):
            sr = sResult[i]
            psi, theta, phi = box.psi, box.theta, box.phi
            sr.phi = phi
            if psi != self.seam_psi:
                phi = -phi
            phi %= 360
            sr.proto_theta = theta
            sr.seam_psi = self.seam_psi
            twist_change += self.twist_per_box
#            twist_change += self.twist_per_box
            if psi != self.seam_psi:
                sr.proto_phi = -1
            elif not found_good_psi:
                sr.proto_phi = phi
                found_good_psi = True
                twist_change = 0
                last_proto_phi = phi
            else:
                best_dphi = 361.
                for j in range(self.num_pfs):
                    dphi = phi + j * 360. / self.num_pfs - (last_proto_phi + twist_change)
                    if dphi > 180: dphi -= 360.
                    if dphi < -180: dphi += 360.
                    if abs(dphi-self.twist_per_box) < best_dphi:
                        best_dphi = abs(dphi-self.twist_per_box)
                        sr.proto_phi = phi + j * 360. / self.num_pfs
                        sr.proto_phi %= 360.
                        if sr.proto_phi < 0: sr.proto_phi += 360.
                twist_change = 0
                last_proto_phi = sr.proto_phi
        return sResult
    def fix_proto_phi(self, boxes, sResult):
        ''' Fix proto phi where psi is not the same as seam_psi,
            eg. where proto_phi == -1
        '''
        num_boxes = len(boxes)
        for i, box in enumerate(boxes):
            sr = sResult[i]
            psi = box.psi
            if psi != self.seam_psi:
                max_delta = num_boxes + 1
                for j in range(len(boxes)):
                    if boxes[j].psi == self.seam_psi and j != i:
                        delta = abs(i-j)
                        if delta < max_delta:
                            max_delta = delta
                            sr.proto_phi = sResult[j].proto_phi + (i-j)*self.twist_per_box
                            sr.proto_theta = boxes[j].theta
    
    def fix_proto_phi2(self, boxes, sResult, best_seam_index, first_good=0, last_good=None):
        num_boxes = len(boxes)
        if last_good is None: last_good = num_boxes - 1
        for i, box in enumerate(boxes):
            sr = sResult[i]
            psi, theta, phi, ccc = box.psi, box.theta, box.phi, box.ccc
            
            phi = int(phi+0.5) % 360
            if i > 0:
                delta1 = abs(sResult[i].seam_phi - sResult[i-1].seam_phi - self.twist_per_box)
            else:
                delta1 = 0
            if i < num_boxes - 1:
                delta2 = abs(sResult[i+1].seam_phi - sResult[i].seam_phi - self.twist_per_box)
            else:
                delta2 = 0
            if delta1 > 180: delta1 -= 360.
            if delta2 > 180: delta2 -= 360.
            if sr.seam_index == best_seam_index and delta1 <= self.phi_tolerance and delta2 <= self.phi_tolerance and psi == self.seam_psi and i >= first_good and i <= last_good:
                sr.selected = True
            else:
                sr.selected = False
        for i, sr in enumerate(sResult):
            if not sr.selected:
                max_delta = num_boxes + 1
                for j in range(num_boxes):
                    sr1 = sResult[j]
                    delta = abs(i-j)
                    if not sr1.selected: continue

                    if delta < max_delta:
                        max_delta = delta
                        sr.proto_phi = sr1.proto_phi + (i-j) * self.twist_per_box
                        sr.proto_theta = sr1.proto_theta


    def guess_seam_1(self, boxes, sResult):
        ''' Guess seam using fixes proto_phi
        '''
        seam_count = [0] * self.num_pfs
        for i, box in enumerate(boxes):
            sr = sResult[i]
            psi, phi = box.psi, box.phi
            best_dphi = 361.
            for j in range(self.num_pfs):
                dphi = sr.proto_phi + j * 360. / self.num_pfs - phi
                dphi = (dphi+180) % 360 - 180
                if abs(dphi) < best_dphi:
                    best_dphi = abs(dphi-self.twist_per_box)
                    best_j = j
            if psi == self.seam_psi:
                sr.seam_index = best_j
                seam_count[best_j] += 1
            else:
                sr.seam_index = -1

        num_seam_boxes = -1
        for j in range(self.num_pfs):
            if seam_count[j] > num_seam_boxes:
                num_seam_boxes = seam_count[j]
                best_seam_index = j

        for i in range(len(boxes)):
            sr = sResult[i]
            sr.seam_phi = sr.proto_phi + best_seam_index * 360. / self.num_pfs
            sr.seam_phi = int(sr.seam_phi+0.5) % 360
        return best_seam_index 
    @property
    def twist_per_box(self):
        if abs(self.seam_psi) < 1.:
            return self.twist * self.num_repeat_per_box
        else:
            return -self.twist * self.num_repeat_per_box

    @property
    def twist(self):
        '''
        '''
        if self._twist is None:
            self._twist = self.twist_per_repeat(self.num_pfs, self.num_starts, self.radius13pf, self.d_repeat)
        return self._twist
    @staticmethod
    def twist_per_repeat(num_pfs, helix_start_num, radius13pf, dimer_repeat_dist):
        ''' Calculate the expected twist per repeat
        '''
        import math
        total_pitch13 = math.atan2(3./2*dimer_repeat_dist, 2*math.pi*dimer_repeat_dist)
        w_pf = 2 * math.pi * radius13pf / (13 * math.cos(total_pitch13))
        delta_n = 3./2 * dimer_repeat_dist * num_pfs / 13 - helix_start_num * dimer_repeat_dist
        total_pitch = total_pitch13 - math.atan2(delta_n, num_pfs * w_pf * math.cos(total_pitch13))
        mt_radius = num_pfs * w_pf * math.cos(total_pitch13) / (2*math.pi)
        axial_repeat_dist = math.cos(total_pitch-total_pitch13)*dimer_repeat_dist
        twist_per_subunit = w_pf * math.cos(total_pitch) / mt_radius
        return_val = (twist_per_subunit * num_pfs - 2*math.pi) / helix_start_num
        return -180/math.pi*return_val
