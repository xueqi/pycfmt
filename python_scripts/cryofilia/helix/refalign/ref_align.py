""" Multi-reference alignments. Align particle to multiple references
"""
import logging
logger = logging.getLogger(__name__)

class MRA(object):
    def __init__(self, aligners):
        self.aligners = aligners
    
    def align_particle(self, particle, update_inplace=False, **kwargs):
        """ particle should have the properties that is needed for alignement
        """
        if not self._check_particle_valid(particle):
            # error message should print in _check_particle_valid
            return False
        
        for aligner in self.aligners:
            aligner.align(particle, **kwargs)
        
        
    
    def _check_particle_valid(self, particle):
        """ check if the particle is valid for alignment
        """
        if not hasattr(particle, "image"):
            logger.info("Particle has no image to align with")
            return False
        
        
    