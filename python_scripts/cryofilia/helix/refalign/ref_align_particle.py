'''
Created on Oct 25, 2017

@author: xueqi
'''
from relion import Container

class RefAlignParticle(Container):
    '''
    RefAlignParticle class
    '''
    _labels = {
        "ccc" : ["crossCorrelation", float],
        "theta" : ["AngleTilt"],
        "psi"   : ["AnglePsi"],
        "phi"   : ["AngleRot"],
        "shx"   : ["OriginX"],
        "shy"   : ["OriginY"],
        
        }
    def __init__(self, *args, **kwargs):
        '''
            Initialization
        '''
        self.ccc = 0
        self.theta = 0
        self.psi = 0
        self.phi = 0
        self.shx = 0
        self.shy = 0
        print args
        super(RefAlignParticle, self).__init__(*args, **kwargs)

    def align_with_aligner(self, ptcl, aln, update_self=True):
        ''' Align particle with aligner
        '''
        align_result = aln.align_grid(ptcl)
        if align_result.ccc > self.ccc and update_self:
            self.phi = align_result.phi
            self.theta = align_result.theta
            self.psi = align_result.psi
            self.shx = align_result.shx
            self.shy = align_result.shy
            self.ccc = align_result.ccc
            
    def copy(self):
        ''' Copy self to another instance
        '''
        from copy import copy
        return copy(self)
    
if __name__ == "__main__":
    a = RefAlignParticle()
    a.phi = 10
    b = a.copy()
    assert(a.phi == b.phi)