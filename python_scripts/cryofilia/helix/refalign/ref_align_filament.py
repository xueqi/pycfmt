'''
Created on Oct 25, 2017

@author: xueqi
'''
from relion import Metadata
from cryofilia.helix.refalign import RefAlignParticle
from cryofilia.align import GridAngle
class RefAlignFilament(Metadata):
    '''
    Filament class for Reference Alignment
    '''
    _labels = {
        "ccc" : ["crossCorrelation", float],
        "theta" : ["AngleTilt"],
        "psi"   : ["AnglePsi"],
        "phi"   : ["AngleRot"],
        "shx"   : ["OriginX"],
        "shy"   : ["OriginY"],
        }
    ContainerClass = RefAlignParticle
    def __init__(self, filament, aligner, *args, **kwargs):
        '''
        :param filament: The filament instance to align
        :param aligner: The aligner is used to align the filament
        '''
        super(RefAlignFilament, self).__init__(*args, **kwargs)
        self.images = [] # The particle list
        self.ptcl_extracted = False # check if the particle is extractd
        self.filament = filament
        self.aligner = aligner
        
    def coarse_align(self):
        ''' Coarse align the filament. 
        For each ptcl, align with aligner
        Angle grid:
            ptcl.phi, self.nphi_coarse, self.dphi_coarse
            ptcl.theta, self.ntheta_coarse, self.dtheta_coarse
            ptcl.psi, self.npsi_coarse, self.dpsi_coarse
        '''
        # make a grid
        psi_g = GridAngle(0, self.npsi_coarse, self.dpsi_coarse)
        theta_g = GridAngle(0, self.ntheta_coarse, self.dtheta_coarse)
        phi_g = GridAngle(0, self.nphi_coarse, self.dphi_coarse)
        
        for i, ptcl in enumerate(self.filament):
            psi_g.start, phi_g.start, theta_g.start = ptcl.psi, ptcl.phi, ptcl.theta
            ptcl.align_with_aligner(self.images[i], 
                                    self.aligner, phi_g, theta_g, psi_g)
        