''' Models for RefAlign
@author: xueqi
'''

class Filament(object):
    def __init__(self, rise, twist, num_pfs=1, num_starts=1):
        self.rise = rise
        self.twist = twist
        self.num_pfs = num_pfs
        self.num_starts = num_starts
        self.particles = []  # a list of particles

class Microtubule(Filament):
    def __init__(self, rise, twist, num_pfs=0, num_pfs=1.5):
        super(Microtubule, self).__init__(rise, twist, num_pfs)
    

class MicrotubuleException(RuntimeError):
    pass

class FilamentException(RuntimeError):
    pass