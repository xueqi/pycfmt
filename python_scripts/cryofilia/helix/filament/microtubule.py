'''Microtubule model

@author: xueqi
'''
import logging
import os
import numpy as np

import chuff as cf
from cryofilia.pdb_util import PDB
from cryofilia.euler import Euler
from cryofilia.coords import Coord
from cryofilia.models.filament import Filament

logger=logging.getLogger(__name__)

class _MicrotubuleVolume(Filament):
    """ Microtubule base model
    """
    def __init__(self, rise_per_subunit, twist_per_subunit,
                 num_pfs, num_starts=1.5):
        ''' Microtubule model. MT axis along z.
        '''
        super(_MicrotubuleVolume, self).__init__(rise_per_subunit, twist_per_subunit,
                                           num_pfs, num_starts)
        
        self.subunit_pdb = None
        self.pdb = None
        self.scale = num_pfs / 13.
        self._ref_com = None
        print(self.rise_per_subunit)
    @property
    def ref_com(self):
        """ reference pdb center of tubulin
        """    
        if self._ref_com is None:
            self._ref_com = self.subunit_pdb.extract_chains(["A", "B"]).cog
        return self._ref_com

    def set_subunit_PDB(self, pdbfile, ref_num_pfs=13):
        """ Set a subunit pdb.
        
        Args:
            pdbfile: A `str` or `PDB`. The subunit pdb used for synthetic mt
        """
        if isinstance(pdbfile, PDB):
            self.subunit_pdb = pdbfile
        elif isinstance(pdbfile, str):
            self.subunit_pdb = PDB()
            self.subunit_pdb.read_file(pdbfile)
        
        if ref_num_pfs != self.num_pf:
            scale = float(self.num_pf) / ref_num_pfs
            # get center of tubulin
            p = self.subunit_pdb.extract_chains("AB")
            center = p.cog
            # mov tubulin center to ref_com
            
            shft = [c_i * (scale - 1.) for c_i in center]
            self.subunit_pdb.transform(*shft)
        # reset _ref_com
        self._ref_com = None

    def to_volume(self, voxel_size, box_size):
        ''' Generate a synthetic volume from subunit pdbs
        
        Args:
            voxel_size: A `float`. The voxel size for the result volume.
            box_size: A `int`. A even integer. The size of the result volume
        
        Returns:
            A `EMImage`. The volume correspond to the MT
        '''
        if self.pdb is None:
            self.get_full_PDB(box_size=box_size * voxel_size)
        return self.pdb.to_volume(voxel_size, box_size)
    
    def get_full_PDB(self, box_size):
        ''' Get symmetrized pdb using current parameters
        
        Args:
            box_size: A `int`. The box size to generate all pdbs. in Angstrom
        '''
        if self.subunit_pdb is None:
            self.set_subunit_PDB(cf.DEFAULT_TUBULIN_PDB, cf.DEFAULT_MICROTUBULE_PF)
            logger.info("Using default pdb: %s" % cf.DEFAULT_TUBULIN_PDB)
            # raise Exception("no subunit pdb provided")

        self.expand_helical_parameters()
                
        ref_com = self.ref_com
        subunits, _ = self.get_helix_subunit_list(box_size, 1, ref_com)

        self.pdb = None
        logger.info("Symmetrizing %d pdbs" % len(subunits))
        for idx in subunits:
            z_shift, rot, _rn, _pn = self.get_helical_transform(idx)
            tpdb = self.subunit_pdb.copy()
            tpdb.rotate(rot, 0, 0)
            tpdb.transform(0, 0, z_shift)
            if self.pdb is None:
                self.pdb = tpdb
            else:
                self.pdb += tpdb

    def get_helical_transform(self, n):
        """ Get helical transform for the nth subunit
        
        Args:
            subunit_index: A `int`. The index of subunit.
        
        Returns:
            (z_shift, rot, repeat_num, pf_num)
            z_shift: A `float`. The shift along z axis
            rot: A `float`. The rotation around z axis
            repeat_num: A `int`. The index of the repeat the subunit belongs to.
            pf_num: A `int`. THe protofilimaent number the subunit belongs to.
        """
        from cryofilia.helix.basics import get_helical_transform
        return get_helical_transform(n,
                                     self.rise_per_subunit,
                                     self.twist_per_subunit,
                                     self.num_pf,
                                     self.num_start)

    def get_helix_subunit_list(self, box_size, voxel_size,
                                  ref_com=None, edge_width=0):
        """ Get a list of subunit that the box contains.
        
        The box size is an integer. The generated subunits are in a cubic box.
        
        Args:
            box_size: A `int`. The box size to query.
            voxel_size: A `float`. The voxel size of the box  to query.
        
        Returns:
            subunits: A `list` of `int`. The list of subunits to generate.
            full_subunit_height: A `float`. The total height of the microtubule
                which is larger than box size
        """
        from cryofilia.helix.basics import get_helix_subunit_list
        return get_helix_subunit_list([box_size, box_size, box_size],
                                      voxel_size,
                                      self.rise_per_subunit,
                                      self.twist_per_subunit,
                                      self.num_pf, self.num_start,
                                      ref_com, edge_width)
    def expand_helical_parameters(self):
        """ Expand helical parameters to repeat_dist, twist_per_repeat, super_twist
        """
        from cryofilia.helix.basics import expand_helical_parameters
        (self.axial_repeat_dist,
         self.twist_per_repeat, 
         self.super_twist, _,_)=expand_helical_parameters(self.rise_per_subunit,
                                       self.twist_per_subunit,
                                       self.num_pf,
                                       self.num_start)
        
    @staticmethod
    def new(num_pf, ref_pdb=None, ref_num_pf=None, ref_com=None):
        ''' Create new microtubule.
            mt13 = Microtubule.new(13)
            mt12 = Microtubule.new(12)
        
        Args:
            num_pf: A `int`. The number of protofilament to generate
            ref_pdb: A `str` or `PDB`. The reference for pdb. If None, use default
                Attention! If provided, the A, B chain should be tubulin.
        '''
        helical_params={
            12 : [83.305, -0.91], # from emd_5192, with hsearch
            13 : [83.305, 0.0], # from emd_6348, with hsearch
            14 : [83.305, 0.91], # from emd_6352, with hsearch, won't work? strange. Just set to something
            }
        if num_pf in helical_params:
            helical_rise, helical_twist=helical_params[num_pf]
        else:
            helical_rise, helical_twist=helical_params[13]
        rise_per_subunit = helical_rise / num_pf * 1.5 # number starts
        twist_per_subunit=(helical_twist + 360) / num_pf
        mt = _MicrotubuleVolume(rise_per_subunit, twist_per_subunit,
                 num_pf, num_starts=1.5)
        return mt
    
    @staticmethod
    def parametric(num_repeats, num_pfs, num_starts, ref_com,
            repeat_dist13pf, radius_scale_factor,
            monomer_offset, phi_params, theta_params, psi_params,
            elastic_params):
        ''' Input ref_com should be from a 13 pf pdb fitting
        '''
        mt = MT(ref_com = ref_com, num_pfs = num_pfs, num_starts = num_starts, phi_params = phi_params, theta_params = theta_params, psi_params = psi_params, elastic_params = elastic_params, dimer_repeat_dist = repeat_dist13pf)
        num_repeats = int(num_repeats+0.5)
        try:
            origin_repeat = num_repeats[1]
            num_repeats = num_repeats[0]
        except TypeError:
            origin_repeat = int(num_repeats/ 2)
        result = []        
        for i in range(0, num_repeats):
            rpt = MTRepeatZ(mt, i - origin_repeat)
            for j in range(num_pfs):
                tub = rpt.tubulin_at_index(j)
                tub.phi_first_pf = rpt.tubulin_at_index(0).mt_repeat.euler.phi_r
                tub.phi_mt_subunit = tub.mt_repeat.euler.phi_r + j * mt.twist_per_subunit
                result.append(tub)
        result.sort(key=lambda x:x.axial_dist + x.mt_repeat.axial_dist)
        result = [r.to_list() for r in result]
        return zip(*result)
def lattice_params(num_pfs, num_starts_dimer, dimer_repeat_dist, radius13pf):
    ''' Calculate the perfect microtubule parameters
    :param dimer_repeat_dist13pf:
    '''
    import numpy as np
    monomer_starts=num_starts_dimer * 2
    subunit_angle13=np.arctan2(3. / 2 * dimer_repeat_dist, 2 * np.pi * radius13pf)
    delta_x=2 * np.pi * radius13pf / (13. * np.cos(subunit_angle13))
    delta_n=dimer_repeat_dist * (3. / 2 * num_pfs / 13 - num_starts_dimer)
    theta=np.arctan2(delta_n, num_pfs * delta_x)
    pitch=num_starts_dimer * dimer_repeat_dist * np.cos(theta)
    rise_per_subunit=pitch / num_pfs
    subunit_angle=subunit_angle13 - theta
    mt_radius=num_pfs * radius13pf / 13.
    twist_per_subunit=delta_x * np.cos(subunit_angle) / mt_radius
    twist_per_repeat=(twist_per_subunit * num_pfs - 2 * np.pi) / num_starts_dimer
    axial_repeat_dist=np.cos(theta) * dimer_repeat_dist

    return twist_per_subunit, rise_per_subunit, mt_radius, axial_repeat_dist, twist_per_repeat

def cylindrical_coords(num_pfs, num_starts_dimer, dimer_repeat_dist13pf, radius13pf):
    ''' Get a cylindrical coords. Deperated by MTRepeatZ
    '''
    mt = MT(num_pfs = num_pfs, num_starts = num_starts_dimer, dimer_repeat_dist = dimer_repeat_dist13pf, ref_com = [0, radius13pf, 0])
    rpt = MTRepeatZ(mt, 0)
    #[z, phi, mt_radius, axial_repeat_dist, twist_per_repeat, repeat_index, pf_index]
    z, phi, repeat_index, pf_index = [],[],[],[]
    tubs = []
    for i in range(int(num_pfs)):
        tubs.append(rpt.tubulin_at_index(i))
    tubs.sort(key=lambda x:x.coord.z)
    for tub in tubs:
        z.append(tub.coord_mt.z)
        phi.append(tub.euler_mt.phi + mt.twist_per_subunit * tub.index)
        repeat_index.append(tub.mt_repeat.index)
        pf_index.append(tub.index)
    return z, phi, mt.mt_radius, mt.axial_repeat_dist, mt.twist_per_repeat, repeat_index, pf_index

def modulate_parameter(x, params):
    '''
    params=[a,b] or [a,b,c,d]
    result=a + b * x, or a + b *sin(2 * pi + (x+d)/c)
    '''
    import numpy as np
    a=b=c=d=0
    try:
        a=params[0]
        b=params[1]
        c=params[2]
        d=params[3]
    except TypeError as e:
        a=params
    except IndexError as e:
        pass
    x = np.array(x)
    if c == 0:
        val=a + b * x
        der=b
    else:
        val=a + b * np.sin(2 * np.pi * (x + d) / c) 
        der=np.cos(2 * np.pi * (x+d) / c) * 2 * np.pi / c * b
    
    return val, der

class MT(object):
    def __init__(self, num_pfs=13, num_starts=3, dimer_repeat_dist=80, ref_com=None, ref_is_pf13=True, monomer_offset=False, phi_params = None, theta_params=None,
            psi_params=None,elastic_params=None):
        import numpy as np
        self.num_pfs = num_pfs
        self.num_starts = num_starts
        phi_params = phi_params or [0.,0.]
        theta_params = theta_params or [0.,0.]
        psi_params = psi_params or [0.,0.]
        elastic_params = elastic_params or [0.,0.,0.,0.]
        self.phi_params = list(phi_params)
        self.phi_params[0] = Euler.d2r(self.phi_params[0])
        self.theta_params = list(theta_params)
        self.theta_params[0] = Euler.d2r(self.theta_params[0])
        self.psi_params = list(psi_params)
        self.psi_params[0] = Euler.d2r(self.psi_params[0])
        self.elastic_params=elastic_params
        self.monomer_offset = monomer_offset
        if ref_com is None:
            raise RuntimeError("ref_com must be provided")
        self.ref_com = Coord(*ref_com)
        radius13pf = np.sqrt(ref_com[0]**2 + ref_com[1]**2)
        self.dimer_repeat_dist = dimer_repeat_dist
        if not ref_is_pf13:
            radius13pf = radius13pf / num_pfs * 13
        else:
            self.ref_com.x *= num_pfs/13.
            self.ref_com.y *= num_pfs/13.
        tmp = lattice_params(self.num_pfs, self.num_starts / 2., self.dimer_repeat_dist,
                radius13pf)
        self.twist_per_subunit = tmp[0]
        self.rise_per_subunit = tmp[1]
        self.mt_radius = tmp[2]
        self.axial_repeat_dist = tmp[3]
        self.twist_per_repeat = tmp[4]
        self.repeats = {}
    def repeat_at_index(self, index):
        '''index is based on 0
        '''
        if index not in self.repeats:
            rpt = MTRepeat(self, index)
            self.repeats[index] = rpt
        return self.repeats[index]

class MTRepeat(object):
    ''' MT Repeat in increasing axial_dist order by increading pf number
    '''
    def __init__(self, mt, repeat_index = 0):
        self.mt=mt
        self.index = repeat_index
        self.axial_dist = repeat_index * self.mt.axial_repeat_dist
        phi_params = self.mt.phi_params[:]
        phi_params[0] += self.index * self.mt.twist_per_repeat
        phi_mt, self.d_phi_mt_d_dist = modulate_parameter(self.axial_dist, phi_params)
        self.d_phi_mt_d_dist += self.mt.twist_per_repeat/self.mt.axial_repeat_dist
        theta_mt = modulate_parameter(self.axial_dist, self.mt.theta_params)[0]
        psi_mt = modulate_parameter(self.axial_dist, self.mt.psi_params)[0]
        self.euler = Euler(phi_mt, theta_mt, psi_mt, mode='r')
        self.euler.mode="degree"
        self.coord = Coord(0,0,self.axial_dist).transform(self.euler.matrix)

        self.tubulins=[None] * int(self.mt.num_pfs)

    def tubulin_at_index(self, idx):
        ''' Get tubulin at index idx
        ''' 
        import numpy as np
        if idx < 0: idx=0
        if idx >= self.mt.num_pfs:
            return None
        if self.tubulins[idx] is None:
            tub=Tubulin(self, idx)
            self.tubulins[idx]=tub
        return self.tubulins[idx]

class MTRepeatZ(MTRepeat):
    ''' Microtubule Repeat in one repeat distance. See MTRepeat for another representation
    '''
    def tubulin_at_index(self, idx):
        import numpy as np
        if idx < 0: idx=0
        if idx >= self.mt.num_pfs:
            return None
        if self.tubulins[idx] is None:
            tub=Tubulin(self, idx)
            if tub.axial_dist > self.mt.axial_repeat_dist:
                mt_r = self.mt.repeat_at_index(self.index-1)
                tub=Tubulin(mt_r, idx)
            self.tubulins[idx] = tub
        return self.tubulins[idx]
class Tubulin(object):
    ''' Tubulin subunit on microtubule
    Attributes: 
        
    '''
    def __init__(self, mt_repeat, pf_index):
        super(Tubulin, self).__init__()
        self.coord = None
        self.euler=None
        self.coord_mt = None
        self.euler_mt = None
        self.r_tub=0.
        self.axial_dist=0. # axial dist related to subunit of pf 0
        self.origin_repeat=0.
        self.phi_first_pf=0.
        self.phi_mt_subunit=0.
        self.mt_repeat = None
        self.index = None
        self.r_tub = None
        self.init(mt_repeat, pf_index)
    
    def init(self, mt_repeat, pf_index):
        self.mt_repeat = mt_repeat
        self.mt = self.mt_repeat.mt
        self.index=pf_index
        self.axial_dist = self.mt_repeat.mt.rise_per_subunit * pf_index
        phi_tub = self.mt_repeat.euler.phi_r + pf_index * self.mt_repeat.mt.twist_per_subunit
        e = Euler(phi_tub, self.mt_repeat.euler.theta_r,
                self.mt_repeat.euler.psi_r, mode = 'r')
        euler_xform = e.matrix
        proto_skew = np.arctan2(self.mt_repeat.mt.mt_radius * self.mt_repeat.d_phi_mt_d_dist, 1)
        skew_xform = Euler.angle2matrix_r(np.pi/2, proto_skew, -np.pi/2)
        e1, e2 = Euler.mat2ang_r(np.matmul(euler_xform, skew_xform), return_euler=True)
        e1.mode = e2.mode = 'degree'
        if e1.theta < 180 and e1.theta>=0:
            e2 = e1
        self.euler = e2
        self.coord_mt = self.mt_repeat.coord + Coord(0,0,self.axial_dist).transform(euler_xform)
        self.euler_mt = Euler(self.mt_repeat.euler.phi_r - self.mt_repeat.index * self.mt.twist_per_repeat, self.mt_repeat.euler.theta_r, self.mt_repeat.euler.psi_r, mode = 'r')
        self.euler_mt.mode = "degree"
        self.coord = self.mt.ref_com.transform(euler_xform) + self.coord_mt
        # TODO: calculate deform parameter
        self.r_tub = self.mt.mt_radius
        self.phi_mt_subunit = self.mt_repeat.euler.phi_r  - self.mt_repeat.index * self.mt.twist_per_repeat + self.index * self.mt.twist_per_subunit
        if self.index == 0:
            self.phi_first_pf = self.euler.phi_r
        else:
            self.phi_first_pf = self.mt_repeat.tubulin_at_index(0).euler.phi_r

    def __str__(self):
        return "Repeat: %d, pf: %d" % (self.mt_repeat.index, self.index)
    
    def to_list(self):
        return [self.coord.x, self.coord.y, self.coord.z, self.euler.phi_r,
                self.euler.theta_r, self.euler.psi_r,
                self.r_tub,
                self.coord_mt.x, self.coord_mt.y, self.coord_mt.z,
                self.euler_mt.phi_r, self.euler_mt.theta_r, self.euler_mt.psi_r,
                self.axial_dist + self.mt_repeat.axial_dist,
                self.mt_repeat.index, self.index,
                self.phi_first_pf, # phi_first_pf,
                self.phi_mt_subunit, # phi_mt_subunit
                ]
