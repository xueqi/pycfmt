''' Smooth utils.

'''
import numpy as np
def get_values(m_in, micrograph_pixel_size):
    """ Get coord values from m_in, as ndarray
    """
    if (m_in[0].has_key('DetectorPixelSize') and m_in[0].has_key('Magnification')
        and m_in[0]['Magnification'] > 0 and m_in[0]['DetectorPixelSize'] > 0):
        scale = (10000. * m_in[0]['DetectorPixelSize'] / m_in[0]['Magnification']
             / micrograph_pixel_size)
    else:
        scale = 1.

    coord_x = np.array([ptcl.x - ptcl.shx * scale for ptcl in m_in])
    coord_y = np.array([ptcl.y - ptcl.shy * scale for ptcl in m_in])
    try:
        coord_z = np.array([ptcl.z - ptcl.shz * scale for ptcl in m_in])
    except:
        coord_z = np.zeros(coord_y.shape)
    phi = np.array([ptcl.phi for ptcl in m_in])
    theta = np.array([ptcl.theta for ptcl in m_in])
    psi = np.array([ptcl.psi for ptcl in m_in])
    return np.vstack((coord_x, coord_y, coord_z, phi, theta, psi)).T


def get_window(index, winsize, total_size):
    """ Get [lower_bound, upper_bound)
    given the center index, the window size and the total number
    if left or right is out of bound of total size or 0, shift the window
    :param index: The index where the window is returned for
    :type index: int
    :param winsize: The window size
    :type winsize: int
    :param total_size: The total amount of points
    """
    min_i = max(index - winsize // 2, 0)
    max_i = min_i + winsize - 1
    if max_i > total_size - 1:
        min_i = max(total_size - winsize, 0)
        max_i = total_size - 1
    return min_i, max_i

def fix_rollover(angles, delta, radix=360., return_anchor=False, indices=None):
    """ Fix rollover of angles around radix.
        delta is the diff between current angle and last angle.
        So if diff is 120, output should be like a, a+120, a+240, a+360, ...
        radix is used to bring angles far away back.
        So if delta is 1, radix is 10,
            input is 1, 12, 3, 44, 5, the output is 1, 2, 3, 4, 5
        if angles is list then return list
        if angles is ndarray return ndarray
        :param angles: The input angle list to fix
        :param delta: The expected diff between two adjacent value
        :param radix: The value to wrap around for calculate the difference.
            Default the radix is 360, for angle. Specify None if not want to use
        :param return_anchor: Return the best anchor used for the best model
        :param indices: The input indices for each angle.
                Default is None, which is using index range(0,angles)
        :type angles: ndarray
        :type delta: float
        :type radix: float
        :type return_anchor: bool
        :type indices: ndarray or list
        :return: angle_model: The best angle angle model
                anchor: The anchor index for best model. If return_anchor.
        :rtype: ndarray
    """
    if not isinstance(angles, np.ndarray):
        angles = np.array(angles)
    if angles.dtype == object:
        raise RuntimeError("The input is object, not numbers")
    nangles = angles.size
    min_stdv = 1000000
    best_anchor = 0
    half_radix = None
    if radix <= 0.: # bad value for radix. Do not use
        radix = None
    if radix:
        half_radix = radix / 2
    best_angles = None
    if indices is None:
        indices = np.arange(nangles)
    base_model = indices * delta
    # anchor means the angle at anchor_index does not roll over
    for anchor_index in xrange(nangles):
        anchor = indices[anchor_index]
        angle_model = angles[anchor_index] + base_model - anchor * delta
        # this calculate the difference between
        # the expected model and the actural data
        test_diff = angles - angle_model
        test_diff = test_diff[:]
        if radix:
            test_diff = (test_diff + half_radix) % radix - half_radix
        stdv = np.std(np.abs(test_diff), ddof=1)
        if stdv < min_stdv:
            min_stdv = stdv
            best_anchor = anchor_index
            best_angles = test_diff + angle_model
    if return_anchor:
        return best_angles, best_anchor
    else:
        return best_angles
