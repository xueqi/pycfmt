'''
smoothed filament plotting functions

@author: xueqi
'''
import numpy as np
import matplotlib.pyplot as plt
import matplotlib
from cryofilia.helix.smoothing.utils import get_values, get_window, fix_rollover
from cryofilia.alg.fitting import trimmed_nd_lsq_fit
from cryofilia.helix.smoothing.discontinuity import DISCONT_COORD, DISCONT_DIST, DISCONT_TWIST

def plot_smooth_result_m(fila_smooth,
                          micrograph_pixel_size=1.,
                          fila_interp=None,
                          fila_ori=None,
                          output_plot=None,
                          # plotting=None,
                          # nuke=True,
                          expect_dphi=None,
                          expect_rise=0.,
                          ylim=None,
                          is_3d=False,
                          figsize=[16, 10]):
    """ Plotting the smooth result
    
    Args:
        fila_smooth: A `SmoothedCoordinate`. The smoothed metadata
        fila_interp: A `SmoothedCoordinate`. The metadata without smooth
        fila_ori: A `SmoothedCoordinate`. The original input data
        output_plot: A `str`. The output plot name.
        micrograph_pixel_size: A `float`. The micrograph pixel size used to calculate the pixel/Angstroms
        expect_dphi: A `float`. The expected twist.
        expect_rise: A `float`. The expected rise in Angstrom
        ylim: A `float`. The y axis limit for distance  plot
        is_3d: A `bool`. If true, the coordinate plot is using 3d view. Not used yet
        figsize: A `list`. The dimension of the output plot. Default is [16, 10]
    """
    if len(fila_smooth) < 2:
        return
    mgname = fila_smooth[0].mg_name
    filaid = fila_smooth[0].fila_id
    
    def get_dist_from_last(coords):
        """ fist one is always the same as second one
        Args:
            coords: A `np.ndarray`. N x M, where N >= 2 and M >= 3
        Returns:
            A `np.ndarray`. 1D. 
        """
        dist = np.zeros(coords.shape[0])
        dist[1:] = np.linalg.norm(coords[1:,:3] - coords[:-1, :3], axis=1)
        dist[0] = dist[1]
        return dist
    
    coords = get_values(fila_smooth, micrograph_pixel_size)
    dist = get_dist_from_last(coords) * micrograph_pixel_size
    if fila_interp:
        coords_interp = get_values(fila_interp, micrograph_pixel_size)
        dist_interp = get_dist_from_last(coords_interp) * micrograph_pixel_size
    if fila_ori:
        coords_ori = get_values(fila_ori, micrograph_pixel_size)
        dist_ori = get_dist_from_last(coords_ori) * micrograph_pixel_size

    x_idx = np.arange(len(fila_smooth))
    discontinuities = np.array([ptcl.discontinuity for ptcl in fila_smooth])
    interpolated = np.array([ptcl.interpolated for ptcl in fila_smooth])
    if output_plot is None or isinstance(output_plot, str):
        fig = plt.figure(figsize=figsize)
    elif isinstance(output_plot, matplotlib.figure.Figure):
        fig = output_plot
    fig.suptitle("%s:%d" % (mgname, filaid))
    
    # plot the distance
    dist_ax = fig.add_subplot("221")
    plot_distance(dist, dist_ax, ylim=ylim, label="smooth dist",
                  interpolated=interpolated, marker="b-")
    # plot distance differences
    dist_diff_ax = dist_ax.twinx()
    repeat_dist = expect_rise / micrograph_pixel_size
    plot_dist_diff(coords[:,:3], dist_diff_ax, repeat_dist,
                   micrograph_pixel_size=micrograph_pixel_size)
    if fila_interp:
        plot_distance(dist_interp, dist_ax, label="interp dist", ms=4, alpha=0.5)
    if fila_ori:
        plot_distance(dist_ori, dist_ax, label="ori dist", ms=4, alpha=0.5)
    if sum(discontinuities) > 0:
        idxes = (discontinuities & DISCONT_DIST) > 0
        if idxes.size > 0:
            dist_ax.plot(x_idx[idxes], dist[idxes], 'rs',
                         markerfacecolor='none',
                         label='discontinuities')
            dist_ax.legend()
    
    # plot angles
    phi_ax = fig.add_subplot("222")
    plot_phi(x_idx, coords[:, 3], phi_ax, interpolated=interpolated)
    if fila_interp:
        plot_phi(x_idx, coords_interp[:, 3], phi_ax,
                 label="phi interp", marker="b-")
    angle_ax = phi_ax.twinx()
    plot_theta_psi(x_idx, coords[:, 4], coords[:, 5], angle_ax)

    handles, labels = phi_ax.get_legend_handles_labels()
    handles1, labels1 = angle_ax.get_legend_handles_labels()
    handles.extend(handles1)
    labels.extend(labels1)
    angle_ax.legend(handles, labels, loc="upper right")

    # plot the twist difference
    twist_disc_idx = np.array([])
    if sum(discontinuities) > 0:
        twist_disc_idx = (discontinuities & DISCONT_TWIST) > 0
    twist_ax = fig.add_subplot("223")
    plot_twist(coords[:,3], expect_dphi, twist_ax, interpolated,
                   discont_idx=twist_disc_idx)

    if fila_interp:
        plot_twist(coords_interp[:,3], expect_dphi, twist_ax,
                   label="net twist interp", marker="+")
    
    # plot the coordinates
    is_3d = False
    if is_3d:
        try:
            from mpl_toolkits.mplot3d import Axes3D
        except ImportError:
            is_3d = False
    if is_3d:
        coord_ax = fig.add_subplot(224, projection="3d")
    else:
        coord_ax = fig.add_subplot("224")
    plot_coords(coords[:,:3], coord_ax, marker="+-", label="smoothed", is_3d=is_3d)
    if fila_interp:
        plot_coords(coords_interp, coord_ax, marker="*", label="interp", is_3d=is_3d)
    if fila_ori:
        plot_coords(coords_ori, coord_ax, marker="x", label="ori", is_3d=is_3d)
    if sum(discontinuities) > 0:
        idxes = (discontinuities & DISCONT_COORD) > 0
        if idxes.size > 0:
            coord_ax.plot(coords[idxes, 0], coords[idxes, 1], 'rs',
                          markerfacecolor='none',

                          label='discontinuities')
            coord_ax.legend()
    coord_ax.axis('equal')
    coord_ax.legend(loc="upper right", fancybox=True, framealpha=0.5)
    return fig

def plot_coords(coords, coord_ax, marker="+", label="Coord", is_3d=False):
    if not is_3d:
        coord_ax.set_xlabel("x")
        coord_ax.set_ylabel("y")
        coord_ax.plot(coords[:, 0], coords[:, 1], marker, label=label, markersize=4)
    else:
        coord_ax.plot(coords[:,0], coords[:,1], coords[:,2])
    
def plot_dist_diff(coords, dist_diff_ax, repeat_dist, micrograph_pixel_size=1.):
    """ Plot dist differences
    """
    npoints = coords.shape[0]
    dists = np.zeros(npoints)
    
    def get_interp_dists(coords, results):
        """ Get distance along the tangent direction with trimmed lsq fit
        """
        n_points = coords.shape[0]
        def func(index, coords, npoints, winsize=7):
            min_i, max_i = get_window(index, winsize, npoints)
            unit_vec, origin, trimmed, fitness_tot, fitness = (
                trimmed_nd_lsq_fit(coords[min_i:max_i+1,:], lsq=True))
            if index > 0:
                diff = coords[index, :] - coords[index-1, :]
            else: # index == 0
                diff = coords[1,:] - coords[0,:]
            return np.dot(diff, unit_vec)
        if npoints > 1:
            for index in range(n_points):
                results[index] = func(index, coords, n_points)
    
    get_interp_dists(coords, dists)
    
    acc_dists = np.cumsum(dists)- dists[0]
    net_dist_diff = acc_dists - repeat_dist * np.arange(npoints)
    
    dist_diff_ax.plot(np.arange(npoints), net_dist_diff * micrograph_pixel_size, 
                      '--',
                      label="Net dist diff")
    dist_diff_ax.set_ylabel("dist diff(A)")
    dist_diff_ax.legend(loc="upper right")
    
def plot_twist(phis, expect_dphi, twist_ax, interpolated=None,
               marker="x--", label="smooth net twist", discont_idx=None):
    """ Plot twist differences
    """
    net_twist = fix_rollover(phis, expect_dphi)
    twist_diff = net_twist[1:] - net_twist[:-1]
    md_dphi = np.median(twist_diff)
    net_twist = fix_rollover(phis, md_dphi)
    net_twist = net_twist - net_twist[0]
    net_twist_diff = net_twist - expect_dphi * np.arange(phis.shape[0])
    
    indexes = np.arange(len(net_twist_diff))
    twist_ax.set_title("Net twist differences between smoothed and predicted")
    twist_ax.plot(indexes, net_twist_diff, marker, label=label)
    twist_ax.set_xlabel("index")
    twist_ax.set_ylabel("twist diff")
    if interpolated is not None:
        plot_interpolated(indexes, net_twist_diff, twist_ax, interpolated)
    if discont_idx is not None:
        discont_idx = np.array(discont_idx)
        if discont_idx.size > 0:
            twist_ax.plot(indexes[discont_idx], net_twist_diff[discont_idx], 'rs',
                          markerfacecolor='none', 
                          label="discontinuity")
    twist_ax.legend(loc="upper right")
    
def plot_interpolated(indexes, values, ax, interpolated):
    """ Plot interpolated points on the curve
    """
    if interpolated is None:
        return
    ax.plot(indexes[interpolated], values[interpolated],'gs',
#                      markersize=10,
                      label='interpolated', 
                      markerfacecolor='none')   
    
def plot_theta_psi(x_idx, theta, psi, angle_ax, interpolated=None):
    """ Plot theta and psi
    """
    psi_m = np.median(psi)
    angle_ax.set_title("Euler angles. median(psi)=%.4f" % psi_m)

    theta = np.array(theta) - 90
    psi = np.array(psi) - psi_m
    psi = fix_rollover(psi, 0)
    angle_ax.set_ylabel("(psi-median(psi)\n(theta-90)")
    angle_ax.set_xlabel("index")
    angle_ax.plot(x_idx, theta,
            'b--', markersize=10, label="tilt")
    angle_ax.plot(x_idx, psi, 'g-', label="dpsi")
    #angle_ax.set_ylim(-20, 20)
    if interpolated is not None:
        plot_interpolated(x_idx, theta, angle_ax, interpolated)
    
def plot_phi(x_idx, phi, phi_ax, interpolated=None, marker="r-",
             label="smoothed_phi"):
    """ Plot phi angle on phi_ax
    """
    # phi = (np.array(phi) + 180) % 360 - 180
    avg_phi = np.mean(phi)
    ymin, ymax = min(phi), max(phi)
    ymin = min(avg_phi - 20, ymin)
    # make some room for label
    ymax = max(avg_phi + 20, ymax + (ymax - ymin) * 0.3)
    phi_ax.set_ylim([ymin, ymax])
    phi_ax.set_ylabel("phi")
    phi_ax.set_xlabel("index")
    phi_ax.plot(x_idx, phi, marker, markersize=10, label=label)
    if interpolated is not None:
        plot_interpolated(x_idx, phi, phi_ax, interpolated)

def plot_distance(dists, dist_ax, ylim=None, label=None, interpolated=None,
                  marker='+', **kwargs):
    """ Plot distance on axis dist_ax
    """
    indexes = np.arange(len(dists))
    dist_ax.set_title("Distance between subunits(A)")
    if ylim:
        dist_ax.set_ylim([0, ylim])
    dist_ax.plot(indexes, dists, marker, label=label or "repeat distances", **kwargs)
    dist_ax.set_xlabel('index')
    dist_ax.set_ylabel('distance/(A)')
    handles, labels = dist_ax.get_legend_handles_labels()
    if interpolated is not None:
        plot_interpolated(indexes, dists, dist_ax, interpolated)
    dist_ax.legend(handles, labels, loc="upper left")
