'''
    Filament functions
'''
import relion
from copy import deepcopy

CTF_KEYS = ['Voltage', 'DefocusU', 'DefocusV', 'DefocusAngle', 'SpericalAberration', 'CtfBfactor', 'CtfScalefactor', 'PhaseShift', 'AmplitudeContrast', 'Magnification', 'DetectorPixelSize', 'CtfFigureOfMerit']
MICROGRAPH_KEYS = ['MicrographName', 'Voltage', 'Magnification', 'DetectorPixelSize']
PARTICLE_KEYS = ['CoordinateX', 'CoordinateY', 'ImageName', 'OriginX', 'OriginY', 'AnglePsi', 'AngleRot', 'AngleTile']
HELICAL_KEYS = ['HelicalTubeID', 'AngleTiltPrior', 'AnglePsiPrior', 'AnglePsiFlipRatio', 'pfs', 'starts']
HELICLA_PARTICLE_KEYS = ['AngleTiltPrior', 'AnglePsiPrior', 'HelicalTrackLength', 'AnglePsiFlipRation']

def get_micrograph_info(ptcl, with_ctf = True):
    d = {}
    d.update(get_info(ptcl, *MICROGRAPH_KEYS))
    if with_ctf:
        d.update(get_info(ptcl, *CTF_KEYS))
    return d

def get_filament_info(ptcl, with_micrograph = True, with_ctf = True):
    d = {}
    if with_micrograph:
        d.update(get_micrograph_info(ptcl, with_ctf))
    d.update(get_info(ptcl, *HELICAL_KEYS))
    return d

def get_info(ptcl, *keys):
    d = {}
    for key in keys:
        key = relion.unify_label(key)
        if key in ptcl:
            d[key] = ptcl[key]
    return d



def symm_mt_star(m, num_pfs = 14, num_starts = 1, super_twist = None,super_rise = None):
    '''
        Input microtubule should be continious.
        m is one microtubule filament in Metadata format.
        coordinates are in coordinate of original micrograph( after movie aligned), 
        so the pixel size is the original pixel size
        
        The originX and originY should be in original pixel size scale. 
        If extraction is done using relion and rescaled, 
        the input m should be modified so that originX and originY in original scale
        And output should also be rescaled to match the particles.
        
        # before the function 
        for i in range(len(m)):
            m[i] = {'rlnOriginX' : m[i]['rlnOriginX'] / scale,
                    'rlnOriginY' : m[i]['rlnOriginY'] / scale}
        
        # after the function
        for i in range(len(m)):
            m[i] = {'rlnOriginX' : m[i]['rlnOriginX'] * scale,
                    'rlnOriginY' : m[i]['rlnOriginY'] * scale}            
        
        angles has no effect
        
    '''

    from relion import Metadata
    import numpy as np
    if type(m) is not Metadata: m = Metadata(m)
    if m[0]['AnglePsiFlipRatio'] > 0.5:
        m.reverse()
    result = Metadata(m.name)
    result.copyLabels(m)
    xs = map(lambda x0, x1 : x0 - x1, m.getValues("rlnCoordinateX"), m.getValues("rlnOriginX"))
    ys = map(lambda y0, y1 : y0 - y1, m.getValues("rlnCoordinateY"), m.getValues("rlnOriginY"))
    coords = np.array([xs,ys])
    dist = np.diff(coords)
    dist = np.linalg.norm(dist, axis=0)
    phis = m.getValues("rlnAngleRot")
    dphis = np.diff(np.array(phis)) 
    thetas = m.getValues('rlnAngleTilt')
    psis = m.getValues("rlnAnglePsi")
    # get the super_rise  
    calc_rise = np.median(dist)
    if super_rise is None: super_rise = calc_rise
    # get the super_twist
    dphi = np.median(dphis)
    calc_twist = (dphi + 180) % 360 - 180
    if super_twist is None: super_twist = calc_twist
    rise_per_subunit = super_rise / num_pfs
    twist_per_subunit = (super_twist + 360) / num_pfs
    print super_twist
    for i in range(len(m) - 1):
        i_psis = np.linspace(psis[i], psis[i+1], num_pfs, endpoint = False)
        i_thetas = np.linspace(thetas[i], thetas[i+1], num_pfs, endpoint = False)
        i_xs = np.linspace(xs[i], (xs[i+1] - xs[i]) * num_starts + xs[i] , num_pfs, endpoint = False)
        i_ys = np.linspace(ys[i], (ys[i+1] - ys[i]) * num_starts + ys[i], num_pfs, endpoint = False)
        phi0 = phis[i]
        phi1 = phis[i+1]
        while phi1 - phi0 < 180: phi1 += 360
        i_phis = np.linspace(phi0, phi1, num_pfs, endpoint = False)
        import math
        calc_rise = math.sqrt((xs[i+1] - xs[i]) * (xs[i+1] - xs[i]) + (ys[i+1] - ys[i]) * (ys[i+1] - ys[i]))
        #print calc_rise, super_rise
        if (calc_rise - super_rise) / super_rise > 0.1: # this usually won't happen if the coordinates are smoothed
        #    print "Calc_rise not expectd", calc_rise, super_rise
        #    print "will skip for now"
            continue
            i_phis = np.linspace(phi0, phi0 + twist_per_subunit * num_pfs, num_pfs, endpoint = False)
            i_phis = [phi0 + twist_per_subunit * ii for ii in range(num_pfs)]
            dx = (xs[i+1] - xs[i]) / calc_rise * super_rise / num_pfs * num_starts
            dy = (ys[i+1] - ys[i]) / calc_rise * super_rise / num_pfs * num_starts
            i_xs = [dx * ii + xs[i] for ii in range(num_pfs)]
            i_ys = [dy * ii + ys[i] for ii in range(num_pfs)]
        for j in range(num_pfs)[12:13]:
            m0 = deepcopy(m[i])            
            ori_shx = m0['rlnOriginX']
            ori_shy = m0['rlnOriginY']
            m0.update({'rlnOriginX'     : ori_shx + (i_xs[0] - i_xs[j]),
                       'rlnOriginY'     : ori_shy + (i_ys[0] - i_ys[j]),
                       'rlnAngleRot'    : i_phis[j],
                       'rlnAngleTilt'   : i_thetas[j],
                       'rlnAnglePsi'    : i_psis[j],
                })
            result.append(m0)
    return result
            
def group_by_filament(m):
    '''
        Group by filaments. Return dict, key is the (mgname, tubeid),
         value is the Metadata containing all the ptcls in the filament
    '''
    from relion import Metadata
    filaments = {}
    for m1 in m:
        mgname = m1['rlnMicrographName']
        tubeid = m1['rlnHelicalTubeID']
        if not (mgname, tubeid) in filaments:
            filaments[(mgname, tubeid)] = Metadata(m.name)
            filaments[(mgname, tubeid)].copyLabels(m)
        filaments[(mgname, tubeid)].append(m1)
    
    return filaments
    
def symmetrize_mt(vol, rise_per_subunit, twist_per_subunit, num_pf, center_pf = None):
    '''
        symmetrize function does not need to know the information of pixel size. 
        So rise_per_subunit unit is number of pixels. So rise_per_subunit 
        should be converted to pixel number before pass to this function.
    '''
    from cryofilia.util3d import rotshift3D_spline
    import numpy as np
    if center_pf is None: center_pf = num_pf / 2
    result = np.copy(vol)
    for i in range(-center_pf, num_pf - center_pf):
        if i == 0: continue
        v1 = rotshift3D_spline(vol, 
                                    twist_per_subunit * i, 
                                    [0, 0, rise_per_subunit * i])
        result += v1
    return result
