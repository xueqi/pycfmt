'''
    Projector.

    Used to do projections on certain volume. 

    Keep projected volume in memory.

    The projection is the most time consuming step in alignment
    
'''
import time
import numpy as np
import scipy.interpolate
import logging
import os

from cryofilia.EMImage import EMImage, TestImage
from chuff.util.ds import IndexedPQ

USE_CUDA = False
logger = logging.getLogger(__name__)
try:
    import abcde
    import pycuda.autoinit
    from pycuda.compiler import SourceModule
    from pycuda.elementwise import ElementwiseKernel
    from skcuda.fft import _fft
except ImportError:
    USE_CUDA= False
#    logger.warn("No CUDA library found. Do not use GPU Accelation")
from cryofilia.euler import Euler


class _Projector(object):
    '''
        Abstract class for Projector.
        A projector keeps one volume in certain format, 
        and project with euler angles in spider convention:YZY
        Use LRU algorithm to kick out projections if memory is exceeded.
        
    '''
    def __init__(self, cache_dir='', tag=0):
        # keep the angle list with time stamps. update the timestamp when the 

        self.cache_dir = cache_dir
        self._in_cache_dir = {}
        self.proj_cache = IndexedPQ()
        self.max_projs = 1000
        self._voxel_size = None
        self._project_resolution = None
        self.tag = tag        # used to identify the volume
    @property
    def voxel_size(self):
        return self._voxel_size or 1.

    @voxel_size.setter
    def voxel_size(self, value):
        if value is not None and value <= 0.:
            value = None
        self._voxel_size = value
    
    @property
    def project_resolution(self):
        return self._project_resolution or self.voxel_size * 2

    @project_resolution.setter
    def project_resolution(self, value):
        if value is None:
            value = self.voxel_size * 2
        if self.voxel_size is not None and value < self.voxel_size * 2:
            value = self.voxel_size * 2
        self._project_resolution = value

    @property
    def fourierRadius(self):
        ''' fourierRadius is used to determine the resolution of projection. 
         use smaller radius or lower resolution keep the memory usages low
         default use nyquist frequency
            :Return: fraction between (0,0.5]
        '''
        return self.voxel_size / self.project_resolution


    def project_real(self, phi, theta, psi, shx = 0, shy = 0):
        return self.project(phi, theta, psi, shx, shy)
    
    def project_fft(self, phi, theta, psi, shx = 0, shy = 0):
        ''' Project as fft. 
        '''
        proj = self.project(phi, theta, psi, shx, shy)
        proj.isFFT = True
        return proj
        
    def project(self, phi, theta, psi, shx = 0, shy = 0, size = None):
        ''' Project using angle phi, theta, psi. 
        '''
        phi = (phi + 360) % 360
        theta = (theta + 360) % 360
        psi = (psi + 360) % 360

        key = (phi, theta, psi, self.project_resolution, self.tag)
        proj = None
        if self.proj_cache.has_key(key):
            proj = self.proj_cache.getValue(key)
            self.proj_cache.set(key, -time.time())
        elif key in self._in_cache_dir:
            fname = self._in_cache_dir[key]
            try:
                proj = EMImage(fname) # bring to memory, not use memmap
                self.proj_cache.add(key, proj.copy(), -time.time())            
            except: # file not exists
                del self._in_cache_dir[key]
        if proj is None:

            proj = self._project(phi, theta, psi, shx, shy, size = size)
            self.proj_cache.add(key, proj, -time.time())
        if self.proj_cache.size() > self.max_projs:

            self._write_to_disk()
        # get a new copy of project to make sure the in cached projection is not modified.
        proj = proj.copy()
        if size is not None:
            ix, iy = int(shx), int(shy)
            fx, fy = shx - ix, shy - iy
            if abs(fx) > 0.001 or abs(fy) > 0.001:
                # do fourier shift for fraction
                proj.fourier_shift(fx, fy)
            proj.isFFT = False
            if size is not None and size != proj.ysize:
                proj = proj.clip(size)
            if ix != 0 or iy != 0:
                # shift 
                proj.shift_i(ix, iy)
        else:
            if abs(shx) > 0.001 or abs(shy) > 0.001:
                proj.fourier_shift(shx, shy)
            proj.isFFT = False
        return proj
    

    def _write_to_disk(self):
        ''' Write some to disk, and clear from memory
        '''
        for _ in range(self.max_projs / 3):
            key, tmp, _ = self.proj_cache.popMax()
            if self.cache_dir and not key in self._in_cache_dir:
                imgname = "%s.mrc" % hash(key)    # be careful of hash collision :TODO:
                # try get lock. Not atomic, might have racing problem. Do not have better solution yet
                lck = os.path.join(self.cache_dir, ".%s.lock" % imgname)
                imgname = os.path.join(self.cache_dir, imgname)
                if not os.path.exists(imgname):
                    while os.path.exists(lck):
                        time.sleep(0.1)
                    # lock
                    try:
                        open(lck,'w').write("lock")
                        # need to check again if other process has write the image while acquiring the lock
                        if not os.path.exists(imgname):
                            tmp.write_image(imgname)
                        self._in_cache_dir[key] = imgname
                    except:
                        pass
                    finally:
                        # unlock
                        try:
                            os.remove(lck)
                        except OSError:
                            pass
            del tmp


    def _project(self, phi, theta, psi, shx, shy, size = None):
        ''' Projection override in subclass
        '''
        return None
    
    def get(self, angle):
        return self.proj_cache.get(angle)

    def cleanCache(self):
        if not self.cache_dir: return
        for angle in self._in_cache_dir:
            imgfile = self._in_cache_dir[angle]
            if os.path.exists(imgfile):
                os.remove(imgfile)


    
class EMProjector(_Projector):
    ''' volume projector.Using algorithm from relion.
    '''
    def __init__(self, filename, 
                 paddingFactor = 2, 
                 projectRadius = None, 
                 projectSize = None,
                 interp_mode = "linear",
                 gridding_correction = "True", 

                 voxel_size = None,
                 **kwargs):
        ''' 

        @param filename: The volume file used for projection
        @param paddingFactor: Padding in fourier space
        @param projectRadius: Not used now
        @param projectSize: The maximum projection size this would project
        @param gridding_correction: Do gridding correction for projection
        @param interp_mode: The interpolation mode. linear or nearest. default linear
        
        memory needed is 8 * (size * padding_factor)^3
        
        '''
        super(EMProjector, self).__init__(**kwargs)
        
        if isinstance(filename, str):
            # The input volume, use mmap 
            self._volume = EMImage(filename, mmap=True)
        elif isinstance(filename, EMImage):
            self._volume = filename
        elif isinstance(filename, np.ndarray):
            self._volume = EMImage(filename)
        else:
            raise Exception("Unknown type of input file. Accepts str, EMImage or ndarray. Got %s" % type(filename))
        # check the volume must be cubic
        if (self._volume.xsize != self._volume.ysize 
            or self._volume.xsize != self._volume.zsize):
            raise Exception("The input volume must be cubic")
        
        # check the projection size.
        if projectRadius is not None:
            if projectRadius > self._volume.xsize / 2:
                logger.warn("project radius is too large. Might not be realistic "
                        + "outside %.2f" % self._volume.xsize / 2)
            elif projectRadius <= 0:
                logger.warn("project radius given is negative. reset to default")
                projectRadius = None
            
        self._rgi = None   # The RegularGridInterpolator
    
        if paddingFactor < 1:
            raise Exception("Padding factor must be greater or equal than 1")
        
        self._paddingFactor = paddingFactor
        # make the padded size divisible by 4
        pad_size = (round(self._volume.xsize * self._paddingFactor)) // 4 * 4
        # recalculate padding factor
        self._paddingFactor = pad_size / self._volume.xsize
        self._projectRadius = projectRadius or self._volume.xsize / 2 

        # default do gridding correction
        self._griddingCorrection = True
        self._projectSize = projectSize or self._volume.xsize
        
        if interp_mode not in ['linear', 'nearest']:
            logger.warn("Not a valid interpolation mode. Use default linear")
            interp_mode = "linear"
        self._interp_mode = interp_mode
        self._voxel_size = voxel_size
        
    def _prepareFFT(self):
        ''' Prepare 3d fourier transform for extracting the central slice 
            for projection. 

        '''
        if self._rgi is not None: return
        
        fft_size = int(round(self._volume.xsize * self._paddingFactor))
        # make a copy of original volume and convert to float32
        _fftVolume = EMImage(self._volume.data.astype(np.float32))
        # gridding correction using sinc function
        if self._griddingCorrection:
            r = TestImage._radius(_fftVolume.size, dtype= np.float32).data
            sinc = r / (self._volume.xsize * self._paddingFactor / np.pi)
            sinc[self._volume.zsize/2, self._volume.ysize/2, self._volume.xsize/2] = 1
            sinc = np.sin(sinc) / sinc
            sinc *= sinc
            sinc[self._volume.zsize/2, self._volume.ysize/2, self._volume.xsize/2] = 1
            _fftVolume /= sinc
            del sinc 
        # pad the volume
        _fftVolume = _fftVolume.clip(fft_size)
        # prefft shift. 
        _fftVolume.data = np.fft.fftshift(_fftVolume.data)
        _fftVolume.isFFT = True
        
        x_idx = np.arange(_fftVolume.xsize) - _fftVolume.xsize / 2
        y_idx = np.arange(_fftVolume.ysize) - _fftVolume.ysize / 2
        z_idx = np.arange(_fftVolume.zsize) - _fftVolume.zsize / 2
        
        # pre normalize the fft
        _fftVolume /= (self._paddingFactor * self._paddingFactor*
                            self._paddingFactor * self._volume.xsize)
        
        self._rgi = scipy.interpolate.RegularGridInterpolator([z_idx,y_idx,x_idx], 
                            np.fft.fftshift(_fftVolume.data),
                            method = self._interp_mode, 
                            bounds_error = False, fill_value = 0)
        self.ori_xsize = self._volume.xsize
        del self._volume
        # a copy of fft is kepted in self._rgi
        # delete _fftVolume to save the memory
        del _fftVolume
        
    def _project(self, phi, theta, psi, shx, shy, size = None):
        ''' Project volume to image with angle and shift. 
        ZYZ convension, CC positive, angles in degree, shift in pixels.
        '''
        if self._rgi is None: self._prepareFFT()
        
        box_size = self.ori_xsize
        A = Euler.angle2matrix(phi, theta, psi) * self._paddingFactor
        A = A.T
        Y, X = TestImage.index([box_size, box_size])
        r = TestImage.radius([box_size, box_size])
        idxes = r.data <= self.fourierRadius * box_size 
        X, Y = X[idxes], Y[idxes]
        xp = A[0,0] * X + A[0,1] * Y
        yp = A[1,0] * X + A[1,1] * Y
        zp = A[2,0] * X + A[2,1] * Y
        coords = np.stack([zp, yp, xp], axis = 1)
        f2d = np.zeros([box_size, box_size], dtype = np.complex64)
        f2d[idxes] = self._rgi(coords)
        f2d = np.fft.ifftshift(f2d)
        pj = EMImage(f2d, isFFT = True)
        if abs(shx) > 0.0001 or abs(shy) > 0.0001:
            pj.fourier_shift(shx, shy)
        pj.isFFT = False
        pj.data = np.fft.fftshift(pj.data)
        if size is not None and size != box_size:
            pj = pj.clip(size)
        return pj

class CUDAProjector(EMProjector):
    ''' CUDA projector, need to use pyCUDA
        
        TODO: consider disable cache in memory. 
        # for 10000 projection on GTX1070, one GPU, 
        # dim     time
        # 400      28s
        # 200      16s
        # 100      13.6s
    '''
    def __init__(self, filename):
        super(CUDAProjector, self).__init__(filename)
        self._gpu_data = None
        self.block_size = (8,8,1) # The block size of gpu
        self._plans = {} # keep a plan cache.
        
        self.cuda_mod = SourceModule('''
        // fftshift
        #include <pycuda-complex.hpp>
        typedef pycuda::complex<float> T_in;
        typedef pycuda::complex<float> T_out;
        
        #define GET_INDEX(x,y,z) \
            int (x) = blockIdx.x * blockDim.x + threadIdx.x; \
            int (y) = blockIdx.y * blockDim.y + threadIdx.y; \
            int (z) = blockIdx.z * blockDim.z + threadIdx.z;
        
        #define CHECK_BOUNDARY(x,y,z,nx,ny,nz) \
            if ((x) < 0 || (x) >=(nx)) return; \
            if ((y) < 0 || (y) >=(ny)) return; \
            if ((z) < 0 || (z) >=(nz)) return;
        
        __global__ void fftshift_c(T_in *in, T_out *out, 
                    int nx, int ny, int nz = 1) {
            GET_INDEX(x,y,z);
            CHECK_BOUNDARY(x,y,z,nx,ny,nz);
            int idx_out = z * nx * ny + y * nx + x;
            // get the original coordinate
            x -= nx / 2;
            y -= ny / 2;
            z -= nz / 2;
            if (x < 0) x += nx;
            if (y < 0) y += ny;
            if (z < 0) z += nz;
            int idx_in = z * nx * ny + y * nx + x;
            out[idx_out] = in[idx_in]; 
        }

        __global__ void ifftshift_c(T_in *in, T_out *out, 
                    int nx, int ny, int nz = 1) {
            GET_INDEX(x,y,z);
            CHECK_BOUNDARY(x,y,z,nx,ny,nz);

            int idx_out = z * nx * ny + y * nx + x;
            // get the original coordinate
            x += nx / 2;
            y += ny / 2;
            z += nz / 2;
            if (x >= nx) x -= nx;
            if (y >= ny) y -= ny;
            if (z >= nz) z -= nz;
            int idx_in = z * nx * ny + y * nx + x;
            out[idx_out] = in[idx_in]; 
        }
        
        __global__ void fftshift(float *in, float *out, 
                    int nx, int ny, int nz) {
            GET_INDEX(x,y,z);
            CHECK_BOUNDARY(x,y,z,nx,ny,nz);
            int idx_out = z * nx * ny + y * nx + x;
            // get the original coordinate
            x -= nx / 2;
            y -= ny / 2;
            z -= nz / 2;
            if (x < 0) x += nx;
            if (y < 0) y += ny;
            if (z < 0) z += nz;
            int idx_in = z * nx * ny + y * nx + x;
            out[idx_out] = in[idx_in]; 
        }

        __global__ void ifftshift(float *in, float *out, 
                    int nx, int ny, int nz) {
            GET_INDEX(x,y,z);
            CHECK_BOUNDARY(x,y,z,nx,ny,nz);

            int idx_out = z * nx * ny + y * nx + x;
            // get the original coordinate
            x += nx / 2;
            y += ny / 2;
            z += nz / 2;
            if (x >= nx) x -= nx;
            if (y >= ny) y -= ny;
            if (z >= nz) z -= nz;
            int idx_in = z * nx * ny + y * nx + x;
            out[idx_out] = in[idx_in]; 
        }
        
        __global__ void pad(float *in, int nx1, int ny1, int nz1,
                    float *out, int nx2, int ny2, int nz2,
                    float value = 0) {
            // index in out array
            GET_INDEX(x,y,z);
            CHECK_BOUNDARY(x,y,z,nx2,ny2,nz2);
   
            int idx_out = z * nx2 * ny2 + y * nx2 + x;
            
            x -= nx2 / 2 - nx1 / 2;
            y -= ny2 / 2 - ny1 / 2;
            z -= nz2 / 2 - nz1 / 2;
            if (x < 0 || x >= nx1 || y < 0 || y >= ny1 || z < 0 || z >=nz1) {
                out[idx_out] = value;
            } else {
                int idx_in = z * nx1 * ny1 + y * nx1 + x;
                out[idx_out] = in[idx_in];
            }
        }
        __global__ void padf2c(float *in, int nx1, int ny1, int nz1,
                    pycuda::complex<float> *out, int nx2, int ny2, int nz2,
                    float value = 0) {
            // index in out array
            GET_INDEX(x,y,z);
            CHECK_BOUNDARY(x,y,z,nx2,ny2,nz2);
   
            int idx_out = z * nx2 * ny2 + y * nx2 + x;
            
            x -= nx2 / 2 - nx1 / 2;
            y -= ny2 / 2 - ny1 / 2;
            z -= nz2 / 2 - nz1 / 2;
            if (x < 0 || x >= nx1 || y < 0 || y >= ny1 || z < 0 || z >=nz1) {
                out[idx_out] = value;
            } else {
                int idx_in = z * nx1 * ny1 + y * nx1 + x;
                out[idx_out] = in[idx_in];
            }
        }
        
        __global__ void mult(pycuda::complex<float> *a, float b, int nx, int ny, int nz) {
            GET_INDEX(x,y,z);
            CHECK_BOUNDARY(x,y,z,nx,ny,nz);
            int idx = z * nx * ny + y * nx + x;
            a[idx] *= b;
        }       
                         
        __global__ void divide(pycuda::complex<float> *a, float b, int nx, int ny, int nz) {
            GET_INDEX(x,y,z);
            CHECK_BOUNDARY(x,y,z,nx,ny,nz);
            int idx = z * nx * ny + y * nx + x;
            a[idx] /= b;
        }                        
        
        __global__ void gridCorrect(float *a, int nx, int ny, int nz, float m) {
            GET_INDEX(x,y,z);
            CHECK_BOUNDARY(x,y,z,nx,ny,nz);
            int idx = z * nx * ny + y * nx + x;
            x = x - nx / 2;
            y = y - ny / 2;
            z = z - nz / 2;
            double r = x * x + y * y + z * z;
            r = sqrt(r);
            double sinc1 = r * m;
            if (r == 0) return;
            sinc1 = sinc1 / sin(sinc1);
            sinc1 *= sinc1;
            a[idx] *= sinc1;
        }
        #define ELEMENT(v, x, y, z) (v)[(z) * NXY + (y) * NX + (x)]
        #define LIN_INTERP(a, l, h) ((l) + ((h) - (l)) * (a))
        // project volume v to result
        typedef pycuda::complex<float> T;
//        template<class T>
        __global__ void project(T * v, int NX, int NY, int NZ, 
                T * result, int NX1, int NY1,
                float *A, // The rotation Matrix
                float r) 
        {
            int NXY = NX * NY;
            // get the x, y coordinate to project
            GET_INDEX(x,y,z);
            CHECK_BOUNDARY(x,y,z,NX, NY, NZ);
            
            int idx_r = y * NX1 + x;

            x -= NX1 / 2;
            y -= NY1 / 2;
            if (x * x + y * y > r * r) {
                return;
            }
            float xp = x * A[0] + y * A[1];
            float yp = x * A[3] + y * A[4];
            float zp = x * A[6] + y * A[7];

            xp += NX / 2;
            yp += NY / 2;
            zp += NZ / 2;
            int x0 = floor(xp), y0 = floor(yp), z0 = floor(zp);
            if (x0 < 0 || y0 < 0 || z0 < 0) return;
            float fx = xp - x0, fy = yp - y0, fz = zp - z0;
            int x1 = x0 + 1, y1 = y0 + 1, z1 = z0 + 1;
            if (x1 >= NX || y1 >= NY || z1 >= NZ) return;
            T d000 = ELEMENT(v, x0, y0, z0);
            T d001 = ELEMENT(v, x1, y0, z0);
            T d010 = ELEMENT(v, x0, y1, z0);
            T d011 = ELEMENT(v, x1, y1, z0);
            T d100 = ELEMENT(v, x0, y0, z1);
            T d101 = ELEMENT(v, x1, y0, z1);
            T d110 = ELEMENT(v, x0, y1, z1);
            T d111 = ELEMENT(v, x1, y1, z1);
            
            T dx00, dx01, dx10, dx11, dxy0, dxy1;
            dx00 = LIN_INTERP(fx, d000, d001);
            dx01 = LIN_INTERP(fx, d100, d101);
            dx10 = LIN_INTERP(fx, d010, d011);
            dx11 = LIN_INTERP(fx, d110, d111);
            dxy0 = LIN_INTERP(fy, dx00, dx10);
            dxy1 = LIN_INTERP(fy, dx01, dx11);
            result[idx_r] =  LIN_INTERP(fz, dxy0, dxy1);
        }
        
        __global__ void fourier_shift(T * v, float shx, float shy, float shz,
                    int nx, int ny, int nz) {
            GET_INDEX(x,y,z);
            CHECK_BOUNDARY(x,y,z,nx,ny,nz);
            
            int idx = z * nx * ny + y * nx + x;
            
            float2 c;
            float p = -2 * 3.141592653589f * (shx * x/nx + shy * y/ny + shz * z/nz);
            sincosf(p, &c.y, &c.x);
            T t(c.x, c.y);
            v[idx] = v[idx] * t;
            
        }
        
        ''')

    def grid_size(self, nx, ny=1, nz=1):
        return (
            (nx + self.block_size[0] - 1) // self.block_size[0],
            (ny + self.block_size[1] - 1) // self.block_size[1],
            (nz + self.block_size[2] - 1) // self.block_size[2],
            )
    def _prepareFFT(self):
        ''' Prepare 3d fourier transform for extracting the central slice 
            for projection. The fft data stay in device memory
        '''
        import pycuda.gpuarray as gpuarray
        import skcuda.cufft as cufft
        
        if self._gpu_data is not None: return
        
        s = time.time()
        #nx, ny, nz = self._volume.size
        fft_size = int(round(self._volume.xsize * self._paddingFactor))
        # make pyCUDA friendly
        nx_fft = ny_fft = nz_fft = np.int32(fft_size)
        nx_gpu = ny_gpu = nz_gpu = np.int32(self._volume.xsize)
        _fftVolume = EMImage(self._volume.data.astype(np.float32))
        self._gpu_data = gpuarray.zeros([fft_size, fft_size, fft_size], 
                                        dtype = np.complex64)
        # copy data to _gpu_data
        x_gpu = gpuarray.to_gpu(_fftVolume.data)
        
        # gridding correction using sinc function
        if self._griddingCorrection:
            grid_correct = self.cuda_mod.get_function("gridCorrect")
            grid_correct(x_gpu, nx_gpu, ny_gpu, nz_gpu,
                         np.float32(np.pi /(self._volume.xsize * self._paddingFactor)),
                         block = self.block_size, 
                         grid = self.grid_size(nx_gpu, ny_gpu, nz_gpu)
                         )
        dummy_data = gpuarray.empty_like(self._gpu_data)

        pad_f2c = self.cuda_mod.get_function("padf2c")
        pad_f2c(x_gpu, nx_gpu, ny_gpu, nz_gpu,
                 dummy_data, nx_fft, ny_fft, nz_fft,
                 block = self.block_size, 
                 grid = self.grid_size(nx_fft, ny_fft, nz_fft))    
        del x_gpu # to save memory
        fftshift_c = self.cuda_mod.get_function("fftshift_c")

        fftshift_c(dummy_data, self._gpu_data, 
                        nx_fft, ny_fft, nz_fft,
                        block = self.block_size, 
                        grid = self.grid_size(nx_fft, ny_fft, nz_fft))
        # do fft
        plan = cufft.cufftPlan3d(nx_fft, ny_fft, nz_fft,
                                 cufft.CUFFT_C2C)
        #print plan
        cufft.cufftExecC2C(plan, 
                           int(self._gpu_data.gpudata), 
                           int(dummy_data.gpudata), 
                           cufft.CUFFT_FORWARD)
        # this plan is only used once. Delete after use
        cufft.cufftDestroy(plan)
        
        # do fftshift
        fftshift_c(dummy_data, self._gpu_data, nx_fft, ny_fft, nz_fft,
                      block = self.block_size, 
                      grid = self.grid_size(nx_fft, ny_fft, nz_fft))
        del dummy_data
        # normalize the fft. So the inverse fft do not need to do this step
        div = self.cuda_mod.get_function("divide")
        div(self._gpu_data, np.float32(self._paddingFactor
                                       *self._paddingFactor
                                       *self._paddingFactor
                                       *self._volume.xsize),
             nx_fft, ny_fft, nz_fft,
             block = self.block_size, 
             grid = self.grid_size(nx_fft, ny_fft, nz_fft))
        print "prepare fft took %.2f seconds" % (time.time() - s)
    
    def _project(self, phi, theta, psi, shx, shy, size = None):
        
        import pycuda.gpuarray as gpuarray
        import skcuda.cufft as cufft
        if self._gpu_data is None: self._prepareFFT()
        box_size = self._volume.xsize
        
        A = Euler.angle2matrix(phi, theta, psi) * self._paddingFactor
        A = A.T
        A = A.astype(np.float32, order = "C")
        A1 = gpuarray.to_gpu(A)
        pj_func = self.cuda_mod.get_function("project")
        pj_fft = gpuarray.zeros([box_size, box_size], np.complex64)
        nz_fft = ny_fft = nx_fft = np.int32(self._gpu_data.shape[0])
        nx = ny = np.int32(box_size)
        pj_func(self._gpu_data, nx_fft, ny_fft, nz_fft,
                      pj_fft, np.int32(pj_fft.shape[1]), np.int32(pj_fft.shape[0]),
                      A1, np.float32(self._fourierRadius),
                      block = self.block_size,
                      grid = self.grid_size(nx, ny))
        # ifftshift
        dummy_data = gpuarray.zeros_like(pj_fft)
        ifftshift_c = self.cuda_mod.get_function("ifftshift_c")
        ifftshift_c(pj_fft, dummy_data, nx, ny, np.int32(1),
                    block = self.block_size,
                    grid = self.grid_size(nx, ny, 1))
        if abs(shx) > 0.001 or abs(shy) > 0.001:
            self.fourier_shift(dummy_data, shx, shy, 0)
        # ifft
        plan = self.plan((nx, ny), cufft.CUFFT_C2C)
        cufft.cufftExecC2C(plan, 
                           int(dummy_data.gpudata), 
                           int(dummy_data.gpudata), 
                          cufft.CUFFT_INVERSE)
        # ifftshift
        ifftshift_c(dummy_data, pj_fft, nx, ny, np.int32(1),
                    block = self.block_size,
                    grid = self.grid_size(nx, ny, 1))
        
        pj_arr = np.zeros([ny, nx])
        pj_arr[:] = pj_fft.get().real
        del pj_fft, A1, dummy_data
        result = EMImage(pj_arr)
        if size is not None:
            result = result.clip(size)
        return result
    
    def plan(self, shape, ffttype):
        import skcuda.cufft as cufft
        key = (shape, ffttype)
        if key not in self._plans:
            if len(shape) == 2:
                self._plans[key] = cufft.cufftPlan2d(shape[0], shape[1], ffttype)
            elif len(shape) == 1:
                self._plans[key] = cufft.cufftPlan1d(shape[1], ffttype, 1)
        return self._plans[key]
if USE_CUDA:
    Projector = CUDAProjector
else:
    Projector = EMProjector

