"""
Created on May 26, 2017

@author: xueqi
"""
from relion import Container, Metadata
import numpy as np
from math import pi, sqrt

CTF_KEYS = ["Voltage", "DefocusU", "DefocusV", "DefocusAngle", 
            "SpericalAberration", "CtfBfactor", "CtfScalefactor", 
            "PhaseShift", "AmplitudeContrast", "Magnification", 
            "DetectorPixelSize", "CtfFigureOfMerit"]

class CTFMeta(Metadata):
    _labels = {"mg_name" : ["MicrographName"],
               "ctf_image" : ["CtfImage"],
               "def1" : ["DefocusU"],
               "def2" : ["DefocusV"],
               "defangle" : ["DefocusAngle"],
               "voltage" : ["Voltage"],
               "cs" : ["SphericalAberration"],
               "amplitude_contrast" : ["AmplitudeContrast"],
               "mag" : ["Magnification"],
               "dstep" : ["DetectorPixelSize"],
               "fom" : ["CtfFigureOfMerit"],
               "max_resolution" : ["CtfMaxResolution"],
               "bfactor" : ["CtfBfactor"],
               "scale_factor" : ["CtfScalefactor"],
               "phase_shift" : ["PhaseShift"],
        }
    def __init__(self, *args, **kwargs):
        super(CTFMeta, self).__init__(*args, **kwargs)
        
class CTF(Container):
    """
    CTF Module
    """
    def __init__(self, voltage=300., cs=2.0, defocusu=0, defocusv=0,
                 defocusangle=0.,
                 bfactor=0, amp_contrast=0.07, alpha =0.):
        """
        Args:
            voltage: Voltage in kV.
            cs:      Spherical Abberation in mm. typical value: 2.0, 2.7
            defocusu:First value reported by CtfFind
            defocuv: Second value reported by CtfFind
            defocusangle: Defocus angle reported by CtfFind
            bfactor: envelop bfactor
            amp_contrast: Amplitude contrast, 0.07 for cryo
        """
        super(CTF, self).__init__()
        self.update({"DefocusU":defocusu, 
                     "DefocusV":defocusv, 
                     "DefocusAngle":defocusangle, 
                     "Voltage":voltage,
                     "AmplitudeContrast":amp_contrast, 
                     "CtfBfactor":bfactor,
                     "SphericalAberration" : cs, 
                     "CtfScalefactor": 0.,
                     "PhaseShift":0.,
                     "Magnification":0.,
                     "DetectorPixelSize":0.,
                     "CtfFigureOfMerit":0.,
                     })
        self._images = {}
        self.alpha = alpha


    def toImage(self, size, pixel_size=None, ifftshift=False):
        ''' Generate a simulated ctf image
        
        Args:
            size: An `int`. The size of output. 
            pixel_size: A `float`. The pixel size of the ctf
            ifftshift: A `bool`. Shift the zero frequency to corner,
                    so it could be directly apply to fft of image
        Returns:
            A `EMImage`. 
    
        '''

        from cryofilia.EMImage import EMImage
        
        # caching the result
        key = (size, pixel_size)
        if key in self._images:
            return self._images[key]
        if pixel_size is None:
            raise Exception("Pixel size must be specified")
        
        r2, angle = self._radius(size, return_angle=True, square=True)
        defocus = (self.defocusU + self.defocusV) / 2
        deltadef = (self.defocusU - self.defocusV) / 2
        theta = self.defocusAngle * np.pi / 180
        df = defocus + deltadef * np.cos(2 * (angle + theta))
        wl = self.wavelength
        cs = self.cs
        B = self.bfactor
        alpha = self.alpha
        amplitude_contrast = self.amplitude_contrast
        f0 = 1. / (size * pixel_size)
        k2 = -df * pi * wl  * f0 * f0
        k4 = pi / 2 * cs * wl * wl * wl * 1e7 * f0 * f0 * f0 * f0
        kr = f0 * f0 * B
        
        if cs == 0:
            h = np.sin(k2*k2-alpha)
        else:
            h = np.sin(k2*r2 + k4 * r2 * r2 - alpha)
        h = (1-amplitude_contrast) * h - amplitude_contrast * np.sqrt(1-h * h) 
        h *= np.exp(-kr * r2)
        if ifftshift:
            h = np.fft.ifftshift(h)
        return EMImage(h)
    
    def _radius(self, size, return_angle=False, square=True):
        X,Y = np.mgrid[-(size/2): (size+1)/2, -(size/2):(size+1)/2]
        r = X * X + Y * Y
        if not square:
            r = np.sqrt(r)
        if not return_angle:
            return r
        angle = np.arctan2(Y, X)
        return r, angle
    
    @property
    def amplitude_contrast(self):
        return self["AmplitudeContrast"]
    
    @amplitude_contrast.setter
    def amplitude_contrast(self, value):
        self["AmplitudeContrast"] = value
    
    @property
    def defocusU(self):
        return self["DefocusU"]
    
    @defocusU.setter
    def defocusU(self, value):
        self["DefocusU"] = value
        
    @property
    def defocusV(self):
        return self["DefocusV"]
    
    @defocusV.setter
    def defocusV(self, value):
        self["DefocusV"] = value
    
    @property
    def defocusAngle(self):
        return self["DefocusAngle"]
    
    @defocusAngle.setter
    def defocusAngle(self, value):
        self["DefocusAngle"] = value
            
    @property
    def cs(self):
        return self["SphericalAberration"]
    
    @cs.setter
    def cs(self, value):
        self["SphericalAberration"] = value   
        
    @property
    def bfactor(self):
        return self["CtfBfactor"]
    
    @bfactor.setter
    def bfactor(self, value):
        self["CtfBfactor"] = value   
    
    @property
    def wavelength(self):
        wl = self._EWavelength(self["Voltage"])
        return wl
        
    def _EWavelength(self, v):
        return 12.2639 / sqrt(v * 1000 + 0.97845 * v * v)
        
