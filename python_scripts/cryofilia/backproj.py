'''
Created on Jun 21, 2017

@author: xueqi
'''
import numpy as np
from cryofilia.EMImage import EMImage, TestImage
from cryofilia.euler import Euler

class EMBackProjector(object):
    ''' Back projector using Numpy
    '''
    def __init__(self, boxSize = None, paddingFactor = 1):
        '''
        
        '''
        self.weight = None
        self.paddingFactor = paddingFactor
        self.boxSize = boxSize
        
    def backproject(self, ptcls, ptclInfos):
        ''' back projection with ptcls
        @param ptcls: EMImage list of particles
        '''
        
        if self.boxSize is None:
            self.boxSize = ptcls[0].xsize
        
        resultVol = EMImage(self.boxSize, self.boxSize, self.boxSize, isFFT = True)
        weights = EMImage(self.boxSize, self.boxSize, self.boxSize)
        for idx in range(len(ptcls)):
            ptcl = ptcls[idx]
            info = ptclInfos[idx]
            phi, theta, psi = info.phi, info.theta, info.psi
            sx, sy = info.shx, info.shy
            defu, defv, defangle = info.defocusU, info.defocusV, info.defocusAngle
            ptclfft = ptcl.copy()
            ptclfft.isFFT = True
            ptclfft.fourier_shift(sx, sy)
            if self.do_ctf:
                self.applyCtf(ptclfft, defu, defv, defangle)
                
            self.insertSlice(resultVol, weights, ptcl, (phi, theta, psi))
        
        return resultVol
    
    def applyCtf(self, ptclfft, defu, defv, defangle):
        from cryofilia.ctf import CTF
        
    
    def insertSlice(self, v, w,  ptclfft, angle, Mweight=None):
        ''' Insert ptcl fft in to central slice of volume(fft)
        Mweight is the weight in fft space.
        '''
        phi, theta, psi = angle
        box_size = v.xsize
        cx, cy, cz = box_size / 2, box_size / 2, box_size / 2
        vol = v.fftdata
        Y, X = TestImage._index([box_size, box_size])
        r = TestImage._radius([box_size, box_size])
        idxes = r.data < self._fourierRadius
        A = Euler.angle2matrix(phi, theta, psi)
        A = A.T
        X, Y = X[idxes], Y[idxes]
        x = A[0,0] * X + A[0,1] * Y
        y = A[1,0] * X + A[1,1] * Y
        z = A[2,0] * X + A[2,1] * Y
        dt = ptclfft.data[Y, X]
        wt = 1 # Get wt from Mweight
        x0 = np.floor(x).astype(np.int32)
        y0 = np.floor(y).astype(np.int32)
        z0 = np.floor(z).astype(np.int32)
        
        fx,fy,fz = x - x0, y-y0, z-z0
        mfx, mfy, mfz = 1. - fx, 1. - fy, 1. - fz

        d000 = mfz * mfy
        d001 = d000 * fx
        d000 *= mfx
        d010 = mfz * fy
        d011 = d010 * fx
        d010 *= mfx
        d100 = fz * mfy
        d101 = d100 * fx
        d100 *= mfx
        d110 = fz * fy
        d111 = d110 * fx
        d110 *= mfx
        x0 += cx
        y0 += cy
        z0 += cz
        x1, y1, z1 = x0+1, y0+1, z0+1
        vol[z0, y0, x0] += d000 * dt
        vol[z0, y0, x1] += d001 * dt
        vol[z0, y1, x0] += d010 * dt
        vol[z0, y1, x1] += d011 * dt
        vol[z1, y0, x0] += d100 * dt
        vol[z1, y0, x1] += d101 * dt
        vol[z1, y1, x0] += d110 * dt
        vol[z1, y1, x1] += d111 * dt
        
        #weight[z0, y0, x0] += d000 * wt

    def reconstruct(self):
        ''' Reconstruct to real space
        '''    
        
        
    
        
