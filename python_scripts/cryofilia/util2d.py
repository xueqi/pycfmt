import numpy as np
from cryofilia.util3d import write_vol

def ramp(input_image):
    """As per the excellent description of the analogous function from SPIDER (RAmp): 
       'Fits a least-squares plane to the picture, and subtracts the plane from the picture.
        A wedge-shaped overall density profile can thus be removed from the picture.'
    """

    sz = input_image.shape
    x,y = np.mgrid[0:sz[0], 0:sz[1]]

    mat = [ [np.average(x.flatten()**2), \
              np.average(x.flatten()*y.flatten()), \
               np.average(x.flatten())], \
            [np.average(x.flatten()*y.flatten()), \
              np.average(y.flatten()**2), \
               np.average(y.flatten())], \
            [np.average(x.flatten()), \
              np.average(x.flatten()), \
               1]]
    vec = [ np.average(x.flatten() * input_image.flatten()), \
            np.average(y.flatten() * input_image.flatten()), \
            np.average(input_image.flatten())]

    coeffs = np.linalg.inv(mat).dot(vec)
    z = coeffs[0]*x + coeffs[1]*y + coeffs[2]
    return input_image - z;

def peakfind1d(yvals):
    """Peak finding of a 1d data series with 3-point paraboloic fitting.

    Returns four values:
    x_int: integer peak x-coordinate
    x_fit: fitted peak x-coordinate
    y_fit: fitted peak y-value
    half_curvature: half-curvature of the parabolic fit

    Parabolic fit in this function snarfed from:
    QINT - quadratic interpolation of three adjacent samples
    Smith, J.O. Spectral Audio Signal Processing,
    http://ccrma.stanford.edu/~jos/sasp/, online book,
    2011 edition, accessed 8/13/2016
      [p,y,a] = qint(ym1,y0,yp1) 
      returns the extremum location p, height y, and half-curvature a
    of a parabolic fit through three points. 
    Parabola is given by y(x) = a*(x-p)^2+b, 
    where y(-1)=ym1, y(0)=y0, y(1)=yp1. 
    """

    x_int = np.argmax(yvals)
    if(x_int == 0):
        x_int = 1
        return 0, 0, yvals[0]
    if(x_int == yvals.shape[0] - 1):
        return x_int, x_int, yvals[x_int]
        x_int = yvals.shape[0] - 2

    mini_yvals = yvals[x_int-1:x_int+2]

    dx = (mini_yvals[2] - mini_yvals[0])/(2*(2*mini_yvals[1] - mini_yvals[2] - mini_yvals[0])); 
    y_fit = mini_yvals[1] - 0.25*(mini_yvals[0] - mini_yvals[2]) * dx;
    half_curvature = 0.5*(mini_yvals[0] - 2*mini_yvals[1] + mini_yvals[2]);

    x_fit = x_int + dx
    return x_int, x_fit, y_fit, half_curvature


def peakfind2d(zvals):
    """Peak finding of a 1d data series with 3-point parabolic fitting.

    Returns three values:
    x_fit: fitted peak x-coordinate
    y_fit: fitted peak y-coordinate
    z_fit: fitted peak z-value

    Purloined from SPIDER (Frank et al.) source code, 'parabl.f'
    CVS 08/2016

    PARABL  9/25/81 : PARABOLIC FIT TO 3 BY 3 PEAK NEIGHBORHOOD
    
    THE FORMULA FOR PARABOLOID TO BE FITTED INTO THE NINE POINTS IS:
    
    F = C1 + C2*Y + C3*Y**2 + C4*X + C5*XY + C6*X**2
    
    THE VALUES OF THE COEFFICIENTS C1 - C6 ON THE BASIS OF THE
    NINE POINTS AROUND THE PEAK, AS EVALUATED BY ALTRAN:
    """
    
    x_int = np.argmax(zvals, axis=None)
    xy_int = np.asarray(np.unravel_index(x_int, zvals.shape))
    if(xy_int[0] == 0):
        xy_int[0] = 1
    if(xy_int[1] == 0):
        xy_int[1] = 1
    if(xy_int[0] == zvals.shape[0] - 1):
        xy_int[0] = zvals.shape[0] - 2
    if(xy_int[1] == zvals.shape[1] - 1):
        xy_int[1] = zvals.shape[1] - 2

    mini_zvals = zvals[xy_int[0]-1:xy_int[0]+2, xy_int[1]-1:xy_int[1]+2]

    c1 = (26.*mini_zvals[0,0] - mini_zvals[0,1] + 2*mini_zvals[0,2] - \
              mini_zvals[1,0] - 19.*mini_zvals[1,1] - 7.*mini_zvals[1,2] + \
              2.*mini_zvals[2,0] - 7.*mini_zvals[2,1] + 14.*mini_zvals[2,2])/9.

    c2 = (8.* mini_zvals[0,0] - 8.*mini_zvals[0,1] + 5.*mini_zvals[1,0] - \
              8.*mini_zvals[1,1] + 3.*mini_zvals[1,2] + 2.*mini_zvals[2,0] - \
              8.*mini_zvals[2,1] + 6.*mini_zvals[2,2])/(-6.)

    c3 = (mini_zvals[0,0] - 2.*mini_zvals[0,1] + mini_zvals[0,2] + \
              mini_zvals[1,0] -2.*mini_zvals[1,1] + mini_zvals[1,2] + \
              mini_zvals[2,0] - 2.*mini_zvals[2,1] + mini_zvals[2,2])/6.

    c4 = (8.*mini_zvals[0,0] + 5.*mini_zvals[0,1] + 2.*mini_zvals[0,2] - \
              8.*mini_zvals[1,0] -8.*mini_zvals[1,1] - 8.*mini_zvals[1,2] + \
              3.*mini_zvals[2,1] + 6.*mini_zvals[2,2])/(-6.)

    c5 = (mini_zvals[0,0] - mini_zvals[0,2] - mini_zvals[2,0] + mini_zvals[2,2])/4.

    c6 = (mini_zvals[0,0] + mini_zvals[0,1] + mini_zvals[0,2] - \
              2.*mini_zvals[1,0] - 2.*mini_zvals[1,1] - 2.*mini_zvals[1,2] + \
              mini_zvals[2,0] + mini_zvals[2,1] + mini_zvals[2,2])/6.

# The peak coordinates of the paraboloid can now be evaluated as:

    ysh=0.
    xsh=0.
    denom=4.*c3*c6 - c5*c5
    if(denom != 0):
        ysh=(c4*c5 - 2.*c2*c6) /denom-2.
        xsh=(c2*c5 - 2.*c4*c3) /denom-2.
        z_fit= 4.*c1*c3*c6 - c1*c5*c5 -c2*c2*c6 + c2*c4*c5 - c4*c4*c3
        z_fit=z_fit/denom
# Limit interplation to +/- 1. range
        if(ysh < -1.):
            ysh = -1.
        if(ysh > 1.):
            ysh = 1.
        if(xsh < -1.):
            xsh = -1.
        if(xsh > 1.):
            xsh = 1.

    y_fit = xy_int[0] + xsh - zvals.shape[0]/2
    x_fit = xy_int[1] + ysh - zvals.shape[1]/2
    if denom < 0.1:
        return x_fit, y_fit, mini_zvals[1,1]
    else:
        return x_fit, y_fit, z_fit


