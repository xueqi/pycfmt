'''
Created on May 19, 2017

@author: xueqi
'''

import os


from chuff.util.project import get_last_workdir, get_next_workdir
from chuff.util.relion_prog import * 
from functools import partial
from cryofilia.EMImage import EMImage
import numpy as np

def _r(path):
    return os.path.splitext(path)[0]
def _t(path):
    return os.path.basename(path)
def _b(path):
    return _r(_t(path))

def main():
    os.chdir('/home/xueqi/proj/mt_sync.bak')
    # get workdir
    refine_dim = 256
    vol_dim = [refine_dim, refine_dim, refine_dim]
    box_dim = [refine_dim, refine_dim]
    workdir = get_last_workdir("RelionRefine")
    _d = partial(os.path.join, workdir)
    _e = os.path.exists
    # input: 
    input_star = "RefAlign/job019/particles_out_relion_14.star"
    ctf_star = "CtfFind/job010/micrographs_ctf1.star"
    voxel_size = 2.45
    num_pfs = 14
    symm = [120, 0]
    
    # extract particles
    ptcl_star = _d('data.star')
    if not _e(ptcl_star):
        relion_extract(reextract_data_star = input_star, part_star = ptcl_star,
                   recenter = True, i = ctf_star, part_dir = workdir,
                   invert_contrast = True, extract_size = box_dim[0])
    
    # relion reconstruct
    outvol = _d("vol_000.mrc")
    #if not _e(outvol):
    if 1:
        reconstruct(ptcl_star, outvol,
                     symm = symm,
                     rise_search = [110, 130], twist_search = [-5, 5],
                     pixelSize = 2.45)
    # loop
    iter = 1
    lastIter = iter - 1
    bname = "cfa"
    while True:
        # mask center
        maski = _d("maski_%03d.mrc" % lastIter)
        if not _e(maski):
            helical_rise, helical_twist = symm
            get_mask_inverse(vol_dim, maski, voxel_size, 
                             helical_rise, helical_twist)
        exit()
        # subtact ptcls
        last_star_subtract = _d("%s_sub.star" % bname)
        out_substar = relion_project(i = outvol, o = last_star_subtract,
                                     subtract_exp = True, 
                                     mask = maski, 
                                     ctf = True 
                                      )
        # relion refine
        this_star = _d("%s_it%03d.star" % (bname, iter))
        relion_refine(i = last_star_subtract,
                      o = this_star,
                      )
        # symmetrize volume
        from cryofilia.helix.filament.filament import symm_mt_star
        from relion import Metadata
        m = Metadata.readFile(this_star)
        symm_star = symm_mt_star(m, )
        relion_reconstruct(i = symm_star, o = outvol, ctf = True)
        
        # rebuild seam
        
        vol = rebuild_seam()

def reconstruct(instar, outvol, symm = None, 
                pixelSize = None, rise_search = None, twist_search = None):
    
    # reconstruct
    if not os.path.exists(outvol): 
        relion_reconstruct(i = instar, o = outvol, ctf=True)
    # get symmetry
    
    search_helical_symm(outvol, symm, pixelSize = pixelSize, outer_radius = 150,
                        rise_search = rise_search,
                        twist_search = twist_search)
    impose_helical_symm(outvol, symm, pixelSize = pixelSize)

def search_helical_symm(invol, symm, pixelSize, outer_radius = None,
                        rise_search = None, twist_search = None
                        ):
    
    if outer_radius is None:
        m = EMImage(invol)
        outer_radius = m.get_xsize() / 2 - 1
    
    if rise_search is None: rise_search = [symm[0] - 2, symm[0] + 2]
    if twist_search is None: twist_search = [symm[1] - 5, symm[1] + 5]
    
    relion_helical_toolbox = partial(relion_command, 'relion_helix_toolbox')

    output = relion_helical_toolbox(i = invol, search = True,
                           cyl_outer_diameter = outer_radius * 2,
                           angpix = pixelSize,
                           rise = symm[0],
                           twist = symm[1],
                           rise_min = rise_search[0],
                           rise_max = rise_search[1],
                           twist_min = twist_search[0],
                           twist_max = twist_search[1], 
                           z_percentage = 0.6)
    pat = 'helical rise = ([\-\d\.]+) Angstroms, twist = ([\-\d\.]+) degrees.'
    import re
    mt = re.search(pat, output)
    if mt:
        symm[0] = float(mt.group(1))
        symm[1] = float(mt.group(2))
        
    else:
        print output
        print "regex wrong: %s" % pat, "symmetry did not changed"
    return symm

def get_mask_inverse(mask_dim, maskvol, voxel_size, helical_rise, helical_twist):
    
    from cryofilia.helix.basics import helical_wedge_mask
    
    print "getting mask"
    num_starts = 1.5
    outer_radius = 200
    v_dim = np.array(mask_dim)
    
    
    mask = helical_wedge_mask(v_dim, 
                               ref_com = (-1,0,0),
                               voxel_size = voxel_size,
                               subunit_num = 1,
                               num_pfs = 1,
                               num_starts = num_starts,
                               rise_per_subunit = helical_rise,
                               twist_per_subunit = 2,
                               outer_radius = outer_radius,
                               eulers = np.array([0,0,0]),
                               shifts = np.array([0,0,0]))
    
    EMImage(mask.T).write_image("%s_noi.mrc" % _r(maskvol))
        
if __name__ == '__main__':
    main()