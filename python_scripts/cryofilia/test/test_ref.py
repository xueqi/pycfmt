from cryofilia.helix.ref_align import MtRefAlign
from relion import Metadata
rf = MtRefAlign()
rf.pixel_size = 5
rf.box_size = 128
rf.micrograph_pixel_size = 2.5
rf.write('ref_align.star', withComment = True)
#rf.add_reference(num_pf=14)
rf.use_gpu = True
rf.add_reference(volume = 'mt_14.mrc', num_pf = 14, voxel_size = rf.pixel_size)
a = rf.edgeMask
m = Metadata.readFile('particles.star')
tubes = m.group_by('MicrographName', 'HelicalTubeID')
for tube in tubes.values():
    rf.align(tube)
    tube.write("aligned.star")
