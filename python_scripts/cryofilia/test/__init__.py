from cryofilia.helix.filament.filament import _Filament
from cryofilia.helix.ref_align import  RefAlign
import os
from chuff.util.project import get_next_workdir
from cryofilia.helix.basics import get_helix_subunit_list
os.chdir("/home/xueqi/proj/mt_sync.bak")
wd = get_next_workdir("TTTT")
pixelSize = 1.5
boxsize = 400
print("making volume")

import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.StreamHandler())
logger.setLevel(logging.DEBUG)

tubefiles = [os.path.join(os.environ['chuff_dir'], 'data', 'kincomplex_apo8A_fit.pdb')]

m = _Filament(8.57, twist = 25.7)
m.read('particles.star')

ra = RefAlign()
ra.addFilament(m)
ra.add_reference(num_pf = 14, voxel_size =  pixelSize)
ra.add_reference(num_pf = 13, voxel_size =  pixelSize)
ra.pixel_size = pixelSize
ra.box_size = boxsize
import cProfile, pstats, StringIO
pr = cProfile.Profile()
pr.enable()
ra.run()
pr.disable()
s = StringIO.StringIO()
sortby = 'cumulative'
ps = pstats.Stats(pr, stream=s).sort_stats(sortby)
ps.print_stats()
print s.getvalue()
