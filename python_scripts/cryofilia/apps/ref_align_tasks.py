'''
Ref align tasks
@author: xueqi
'''

from chuff.util.apps.celery import app
from cryofilia.proj import EMProjector

pjs = {}
def get_projector(ref_file):
    if ref_file not in pjs:
        projector = EMProjector(ref_file)
        pjs[ref_file] = projector
@app.task
def align_single_mt(ref_file, mt, **kwargs):
    """ Align single microtubule
    """
    pj = get_projector(ref_file)
    
