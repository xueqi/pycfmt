'''
Created on Mar 23, 2018

@author: xueqi
'''
import cryofilia as cf
from cryofilia.project.parameter import MPIParameter

class StarInfo(MPIParameter):
    '''
    classdocs
    '''
    def __init__(self):
        '''
        Constructor
        '''
        super(StarInfo, self).__init__()
    
    def add_arguments(self):
        self.add_argument("input_star", required=True)
    
        super(StarInfo, self).add_arguments()
    
    def run(self):
        from relion import Metadata
        m = Metadata.readFile(self.input_star)
        if len(m) == 0:
            cf.info("Dont contain any ptcls")
        cf.info("Total %d entries" % len(m))
        ptcl = m[0]

        if ptcl.hasKey('Magnification') and ptcl.hasKey('DetectorPixelSize'):
            cf.info("pixel size: %.4f" % (10000. * ptcl['DetectorPixelSize']
                                          / ptcl['Magnification']))
        