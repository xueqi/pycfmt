'''
Created on Mar 23, 2018

@author: xueqi
'''
import os
import cryofilia as cf
from cryofilia.project.parameter import MPIParameter
from relion import Metadata
from chuff.util.apps import start_worker

class ModelMeta(Metadata):
    _labels = {
        "file" : ["ReferenceImage"],
        "rise" : ["HelicalRise"],
        "twist" : ["HelicalTwist"]
        }
    def __init__(self):
        super(ModelMeta, self).__init__("model_classes")
class SynthMicrotubule(MPIParameter):
    '''
    synthsize microtubule from pdbs
    '''
    def __init__(self):
        '''
        Constructor
        '''
        super(SynthMicrotubule, self).__init__()
    
    def add_arguments(self):
        self.add_argument("num_pf", nargs="*", type=int)
        self.add_argument("ref_pdb", nargs="+", type=str,
            help=" mt_pdb kin_pdb")
        self.add_argument("ref_num_pf", default=13)
        self.add_argument("voxel_size", type=float, required=True)
        self.add_argument("box_size", type=int, required=True)
        #self.add_argument("nuke", action="store_true")
        super(SynthMicrotubule, self).add_arguments()
        
    def start(self, *arg, **kwargs):
        start_worker()
        super(SynthMicrotubule, self).start(*arg, **kwargs)
        
    def run(self):
        from chuff.util.apps.tasks import synth_microtubule, ModelMeta
        ref_pdb = self.ref_pdb
        ref_num_pf = self.ref_num_pf
        
        if not ref_pdb:
            ref_pdb = None
            ref_num_pf = 13
        model = ModelMeta()
        syn_mt = synth_microtubule.s(voxel_size=self.voxel_size, box_size=self.box_size)
        def gen_args():
            for num_pf in self.num_pf:
                output_file = os.path.join(self.workdir, "mt_pf%d.mrc" % num_pf)
                yield((output_file, num_pf), {"ref_num_pf":ref_num_pf})
        result = self.parallel_job(syn_mt, gen_args())
        for r in result:
            model.append(r)
        model.write(os.path.join(self.workdir, "models.star"))
        cf.info("Model star file: %s" % (os.path.join(self.workdir, "models.star")))
        cf.info("Done")
        
    def parallel_job(self, func, list_of_args, progress_bar=False):
        async_r = []
        for args, kwargs in list_of_args:
            r = func.delay(*args, **kwargs)
            async_r.append(r)
        for r in async_r:
            yield r.get()
                   