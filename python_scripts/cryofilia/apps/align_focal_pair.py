import os
from relion import MPIParameter, Metadata
from chuff.util.project import OldCF
from cryofilia.EMImage import EMImage

class MicrographImage(EMImage):
    def __init__(self, *args, **kwargs):
        super(MicrographImage, self).__init__(*args, **kwargs)
    
    def removeOutliers(self, sigma=5):
        ''' Remove outliers
        :param sigma: The amount of stdv that consider as outliers
        :return: self. Inplace ops
        '''
        import numpy as np
        std = np.std(self.data)
        avg = np.mean(self.data)

class AlignmentData(Metadata):
    _labels = {
            "dx" : ["OriginX"],
            "dy" : ["OriginY"],
            "angle" : ["AnglePsi"],
            "scale" : ["scale", float, "Scaling factor to match the original image"],
            "ld_mg" : ["MicrographName"],
            "hd_mg" : ["micrographNameHD", str, "High defocus MicrographName"],
            "hd_mg_align"   : ["micrographNameHDAlign", str, "High defocus MicrographName aligned"],
            "ld_d1" : ["DefocusU"],
            "ld_d2" : ["DefocusV"],
            "ld_dangle" : ["DefocusAngle"],
            "hd_d1" : ["defocusUHD", float, "High defocus 1"],
            "hd_d2" : ["defocusVHD", float, "high defocus 2"],
            "hd_dangle" : ["defocusAngleHD", float, "high defocus angle"],
            }

AlignmentData()
class CtfMeta(Metadata):
    _labels = {
            "d1" : ["DefocusU"],
            "d2" : ["DefocusV"],
            "dangle"    : ["DefocusAngle"],
            }
CtfMeta()
class DefocusPair(Metadata):
    _labels = {
            "ld_mg" : ["MicrographName"],
            "hd_mg" : ["micrographNameHD", str, "High defocus Micrograph Name"],
            }
DefocusPair()
class AlignFocalPairParameter(MPIParameter):
    '''
    '''
    def __init__(self):
        super(AlignFocalPairParameter, self).__init__()
    
    def add_arguments(self):
        self.add_argument("input_star", widget="FileChooser", required=True,
                help="Input star file with low defocus and high defocus")
        self.add_argument("output_star", 
                help = "output star file")
        self.add_argument("ctf_star", widget="FileChooser", required=True,
                help="ctf_star file")
        self.add_argument("simple", action="store_true",
                help="simple align. No rotation and scaling")
        self.add_argument("debug", action="store_true",
                help="debug option. Do not use")
        
        self.add_argument("max_iter", type=int, default="",
                help="Maximum iterations", group="not_used")
        
        super(AlignFocalPairParameter, self).add_arguments()

class AlignFocalPair(AlignFocalPairParameter, OldCF):
    ''' Align high defocus image to low defocus image. 
    '''
    def __init__(self):
        super(AlignFocalPair, self).__init__()
        OldCF.__init__(self)

    def run(self):
        if self._rank == 0:
            self.workdir = self.workdir or self.get_next_workdir()
            self.create_dirs()
        self.workdir = self.bcast(self.workdir)

        mgs = DefocusPair.readFile(self.input_star)
        ctfs = CtfMeta.readFile(self.ctf_star)
        ctfs = ctfs.group_by("MicrographName")
        m = AlignmentData()
        if not self.debug:
            results = self.parallel_job(self.align_one, self.get_arguments(mgs, ctfs))
        else:
            results = []
            for args, kwargs in self.get_arguments(mgs, ctfs):
                results.append(self.align_one(*args, **kwargs))

        for result in results:
            m.append(result)
        m.write(self.path("focal_pair_aligned.star"))
            
    def get_arguments(self, mgs, ctfs):
        for i in range(len(mgs)):
            mg = mgs[i]
            ctf_l = ctfs[mg.ld_mg][0]
            ctf_h = ctfs[mg.hd_mg][0]
            if ctf_l.d1 < 0. or ctf_l.d2 < 0. or ctf_h.d1 < 0. or ctf_h.d2 < 0.:
                continue
            yield ((mg, i + 1, ctfs[mg.ld_mg][0], ctfs[mg.hd_mg][0]), {})
    
    def align_one_p(self, ld, hd):
        ''' Align high defocus image to low defocus image
            The input should have 
                MicrographName, DefocusU, DefocusV, DefocusAngle columns
            
            The following fields should be set for AlignFocalPair instance:
                pixel_size, filter_resolution, n_iterations,
                angle_spec, scale_spec, rot_bin_factor, scale_bin_factor,
                supersample
        :param ld: The low defocus image. 
        :param hd: The high defocus image. 
        '''
        ldname = ld.mg_name
        hdname = hd.mg_name
        ld_mg = EMImage(ldname)
        hd_mg = EMImage(hdname)
        ld_mg.removeOutliers()
        hd_mg.removeOutliers()
    def align_one(self, mg, mg_id, ld_ctf, hd_ctf):
        ''' 
        '''
        mg_name = "%04d" % mg_id
        mat_result = os.path.join(self.scan_dir, '%s_focal_align_pair.mat' % mg_name)
        ld_mgname = mg.ld_mg
        hd_mgname = mg.hd_mg
        pwd = os.getcwd()
        if not os.path.exists(mat_result):

            if not os.path.exists(ld_mgname):
                print("Warnning: low defocus micrograph %s does not exists" % ld_mgname)
                return
            if not os.path.exists(hd_mgname):
                print("Warnning: high defocus micrograph %s does not exists" % hd_mgname)
                return
            if os.path.exists(os.path.join(self.scan_dir, "%s.mrc" % mg_name)) and os.path.islink(os.path.join(self.scan_dir, "%s.mrc" % mg_name)):
                os.remove(os.path.join(self.scan_dir, "%s.mrc" % mg_name))
            os.symlink(os.path.realpath(ld_mgname), os.path.join(self.scan_dir, "%s.mrc" % mg_name))
            if os.path.exists(os.path.join(self.scan_dir, "%s_focal_mate.mrc" % mg_name)) and os.path.islink(os.path.join(self.scan_dir, "%s_focal_mate.mrc" % mg_name)):
                os.remove(os.path.join(self.scan_dir, "%s_focal_mate.mrc" % mg_name))
            os.symlink(os.path.realpath(hd_mgname), os.path.join(self.scan_dir, "%s_focal_mate.mrc" % mg_name))

            # create the defocus file
            open(os.path.join(self.scan_dir, "%s_ctf_doc.spi" % mg_name), 'w').write(
                    '1 3 %.4f %.4f %.4f\n' % (ld_ctf.d1, ld_ctf.d2, ld_ctf.dangle))
            open(os.path.join(self.scan_dir, "%s_focal_mate_ctf_doc.spi" % mg_name), 'w').write(
                    '1 3 %.4f %.4f %.4f\n' % (hd_ctf.d1, hd_ctf.d2, hd_ctf.dangle))
            
            # run align focal pair

            import subprocess
            os.chdir(self.workdir)
            if self._rank == 0:
                stdout = None
                stderr = None
            else:
                stdout=open('logs/%s.out' % mg_name, 'w')
                stderr=open('logs/%s.err' % mg_name, 'w')
        
            args = ['chumps_align_focal_pairs', 'scans/%s.mrc' % mg_name]
            if not self.simple:
                args.append("simple=0")
            if self.nuke:
                args.append("nuke=1")
            print " ".join(args)
            p = subprocess.Popen(args, stdout = stdout, stderr=stderr)
            p.communicate()
            os.chdir(pwd)
            
            # get the aligned image
        aligned_image  = os.path.join(self.scan_dir, "%s_focal_mate_align.mrc" % mg_name)
        aligned_image = os.path.relpath(aligned_image, pwd)
        
        import scipy.io as sio
        result = sio.loadmat(mat_result)
        rotation = result['best_angle'][0,0]
        scale = result['best_scale'][0,0]
        dx = result['dx'][0,0]
        dy = result['dy'][0,0]
        result = AlignmentData().newObject()
        result.ld_mg = ld_mgname
        result.hd_mg = hd_mgname
        result.hd_mg_align = aligned_image
        result.ld_d1 = ld_ctf.d1
        result.ld_d2 = ld_ctf.d2
        result.ld_dangle = ld_ctf.dangle
        result.hd_d1 = hd_ctf.d1
        result.hd_d2 = hd_ctf.d2
        result.hd_dangle = hd_ctf.dangle
        result.angle = rotation
        result.scale = scale
        result.dx = dx
        result.dy = dy

        return result
