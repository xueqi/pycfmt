'''
Created on May 18, 2017

@author: xueqi
'''
from relion import Container, addLabel
from cryofilia.EMImage import EMImage
from __builtin__ import float

addLabel("correlation", float, "cross correlation coefficient")

class _Particle(Container):
    '''
        Everything here is in pixels with bin 1 relative to micrograph
    '''
    def __init__(self):
        super(_Particle, self).__init__()
        self._micrograph = None # the micrograph it belongs to
        self._image = None      # The image instance
        self._size = [256,256]
        self._inverse = True
        
        self.update({
            "CoordinateX"   : 0.0,
            "CoordinateY"   : 0.0,
            "OriginX"       : 0.0,
            "OriginY"       : 0.0,
            "AnglePsi"      : 0.0,
            "AngleRot"      : 0.0,
            "AngleTilt"     : 0.0,
            })
        
        self._images = {} # key is (boxsize, pixelsize)
    
    def getImage(self, pixelSize = None, boxsize = None):
        if pixelSize is None or boxsize is None: return self.image

        try: boxsize = list(boxsize)
        except: boxsize = [boxsize, boxsize]
        if (tuple(boxsize), pixelSize) in self._images: 
            return self._images[(tuple(boxsize), pixelSize)]
        
        scale = self.pixelSize / pixelSize
        newboxsize = [bs / scale for bs in boxsize]
        image = self.micrograph.box([int(_z) for _z in self.center], newboxsize)
        image.resample(scale = scale)
        if self._inverse: image.inverse()
        image.normalize()
        self._images[(tuple(boxsize), pixelSize)] = image
        return image    
    
    ########### Alignment ###################
    def alignShiftWithImage(self, other, x_range = None, y_range = None, 
                            useCCC = True, pixelSize = None, boxsize = None,
                            searchMask = None, ccmask = None):
        '''
            align with image other, return the best fit shift, and correlation
            real space input, 
        '''
                
        assert(type(other) is EMImage)
        
        if pixelSize is None or boxsize is None:
            assert(self.image.get_xsize() == other.get_xsize())
            image = self.image
        else:
            image = self.getImage(pixelSize = pixelSize, boxsize = boxsize)
        #image.rotate2d(self.psi)
        corrmap = image.corrmap(other, fftshift = True)
        x_fit, y_fit, ccc = corrmap.peakfind2d(mask = searchMask)

        if useCCC:  # use coefficient instead of correlation
            shift_img = image.copy()
            shift_img.fourier_shift(x_fit, y_fit)
            ccc = shift_img.correlation(other, mask = ccmask)

        return x_fit, y_fit, ccc
    
    ################# Properties ########################
    ## for easy access ##
    @property
    def size(self):
        return self._size
    @property
    def center(self):
        return [self['rlnCoordinateX'], self['rlnCoordinateY']]
    
    @property
    def micrograph(self):
        if self._micrograph is None:
            if 'rlnMicrographName' in self:
                mgname = self['rlnMicrographName']
                self._micrograph = EMImage(mgname)
        
        return self._micrograph
    
    @property
    def image(self):
        if self._image is None:
            self._image = self.micrograph.box(self.center, self.size)
            self._image.inverse()
        return self._image
    
    @image.setter
    def image(self, value):

        if isinstance(value, EMImage):
            self._image = value
        else:
            raise Exception("Don't support image type: %s" % type(value))
    
    @property
    def pixelSize(self):
        return self['rlnDetectorPixelSize'] * 10000. / self['rlnMagnification']
    
    @property
    def x(self):
        return self['rlnCoordinateX']
    
    @property
    def shx(self):
        return self['rlnOriginX']
    
    @shx.setter
    def shx(self, value):
        self['rlnOriginX'] = value
    
    @property
    def shy(self):
        return self['rlnOriginY']
    
    @shy.setter
    def shy(self, value):
        self['rlnOriginY'] = value
    
    @property
    def correlation(self):
        if not "rlncorrelation" in self:
            return 0.
        return self['rlncorrelation']
    
    @correlation.setter
    def correlation(self, value):
        self['rlncorrelation'] = value
    
    @property
    def psi(self):
        if 'rlnAnglePsi' in self:
            return self['rlnAnglePsi']
        else: return None
    
    @psi.setter
    def psi(self, value):
        self['rlnAnglePsi'] = value % 360
    
    @property
    def theta(self):
        if 'rlnAngleTilt' in self:
            return self['rlnAngleTilt']
        else: return None
    
    @theta.setter
    def theta(self, value):
        self['rlnAngleTilt'] = value % 360
    
    @property
    def phi(self):
        if 'rlnAngleRot' in self:
            return self['rlnAngleRot']
        else: return None
    
    @phi.setter
    def phi(self, value):
        self['rlnAngleRot'] = value % 360
Particle = _Particle


    