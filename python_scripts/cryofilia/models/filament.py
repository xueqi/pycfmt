'''Filament model.

Filament contains list of particles.
All coordinate and shift are in angstroms. convert to pixel before use
'''
import numpy as np

from relion import Metadata
class Filament(Metadata):
    _labels = {
        'x'         : ['CoordinateX'],
        'y'         : ['CoordinateY'],
        'z'         : ['CoordinateZ'],
        'shx'       : ['OriginX'],
        'shy'       : ['OriginY'],
        'shz'       : ['OriginZ'],
        'phi'       : ['AngleRot'],
        'theta'     : ['AngleTilt'],
        'psi'       : ['AnglePsi'],
        'pf'        : ["pf", int, "protofilament number"],
        'repeat'    : ["repeat", int, "Repeat number"],
        "fila_id"   : ["HelicalTubeID"]
        }
    
    @staticmethod
    def new_like(other):
        return Filament(other.rise_per_subunit, other.twist_per_subunit,
                        num_pfs=other.num_pf,
                        num_starts=other.num_start,
                        filament_id=other._fila_id)
    
    def __init__(self, rise_per_subunit, twist_per_subunit,
                 num_pfs=1, num_starts=1, origin=None,
                 filament_id=1):
        """ 
        Args:
            rise_per_subunit: The rise per subunit along axis
            twist_per_subunit: The twist per subunit around axis. right hand twist
            num_pfs: Number of protofilament. usually 1 for None microtubule
            num_starts: Number of start. usually 1 for non microtubule, 1.5 for microtubule
            filament_id: A `int`. The id of the filament
        """
        super(Filament, self).__init__()
        self.rise_per_subunit = float(rise_per_subunit)
        self.twist_per_subunit = float(twist_per_subunit)
        self.num_pf = num_pfs
        self.num_start = float(num_starts)
        
        self.origin = origin
        if self.origin is None:
            self.origin = self.new_object()
            self.origin.theta = 90.
        self.matrix = np.array([[0, 1, 0], [0, 0, 1,], [1, 0, 0]])
        # axial_repeat_dist is distance between two repeats
        self.axial_repeat_dist = 0
        # twist_per_repeat is the twist between two repeats
        self.twist_per_repeat = 0
        # supertwist is how much of the twist between first pf and last pf + 1
        self.supertwist = 0
        self._fila_id = filament_id
        self.reset()
    
    def reset(self):
        """ Reset other parameters according to rise, twist, num_pfs, num_starts
        """
        from cryofilia.helix.basics import expand_helical_parameters
        self.axial_repeat_dist, self.twist_per_repeat, self.supertwist, _, _ = (
            expand_helical_parameters(self.rise_per_subunit,
                                      self.twist_per_subunit,
                                      self.num_pf,
                                      self.num_start)
            )

    def repeat_at_index(self, repeat_index):
        """ Get repeat at index. The same as get first subunit in the repeat
        Args:
            repeat_index: The repeat index in this filament
        Returns:
            repeat: A `Container`. The repeat object.
        """
        return self.subunit_at_index(repeat_index * self.num_pf)
    
    def subunit_at_index(self, subunit_index):
        """ Given a subunit number, get the subunit
        
        Args:
            subunit_index: An `int`. The index in the filament
        Returns:
            subunit: A `Container`. The subunit information 
        """
        repeat_num = subunit_index // self.num_pf
        pf_num = subunit_index % self.num_pf
        z_shift = repeat_num * self.axial_repeat_dist + pf_num * self.rise_per_subunit
        rot = repeat_num * self.twist_per_repeat + pf_num * self.twist_per_subunit
        
        subunit = self.origin.copy()
        subunit.repeat_num = repeat_num
        subunit.pf_num = pf_num
        vec = np.array([z_shift, 0, 0])
        vec = self._transform(vec)
        subunit.x += vec[0]
        subunit.y += vec[1]
        subunit.z += vec[2]
        subunit.phi += rot
        subunit.fila_id = self._fila_id
        return subunit
    
    def _transform(self, vec):
        return np.dot(self.matrix, vec)

    
class Microtubule(Filament):
    def __init__(self, rise, twist, num_pfs=13, num_starts=1.5):
        super(Microtubule, self).__init__(float(rise)/num_pfs * 1.5,
                                          float(twist * num_starts + 360)/num_pfs,
                                          num_pfs, num_starts)