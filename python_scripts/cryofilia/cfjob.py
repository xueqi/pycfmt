from chuff.util.mp import MP
from relion import Parameter
from chuff.util.command import build_commands
import argparse
class CFJobParameter(Parameter, MP):
    _labels = {
#        "script" : ["script", str, "Script to run. Should be in commands directory"],
        "nodes" : ['nodes', int, 'number of cpus to run'],
        "cores" : ["cores", int, "number of cores per job"],
            }
    def __init__(self):
        super(CFJobParameter, self).__init__()
        if not hasattr(self, 'ncpus'):
            setattr(self, 'ncpus', 1)

class CFJob(CFJobParameter):
    def __init__(self):
        super(CFJob, self).__init__()
        
    def run(self):
        import os
        import sys
        self.parallel_job(self.run_command, self.get_arguments())

    def get_arguments(self):
        parser = self.to_parser()
        parser.add_argument("command")
        parser.add_argument("job_dir", nargs="*")
        opt, unknown = parser.parse_known_args()
        cmd = opt.command
        job_dirs = opt.job_dir
        nargs = len(job_dirs)
        nodes = opt.nodes
        cores = opt.cores
        if nodes <= 0:
            nodes = 1
        if cores <= 0:
            cores = 1
        self.ncpus = cores * nodes
        for job_dir in job_dirs:
            cmd1 = [cmd, job_dir] + unknown
            yield(cmd1, {})

    def run_command(self, *args, **kwargs):
        ''' Run a command with subprocess
        '''
        cmd = build_commands(*args, **kwargs)
        import subprocess
        try:
            p = subprocess.Popen(cmd)
            p.communicate()
        except OSError as e:
            if e.errno == 2:
                print "No such command: %s" % cmd[0]
                self.print_command_list()

    def print_command_list(self):
        from chuff import chuff_dir
        import os
        import stat
        for cmdname in os.listdir("%s/commands" % chuff_dir):
            cmdpath = os.path.join(chuff_dir, "commands", cmdname)
            if (stat.S_IXUSR & os.stat(cmdpath).st_mode) and os.path.isfile(cmdpath):
                print cmdname
