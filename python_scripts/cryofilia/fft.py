'''
Created on Jul 13, 2017

@author: xueqi
'''
import numpy as np
class FFT(object):
    '''
    classdocs
    '''
    def __init__(self, use_gpu = False):
        '''
        Constructor
        '''
        self._use_gpu = use_gpu
        if self._use_gpu:
            import pycuda.autoinit
            self._plans = {}
    def fft(self, in_arr, out_arr = None, **kwargs):
        if out_arr is None:
            out_arr = np.zeros(in_arr.shape, np.complex64)
        if self._use_gpu:
            self._gpu_fft(in_arr, out_arr)
        else:
            out_arr = np.fft.fftn(in_arr)
        return out_arr
    
    def ifft(self, in_arr, out_arr = None, **kwargs):
        if out_arr is None:
            out_arr = np.zeros(in_arr.shape, np.complex64)
        if self._use_gpu:
            self._gpu_ifft(in_arr, out_arr)
        else:
            out_arr = np.fft.ifftn(in_arr)
        return out_arr
    
    def _gpu_fft(self, in_arr, out_arr, **kwargs):
        import pycuda.gpuarray as gpuarray
        from skcuda.fft import Plan, fft
        c_array = np.zeros(in_arr.shape, dtype=np.complex64)
        c_array[:] = in_arr[:]
        x_gpu = gpuarray.to_gpu(c_array)
        xf_gpu = gpuarray.empty(in_arr.shape, np.complex64)
        in_dtype = np.complex64
        out_dtype = np.complex64
        arg = (x_gpu.shape, in_dtype, out_dtype, 'forward')
        if arg in self._plans:
            plan = self._plans[arg]
        else:
            plan = Plan(x_gpu.shape, in_dtype, out_dtype)
            self._plans[arg] = plan
        # fft gives half size of r2c transform.
        fft(x_gpu, xf_gpu, plan)
        # expand to 
        out_arr[:] = xf_gpu.get()
        del plan, x_gpu, xf_gpu
    def _gpu_ifft(self, in_arr, out_arr, **kwargs):
        import pycuda.gpuarray as gpuarray
        from skcuda.fft import Plan, ifft
        x_gpu = gpuarray.to_gpu(in_arr.astype(np.complex64))
        xf_gpu = gpuarray.empty(in_arr.shape, np.complex64)
        in_dtype = np.complex64
        out_dtype = np.complex64
        arg = (x_gpu.shape, in_dtype, out_dtype, 'backward')
        if arg in self._plans:
            plan = self._plans[arg]
        else:
            plan = Plan(x_gpu.shape, in_dtype, out_dtype)
            self._plans[arg] = plan
        # fft gives half size of r2c transform.
        ifft(x_gpu, xf_gpu, plan)
        # expand to 
        out_arr[:] = xf_gpu.get().real
        del plan, x_gpu, xf_gpu
        