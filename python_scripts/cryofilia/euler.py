'''
Euler angle function

@author: xueqi
'''
import numpy as np

PI = np.pi

class Euler(object):
    '''
    Euler angle class
    '''
    def __init__(self, phi=None, theta=None, psi=None, matrix=None, mode="degree"):
        '''
        Constructor
        '''
        self._phi = phi
        self._theta = theta
        self._psi = psi
        self._matrix = matrix
        self.mode = mode
        if self._phi is not None and self.mode == "degree": self._phi *= PI/180
        if self._theta is not None and self.mode == "degree": self._theta *= PI/180
        if self._psi is not None and self.mode == "degree": self._psi *= PI/180

    def getx(self, angle = "_phi"):
        if getattr(self, angle) is None:
            if self._matrix is None:
                raise RuntimeError("No matrix provided")
            e1, e2 = self.mat2ang_r(self._matrix, return_euler = True)
            if e1._theta < PI and e1._theta >= 0:
                e2 = e1
                self._phi, self._theta, self._psi = e2._phi, e2._theta, e2._psi
        v = getattr(self, angle)
        if self.mode == "degree":
            return v * 180 / PI
        return v

    def setx(self, value, angle="_phi"):
        setattr(self, angle, value)
        self._matrix = None

    from functools import partial
    phi = property(partial(getx, angle="_phi"), partial(setx,angle="_phi"))
    theta = property(partial(getx,angle="_theta"), partial(setx,angle="_theta"))
    psi = property(partial(getx, angle="_psi"), partial(setx,angle="_psi"))
    
    @property
    def phi_r(self):
        if self.mode == "degree":
            return Euler.d2r(self.phi)
        return self.phi

    @property
    def theta_r(self):
        if self.mode == "degree":
            return Euler.d2r(self.theta)
        return self.theta
    @property
    def psi_r(self):
        if self.mode == "degree":
            return Euler.d2r(self.psi)
        return self.psi

    @property
    def matrix(self):
        if self._matrix is None:
            self._matrix = self.angle2matrix_r(self._phi, self._theta, self._psi)
        return self._matrix
    @matrix.setter
    def matrix(self, value):
        self._matrix = value
        self._phi, self._theta, self._psi = None, None, None

    @staticmethod
    def angle2matrix_r(phi, theta = None, psi = None):
        if theta is None:
            ca, sa = np.cos(phi), np.sin(phi)
            return np.array([[ca, -sa], [sa, ca]])
        else:
            a = np.zeros([3,3])
            ca,cb,cg = map(np.cos, (phi, theta, psi))
            sa,sb,sg = map(np.sin, (phi, theta, psi))
            cc, cs, sc, ss = cb*ca, cb*sa, sb*ca, sb*sa
            a[0,:] = [cg * cc - sg * sa, cg * cs + sg * ca, -cg * sb]
            a[1,:] = [-sg*cc - cg*sa, -sg*cs+cg*ca, sg*sb]
            a[2,:] = [sc, ss, cb]
            return a

    @staticmethod
    def angle2matrix(phi, theta = None, psi = None):
        ''' Convert rotation angle to rotation matrix. YZY rotation, in degree.
        ''' 
        if theta is None:
            phi = np.deg2rad(phi)
            return Euler.angle2matrix_r(phi)
        else:
            # 3d rotation
            phi, theta, psi = map(np.deg2rad, (phi, theta, psi))
            return Euler.angle2matrix_r(phi, theta, psi)
    
    @staticmethod
    def matrix2angle_r2(A):
        import numpy as np
        phi, theta, psi = Euler.matrix2angle_r(A)
        return phi + np.pi, -theta, psi+np.pi

    @staticmethod
    def matrix2angle_r(A):
        ''' Convert a rotation matrix to three angle, YZY rotation, in degree.
        A should be a valid rotation matrix.
        Reference: Richard P. Paul  Robot Manipulators: Mathematics, Programming and Control.
        MIT Press 1981. Page 68
        phi: p, theta: t, psi: s. cos: c, sin: s
        ['cs*ct*cp+ss*-sp', 'cs*ct*sp+ss*cp', 'cs*-st']
        ['-ss*ct*cp+cs*-sp', '-ss*ct*sp+cs*cp', '-ss*-st']
        ['st*cp', 'st*sp', 'ct']

        '''
        import numpy as np
        if not isinstance(A, np.ndarray):
            A=np.array(A)
        phi = np.arctan2(A[2,1], A[2,0])
        theta = np.arctan2(np.cos(phi) * A[2,0] + np.sin(phi) * A[2,1], A[2,2])
        psi = np.arctan2(-np.sin(phi)*A[0,0] + np.cos(phi) * A[0,1],
                -np.sin(phi)*A[1,0] + np.cos(phi)*A[1,1])
        if phi < 0: phi+=np.pi * 2
        if theta < 0: theta += np.pi * 2
        if psi < 0: psi+=np.pi * 2
        return phi, theta, psi
    @staticmethod
    def matrix2angle(A):
        phi, theta, psi = Euler.matrix2angle_r(A)
        return phi * 180 / np.pi, theta * 180 / np.pi, psi * 180 / np.pi
    @staticmethod
    def matrix2angle_2(A):
        phi, theta, psi = Euler.matrix2angle_r2(A)
        return phi * 180 / np.pi, theta * 180 / np.pi, psi * 180 / np.pi
    
    @staticmethod
    def mat2ang_r(A, return_euler=False):
        a1,a2 = Euler.matrix2angle_r(A), Euler.matrix2angle_r2(A)
        if return_euler:
            return Euler(*a1, mode='r'), Euler(*a2, mode='r')
        return a1,a2
    @staticmethod
    def mat2ang(A):
        return Euler.matrix2angle(A), Euler.matrix2angle_2(A)
    
    @staticmethod
    def d2r(d):
        return d / 180. * PI
    
    @staticmethod
    def r2d(r):
        return r * 180. / PI

    def to_string(self):
        return "phi: %.4f, theta: %.4f, psi: %.4f" % (self.phi, self.theta, self.psi)

    def __str__(self):
        return self.to_string()

    def transform(self, vec):
        """ Transform vec
        """
        return np.matmul(np.array(self.matrix), vec)
