import __main__ as main
import matplotlib
if hasattr(main, '__file__'):
    matplotlib.use('TkAgg')
else:
    matplotlib.use('Agg')
