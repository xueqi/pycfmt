'''Synthesize filament images, volumes, parameters, etc.
'''
import chuff as cf
from cryofilia.models.filament import Filament, Microtubule
from cryofilia.helix.filament.microtubule import _MicrotubuleVolume

def make_synth_filament(rise_per_subunit,
                        twist_per_subunit,
                        num_repeats,
                        num_pfs=1,
                        num_starts=1):
    """ Make a synthetic filament metadata
    
    Args:
        rise_per_subunit: A `float`. The helical symmetry distance along helical axis
        twist_per_subunit: A `float. The twist angle around the helical axis per subunit
        num_repeats: A `int`. The number of repeat to make.
            Index for center is 0, [-num_repeats // 2, (num_repeats-1)/2]
        num_pfs: A `int`. Number of protofilaments of the filament
        num_starts: A `float`. Number of starts of the filament.
    """
    # The filament model.
    filament = Filament(rise_per_subunit, twist_per_subunit,
                        num_pfs=num_pfs, num_starts=num_starts)
    for i in range(num_repeats):
        for j in range(num_pfs):
            subunit_index = i * num_pfs + j
            filament.append(filament.subunit_at_index(subunit_index))
    return filament
        
def make_synth_microtubule(rise,
                           twist,
                           num_repeats,
                           num_pfs=13):
    """ Make synthetic microtubule metadata
    """
    mt = Microtubule(rise, twist, num_pfs)
    for i in range(num_repeats):
        for j in range(num_pfs):
            subunit_index = i * num_pfs + j
            mt.append(mt.subunit_at_index(subunit_index))
    return mt

def make_syn_mt_vol(tubpdbfiles=[],
                num_pf=14,
                box_size=128,
                voxel_size=1.,
                ref_num_pf=None):
    """Make synthetic microtubule volume.
    
    Args:
        tubpdbfiles: A `list` of `str`. The input pdb list for microtubules.
            First in the list is microtubule dimer, with A/B chain represent alpha, beta tubulin.
            Second in the list is decorated pdb. (Optinal)
        num_pf: A `int`. Number of protofilament to make
        box_size: A `int`. Even number. The size in pixels.
        voxel_size: A `float`. The pixel size or voxel size of the volume.
        ref_num_pf: A `int`. The number of protofilament of the reference pdb.
            This is used to determine the tubulin location according to the reference pdb
    """
    from cryofilia.pdb_util import PDB
    pdb = None
    # Get default tubulin pdb if not provided.
    if len(tubpdbfiles) == 0:
        tubpdbfiles=[cf.DEFAULT_TUBULIN_PDB]
    
    for pdbfile in tubpdbfiles:
        p = PDB()
        p.read_file(pdbfile)
        if pdb is None: pdb=p
        else: pdb += p
    box_size = int(box_size)
    ref_com =  PDB.get_center(cf.DEFAULT_TUBULIN_PDB, gravity=True)
    mt = _MicrotubuleVolume.new(num_pf=num_pf, ref_pdb=pdb,
                         ref_num_pf=ref_num_pf, ref_com=ref_com)
    mt.set_subunit_PDB(pdb)
    vol = mt.to_volume(box_size=box_size, voxel_size=voxel_size)
    return vol

def make_syn_mt_kinesin_vol(tubpdbfiles=None, **kwargs):
    """ Make synthetic kinesin volume from pdb
    
    Args:
        tubepdbfiles: A `list` of `str`. The pdb files used to generate the volume
    """    
    if tubpdbfiles is None:
        tubpdbfiles = []
    if len(tubpdbfiles) == 0:
        tubpdbfiles = [cf.DEFAULT_TUBULIN_PDB, cf.DEFAULT_KINESIN_PDB]
        kwargs['ref_num_pf'] = 13
        kwargs['tubpdbfiles'] = tubpdbfiles
    return make_syn_mt_vol(**kwargs)
