'''
Created on Jun 22, 2017

@author: xueqi
'''
from relion import Container

class Micrograph(Container):
    '''
    One Micrograph
    '''
    def __init__(self, filename = ""):
        '''
        Constructor
        '''
        super(Micrograph, self).__init__()
        self.update({'MicrographName' : filename
                     })
    