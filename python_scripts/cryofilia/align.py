''' Aligner is an object that could align a particle.

Basic functionality: given a particle, get the best (ang, shft) for this ptcl.

Aligner:
1. given a set of angles, return the best angle and cross correlation(ccc).
2. given a set of angles, return all cross correlations for every angle.

Grid Aligner, inherit Aligner. Extra functions include:
1. given a initial angles, the number of angles to search, the delta angle,
    return either the best angle and ccc, or all the cccs and angles.

Result is returned as an AlignResultData instance,
    which is indirectly inherited from dict
'''

from relion import Parameter, Metadata, Container
from cryofilia.EMImage import EMImage
import numpy as np
import logging

logger = logging.getLogger(__name__)

class AlignResultData(Container):
    _labels = {
        'shx'   : ['OriginX'],
        'shy'   : ['OriginY'],
        'phi'   : ['AngleRot'],
        'theta' : ['AngleTilt'],
        'psi'   : ['AnglePsi'],
        'ccc' : ['crossCorrelation', float, 'cross correlation coefficient']
        } 
class Orientation(Metadata):
    _labels = {
        'x' : ['CoordinateX'],
        'y' : ['CoordinateY'],
        'shx'   : ['OriginX'],
        'shy'   : ['OriginY'],
        'phi'   : ['AngleRot'],
        'theta' : ['AngleTilt'],
        'psi'   : ['AnglePsi']
        }

class ARContainer(Container):
    def __str__(self):
        return "%6.3f %6.3f %6.3f %6.3f %6.3f %6.3f %6.3f " % (
            self.shx, self.shy, self.phi, self.theta, self.psi, self.phi0, self.ccc)

class AlignResult(Metadata):

    ContainerClass = ARContainer
    _labels = {
            'shx'   : ['OriginX'],
            'shy'   : ['OriginY'],
            'phi'   : ['AngleRot'],
            'theta' : ['AngleTilt'],
            'psi'   : ['AnglePsi'],
            'phi0'  : ['phiNoSeam', float],
            'ccc' : ['crossCorrelation', float, 'cross correlation coefficient']
            }
AlignResult()
class Aligner(Parameter):
    def __init__(self, projector, debug=False, project_mask=None, ctf_img=None, *args, **kwargs):

        super(Aligner, self).__init__(*args, **kwargs)
        self.projector = projector
        self.ctf_img = ctf_img
        self.fft = np.fft.fft2
        self.rfft = np.fft.rfft2
        self.ifft = np.fft.ifft2
        self.irfft = np.fft.irfft2
        self.ccmapMask = None
        self.cccmapMask = None
        self.debug = debug
        self.project_mask = project_mask


    def align(self, img, angles, **kwargs):
        '''
            Align img with projector for best ccc value.
            :param img: The input image to align
            :param angles: The angles to search
            :param kwargs: The other options for cross correlation function
            :return: The best result
        '''
        align_result = self.align_with_angles(img, angles, **kwargs)
        max_ccc = -2
        best_r = None
        for r in align_result:
            if r.ccc > max_ccc:
                best_r = r
                max_ccc = r.ccc
        return best_r

    def align_with_angles(self, img, angles, *args, **kwargs):
        ''' Default use cross correlation to align the particle
        '''
        return self.cccs(img, angles, *args, **kwargs)

    def cccs(self, img, angles, ccmapMask=None, cccmapMask=None):
        ''' align a particle to this volume.
            Use rfft for calculation
            :param data: The input data. Should already be normalized, edge masked, intensity inversed.
            :return: list of AlignResult Container data (dict)
        '''
        if not isinstance(img, EMImage):
            raise Exception("Align must take EMImage instance as first argument")
        ccmapMask = ccmapMask or self.ccmapMask
        cccmapMask = cccmapMask or self.cccmapMask

        result = AlignResult()
        for phi, theta, psi in angles:
            r = result.new_object()
            self.ccc(img, phi, theta, psi, result=r, ccmapMask=ccmapMask,
                    cccmapMask=cccmapMask)
            result.append(r)
        return result

    def ccc(self, img, phi, theta, psi, ccmapMask=None, cccmapMask=None,
            result=None):
        ''' Get best alignment between img and projector with angle phi, theta, psi
            :param img: The EMImage instance of an image
            :param phi:
            :param theta:
            :param psi: The euler angle for projection
            :param result: The AlignResult container for storing the result
            :return: result if provided, otherwise new result
        '''
        pj = self.projector.project(phi, theta, psi)
        if self.ctf_img:
            pj.fftdata *= self.ctf_img.data
        #if self.project_mask:
        #    pj *= self.project_mask
        x_fit, y_fit, ccc = self.ccc_proj(img, pj, ccmapMask, cccmapMask)
        if result is None:
            result = AlignResult().new_object()
        result.shx, result.shy, result.ccc = x_fit, y_fit, ccc
        result.phi, result.theta, result.psi = phi, theta, psi
        if self.debug:
            s = "%8.5f %8.5f %8.5f %8.5f %8.5f %6.6f" % (
                result.shx, result.shy,
                result.phi, result.theta, result.psi, result.ccc)
            logger.debug(s)
        return result

    def ccc_proj(self, img, ref, ccmapMask=None, cccmapMask=None):
        ''' Calculate cross correlation coefficient.
        First find peak in cross correlation map, 
        the use the shift to calculate the coefficient
        if one of img or ref is constant, return -1
        '''
        corrmap = ref.corrmap(img, fftshift=True)
        if ccmapMask:
            corrmap *= ccmapMask
        x_fit, y_fit, _ = corrmap.peakfind2d()
        shifted_d = img.copy()
        shifted_d.fourier_shift(x_fit, y_fit)
        if cccmapMask:
            idxes = cccmapMask > 0
            shifted_d_data = shifted_d.data[idxes]
            ref_data = ref.data[idxes]
        else:
            shifted_d_data = shifted_d.data.flatten()
            ref_data = ref.data.flatten()
        # special case for one of the image is constant
        if np.std(shifted_d_data) == 0. and np.std(ref_data) == 0.:
            x_fit, y_fit = 0., 0.
            ccc = 1
        elif np.std(shifted_d_data) == 0. or np.std(ref_data) == 0.:
            x_fit, y_fit = 0., 0.
            ccc = -1
        else:
            ccc = np.corrcoef(shifted_d_data, ref_data)[0,1]
        return x_fit, y_fit, ccc
class GridAngle(object):
    ''' Angles
    '''
    def __init__(self, start, num, delta):
        self._start = start
        self._num = num
        self._delta = delta
class Grid(object):
    ''' Grid for angles
    '''

    def __init__(self, phi, nphi, dphi, theta, ntheta, dtheta, psi, npsi, dpsi):
        self.phi = GridAngle(phi, nphi, dphi)
        self.theta = GridAngle(theta, ntheta, dtheta)
        self.psi = GridAngle(psi, npsi, dpsi)

class GridAligner(Aligner):


    def __init__(self, *args, **kwargs):
        super(GridAligner, self).__init__(*args, **kwargs)
        self.max_theta = 105
        self.min_theta = 75
        logger.debug("Aligner created")

    def align_grid(self, img, anggrid, **kwargs):
        phi, theta, psi = anggrid.phi, anggrid.theta, anggrid.psi
        return self.align(img, psi.start,
                theta.start, theta.num, theta.delta,
                phi.start, phi.num, phi.delta, **kwargs)

    def align(self, img, psi_start, theta_start, num_theta, dtheta, phi_start, num_phi, dphi, **kwargs):
        ''' Align image with 2D angle grid of phi and theta. psi is fixed.
        '''
        angles = self._grid_angles(psi_start, theta_start, num_theta, dtheta, phi_start, num_phi, dphi)
        return super(GridAligner, self).align(img, angles, **kwargs)

    def _grid_angles(self, psi_start, theta_start, num_theta, dtheta, phi_start, num_phi, dphi):
        ''' Generate 2d angle grid, psi is fixed
        '''
        angles = []
        for k in range(num_phi):
            phi=phi_start + k * dphi - int(num_phi/2)*dphi
            phi=int(phi+0.5)
            if phi < 0: phi+=360
            if phi > 360: phi-=360
            for j in range(num_theta):
                psi = psi_start
                theta=theta_start + j * dtheta - int(num_theta/2)*dtheta
                angles.append([phi, theta, psi])
        return angles

