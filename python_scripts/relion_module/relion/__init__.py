from .relion import *
import re
from chuff import chuff_dir
import os
relion_src = os.path.join(os.path.dirname(__file__), "labels.txt")

types = {"float" : float, "int" : int, "bool" : bool, 'str':str
        }

for line in open(relion_src):
    line = line.strip()
    tmp = line.split()
    lblName = tmp[0]
    pyType = types[tmp[1]]
    lblComment = " ".join(tmp[2:])
    EMDLabel.addLabel(lblName, pyType, lblComment)


# MT related
EMDLabel.addLabel('pfs', int, "Number of Protofilament")
EMDLabel.addLabel('starts', int, "Number of starts")
EMDLabel.addLabel('crossCorrelation', float, "Cross Correlation Score")
EMDLabel.addLabel('seampos', int, "Seam position")
