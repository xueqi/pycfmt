'''
Relion Star File class.

To add customized label: 

from relion import addLabel
addLabel('crossCorrelation', float, "Cross Correlation Score")

'''
import logging
logger = logging.getLogger(__name__)

def unify_label(lbl):
    '''
    convert the label to startwith rln.
    '''
    if lbl.startswith("_"): lbl = lbl[1:]
    if not lbl.startswith('rln'): lbl = 'rln' + lbl
    return lbl

def addLabel(labelName, labelType, labelComment):
    EMDLabel.addLabel(labelName, labelType, labelComment)
    
class EMDLabel(object):
    '''
        Label that represent a column. 
        This is used to automatically wrap and unwarp the value from star file
        
        type is the data type
        name is the label name
    '''
    LABELS = {}
    def __init__(self, name, valueType, comment= ""):
        self.name = name
        self.type = valueType
        self.comment = comment
    def format(self, value = None):
        if self.type is float:
            if value is None: value = 0
            if (abs(value) > 0. and abs(value) < 0.001) or abs(value) > 100000.:
                fmt = '%12e'
            else:
                fmt = '%12f'
            value = fmt % value
        elif self.type is int:
            if value is None: value = 0
            value = '%12d' % value
        elif self.type is bool:
            if value is None: value = 0
            value = '%12d' % 1 if value else '%12d' % 0
        else:
            if value is None: value = "None"
            elif self.type is str:
                if len(value) == 0: return '@@@'
            else:
                value = self.type(value)
        return value

    def parse(self, s):
        try:
            return self.type(s)
        except Exception as e:
            print self.name
            raise e
    
    @staticmethod
    def addLabel(lblName, *args, **kwargs):
        lblName = unify_label(lblName)
        EMDLabel.LABELS[lblName] = EMDLabel(lblName, *args, **kwargs)
        
    @staticmethod
    def getLabel(labelName):
        labelName = unify_label(labelName)
        return EMDLabel.LABELS.get(labelName)
    
class Container(dict):
    '''
        container used to store the value and format the value
        key of the Container should be the unified label, with rln as start
        value is type of the label type
    '''
    
    def __init__(self, dt = None):
        super(Container, self).__init__()
        if dt:
            for key, value in dt.items():
                key = unify_label(key)
                self[key] = value
        self.alias = {}
        
    def __getitem__(self, key):
        key = unify_label(key)
        if EMDLabel.getLabel(key) is None:
            print EMDLabel.LABELS.keys()
            raise Exception("Label '%s' is not recognized" % key)
        return super(Container, self).__getitem__(key)
    
    def __setitem__(self, key, value):
        key = unify_label(key)
        if EMDLabel.getLabel(key) is None:
            print EMDLabel.LABELS.keys()
            raise Exception("Label '%s' is not recognized" % key)
        if value is None:
            print key, value
            raise Exception("None value not supported in Metadata")
        super(Container, self).__setitem__(
            key, 
            EMDLabel.getLabel(key).parse(value)
            )
    def hasKey(self, key):
        key = unify_label(key)
        return key in self
#     def __getattr__(self, attr):
# 
#         if attr == 'alias':
#             raise AttributeError()
#         if attr in self.alias: attr = self.alias[attr]
#         attr = unify_label(attr)
#         return super(Container, self).get(attr)
#     
#     def __setattr__(self, attr, value):
#         if attr in self.alias: attr = self.alias[attr]
#         attr = unify_label(attr)
#         self[attr] = value
#     
class _Metadata(object):
    '''
        Metadata class to handle relion metadata star file
    '''
    ContainerClass = Container
    def __init__(self, metaname = '', comment = "", isList = False):
        '''
        Constructor 
        '''
        self.name = metaname
        self.objects = [] #std::vector<MetaDataContainer *> objects;
        self.current_objectID = 0 
        self.isList = isList
        self.comment = comment
        self.activeLabels = [] # active labels. EMDLabel instances
        self.ignoreLabels = [] # ignored labels that is not going to file
    
    def reverse(self):
        self.objects.reverse()
    
    def sort_by(self, *keys):
        self.objects.sort(key = lambda x: [x[key] for key in keys])

    def clear(self):
        '''
            remove all objects
        '''
        self.objects = []
    def append(self, d):
        '''
            append one data to the list.
            @param d: dictionary or Container object
        '''
        self.objects.append(self.ContainerClass(d))
    
    def addLabel(self, lblName, unify = True):
        if isinstance(lblName, EMDLabel):
            label = lblName
            label = EMDLabel.getLabel(label.name) 
        else:
            if unify:
                lblName = unify_label(lblName)
            label = EMDLabel.getLabel(lblName)
        if label is not None:
            if not label in self.activeLabels:
                self.activeLabels.append(label)
    def hasLabel(self, lblName, unify = True):
        if unify:
            lblName = unify_label(lblName)
        label = EMDLabel.getLabel(lblName)
        if label is None: return False
        return label in self.activeLabels
    
    def getObject(self, idx):
        return self.objects[idx]
    
    def setObject(self, idx, obj):
        self.objects[idx] = obj
        
    def setLabels(self, lblsOrCont):
        '''
            set labels from container or list labels
        '''
        if isinstance(lblsOrCont, Container):
            lblsOrCont = lblsOrCont.data.keys()
        self.activeLabels = lblsOrCont[:]
    
    def getActiveLabels(self):
        return [lbl.name for lbl in self.activeLabels]
    
    def writeToStream(self, f, withComment = False):
        if len(self.objects) == 0: return
        f.write("\n")
        f.write("data_%s\n" % self.name)
        if self.comment:
            f.write("#%s\n" % self.comment)
        f.write("\n")
        
        if not self.isList:
            f.write("loop_ \n")
            for ii, lbl in enumerate(self.activeLabels):
                f.write("_%s #%d \n" % (lbl.name, ii+1))
            
            for data in self.objects:
                for lbl in self.activeLabels:
                    if unify_label(lbl.name) in data:
                        f.write(lbl.format(data[lbl.name]))
                    else:
                        f.write(lbl.format())
                    f.write(" ")
                f.write("\n")
            f.write("\n")
        else:
            data = self.objects[0]
            maxWidth = max(map(len, [lbl.name for lbl in self.activeLabels]))
            for lbl in self.activeLabels:
                f.write(("%-" + str(4 + maxWidth) + "s ") % ("_" + lbl.name))
                f.write("%-24s" % data[lbl.name])
                if withComment:
                    f.write("# " + lbl.comment)
                f.write("\n")
            f.write("\n")
            
    def write(self, fname, **kwargs):
        f = open(fname, 'w')
        self.writeToStream(f, **kwargs)
        f.close()
        
    
    def read(self, fname):
        return self.readStar(open(fname))
    
    def readStar(self, f, labels = None):
        line = f.readline()
        while line:
            if 'data_' not in line: 
                line = f.readline()
                continue
            name = line[line.index('data_') + 5 : ].strip() # to remove last \n
            if self.name == '' or name == self.name:
                self.name = name
                while True:
                    pos = f.tell()
                    line = f.readline()
                    line = line.strip()
                    if not line: continue
                    if "loop_" in line:
                        return self.readStarLoop(f, labels)
                    elif line[0] == '_':
                        f.seek(pos)
                        self.isList = True
                        return self.readStarList(f, labels)
            
            else:
                line = f.readline()
                
    def readStarLoop(self, f, labels):
        self.isList = False
        allLabels = []
        # get columns
        while True:
            line = f.readline().strip()
            if not line or line[0] in ['#', ';']: continue
            
            if line[0] == '_':
                if '#' in line:
                    token = line[1:line.index('#') - 1].strip()
                else:
                    token = line[1:].strip()
                label = EMDLabel.getLabel(token)
                if label is None:# or (labels is not None and label not in labels):
                    self.ignoreLabels.append(token)
                else:
                    self.addLabel(label)
                allLabels.append(label)
            else:
                break
        # get data
        while line:
            obj = Container()
            if not line:
                break
            line = line.strip()
            if not line:
                continue
            tmp = line.split()
            for i in range(len(allLabels)):
                lbl = allLabels[i]
                if lbl in self.activeLabels:
                    obj[lbl.name] = tmp[i]
            self.append(obj)
            line = f.readline()

    def readStarList(self, f, labels):
        self.isList = True
        obj = {}
        while True:
            line = f.readline().strip()
            if not line: continue
            if '#' in line: line = line[:line.index('#')]
            key = line.split()[0]
            if key[0] == "_":
                obj[key[1:]] = line.split()[1]
            elif key.startswith("data_"): 
                break
            elif key.startswith("loop_"):
                # also has loop. TODO
                break
        self.append(obj)
           
    
    def __str__(self):
        return "Metadata with %d labels and %d entries" % (len(self.activeLabels),
                                                           len(self.objects))
    
    def __repr__(self):
        return self.__str__()
    
    def __iter__(self):
        for obj in self.objects:
            yield obj
    
    def size(self):
        return len(self.objects)
    
class Metadata(_Metadata):
    def __init__(self, *args, **kwargs):
        super(Metadata, self).__init__(*args, **kwargs)

    def copyLabels(self, other):
        '''
            copy the all labels from another Metadata

            @param other: Another Metadata instance
        '''
        for lbl in other.getActiveLabels():
            self.addLabel(lbl)
    
    def getValues(self, labelName):
        '''
            @param labelName: The column to get
        '''
        if type(labelName) is str:
            if not self.hasLabel(labelName): return None
            labelName = unify_label(labelName)
            return [m1[labelName] for m1 in self]
        elif type(labelName) is list or type(labelName) is tuple:
            return [self.getValues(x) for x in labelName]
    @classmethod
    def new(cls):
        return cls()
    
    def filter(self, func=True):
        '''
            Create a new Metadata by filter with function func
            @param func: The function that takes one dictionary as input, return true if selected, else false. if func is True, copy self to new object. If False, do not copy anything
            
            example:
            def func(d):
                return d['rlnClassNumber'] != 2 and d['rlnMicrographName'].endswith('0001.mrc')
        '''
        m = self.new()
        m.copyLabels(self)
        for m1 in self:
            if func is False: continue
            if (func is True) or (callable(func) and func(m1)):
                m.append(m1)
        return m
    
    def map(self, func, key):
        '''
            call func on all entries with key.
            This is inplace function
            For instance, to reverse all sign of phi:
            m.map(lambda x: -x, 'AngleRot')
        '''
        key = unify_label(key)
        for m1 in self.objects:
            m1[key] = func(m1[key])

    @classmethod
    def readFile(cls, *args, **kwargs):
        m = cls()
        m.read(*args, **kwargs)
        return m
    
    def copy(self):
        '''
            return a deep copy of this metadata
            @return: the new metadata object with same value
        '''
        return self.filter()
    
    def __iadd__(self, other):
        for m in other:
            self.append(m)

        return self

    def __add__(self, other):
        result = self.copy()
        if other is None: return result
        for m in other:
            result.append(m)
        return result
    
    def __radd__(self, other):
        if other is None:
            return self.copy()
        return super(Metadata, self).__radd__(other)
    
    def deactivateLabels(self, *labels):
        for label in labels:
            if type(label) in [list, tuple]:
                self.deactivateLabels(*label)
            else:
                self.deactivateLabel(label)

    def addLabels(self, *labels):
        for label in labels:
            if type(label) in [list, tuple]:
                self.addLabels(*label)
            else:
                self.addLabel(label)
    def deactivateLabel(self, lblName):
        lbl = EMDLabel.getLabel(lblName)
        if lbl in self.activeLabels:
            self.activeLabels.remove(lbl)
            self.ignoreLabels.append(lbl)
    def clean_for_relion(self):
        '''
            clean labels for relion input.
            all labels start with Caption  will keep, others are deactivated

            return a new copy 
        '''
        result = self.copy()
        for label in result.getActiveLabels():
            if ord(label[3]) >= ord('a'):  # 'A' = 65, 'a' = 97, ASCII 
                result.deactivateLabel(label)
        return result
                
    def __len__(self):
        return self.size()

    def __getitem__(self, key):
        if type(key) == int:
            return self.getObject(key)
        else:
            if len(self) == 0:
                self.append(self.newObject())
            return self[0][key]

        return self.getObject(key)

    def __setitem__(self, key, value):
        if type(key) == int:
            currentValue = self[key]
            currentValue.update(value)
            self.setObject(key, currentValue)
        else:
            if len(self) == 0 and self.isList:
                self.append(self.newObject())
            self[0][key] = value

    def diff(self, other, key, func = None):
        import numpy as np
        if func is None:
            import operator
            func=operator.sub
        key = unify_label(key)
        v1 = self.getValues(key)
        v2 = other.getValues(key)
        d = map(func, v1, v2)
        return np.median(d), np.std(d), np.max(d), np.min(d)
    
    def newObject(self):
        c = Container()
        for lbl in self.activeLabels:
            c[lbl.name] = lbl.type()
        return c


    def group_by(self, *keys):
        '''
            group values to new list of metadata
            a = m.group_by('rlnMicrographName', 'rlnHelicalTubeID')
            key is (mgname, tubid), value is metadata containing all the matched data
        '''
        from collections import OrderedDict
        if len(keys) == 0: return self
        result = OrderedDict()
        for m1 in self:
            if len(keys) == 1:
                values = m1[keys[0]]
            else:
                values = tuple([m1[key] for key in keys])
            if not values in result:
                result[values] = self.new()
                result[values].copyLabels(self)
            result[values].append(m1)
        return result
    
