'''
Metadata class.
To add customized label:

from metadata import add_label
add_label('crossCorrelation', float, "Cross Correlation Score")

Type of label should have one str parameter optional constructor.

Full usage:

from metadata import Metadata

# Metadata is class for reading and writing star file.

# To read the first block in star file, (first data_  label)
m = Metadata.read_file('star_file.star')
# or
m = Metadata()
m.read('star_file.star')

# to read other blocks,
m = Metadata('block_name') # block name is the label without data_
m.read("star_file.star")

# to write star file:
m.write("star_file_to_write.star")

# get all column names:
m.get_active_labels()

# get all value for one or multi column:
phi, theta, psi = m.get_values(['AngleRot', 'AngleTilt', 'AnglePsi')

# loop from all rows:
for m0 in m:
    print(m0['AngleRot'])

# get value for i'th row
m0 = m[i]

# concatenate two star file
m3 = m1 + m2
# or inplace, faster
m2 += m1

# add one row
m.append({'AngleRot': 40, 'AngleTilt':90})
# if the value is not specified for the label, the value is 0 or '' for str.

# to filter the rows:
# for example: get all micrograph with name: mgname
filtered_m = m.filter(lambda x: x['MicrographName'] == mgname)

# to group by value in column
mgs = m.group_by('MicrographName')

# to create new star file with same columns
new_m = m.new_like(m)

'''
import logging
from collections import OrderedDict
import abc

METADATA_LOGGER = logging.getLogger(__name__)

class _List(list):
    """ Class for list value
    """
    __metaclass__ = abc.ABCMeta
    def __init__(self, init_value=""):
        """
        Args:
            init_value: The init string list representation using string.
               Could be space sperated or comma seperated
        
        Raises:
            TypeError: init_value is not a list of str representation of list
        """
        super(_List, self).__init__()
        if isinstance(init_value, str):
            tmp = init_value.split(",")
            tmp = [_.strip() for _ in tmp]
            if len(tmp) == 1:
                tmp = init_value.split()
            self.extend([self.parse(_) for _ in tmp])
        elif isinstance(init_value, list):
            for value in init_value:
                if isinstance(value, self.__class__):
                    self.extend(value) # extend a list
                else:
                    self.append(self.parse(value))
        else:
            raise TypeError("Input type should be str or list")
    def parse(self, str_value):
        return str_value
    
class StrList(_List):
    """ Datatype for list of string.
    """
    def __init__(self, init_value=""):
        """
        Args:
            init_value: The init string list representation using string.
               Could be space sperated or comma seperated
        
        Raises:
            TypeError: init_value is not a list of str representation of list
        """
        super(StrList, self).__init__(init_value)

    def __str__(self):
        """Used for printing
        """
        return "[%s]" % ", ".join(self)

class IntList(_List):
    """ IntList Type. A list of int.
    """
    def __init__(self, init_value=""):
        """
        Args:
            init_value: A `str` or list of int values.
        Raises:
            TypeError: init_value is not a list of str representation of list
        """
        super(IntList, self).__init__(init_value)
    
    def parse(self, str_value):
        """ Parse str value to int
        """
        return int(str_value)

    def __str__(self):
        """ String representation of int list
        """
        return "[%s]" % ", ".join([str(_) for _ in self])

class FloatList(_List):
    """FloatList Type. A list of Float"""
    def __init__(self, init_value="", precision=4):
        """
        Args:
            init_value: A `str` or list of float values.
            precision: A `int`. The number after period to print
        Raises:
            TypeError: init_value is not a list of str representation of list
        """
        self._precision = precision
        super(FloatList, self).__init__(init_value)
    def parse(self, str_value):
        return float(str_value)
    def __str__(self):
        """ String representation of float list
        """
        return "[%s]" % ", ".join([("%%.%df" % self._precision) % _ for _ in self])

class DataType(object):
    """ DataType Container
    """
    strlist = StrList
    intlist = IntList
    floatlist = FloatList

def unify_label(label):
    '''convert the label to startwith rln.
    Args:
        label: A `str`. The label to unify
    '''
    # remove the first _, as in star format
    if label[0] == '_':
        label = label[1:]
    # add rln at begining. so PixelSize -> rlnPixelSize
    if not label.startswith('rln'):
        label = 'rln' + label
    return label

def add_label(label_name, label_type=str, label_comment=""):
    ''' Add a new label
    Args:
        label_name: A `str`. The label name to add
        label_type: A `type`. The type of the label
        label_comment: A `str`. The description of the label
    '''
    return EMDLabel.add_label(label_name, label_type, label_comment)

class EMDLabel(object):
    '''
        Label that represent a column.
        This is used to automatically wrap and unwarp the value from star file
        type is the data type
        name is the label name
    '''
    LABELS = {} # Hold global known labels
    def __init__(self, label_name, label_type, label_comment="", format_str="%s"):
        ''' EMDLabel instance
        Args:
            label_name: A `str`. The label name to add
            label_type: A `type`. The type of the label
            label_comment: A `str`. The description of the label
        '''
        self.name = label_name
        self.type = label_type
        self.comment = label_comment
        self.format_str = format_str
        if self.name == "":
            raise ValueError("label name must not be empty")

    def format(self, value=None):
        ''' format the value to string
        Args:
            value: The value to format. This must be of self.type
        '''
        if self.type is float:
            if value is None:
                value = 0
            if (abs(value) > 0. and abs(value) < 0.001) or abs(value) > 100000.:
                fmt = '%12e'
            else:
                fmt = '%12f'
            return fmt % value
        elif self.type is int:
            if value is None:
                value = 0
            return '%12d' % value
        elif self.type is bool:
            if value is None:
                value = 0
            return '%12d' % 1 if value else '%12d' % 0
        else:
            if value is None:
                return "None"
            elif self.type is str:
                if len(value) == 0:
                    return '@@@'
            else:
                value = self.type(value)
            return value

    def parse(self, str_value):
        ''' Parse a value from string str_value
        Args:
            str_value: The input string for parsing
        Returns:
            The parsed value with self.type
        Raises:
            Exception: If the str_value can not convert to self.type
        '''
        if isinstance(str_value, self.type):
            return str_value
        
        try:
            if self.type is bool:
                if isinstance(str_value, str):
                    return str_value != '0'
            if self.type is int: # The change from float to int would break if not do this
                return int(float(str_value))
            return self.type(str_value)
        except Exception as exc:
            import traceback as tb
            tb.print_exc()
            METADATA_LOGGER.error("Unable parse value for %s with type %s for label %s",
                                  str_value, self.type, self.name)
            raise exc

    @staticmethod
    def add_label(label_name, value_type, comment=""):
        ''' Add a label to class
        Args:
            label_name: A `str`. The name of the label/column/key
            value_type: The type of the value.
            comment: A `str`. The description of the label.
        Returns:
            A `EMDLabel` instance. Newly added label.
        '''
        label_name = unify_label(label_name)
        if label_name not in EMDLabel.LABELS:
            EMDLabel.LABELS[label_name] = EMDLabel(label_name, value_type, comment)
        return EMDLabel.LABELS[label_name]

    @staticmethod
    def get_label(label_name):
        ''' Get a label instance by label_name
        Args:
            label_name: A `str`. The lael name to get
        Returns:
            A `EMDLabel`. The label instance.
        '''
        label_name = unify_label(label_name)
        return EMDLabel.LABELS.get(label_name)

    def __str__(self):
        """ String representation
        """
        return "Label(%s)" % self.name

class Container(dict):
    '''
        container used to store the value and format the value
        key of the Container should be the unified label, with rln as start
        value is type of the label type
    '''
    def __init__(self, init_value=None):
        ''' Container class init
        Args:
            init_value: A `dict`. The dict like instance.
                init the container with init_value
        '''
        super(Container, self).__init__()
        if init_value is not None:
            self.update(init_value)

    def update(self, data, extra=False):
        ''' Update the container using another container or dict
        Args:
            data: A `dict` or `Container`. Update from data. data must has __getitem__
        '''
        for key in data:
            u_key = unify_label(key)
            self[u_key] = data[key]

    def __getitem__(self, key):
        """ dict protocol. unify key before fetch value.
        """
        key = unify_label(key)
        if EMDLabel.get_label(key) is None:
            METADATA_LOGGER.error("__getitem__, Label '%s' is not recognized",
                                  key)
            raise Exception("Label '%s' is not recognized" % key)
        return super(Container, self).__getitem__(key)

    def __setitem__(self, key, value):
        """ dict protocol. unify key before set value.
        Args:
            key: A `str`. The label name.
            value: The value to set to label
        """
        key = unify_label(key)
        if EMDLabel.get_label(key) is None:
            METADATA_LOGGER.error("__setitem__, Label '%s' is not recognized",
                                  key)
            raise Exception("Label '%s' is not recognized" % key)
        if value is None:
            # set to default value.
            value = EMDLabel.get_label(key).type()

        super(Container, self).__setitem__(key,
                                           EMDLabel.get_label(key).parse(value))

    def has_key(self, key):
        ''' Check if instance has key
        Args:
            key: The label key
        '''
        key = unify_label(key)
        return super(Container, self).has_key(key)

    @classmethod
    def copy_from(cls, other):
        """ Copy value from another Container or dict.
        Args:
            other: A `dict` or `Container`. The data copied from.
        Returns:
            A `Container` or subclass instance.
        """
        obj = cls()
        for key in obj:
            if other.has_key(key):
                obj[key] = other[key]
        return obj

    def copy(self):
        """ Return a copy of self
        Returns:
            Same data and same class
        """
        obj = self.__class__()
        for key in self:
            obj[key] = self[key]
        return obj

class _Metadata(object):
    '''
        Metadata class to handle metadata star file
    '''
    # ContainerClass is used to store one data entry
    ContainerClass = Container

    def __init__(self, metaname='', comment="",
                 is_list=False,
                 ):
        '''
        Args:
            metadata: A `str. The name of this metadata section
            comment: A `str`. The comment for this section
            is_list: A `bool`. The style of the data. if True, only contain one data
        '''
        self.name = metaname
        self._objects = []
        self._current_object_id = 0
        self.is_list = is_list    # if instance is list
        self.comment = comment
        self.labels = OrderedDict()
        self.ignore_labels = []
        self.label_alias = {}

    def reverse(self):
        ''' reverse the order of objects
        '''
        self._objects.reverse()

    def sort_by(self, *keys):
        """ Sort the objects by keys. Inplace
        Args:
            *keys: The input keys for sorting
        """
        self._objects.sort(key=lambda x: [x[key] for key in keys])

    def clear(self):
        ''' remove all objects
        '''
        self._objects = []

    def append(self, obj):
        '''append one data to the list. Data is always copied.
        Args:
            obj: A `dict` or `ContainerClass`. dictionary or Container object
        '''
        self._objects.append(self.ContainerClass(obj))

    def pop(self, idx):
        ''' Delete and return at idx
        Args:
            idx: A `int`. The index to delete
        Returns:
            A `ContainerClass` instance.
        '''
        return self._objects.pop(idx)

    def add_label(self, label_name, unify=True):
        """ Add one label/column
        Args:
            label_name: A `str` or `EMDLabel`. The label to add
        """
        old_label_name = label_name
        if isinstance(label_name, EMDLabel):
            label = label_name
            label_name = label.name
            label = EMDLabel.get_label(label.name)
        else:
            if unify:
                label_name = unify_label(label_name)
            label = EMDLabel.get_label(label_name)
        if label is not None:
            if label not in self.labels:
                self.labels[label_name] = label
            if old_label_name != label_name:
                self.label_alias[old_label_name] = label
        else:
            METADATA_LOGGER.error("Could not add label: %s", label_name)
            METADATA_LOGGER.error("Maybe you want to add global label first?")
            raise MetadataException("No such label")

    def has_label(self, label_name):
        ''' Check if instance has label
        Args:
            label_name: A `str`. The label name to check
        '''
        if label_name in self.labels:
            return True
        if label_name in self.label_alias:
            return True
        label_name = unify_label(label_name)
        if label_name in self.labels:
            self.label_alias[label_name] = self.labels[label_name]
            return True
        return False

    def get_object(self, idx):
        """ Get object at index idx
        Args:
            idx: A `int`. The index of the object in internal object list
        Returns:
            A `ContainerClass` instance.
        """
        return self._objects[idx]

    def set_object(self, idx, obj):
        """ Set object at index idx.
        Args:
            idx: A `int`. The index of the object in internal object list
        """
        self._objects[idx] = obj

    def set_labels(self, *labels):
        '''
            set labels from list of labels.
        Args:
            labels: A `str` list. The labels to set to current metadata.
        '''
        self.labels.clear()
        for label in labels:
            self.labels[label.name] = label

    def get_active_labels(self):
        ''' Get all labels activated. unified label name
        Returns:
            List of `str`. A list of active labels currently used.
        '''
        return [label.name for label in self.labels.values()]

    def _write_to_stream_list(self, star_file, with_comment=False):
        """ Write the list style file
        Args:
            star_file: A `File` like object. Must has write method
            with_comment: A `bool`. also write comment to stream if true.
        """
        data = self._objects[0]
        max_width = max([len(label.name) for label in self.labels.values()])
        for label in self.labels.values():
            star_file.write(("%-" + str(4 + max_width) + "s ") % ("_" + label.name))
            star_file.write("%-24s" % data[label.name])
            if with_comment:
                star_file.write("# " + label.comment)
            star_file.write("\n")
        star_file.write("\n")

    def _write_to_stream_loop(self, star_file):
        """ Write the loop style file
        Args:
            star_file: A `File` like object. Must has write method
        """
        self.write_to_stream_loop_header(star_file)
        for data in self._objects:
            self.write_to_stream_loop_single(star_file, data)
        star_file.write("\n")
    
    def write_to_stream_loop_header(self, star_file):
        """Write the loop style file header
        Args:
            star_file: A `File` like object. Must has write method
        """
        star_file.write("loop_ \n")
        for idx, label in enumerate(self.labels.values()):
            star_file.write("_%s #%d \n" % (label.name, idx+1))

    def write_to_stream_loop_single(self, star_file, data):
        """ Write a single object to star file
        
        Args:
            star_file: A `File` like object supports write
            object: A `Container`. The object to write
        """
        for label in self.labels.values():
            if unify_label(label.name) in data:
                star_file.write(label.format(data[label.name]))
            else:
                star_file.write(label.format())
            star_file.write(" ")
        star_file.write("\n")
    
    def write_to_stream(self, star_file, with_comment=False):
        ''' Write the object to a stream star_file.
        Args:
            star_file: A `File` like object. Must has write method
            with_comment: A `bool`. also write comment to stream if true.
        '''
        if len(self._objects) == 0:
            return
        star_file.write("\n")
        star_file.write("data_%s\n" % self.name)
        if self.comment:
            star_file.write("#%s\n" % self.comment)
        star_file.write("\n")
        if not self.is_list:
            self._write_to_stream_loop(star_file)
        else:
            self._write_to_stream_list(star_file, with_comment)

    def write(self, fname, **kwargs):
        """ Write the metadata to file
        Args:
            fname: A `str`. The path of the file to write.
            **kwargs: Arguments passed to write_to_stream.
        """
        with open(fname, 'w') as star_file:
            self.write_to_stream(star_file, **kwargs)

    def size(self):
        """
        Returns:
            A `int`. The number of objects
        """
        return len(self._objects)

####protocols====
    def __str__(self):
        return "Metadata with %d labels and %d entries" % (len(self.labels),
                                                           len(self._objects))
    def __repr__(self):
        return self.__str__()
    def __iter__(self):
        for obj in self._objects:
            yield obj
####end protocols====

class Metadata(_Metadata):
    """ Metadata class. The `Metadata` class is used for storing key value pairs.
    Subclass of `Metadata` need to add custom labels.
    """
    _labels = {}
    _label_inited = False

    def __init__(self, *args, **kwargs):
        super(Metadata, self).__init__(*args, **kwargs)
        for value in self._labels.values():
            self.add_label(value[0])

    @classmethod
    def init_labels(cls):
        """Init labels for columns.
        """
        if cls._label_inited:
            return
        # add new labels. labels with only one value is skipped.
        for label in cls._labels.values():
            if len(label) == 1:
                continue
            add_label(*label)

        # set attributes for easy access
        # like m.x, m.y is really m['CoordinatX'], m['CoordinateY']
        # if there is entry "x" : ['CoordianteX'] in Metadata._labels
        from functools import partial
        def _getx(attr_x, self):
            return self[attr_x]
        def _setx(attr_x, self, value):
            self[attr_x] = value
        for attr, value in cls._labels.items():
            label_name = value[0]
            # add attribute to the metadata class, used by list metadata
            setattr(cls, attr, property(partial(_getx, label_name),
                                       partial(_setx, label_name)))
            # add properties to the Container class
            setattr(cls.ContainerClass, attr, property(partial(_getx, label_name),
                                                      partial(_setx, label_name)))
        if cls.__base__ is not Metadata and cls is not Metadata:
            cls.__base__.init_labels()

    @classmethod
    def __new__(cls, *args, **kwargs):
        cls.init_labels()
        new_instance = super(Metadata, cls).__new__(cls, *args, **kwargs)
        return new_instance

    def set_attribute(self, metaname, attrname, init_value):
        """ Add property of this class and `ContainerClass`, and set init value
        Args:
            metaname: The label name used to store the value. eg. The column.
            attrname: The attribute name to call. eg. property
            init_value: The default value to assign.
        """
        from functools import partial
        def _getx(attr, self):
            return self[attr]
        def _setx(attr, self, value):
            self[attr] = value
        setattr(self.__class__, attrname,
                property(partial(_getx, metaname),
                         partial(_setx, metaname)))
        setattr(self.ContainerClass, attrname,
                property(partial(_getx, metaname),
                         partial(_setx, metaname)))
        if self.is_list:
            setattr(self, attrname, init_value)

    def copy_labels(self, other):
        '''copy the all labels from another Metadata. This won't delete original labels.
        Args:
            other: A `Metadata` instance
        '''
        for label in other.labels.values():
            self.add_label(label.name)

    def get_values(self, label_name):
        ''' Get values as list for one column
        Args:
            label_name: The column to get
        return:
            list of values. if lael_name is str, the returned result is 1D list.
                if lael_name is list or tuple, the returned result is 2D list
                if label does not exist, return None
        '''
        if isinstance(label_name, str):
            if not self.has_label(label_name):
                return None
            label_name = unify_label(label_name)
            return [obj[label_name] for obj in self]
        elif isinstance(label_name, list) or isinstance(label_name, tuple):
            return [self.get_values(label) for label in label_name]

    def filter(self, func=True):
        '''Create a new Metadata by filter with function func
        Args:
            func: The function that takes one dictionary as input,
                return true if selected, else false.

                example:
                def func(d):
                    return (d['rlnClassNumber'] != 2
                        and d['rlnMicrographName'].endswith('0001.mrc'))
        Returns:
            A `Metadata` or subclass instance.
                The returned type is the same as self, whith same labels.
        '''
        result = self.new_like(self)
        for obj in self:
            if func is False:
                continue
            if (func is True) or (callable(func) and func(obj)):
                result.append(obj.copy())
        return result

    def map(self, func, key):
        '''call func on all entries with key.
            This is inplace function
            For instance, to reverse all sign of phi:
            m.map(lambda x: -x, 'AngleRot')
        Args:
            func: A `python function`. this function tasks one parameter.
            key: A `str`. The column that the function applied on
        '''
        key = unify_label(key)
        if not self.has_label(key):
            return
        for obj in self._objects:
            obj[key] = func(obj[key])

    @classmethod
    def read_all(cls, fname):
        """ Read all Metadata from one file
        Args:
            fname: The filename to read from
        Return:
            A `OrderedDict`. key is the data name, value is metadata
        """
        result = OrderedDict()
        with open(fname) as star_file:
            while True:
                mdata = cls()
                rtn = mdata.read_star(star_file)
                if not rtn:
                    break
                result[mdata.name] = mdata
        return result

    def read(self, fname):
        """ Read data from file.
        Args:
            fname: A `str`. File name of the data
        Returns:
           True if the data is successfully read, otherwise False
        """
        return self.read_star(open(fname))

    def read_star(self, star_file, labels=None):
        """ Read one data section, with name of self.name
        Args:
            star_file: A `File` like object.
        """
        while True:
            line = star_file.readline()
            if not line: # end of file
                return False # Could not read section
            if 'data_' not in line:
                continue
            name = line[line.index('data_')+5:].strip() # to remove last \n
            if self.name == '' or name == self.name:
                self.name = name
                break

        while True:
            pos = star_file.tell() # for list we need to unread line
            line = star_file.readline()
            line = line.strip()
            if not line:
                continue
            if "loop_" in line:
                return self._read_star_loop(star_file, labels)
            elif line[0] == '_':
                star_file.seek(pos)
                return self._read_star_list(star_file)

    def _read_star_loop(self, star_file, labels=None):
        """Read Loop style star file
        Args:
            star_file: A `File` like object.
        """
        self.is_list = False
        all_labels = []
        column_map = {}
        # get labels. label line start with _
        column_index = 0
        ignored_labels = []
        while True:
            loc = star_file.tell()
            line = star_file.readline()
            if line is None: # endof file
                return
            if '#' in line:
                line = line[:line.index('#')]
            line = line.strip()
            if not line:
                continue
            tmp = line.split()
            token = tmp[0]
            if line[0] == '_':
                token = tmp[0][1:]
            else:
                # end of label
                star_file.seek(loc)
                break
            label = EMDLabel.get_label(token)
            if label is None or (labels is not None and label not in labels):
                ignored_labels.append(token)
            else:
                self.add_label(label)
                column_map[token] = column_index
            all_labels.append(label)
            column_index += 1

        # get data
        ncols = len(all_labels)
        while True:
            loc = star_file.tell()
            line = star_file.readline()
            if not line: # end of file
                break
            line = line.strip()
            if not line:
                continue
            # until we see next data
            if line.startswith('data_'):
                star_file.seek(loc)
                break
            obj = self.new_object()
            tmp = line.split()
            if len(tmp) != ncols:
                METADATA_LOGGER.warn(("Metadata malformed. number labels: %s," +
                                      "num columns in data: %s\n%s"), ncols, len(tmp), line)
            for label in self.labels.values():
                if label.name in column_map:
                    obj[label.name] = tmp[column_map[label.name]]
            self.append(obj)
        return True

    def _read_star_list(self, star_file):
        """ Read List style star file
        Args:
            star_file: A `File` like object. Random access file.
        """
        self.is_list = True
        obj = {}
        while True:
            loc = star_file.tell()
            line = star_file.readline()
            if not line: # end of file
                break
            if '#' in line: # has comment
                line = line[:line.index('#')]
            line = line.strip()
            if not line: # empty line or comment line
                continue
            tmp = line.split() # this means the key should not has space
            key = tmp[0]
            if key[0] == "_":
                key = key[1:]
                try:
                    self.add_label(key)
                    if len(tmp) > 1:
                        obj[key] = " ".join(tmp[1:])
                    else:
                        obj[key] = "" # No value. use empty string
                except MetadataException:
                    self.ignore_labels.append(key)

            elif key.startswith("data_"): # next record
                star_file.seek(loc)
                break
            elif key.startswith("loop_"): # also has loop? Not implemented
                # also has loop. TODO
                break
        self.clear()
        self.append(obj)
        return True

    @classmethod
    def read_file(cls, filename, metaname=""):
        """A convenient function to read a star file.
        Args:
            *args: argument passing to metadata.read
            **kawrgs: argument passing to emtadata.read
        """
        result = cls(metaname)
        result.read(filename)
        return result


    def copy(self):
        '''return a deep copy of this metadata
        Returns: the new metadata object with same value
        '''
        return self.filter()

    def add_labels(self, *labels):
        """ Add multiple labels to active labels
        Args:
            *labels: The labels to add.
        """
        for label in labels:
            if isinstance(label, (list, tuple)):
                self.add_labels(*label)
            else:
                self.add_label(label)

    def deactivate_label(self, label_name):
        """ Move label name to ignored labels.
        Args:
            label_name: A `str`. The label name to deactivate
        """
        label_name = unify_label(label_name)
        if label_name in self.labels:
            if label_name not in self.ignore_labels:
                self.ignore_labels.append(label_name)
            del self.labels[label_name]

    def deactivate_labels(self, *label_names):
        """ Move multiple label name to ignored labels.
        Args:
            *label_names: A `str`. The label name to deactivate
        """
        for label in label_names:
            if isinstance(label, (list, tuple)):
                self.deactivate_labels(*label)
            else:
                self.deactivate_label(label)

    def new_object(self):
        ''' Create a new Container Object to store the data
        Returns:
            obj: A `ContainerClass` instance.
        '''
        result = self.ContainerClass()
        for label in self.labels.values():
            result[label.name] = label.type()
        return result


    def group_by(self, *keys):
        '''group values to new list of metadata
            a = m.group_by('rlnMicrographName', 'rlnHelicalTubeID')
            key is (mgname, tubid), value is metadata containing all the matched data
        Args:
            keys: The keys that is used to group the metadata.
        Returns:
            groups: A `OrderedDict`.
                Key is tuple of values for input keys if multi key, else single value.
                    The value must be hashable. (eg. not list)
                Value is the new Metadata that belongs to this key pairs.
                The order is keeped. If merge all groups, the result should be the same.
                if no key is supplied, the key would be empty tuple, and value is self copied.
        '''
        result = OrderedDict()
        if len(keys) == 0:
            result[()] = self.copy()
            return result

        for obj in self:
            if len(keys) == 1:
                values = obj[keys[0]]
            else:
                values = tuple([obj[key] for key in keys])
            if values not in result:
                result[values] = self.new_like(self)
            result[values].append(obj)
        return result

    @staticmethod
    def new_like(other):
        ''' Create a new instance with same class, name and labels
        Args:
            other: A `Metadata` instance or subclass instance.
                The label is copied from this metadata
        Returns:
            new_metadata: A `Metadata` instance. With same labels as input
        '''
        new_m = other.__class__(other.name)
        new_m.copy_labels(other)
        return new_m

    @classmethod
    def copy_from(cls, other, extra=False):
        ''' Copy from other instance.
        The columns those are not in 'other' would set to default value.
        Args:
            other: A `Metadata` instance or subclass instance. The data copied from
            extra: A bool. Set to True if copy extra columns.
        '''
        result = cls()
        if extra:
            result.add_labels(other.get_active_labels())
        for data in other:
            obj = result.new_object()
            obj.update(data)
            result.append(obj)
        return result

####protocols####
    def __len__(self):
        return self.size()

    # indexing protocol.
    # if index is integer, the indexing is list
    # if index is string, the indexing is dict for first object.
    def __getitem__(self, key):
        if isinstance(key, int) and not self.is_list:
            return self.get_object(key)
        else:
            if len(self) == 0:
                self.append(self.new_object())
            if isinstance(key, int):
                return self.get_object(0)
            return self.get_object(0)[key]
        return self.get_object(key)

    def __setitem__(self, key, value):
        if isinstance(key, int):
            current_value = self[key]
            current_value.update(value)
            self.set_object(key, current_value)
        else:
            if len(self) == 0 and self.is_list:
                self.append(self.new_object())
            self[0][key] = value

    def __iadd__(self, other):
        ''' inplace add, implents += operator
        '''
        if other is not None:
            for obj in other:
                self.append(obj)
        return self

    def __add__(self, other):
        result = self.copy()
        if other is None:
            return result
        for obj in other:
            result.append(obj)
        return result

    def __radd__(self, other):
        if other is None or other == 0:
            return self.copy()
        return super(Metadata, self).__radd__(other)

####end protocols####

# TODO: This should move out from this class.
    def clean_for_relion(self):
        '''
            clean labels for relion input.
            all labels start with Caption  will keep, others are deactivated
            return a new copy
        '''
        result = self.copy()
        for label in result.get_active_labels():
            if ord(label[3]) >= ord('a'):  # 'A' = 65, 'a' = 97, ASCII
                result.deactivate_label(label)
        return result

    @classmethod
    def new(cls):
        """ Return a new metadata instance. Should not use in new code
        """
        return cls()


class MetadataException(RuntimeError):
    """ Metadata Exception class.
    """
