""" Metadata class. Represent data store.
"""
from .metadata import *
from .parameter import Parameter, MPIParameter
import os

_labels_file = os.path.join(os.path.dirname(__file__), "labels.txt")

_types = {"float" : float, "int" : int, "bool" : bool, 'str':str
        }

# import the default labels. This is from relion
for line in open(_labels_file):
    line = line.strip()
    tmp = line.split()
    if len(tmp) < 3: 
        logging.warn("labels.txt file corrupted")
        continue
    lblName = tmp[0]
    pyType = _types[tmp[1]]
    lblComment = " ".join(tmp[2:])
    EMDLabel.add_label(lblName, pyType, lblComment)

# MT related
add_label('pfs', int, "Number of Protofilament")
add_label('starts', int, "Number of starts")
add_label('crossCorrelation', float, "Cross Correlation Score")
add_label('seampos', int, "Seam position")
