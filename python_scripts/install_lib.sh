#!/bin/bash

export chuff_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
export chuff_dir="$(dirname $chuff_dir)"
source $chuff_dir/chuff.bashrc
lib_dir="$chuff_dir/python_scripts/lib"
type pip >/dev/null 2>&1 || {
    # install pip
    python $chuff_dir/python_scripts/install/get-pip.py -I \
        --install-option="--install-lib=$chuff_dir/python_scripts/lib" \
        --install-option="--install-scripts=$chuff_dir/linux"   
}

# plot
pip install  --target="$lib_dir" matplotlib
# jpeg/tiff
pip install  --target="$lib_dir" Pillow
# numpy, scipy
pip install  --target="$lib_dir" numpy
pip install  --target="$lib_dir" scipy
# mpi4py for mpi
pip install  --target="$lib_dir" mpi4py
# interface
pip install  --target="$lib_dir" gooey
# progress bar
pip install  --target="$lib_dir" tqdm
# mrcfile 
pip install  --target="$lib_dir" mrcfile
# relion
cd $chuff_dir/python_scripts/relion_module
python setup.py  install --install-lib="$lib_dir"

echo "source $chuff_dir/chuff.bashrc" >> ${HOME}/.bashrc

