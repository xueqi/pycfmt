#! /bin/csh

if($#argv < 1) then
    echo 'ERROR: must give at least one argument:'
    echo '  "<n1>_<n2>" where <n1>,<n2> are first, last particles, respectively'
    exit(2)
endif

if($#argv > 1) then
    set extra_args = ($argv[2-])
else
    set extra_args = ()
endif

set particle_spec = `echo $1 | awk '{n = split($1,p,"_"); print p[1], p[2]}'`

$chuff_dir/commands/cffrealign $extra_args \
  first=$particle_spec[1] last=$particle_spec[2]
if($status != 0) exit(2);
