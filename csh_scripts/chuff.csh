####################
# chuff.csh : 
#  NOTE: "chuff.csh" is intended to never be modified or even read.
#   Please just use it as a handy helper script.
#
# It accomplishes the following things:
#
#  1.  Reads from the MATLAB style parameter file "chuff_parameters.m"
#    and sets shell scripts variables (for example, "$target_magnification")
#    corresponding to the matlab variables that are assigned in this file.
#
#  2. Reads the command line arguments and sets shell variables 
#    according to the following scheme:
#   <chuff_command> $script_args[1]> $script_arg[2] [...] 
#        [option1 = x[,y,...]]> [option2 = x[,y,...]] [...]
#
#  Example: chumps_frealign 0001_MT1_1 0034_MT2_1 nuke=1 refine_coordinate_mask=1,1,0,0,0
#    Doing "source chuff.csh" within the "chumps_frealign" script would set the following
#     shell variables:
#     $script_args[1] will be "0001_MT1_1"
#     $script_args[2] will be "0034_MT2_1"
#     $nuke will be "1"
#     $refine_coordinate_mask will be "1 1 0 0 0"
#
#  3. For convenience: sets "$orig_dir" to the directory where the
#    current script was executed from.

#  4. Reads from the header of the <chuff_command> script to set
#   the following (optional) special shell variables
#  
#     chuff_args, chuff_required_variables, chuff_required_executables
#  
#   Please check a sample chuff script (i.e., "chumps_ref_align")
#    for an example of how the above special variables can be specified.
#
#  5. Cross-checks the shell variables assigned in steps 1,2 above
#    with the special user-defined shell variable "$chuff_args" 
#    (defines what options the user can type on the command line) and
#    "$chuff_required_variables" (defines which variables NEED to be set
#    in steps 1,2)
#
#  Note that steps 4,5 aren't required: they will be skipped if the 
#    script writer chooses to omit the corresponding header
#    line for step 4.  In this case, command line arguments will not be
#    recognized.
####################

if($?chuff_get_extra_variables) then
  set awk_option = '-v chuff_get_extra_args=1'
else
  set awk_option = ''
endif

if($?noclobber) unset noclobber             # This shell variable causes endless pain!

set orig_dir = `pwd`

set project_name = `echo "$orig_dir:t" | awk '{gsub(" ","_"); print}'`
set script_cmd = "$0"
set script_cmd = "$script_cmd:t"

######
# Note, modern scripts will use 'cf-parameters.m'; but for backwards compatibility, if this 
#   does not exist, we will look for 'chuff_parameters.m' or 'chuff_parameters.txt'
######

if(-e "$orig_dir/cf-parameters.m") then
    set parameter_file = "$orig_dir/cf-parameters.m"
else if(-e "$orig_dir/chuff_parameters.m") then
    set parameter_file = "$orig_dir/chuff_parameters.m"
else if(-e "$orig_dir/chuff_parameters.txt") then
    set parameter_file = "$orig_dir/chuff_parameters.txt"
else
    set parameter_file = "$orig_dir/cf-parameters.m"
endif

###################
# Warning: the above shell variables would be dangerous to use if the directory name
#  contains a space.
###################

set spaces_in_dir = `echo $project_name "$orig_dir:t" | awk '{if($1 == $2) print 0; else print 1}'`
if($spaces_in_dir == 1) then
    echo 'ERROR: please do not use these scripts in a directory whose name contains spaces:'
    echo   'Directory name: "'$orig_dir'"'
    exit(2)
endif

####################
# Set all "chuff" shell variables by parsing the parameter file, command line, and script file
####################

if(! -e "$parameter_file") then
    if(! $?chuff_no_parameter_file) then
#        echo 'NOTE: file "chuff_parameters.m" (or "chuff_parameters.txt") not found.'
#        echo ' If needed, you can copy the template of this file from your "chuff3"'
#        echo '   directory into the current directory and edit appropriately.'
    else
        set parameter_file=''
    endif
endif

if(-e "$chuff_dir/cf-machine-parameters.m") then
    set parameter_files = $parameter_file','$chuff_dir/cf-machine-parameters.m
else if(-e "$chuff_dir/../chuff_machine_parameters.m") then
    set parameter_files = $parameter_file','$chuff_dir/../chuff_machine_parameters.m
else
    set parameter_files = $parameter_file','$chuff_dir/cf-machine-parameters.m
endif

set nargv = `echo $argv | awk '{print NF}'`

if("$argv" == null || $nargv == 0) then
    set chuff_cmd = ()
    set argv_new = ()
else
    set chuff_cmd = $argv[1]
    if($#argv > 1) then
        set argv_new = ($argv[2-])
    else
        set argv_new = ()
    endif
endif

if($#chuff_cmd > 0) then
    echo $chuff_cmd:t $argv_new "#" `date` `hostname` : $orig_dir >>! $orig_dir/cf-commands.log
    set chuff_cmd_contents = "cat -- $chuff_cmd ;"
else
    set chuff_cmd_contents = "echo ''"
endif

set arg_cmd = `(echo "$argv_new"; eval $chuff_cmd_contents) | awk $awk_option -v parameter_file="$parameter_files" -f $chuff_dir/awk_scripts/get_chuff_variables.awk`

eval "$arg_cmd"

if($?chuff_error) then
    set ask_for_help = `(echo "$argv_new"; cat -- "$chuff_cmd") | awk -v diagnose=1 $awk_option -v parameter_file="$parameter_files" -f $chuff_dir/awk_scripts/get_chuff_variables.awk`

    if($#ask_for_help == 4) then
        if($ask_for_help[2] == unrecognized && $ask_for_help[3] == option) then
            if($ask_for_help[4] == \"-h\" || $ask_for_help[4] == \"--help\") then
                unset chuff_error
                set help
            endif
        endif
    endif
endif

if($?chuff_error) then
    (echo "$argv_new"; cat -- "$chuff_cmd") | awk -v diagnose=1 $awk_option -v parameter_file="$parameter_files" -f $chuff_dir/awk_scripts/get_chuff_variables.awk

    set help
endif

if($?cfdebug) then
    echo 'Entering accessory script: chuff.csh ...'
    (echo "$argv_new"; cat -- "$chuff_cmd") | awk $awk_option -v parameter_file="$parameter_files" -f $chuff_dir/awk_scripts/get_chuff_variables.awk | awk '{n = split($0, a, "[ ]*[;][ ]*"); for(i = 1; i <= n; ++i) print a[i]}'

endif

################################################################################
# Make sure the essential parameters have been assigned...
################################################################################

if(! $?chuff_required_variables) set chuff_required_variables = ()
foreach required_var($chuff_required_variables)
	if(! `eval 'if($?'$required_var') echo 1'`) then
	    echo 'ERROR: parameter files:' 
            echo   "$parameter_files"
            echo   'are lacking the following variable definition:'
	    echo '       "'$required_var'"'
            echo ' (do the parameter files exist?)'
	    exit(2)
	endif
end

####################
# Set the following variables for convenience...
####################
if($?target_magnification && $?scanner_pixel_size) then
    set micrograph_pixel_size = `echo $target_magnification $scanner_pixel_size | awk '{print $2 * 10000 / $1}'`
endif

if($?micrograph_pixel_size && $?helical_repeat_distance) then
    set chumps_pixels_per_repeat = `echo $micrograph_pixel_size $helical_repeat_distance | awk '{print $2/$1}'`
endif

####################
# Check the saved version of the parameter file to make sure
#  nothing has changed.
####################

set chuff_params_copy = $parameter_file:r'_saved.'$parameter_file:e
if(-e $chuff_params_copy) then
    set safety_copy = `diff -d $parameter_file $chuff_params_copy`

    if($#safety_copy > 0) then
        if(! $?prompt) then
	    echo "$safety_copy"

	    echo 'WARNING: chuff previously run with a different set of parameters.'
	    printf '\n'
	    echo '###########################################'
	    echo 'Results of "diff '$parameter_file:t' '$chuff_params_copy'" :'
	    diff $parameter_file $chuff_params_copy
	    echo '###########################################'

	    echo '  If you have not changed a fundamentally important parameter, such as'
	    echo '   pixel size, voltage, etc.'
	    echo '   (please refer to above differences as reported by the "diff" command)'
	    echo '   then it is safe to proceed (type "Y").'
	    echo '  Otherwise, please type "N" (or hit return), delete the analysis directories'
            echo '   (ctf_files, chumps_round1, chumps_round2, etc.)'
	    echo '   and then run "chuff" again.'
	    printf 'Proceed?  (Y or N):'

	    set response = $<
	    set response = `echo $response | awk '{printf substr(tolower($1), 1, 1);}'`
	    if($response == "y") then
		/bin/cp $parameter_file $chuff_params_copy
	    else
		exit(2)
	    endif
        else
            echo 'ERROR: chuff was previously run with a different set of parameters.'
            printf '\n'
            echo '###########################################'
            echo 'Results of "diff '$parameter_file:t' '$chuff_params_copy'" :'
            diff $parameter_file $chuff_params_copy
            echo '###########################################'

            echo '  If you have not changed a fundamentally important parameter, such as'
            echo '   pixel size, voltage, etc.'
            echo '   (please refer to above differences as reported by the "diff" command)'
            echo '   then please type the following command before rerunning chuff:'
            echo "      /bin/cp $parameter_file $chuff_params_copy"
            echo '  Otherwise, please type "N" (or hit return), delete the analysis directories'
            echo '   (ctf_files, chumps_round1, chumps_round2, etc.)'
            exit(2)
	endif
    endif
else
    if(-e $parameter_file) /bin/cp $parameter_file $chuff_params_copy
endif

####################
# Handle help request
####################
if($?help) then
    echo 'Usage:'
    printf '  '$script_cmd' [<file1> <file2> ...]  '

    set arg_cmd = `(echo "help=1"; cat -- "$chuff_cmd") | awk $awk_option -v parameter_file="$parameter_files" -f $chuff_dir/awk_scripts/get_chuff_variables.awk`

    eval "$arg_cmd"

    foreach blah($chuff_args)
        if(`eval echo '$?'$blah`) then
            set default = `eval echo '$'$blah | awk '{gsub("[ ]",",",$0); print}'`
            printf $blah' [='$default']  '
        else
            printf $blah'  '
        endif
    end
    echo null | awk '{printf("\n")}'

    awk 'BEGIN {keyword = "chuff_help_info"; keyword2 = "chuff_end_help"; printf("\n");} NF < 1 || tolower($0) ~ "^#[ ]*" keyword2 {print_help = 0} print_help == 1 {sub("^[ ]*#[ ]?",""); print} tolower($0) ~ "^#[ ]*" keyword {sub("^#[ ]*" keyword, ""); print_help = 1}' "$0"

    exit(2)
endif

set argv = ''

if($?cfdebug) then
    echo 'Successfully completed accessory script: chuff.csh .'
endif
