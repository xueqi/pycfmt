function [twist_per_subunit, rise_per_subunit, mt_radius,axial_repeat_dist,twist_per_repeat] = mt_lattice_params(num_pfs,  num_starts, dimer_repeat_dist, radius13pf)
% function [twist_per_subunit, rise_per_subunit, mt_radius,axial_repeat_dist,twist_per_repeat] = mt_lattice_params(num_pfs,  num_starts, dimer_repeat_dist, radius13pf)
%
%%%%%%%%%%%%%%%%%%%%%%%%%%
% PLEASE NOTE:
%  The number of starts MUST be input in terms of 80A repeats, so for example
%   canonical microtubules will have 1.5 starts in this notation!!!!
%  Another point of confusion: twist_per_subunit and twist_per_repeat are returned
%   according to a ***right-handed*** coordinate convention, which contrasts with the
%   left-handed convention used by SPIDER, etc. for Euler angles. 
%%%%%%%%%%%%%%%%%%%%%%%%%%

monomer_starts = num_starts*2;

subunit_angle13 = atan2(3/2*dimer_repeat_dist, 2*pi*radius13pf);
% subunit_angle13*180/pi
% delta_x = 51.5;
delta_x = 2*pi*radius13pf/(13*cos(subunit_angle13));

%halfdist = dimer_repeat_dist/2;
%delta_n = 3 * halfdist * num_pfs / 13 - num_starts * halfdist;
% The following line is mathematically equivalent to the above 2 lines, but
% is written in terms of 80A tub dimer repeats rather than 40A tub monomer repeats:
delta_n = dimer_repeat_dist * (3/2 * num_pfs / 13 - num_starts);

theta = atan2(delta_n,num_pfs*delta_x);
% theta*180/pi

%pitch = num_starts*halfdist*cos(theta);
% The following line is mathematically equivalent to the above line, but also 
%  written in terms of 80A repeats:
pitch = num_starts*dimer_repeat_dist*cos(theta);

rise_per_subunit = pitch/num_pfs;
subunit_angle = subunit_angle13 - theta;

%mt_radius = num_pfs * delta_x * cos(subunit_angle13) / (2*pi);
%          = num_pfs * 2*pi*radius13pf/(13*cos(subunit_angle13))/*(2*pi) / cos(subunit_angle13)
mt_radius = num_pfs * radius13pf/13;
twist_per_subunit = delta_x * cos(subunit_angle) / mt_radius;

twist_per_repeat = (twist_per_subunit * num_pfs - 2*pi) / num_starts;
axial_repeat_dist = cos(theta) * dimer_repeat_dist;

return
