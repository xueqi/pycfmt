function out=BinImage(in,nb,mode)
% function out=BinImage(in,nb)
% Bin a square image by averaging nb x nb pixels to produce each output
% pixel.  If the image size is not a multiple of nb, it is cropped first.
nb=round(nb);
if nb<=1
    out=in;
    return;
end;

[n ny]=size(in);
if ny~=n
    error('Input image must be square');
end;
if nargin<3
    mode='mean';
end;

% Check to see if n is a multiple of nb, and if not, force it.
q=mod(n,nb);
if q>0
    q1=floor(q/2)+1;
    q2=n-(q+1-q1);
    in=in(q1:q2,q1:q2);
    n=n-q;
end;
n1=n/nb;
% Bin along x
in=reshape(in,nb,n*n1);   % make nb adjacent pixels a column
switch mode
    case 'mean'
        out1=mean(in);
    case 'max'
        out1=max(in);
    otherwise
        error(['Unrecognized bin mode ' mode]);
end;
        out1=reshape(out1,n1,n)';  % convert back to rectangular image
        % Bin along y
        out1=reshape(out1,nb,n1*n1); % take the mean along y
switch mode
    case 'mean'
        out=mean(out1);
    case 'max'
        out=max(out1);
end;
        
out=real(reshape(out,n1,n1)');
