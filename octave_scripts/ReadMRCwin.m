function [map,s,msg]=ReadMRCwin(filename,window_dim,window_origin,pad_out, test)
% function map=ReadMRCwin(filename);  --all defaults.   Or, with every
% parameter,
% function [map,s]=ReadMRC(filename,startSlice, numSlices,test)
%
% 'Windowed' version of ReadMRC (CVS 1/15)
% 'pad_out' can be used to control what happens if the window is partially or
%   fully outside the file.
%  pad_out = 3 means to fill out the window with a mirror image of
%   the box near the boundary, possibly augmented 'noise' from  a nearby tile within the file)
%  pad_out = 2 means to fill out the window with 'noise' from a nearby tile within the file)
%  pad_out = 1 means to fill out the window with a constant background value, estimated
%   by the median of the truncated edge(s).
%  pad_out = 0 means return whatever clipped window is within the file
%  pad_out = -1 means return -1 if any clipping is detected.
%
% Read a 3D map from a little-endian .mrc file
% and return a structure containing various parameters read from the file.
% This function reads 2d and 3d real maps in byte, int16 and float32 modes.
% Added interpretation of the extended header (length = c(2) which is important with imod and
% serialEM at Brandeis.  lw 15 Aug 07
% Added error tests.  fs 2 Sep 07
% Added the ability to read selected slices, and read big-endian files
% fs 17 Sep 09
% Changed the a array so that the returned s values are doubles.  5 Nov 09
% Added data mode 6 (unsigned int16) fs 20 Jan 10
% Changed s fields to doubles fs 20 Feb 10
% Added fields s.mx, s.my and s.mz (unit cell dimensions), s.org (origin
% offset) along with s.pixA the pixel (voxel) size, which is computed as
% s.pixA=s.rez/s.mx.  The minimum, maximum and average pixel values are
% returned as s.mi, s.ma and s.ma.fs 10 Jul 11

msg = '';

if nargin<4
  pad_out=-1;
end;
if nargin<5
  test=0;
end;

% We first try for little-endian data
f=fopen(filename,'r','ieee-le');
if f<0
    error(['in ReadMRC the file could not be opened: ' filename])
end;
% Get the first 10 values, which are integers:
% nc nr ns mode ncstart nrstart nsstart nx ny nz
a=fread(f,10,'*int32');

if abs(a(1))>1e5  % we must have the wrong endian data.  Try again.
    fclose(f);
    f=fopen(filename,'r','ieee-be');
    a=fread(f,10,'int32');  % convert to doubles
end;

if test
    a(1:10)
end;

mode=a(4);

% Get the next 12 (entries 11 to 23), which are floats.
% the first three are the cell dimensions.
% xlength ylength zlength alpha beta gamma mapc mapr maps
% amin amax amean.

[b,cnt]=fread(f,12,'float32');
if test
   b
end;

% b(4,5,6) angles
s.mi=b(10); % minimum value
s.ma=b(11); % maximum value
s.av=b(12);  % average value

s.rez=double(b(1)); % cell size x, in A.

% get the next 30, which brings us up to entry no. 52.

[c,cnt]=fread(f,30,'*int32');
if test, c(1:3), end;

% c(2) is the extended header in bytes.

% the next two are supposed to be character strings.
[d,cnt]=fread(f,8,'*char');
d=d';
s.chars=d;
if test
    d    
end;

% Two more ints...
[e,cnt]=fread(f,2,'*int32');
if test, e, end;

% up to 10 strings....
ns=min(e(2),10);
for i=1:10
	[g,cnt]=fread(f,80,'uchar');
	str(i,:)=char(g)';
end;
% disp('header:'); disp(' ');
% disp(str(1:ns,:));
% disp(' ');
s.header=str(1:ns,:);

% Get ready to read the data.
s.nx=double(a(1));
s.ny=double(a(2));
s.nz=double(a(3));
s.mx=double(a(8));
s.my=double(a(9));
s.mz=double(a(10));
s.org=double(a(5:7));

if(s.mx != 0)
  s.pixA=s.rez/s.mx;
else
  s.pixA=s.rez;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%   
% Set default window size : whole image, or square/cube if some dim's
%  not specified.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%   

if(nargin < 2)
  wi_x = s.nx;
  wi_y = s.ny;
  wi_z = s.nz;
else
  wi_x = window_dim(1);
  if(prod(size(window_dim)) < 2)
    wi_y = wi_x;
  else
    wi_y = window_dim(2);
  end
  if(prod(size(window_dim)) < 3)
    if(s.nz == 1)
      wi_z = 1;
    else
      wi_z = wi_x;
    end
  else
    wi_z = window_dim(3);
  end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%   
% Set default window origin (1,1,1)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%   

if(nargin < 3)
  wi_ox = 1;
  wi_oy = 1;
  wi_oz = 1;
else
  wi_ox = window_origin(1);
  if(prod(size(window_origin)) < 2)
    wi_oy = 1;
  else
    wi_oy = window_origin(2);
  end
  if(prod(size(window_origin)) < 3)
    wi_oz = 1;
  else
    wi_oz = window_origin(3);
  end
end

[wi_xyz_clip, wi_oxyz_clip, background_oxyz] = ...
  get_clipping([wi_x wi_y wi_z], [wi_ox wi_oy wi_oz], [s.nx s.ny s.nz]);

wi_tx = wi_x; wi_ty = wi_y; wi_tz = wi_z; 
wi_tox = wi_ox; wi_toy = wi_oy; wi_toz = wi_oz; 
wi_x = wi_xyz_clip(1); wi_y = wi_xyz_clip(2); wi_z = wi_xyz_clip(3);
wi_ox = wi_oxyz_clip(1); wi_oy = wi_oxyz_clip(2); wi_oz = wi_oxyz_clip(3);

switch mode
    case 0
        string='*uint8';
        pix_bytes=1;
    case 1
        string='*int16';
        pix_bytes=2;
    case 2
        string='*float32';
        pix_bytes=4;
    case 6
        string='*uint16';
    otherwise
        error(['ReadMRC: unknown data mode: ' num2str(mode)]);
        string='???';
        pix_bytes=0;
end;

if(c(2)>0)
    [ex_header,cnt]=fread(f,c(2),'uchar');   
    disp(['Read extra header of ',num2str(c(2)),' bytes!'])
%    disp((ex_header'));
end

ndata=wi_x*wi_y*wi_z;
if test
  string
  ndata
end;

nsam=s.nx;
nrow=s.ny;
nslice=s.nz;

if(nargin < 2)
  [map,cnt]=fread(f,ndata,string);
else
  cnt = 0;
  map = zeros([wi_x wi_y wi_z], 'single');

  if(wi_x > 0 & wi_y > 0 && wi_z > 0)
    fseek(f, pix_bytes*((wi_oz-1)*nsam*nrow), 'cof');
    for j=1:wi_z
      fseek(f, pix_bytes*(wi_oy-1)*nsam, 'cof');
      for i=1:wi_y
        fseek(f, pix_bytes*(wi_ox-1), 'cof');
        [temp_map,temp_cnt] = fread(f, wi_x, string);
        map(:,i,j) = temp_map;
        cnt = cnt + temp_cnt;
        fseek(f, pix_bytes*(nsam - (wi_ox+wi_x-1)), 'cof');
      end
      fseek(f, pix_bytes*(nrow - (wi_oy+wi_y-1))*nsam, 'cof');
    end
  end
end

fclose(f);

if cnt ~= ndata
%    error('ReadMRC: not enough data in file.');
  map = zeros([wi_x wi_y wi_z]);
else
  map=reshape(map,wi_x,wi_y,wi_z);
end;

%%%%%%%%%%%
% Fill in any part of the box that falls outside the micrograph, with
%  a mirror image of the matching edge segment.
%%%%%%%%%%%

if(!isequal([wi_tx wi_ty wi_tz], wi_xyz_clip(1:3)))
  if(pad_out == 3)
    mirrorpad = 1;
  else
    mirrorpad = 0;
  end
  if(pad_out >= 2 && background_oxyz(1) != -1)
    noise_box = ReadMRCwin(filename, [wi_tx wi_ty wi_tz], background_oxyz, -1);

    map = pad_gently(map, [wi_tx wi_ty wi_tz], ...
                          wi_oxyz_clip, [wi_tox wi_toy wi_toz],...
                          mirrorpad, noise_box);
  elseif(pad_out > 0)  % 
    map = pad_gently(map, [wi_tx wi_ty wi_tz], ...
                          wi_oxyz_clip, [wi_tox wi_toy wi_toz],mirrorpad);
  elseif(pad_out == -1)
    fprintf('WARNING: ReadMRCwin.m: window is not completely within the image file!\n');
    map = -1;
  end
end
