function [dx, dy] = chumps_align_focal_pairs_simple(micrograph,filter_resolution)

run 'cf-parameters.m'

micrograph_pixel_size = scanner_pixel_size*10000/target_magnification;

%%%%%%%%%%%%%%%%%%
% Derive related filenames
%%%%%%%%%%%%%%%%%%

if(micrograph(1:6) != 'scans/')
  micrograph = sprintf('scans/%s',micrograph);
end
focal_mate = sprintf('%s_focal_mate.mrc',micrograph(1:prod(size(micrograph))-4));
focal_mate_align = sprintf('%s_focal_mate_align.mrc',micrograph(1:prod(size(micrograph))-4))

ctf_doc_name = sprintf('%s_ctf_doc.spi',micrograph(1:prod(size(micrograph))-4));
fmate_ctf_doc_name = sprintf('%s_focal_mate_ctf_doc.spi',micrograph(1:prod(size(micrograph))-4));

%%%%%%%%%%%%%%%%%%
% Read in parameter files and extract relevant data
%%%%%%%%%%%%%%%%%%

ctf_doc = dlmread(ctf_doc_name);

index = find(ctf_doc(:,1) == 1);
if(prod(size(index)) == 0)
  fprintf(stdout,'ERROR: no CTF information found... (please run chumps_ctf first)\n');
  return
end

index = index(prod(size(index)));
defocus = ctf_doc(index,3);
astig_mag = ctf_doc(index,4);
astig_angle = ctf_doc(index,5);

ctf_doc = dlmread(fmate_ctf_doc_name);

index = find(ctf_doc(:,1) == 1);
if(prod(size(index)) == 0)
  fprintf(stdout,'ERROR: no CTF information found... (please run chumps_ctf first)\n');
  return
end

index = index(prod(size(index)));
fmate_defocus = ctf_doc(index,3);
fmate_astig_mag = ctf_doc(index,4);
fmate_astig_angle = ctf_doc(index,5);

printf('Defocus for micrograph: %7.0f ; for focal mate: %7.0f\n',defocus,fmate_defocus);

b_factor = 0;

electron_lambda = EWavelength(accelerating_voltage);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Read micrographs
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

m1 = ReadMRC(micrograph);
m2 = ReadMRC(focal_mate);
s = size(m1);

if(s(1) != s(2))
  if(s(1) < s(2))
    big_dim = s(2);
  else
    big_dim = s(1);
  end
else
  big_dim = s(1)
end

if(defocus > 0)
  ctf_flip1 = sign(CTF_ACR(big_dim, micrograph_pixel_size, ...
                     electron_lambda, defocus/10000, ...
                     spherical_aberration_constant, ...
                     b_factor, amplitude_contrast_ratio, 0));
  ctf_flip2 = sign(CTF_ACR(big_dim, micrograph_pixel_size, ...
                     electron_lambda, fmate_defocus/10000, ...
                     spherical_aberration_constant, ...
                     b_factor, amplitude_contrast_ratio, 0));
  ctf_flip1 = ifftshift(ctf_flip1);
  ctf_flip2 = ifftshift(ctf_flip2);
else
  fprintf(stdout, 'ERROR: ctf information not found...\n');
end

if(s(1) != s(2))
  m1 = pad_gently(m1, [big_dim big_dim], [1 1], [1 1], 1);
  m2ctf = pad_gently(m2, [big_dim big_dim], [1 1], [1 1], 1);

  m1ctf = ctf_flip1.*fftn(m1);
  m2ctf = ctf_flip2.*fftn(m2ctf);
else
  m1ctf = ctf_flip1.*fftn(m1);
  m2ctf = ctf_flip2.*fftn(m2);
end

shift_map = real(ifftn(m1ctf.*conj(m2ctf)));

if(filter_resolution != 0)
  shift_map = SharpFilt(shift_map, micrograph_pixel_size/filter_resolution, 3/s(1));
end

[dx, dy] = find(shift_map == max(shift_map(:)))

% due to padding, the circular shift -x is not equal to xsize - x, where xsize is the padded size

dx = mod(dx + big_dim / 2, big_dim) - big_dim / 2
dy = mod(dy + big_dim / 2, big_dim) - big_dim / 2

if (prod(size(dx)) > 1) 
    dx = round(mean(dx(:)))
end
if (prod(size(dy)) > 1) 
    dy = round(mean(dy(:)))
end

WriteMRC(circshift(m2, [dx dy]),micrograph_pixel_size,focal_mate_align);
