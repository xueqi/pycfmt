function [seam_map, seam_offset] = find_seams_totdecmap(tot_decoration_map)

s = size(tot_decoration_map);


seam_map = zeros(s);

% for pf = 1:2:s(1)
%   if(sum(tot_decoration_map(pf,1:s(2))) > sum(tot_decoration_map(pf+1,1:s(2))))
%     seam_map(pf,1:s(2)) = 1;
%   else
%     seam_map(pf+1,1:s(2)) = 1;
%   end

sum_pfs = sum(tot_decoration_map, 2);
best_sum1 = -1;
best_sum2 = -1;
sum1 = zeros([1 s(1)]);
sum2 = zeros([1 s(1)]);
for seam_pos = 0:2:s(1) - 1
%  current_sum1 = sum(sum(tot_decoration_map(1:2:seam_pos,1:s(2))));
%  current_sum2 = sum(sum(tot_decoration_map(seam_pos+1:2:s(1),1:s(2))));
%
% original implementation will always take seam_pos = 0 or s(1) as maximum.
% because current_sum1(s(1)) always > other current_sum1, same as current_sum2
  sum1(seam_pos + 1) = sum(sum_pfs(1:2:seam_pos)) + sum(sum_pfs(seam_pos + 2:2:s(1)));
  sum2(seam_pos + 1) = sum(sum_pfs(2:2:seam_pos)) + sum(sum_pfs(seam_pos + 1:2:s(1)));
end
[max_sum, seam_offset] = max([sum1(:) sum2(:)](:));
seam_offset = mod(seam_offset - 1, s(1))

% still we need to jump around alpha and beta. because if there is sum decor pattern like 5, 4, 3, 2, 1, all the first 4 pfs will be alpha.
% 
% TODO: And maybe we can use seam pos to optimize the seam map?
%

for pf = 1:2:s(1) - 1
  if(sum_pfs(pf) > sum_pfs(pf+1))
    seam_map(pf,1:s(2)) = 1;
  elseif (sum_pfs(pf) == sum_pfs(pf+1)) 
      % we kind of can not decide in this case. so if left is beta, this is alpha, else this is beta, next is alpha
      if (pf == 1 || seam_map(pf-1, 1) == 0) 
          seam_map(pf,1:s(2)) = 1;
      else
          seam_map(pf+1, 1:s(2)) =1;
      end
  else
    seam_map(pf+1,1:s(2)) = 1;
  end
end
