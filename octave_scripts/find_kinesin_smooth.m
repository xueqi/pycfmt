function [ref_tub, ref_kin, kin_repeat_index,kin_pf_index] = ...
  find_kinesin_smooth(job_dir,filter_resolution,highpass_resolution,use_ctf,focal_mate, ...
            chumps_round,smooth_dir,bin_factor,invert_density_local,b_factor,...
            fit_order, data_redundancy, max_outliers_per_window, ...
            twist_tolerance, coord_error_tolerance,...
            phi_error_tolerance, theta_error_tolerance, ...
            psi_error_tolerance, min_seg_length, ...
            tubrefs,kinrefs,ref_tub,ref_kin,kin_repeat_index,kin_pf_index)
stdout = 1;
n_normal_args = 19;

iterate = 1;
iteration = 1;

% highpass_resolution = 200;
% highpass_resolution = 1e9

job_dir = trim_dir(job_dir);

mkdir(sprintf('chumps_round%d/%s/find_kinesin/',chumps_round,job_dir));
mkdir(sprintf('chumps_round%d/%s/fmate_find/',chumps_round,job_dir));

%%%%%%%%%%%%%%%%%%
% Set other parameters
%%%%%%%%%%%%%%%%%%

ref_dir = sprintf('chumps_round%d/ref_tub_subunit_bin%d',chumps_round,bin_factor);
%tub_ref_file = sprintf('%s/ref_tot.spi',ref_dir);
tub_ref_file = sprintf('%s/tub_centered_vol.spi',ref_dir);
ref_params_name = sprintf('%s/ref_params.spi',ref_dir);
com_doc_name = sprintf('%s/tub_cgr_doc.spi',ref_dir);

ref_dir = sprintf('chumps_round%d/ref_kin_subunit_bin%d',chumps_round,bin_factor);
%kin_ref_file = sprintf('%s/ref_tot.spi',ref_dir);
kin_ref_file = sprintf('%s/kin_centered_vol.spi',ref_dir);

elastic_params = [0 0 0 0];
radius_scale_factor = 1;

%%%%%%%%%%%%%%%%%%
% Get user parameters
%%%%%%%%%%%%%%%%%%

if(isoctave)
  run 'cf-parameters.m'
else
  run 'cf-parameters'
end

invert_density = invert_density_local;

micrograph_pixel_size = scanner_pixel_size*10000/target_magnification;

%%%%%%%%%%%%%%%%%%
% Get alignment and micrograph info
%%%%%%%%%%%%%%%%%%

[micrograph,defocus,astig_mag,astig_angle,b_factor_temp] = ...
  chumps_micrograph_info(job_dir,focal_mate,chumps_round, use_ctf);

[num_pfs,num_starts] = ...
  read_chumps_alignment(job_dir,helical_repeat_distance,chumps_round,micrograph_pixel_size);

sites_per_repeat = 2*num_pfs;
max_overlap = 2*num_pfs;

kin_num_repeats = ceil( (sites_per_repeat+2*max_overlap)/num_pfs );
tub_num_repeats = 2*kin_num_repeats; % make the underlying MT extra long

kin_lib_size = 2*kin_num_repeats*num_pfs;

ref_pixel = dlmread(sprintf('%s/info.txt',ref_dir));
ref_pixel = ref_pixel(2);

% ref_pixel_info = readSPIDERdoc_dlmlike(ref_params_name);

% index = find(ref_pixel_info(:,1) == 2);
% ref_pixel = ref_pixel_info(index(1),5);

% index = find(ref_pixel_info(:,1) == 1);
% min_theta = ref_pixel_info(index(1),5);

% index = find(ref_pixel_info(:,1) == 3);
% d_angle = ref_pixel_info(index(1),3);

%%%%%%%%%%%%%%%%%%
% Read in parameter files and extract relevant data
%%%%%%%%%%%%%%%%%%

com_info = readSPIDERdoc_dlmlike(com_doc_name);

index = find(com_info(:,1) == 1);
ref_com = com_info(index(1),3:5);

est_repeat_distance = helical_repeat_distance;
est_pix_per_repeat = est_repeat_distance/micrograph_pixel_size;

%%%%%%%%%%%%%
% Old
%%%%%%%%%%%%%
% [coords,phi,theta,psi,...
%  d_phi_d_repeat,d_theta_d_repeat,d_psi_d_repeat,...
%  est_repeat_distance_smooth, psi_ref, psi_ref_smooth, discontinuities, outliers] = ...
%   fixup_chuff_alignment(coords,phi,theta,psi,...
%                       directional_psi,micrograph_pixel_size,helical_repeat_distance,...
%                       0, 1, ...
%                       'smooth_all', fit_order, data_redundancy, max_outliers_per_window, ...
%                       twist_tolerance, coord_error_tolerance,...
%                       phi_error_tolerance, theta_error_tolerance, psi_error_tolerance);

%%%%%%%%%%%%%
% Older
%%%%%%%%%%%%%
%[coords,phi,theta,psi,...
% d_phi_d_repeat,d_theta_d_repeat,d_psi_d_repeat,est_repeat_distance] = ...
%  fixup_chumps_alignment(coords,phi,theta,psi,directional_psi,...
%                         micrograph_pixel_size,helical_repeat_distance,...
%                         est_pix_per_repeat, num_pfs, num_starts, ref_com);

load(sprintf('%s/%s_fil_info.mat', smooth_dir, job_dir));
coords = coords_est_smooth;
phi = phi_est_smooth;
theta = theta_est_smooth;
psi = psi_est_smooth;

[coords_dummy,phi_dummy,theta_dummy,psi_dummy,...
 d_phi_d_repeat,d_theta_d_repeat,d_psi_d_repeat] = ...
  fixup_chumps_alignment(coords,phi,theta,psi,90,...
                         micrograph_pixel_size,helical_repeat_distance,...
                         est_pix_per_repeat, num_pfs, num_starts, ref_com);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Find the best continuous segment
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%
% Derive related filenames
%%%%%%%%%%%%%%%%%%

[start] = regexp(job_dir,'_MT[0-9]+[_][0-9]+$','start');

if(focal_mate == 0)
  output_dir = sprintf('chumps_round%d/%s/find_kinesin',chumps_round,job_dir);
  micrograph = sprintf('scans/%s.mrc',job_dir(1:start-1));
else
  output_dir = sprintf('chumps_round%d/%s/fmate_find',chumps_round,job_dir);
  micrograph = sprintf('scans/%s_focal_mate_align.mrc',job_dir(1:start-1));

  lowdf_dir = sprintf('chumps_round%d/%s/find_kinesin',chumps_round,job_dir);
  lowdf_micrograph = sprintf('scans/%s.mrc',job_dir(1:start-1));

  if(use_ctf)
    [micrograph_t,lowdf_defocus,lowdf_astig_mag,lowdf_astig_angle] = ...
      chumps_micrograph_info(job_dir,0,chumps_round);
  endif
end

overlap_file = sprintf('%s/overlap_map.spi',output_dir);

%%%%%%%%%%%%%%%%%%
% Initialize file-related parameters
%%%%%%%%%%%%%%%%%%

n_good_boxes = prod(size(phi));

fprintf(stdout,'%s %4d\n', job_dir, n_good_boxes);

bin_factor = round(ref_pixel/micrograph_pixel_size);
final_pixel_size = micrograph_pixel_size*bin_factor;

box_dim = ceil(1.5*2*filament_outer_radius/final_pixel_size);
box_dim = 16*ceil(box_dim/16);
box_dim = box_dim*bin_factor;
box_dim = [box_dim box_dim];

ref_dim = floor(box_dim/bin_factor);
pad_ref_dim = 2*ref_dim;

%%%%%%%%%%%%%%%%%%%%%%%
% Compute CTF's
%%%%%%%%%%%%%%%%%%%%%%%

electron_lambda = EWavelength(accelerating_voltage);

if(defocus > 0 && use_ctf ~= 0)
  ctf = CTF_ACR(pad_ref_dim(1), ref_pixel, electron_lambda, defocus/10000, ...
                       spherical_aberration_constant, ...
                       b_factor, amplitude_contrast_ratio, 0);
  ctf = ifftshift(ctf);

  box_ctf = CTF_ACR(2*box_dim(1), micrograph_pixel_size, electron_lambda, defocus/10000, ...
                     spherical_aberration_constant, ...
                     b_factor, amplitude_contrast_ratio, 0);
  box_ctf = ifftshift(box_ctf);

  if(focal_mate)
    lowdf_box_ctf = CTF_ACR(2*box_dim(1), micrograph_pixel_size, electron_lambda, lowdf_defocus/10000, ...
                            spherical_aberration_constant, ...
                            b_factor, amplitude_contrast_ratio, 0);
    lowdf_box_ctf = ifftshift(lowdf_box_ctf);
  end
else
  ctf = -ones(pad_ref_dim);
  box_ctf = -ones(2*box_dim);
  if(focal_mate)
    lowdf_box_ctf = -ones(2*box_dim);
  end
end

ctf_flip = sign(ctf);
box_ctf_flip = sign(box_ctf);

if(focal_mate)
  lowdf_box_ctf_flip = sign(lowdf_box_ctf);
end

%%%%%%%%%%%%%%%
% Pre-compute filtering terms
%%%%%%%%%%%%%%%

filter = ifftn(ones(pad_ref_dim));
lp_freq = 0.5*(2*ref_pixel) / filter_resolution;
delta_lp_freq = lp_freq/10;
filter = SharpFilt(filter,lp_freq,delta_lp_freq);

hp_freq = 0.5*(2*ref_pixel) / highpass_resolution;
delta_hp_freq = hp_freq/10;
filter = SharpHP(filter,hp_freq,delta_hp_freq);

filter = fftn(filter);

box_filter = ifftn(ones(2*box_dim));
lp_freq = 0.5*(2*micrograph_pixel_size) / filter_resolution;
delta_lp_freq = lp_freq/10;
box_filter = SharpFilt(box_filter,lp_freq,delta_lp_freq);

hp_freq = 0.5*(2*micrograph_pixel_size) / highpass_resolution;
delta_hp_freq = hp_freq/10;
box_filter = SharpHP(box_filter,hp_freq,delta_hp_freq);

box_filter = fftn(box_filter);

%%%%%%%%%%%%%%%%%%%%%%%
% To save time, can pre-load the reference images
%%%%%%%%%%%%%%%%%%%%%%%

if(nargin < n_normal_args+1)
%  tubrefs = readSPIDERfile(tub_ref_file);
  tub_ref_vol = readSPIDERfile(tub_ref_file);
end
if(nargin < n_normal_args+2)
%  kinrefs = readSPIDERfile(kin_ref_file);
  kin_ref_vol = readSPIDERfile(kin_ref_file);
end

%%%%%%%%%%%%%%%%%%%%%%%
% Finally- the kinesin-finding loop
%%%%%%%%%%%%%%%%%%%%%%%

if(iteration > 1)
  tot_decorate_map = readSPIDERfile(sprintf('%s/tot_decorate_map.spi',output_dir));
else
  tot_decorate_map = zeros([2*num_pfs n_good_boxes]);
end

sort_map_pad = 10;

sorted_map = zeros([num_pfs 2*n_good_boxes]);
full_sorted_map = zeros([num_pfs 2*(sort_map_pad+n_good_boxes)]);

tot_overlap_map = [];

mic_dim = ReadMRCheader(micrograph);

%%%%%%%%%%%%%%%%%%
% Count how many "good" boxes are fully inside the micrograph
%%%%%%%%%%%%%%%%%%

real_n_good_boxes = 0;
for box_num=1:n_good_boxes
  int_coords = round(coords(box_num,:));
  box_origin = int_coords-floor(box_dim/2);
  if(box_origin(1) < 1 || box_origin(2) < 1 || ...
     box_origin(1) + box_dim(1) - 1 > mic_dim(1) || box_origin(2) + box_dim(2) - 1 > mic_dim(2))
    continue
  end
  real_n_good_boxes = real_n_good_boxes + 1;
end

%%%%%%%%%%%%%%%%%%
% DELETE any pre-computed overlap map (could also 
%  read pre-computed overlap map if it has been successfully created)
%%%%%%%%%%%%%%%%%%

if(fopen(overlap_file) > 0)
  delete(overlap_file);
%  [header, overlap_dim] = readSPIDERheader(overlap_file);
%  if(overlap_dim(3) == real_n_good_boxes)
%    tot_overlap_map = readSPIDERfile(overlap_file);
%    fprintf(stdout,'Finished reading pre-existing overlap map...\n');
%    n_good_boxes
%  else
%    fprintf(stdout,'ERROR: incomplete overlap map found: %s\n',overlap_file);
%    fprintf(stdout,'  Expected # of entries: %d\n',real_n_good_boxes);
%    fprintf(stdout,'  # of entries in file: %d\n',overlap_dim(3));
%
%    return;
%  end
end

real_box_num = 0;
for box_num=1:n_good_boxes
  real_box_num = real_box_num + 1;

  fprintf(stdout,'\nBox %4d/%4d : \n', box_num, n_good_boxes);
%  fflush(stdout);

  box = read_chumps_box(micrograph,box_num,coords,box_dim,...
                        bin_factor,0,box_ctf_flip.*box_filter);

  if(focal_mate != 0)
    ref_box = read_chumps_box(lowdf_micrograph,box_num,coords,box_dim,...
                        bin_factor,0,lowdf_box_ctf_flip.*box_filter);
    ccmap = abs(ifftshift(ifftn(fftn(box).*conj(fftn(ref_box)))));
    [findx, findy] = find(ccmap == max(ccmap(:)));
    findx = findx(1) - (floor(ref_dim(1)/2) + 1);
    findy = findy(1) - (floor(ref_dim(2)/2) + 1);
    coords(box_num, :) = coords(box_num, :) + bin_factor*[findx findy];

    box = read_chumps_box(micrograph,box_num,coords,box_dim,...
                          bin_factor,0,box_ctf_flip.*box_filter);
  end    

%  test_box = invert_density*rotate2d(box, psi(box_num)+directional_psi(box_num));   
%  writeSPIDERfile('test_box_align.spi',test_box);

  if(box == -1)
    continue;
  end

  box = invert_density * box;

%%%%%%%%%%%%%%%%%%
% Generate the reference images, padded 2x to ensure that 
%  generate_micrograph() has room to place all the subunits
%%%%%%%%%%%%%%%%%%
  if(nargin < n_normal_args+4)
%    filter = ones(pad_ref_dim);

%    lib_origin_offset = -floor(sites_per_repeat/2);
    lib_origin_offset = 0;

    [box_repeat_origin, kin_repeat_index, kin_pf_index,...
     x_tub, y_tub, phi_tub, theta_tub, psi_tub, phi_mt, theta_mt, psi_mt, ref_tub, ref_kin] = ...
      get_findkin_refs(box_num, ref_dim,[tub_num_repeats kin_num_repeats],...
                   num_pfs,num_starts,...
                   phi, theta, psi, elastic_params,...
                   d_phi_d_repeat,d_theta_d_repeat,d_psi_d_repeat,...
                   est_repeat_distance,radius_scale_factor,ref_com,...
                   tub_ref_vol,kin_ref_vol,ctf,ctf_flip,filter,...
                   ref_pixel);
  end

%%%%%%%%%%%%%%%%%%
% Search for best decoration pattern and store the resulting map
%  Each "repeat map" consists of 2*num_pfs possible locations
%   (1 for each tubulin monomer in a given 80A repeat)
%%%%%%%%%%%%%%%%%%
  decorate = zeros(kin_lib_size,1);

  [decorate] = combinatorial_cc(box,ref_tub,ref_kin, sites_per_repeat, ...
                                lib_origin_offset,max_overlap,tot_overlap_map, ...
                                overlap_file,real_box_num);
%  [decorate] = combinatorial_cc_v2(box,ref_tub,ref_kin, sites_per_repeat, ...
%                                max_overlap,tot_overlap_map, ...
%                                overlap_file,real_box_num,decorate,dec_action_mask,box_num);

  [box_bounds, tot_bounds] = ...
     box_to_tot_findmap(decorate, [box_repeat_origin(2); box_repeat_origin(2)], ...
                   box_repeat_origin(2), ...
                   tot_decorate_map, box_num, sites_per_repeat);

  tot_decorate_map(tot_bounds(1):tot_bounds(2)) = decorate(box_bounds(1):box_bounds(2));

%  tot_decorate_map( 1+(box_num-1)*2*num_pfs : box_num*2*num_pfs) = ...
%     decorate(dec_origin: dec_origin+2*num_pfs-1);

%%%%%%%%%%%%%%%%%%
% Make a physically understandable "decoration map"
%%%%%%%%%%%%%%%%%%



%%%%%%%%%%%%%%%%%%
% Re-create the best match
%%%%%%%%%%%%%%%%%%

  final_map = ref_tub;
  final_map2 = ref_tub;
%  for index=1:sites_per_repeat
%    i = index + dec_origin;
  for i = 1:kin_lib_size
    final_map = final_map + decorate(i)*ref_kin(:,:,i);
%    if(i >= dec_origin && i <= dec_origin + sites_per_repeat - 1)
    if(i < box_bounds(1))
      final_map2 = final_map2 + ref_kin(:,:,i);
    end
  end

%%%%%%%%%%%%%%%%%%
% Write files
%%%%%%%%%%%%%%%%%%

  outfile = sprintf('%s/find_%04d_1.spi',output_dir,box_num);
  writeSPIDERfile(outfile,box);

  outfile = sprintf('%s/find_%04d_2.spi',output_dir,box_num);
  writeSPIDERfile(outfile,ref_tub);

  outfile = sprintf('%s/find_%04d_3.spi',output_dir,box_num);
  writeSPIDERfile(outfile,final_map);

  outfile = sprintf('%s/find_%04d_4.spi',output_dir,box_num);
  writeSPIDERfile(outfile,final_map2);

  outfile = sprintf('%s/tot_decorate_map_%d.spi',output_dir,iteration);
  writeSPIDERfile(outfile,tot_decorate_map);

  outfile = sprintf('%s/sorted_map_%d.spi',output_dir,iteration);
  writeSPIDERfile(outfile, sorted_map);

  outfile = sprintf('%s/tot_decorate_map.spi',output_dir);
  writeSPIDERfile(outfile,tot_decorate_map);

  outfile = sprintf('%s/sorted_map.spi',output_dir);
  writeSPIDERfile(outfile, sorted_map);

  sorted_map_file = sprintf('%s/sorted_map_full.spi',output_dir);
  writeSPIDERfile(sorted_map_file,full_sorted_map);

  fprintf(stdout, ...
    '\n%2d/%2d occupied; CCC = %6.4f; Naked CCC = %6.4f; Fulldec CCC = %6.4f\n', ...
     sum(decorate(box_bounds(1):box_bounds(2))), num_pfs,...
                 ccc(box,final_map),...
                 ccc(box,ref_tub),...
                 ccc(box,final_map2));
%  fflush(stdout);
end
return
