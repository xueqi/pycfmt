function out = rotate2d(inp,angle,epsilon)

if(nargin < 3)
  epsilon = 1e-9;
end

angle_round90 = 90*round(angle/90);
if(abs(angle - angle_round90) < epsilon)
  angle = angle_round90;
end

s = size(inp);
pow2dim = max(2.^ceil(log2(s)));
if(pow2dim != s(1) || pow2dim != s(2))
  m = ceil(pow2dim/s(1));
  n = ceil(pow2dim/s(2));
  out = repmat(inp, m, n);
  s2 = size(out);
  out = circshift(out,floor(s2/2) - floor(s/2));

  orig = 1 + floor(s2/2) - pow2dim/2;
  out = out(orig(1):orig(1)+pow2dim-1,orig(2):orig(2)+pow2dim-1);

  out = dctrotate(out,angle);

  orig = 1 + pow2dim/2 - floor(s/2);
  out = out(orig(1):orig(1)+s(1)-1,orig(2):orig(2)+s(2)-1);
else
  out = dctrotate(inp,angle);
end
