%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Accessory functions to make the code more readable...
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function out_dir = trim_dir(job_dir)

if(job_dir(prod(size(job_dir))) == '/')
  job_dir=job_dir(1:prod(size(job_dir))-1);
end
[start] = regexp(job_dir,'[^/]*$','start');
out_dir = job_dir(start:prod(size(job_dir)));

return
