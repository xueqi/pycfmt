function [d_phi_d_repeat,d_theta_d_repeat,d_psi_d_repeat,est_repeat_distance] = ...
  chuff_estimate_derivatives(phi,theta,psi, expected_dphi,smoothing_hwidth)

n_good_boxes = prod(size(phi));

%%%%%%%%%%%%%%%%%%%%
% Fix psi so it is continuous (avoid wraparound at 360)
%%%%%%%%%%%%%%%%%%%%

  psi_mid = floor(n_good_boxes/2)+1;
  psi_low = psi_mid - smoothing_hwidth;
  psi_high = psi_mid + smoothing_hwidth;
  if(psi_low < 1)
    psi_low = 1;
  end
  if(psi_high > n_good_boxes)
    psi_high = n_good_boxes;
  end
  psi_mid_val = median(psi(psi_low:psi_high));

  psi(find(psi > psi_mid_val + 180)) = ...
    psi(find(psi > psi_mid_val + 180)) - 360;
  psi(find(psi < psi_mid_val - 180)) = ...
    psi(find(psi < psi_mid_val - 180)) + 360;

for j=1:n_good_boxes

  l_bound = j-smoothing_hwidth-1;
  if(l_bound < 1)
    l_bound = 1;
  end
  u_bound = j+smoothing_hwidth;
  if(u_bound > n_good_boxes)
    u_bound = n_good_boxes;
  end

  phi_temp = phi(l_bound:u_bound);
  phi_temp = fix_rollover(phi_temp, expected_dphi);

  theta_temp = theta(l_bound:u_bound);
  psi_temp = psi(l_bound:u_bound);

%%%%%%%%%%%%%%%
% estimate derivates from linear fits, if possible
%%%%%%%%%%%%%%%

  j_temp = l_bound:u_bound;
  if(prod(size(phi_temp > 1)))
    phi_params=linear_fit(j_temp,phi_temp,1);
    d_phi_d_repeat(j) = phi_params(1);
  else
    d_phi_d_repeat(j) = 0;
  end

  j_temp = l_bound:u_bound;

  if(prod(size(theta_temp > 1)))
    theta_params=linear_fit(j_temp,theta_temp);
    d_theta_d_repeat(j) = theta_params(1);
  else
    d_theta_d_repeat(j) = 0;
  end

  j_temp = l_bound:u_bound;
  if(prod(size(psi_temp > 1)))
    psi_params=linear_fit(j_temp,psi_temp);
    d_psi_d_repeat(j) = psi_params(1);
  else
    d_psi_d_repeat(j) = 0;
  endif
end
