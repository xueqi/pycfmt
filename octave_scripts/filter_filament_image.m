function out_stack = filter_filament_image(stack, pixel_size, lp_resolution, hp_resolution, circular_mask_radius, reduce_boundary_artifacts)
%%%%%%%%%%%%%%%%%%
% function filter_filament_image(stack, pixel_size, lp_resolution, hp_resolution, circular_mask_radius)
%  stack: 1 or more 2d images (square)
%  pixel_size: pixel size (A or other arbitrary units)
%  lp_resolution: resolution of low-pass filter to be applied (units of 'pixel_size'); set to <= 0 to ignore
%  hp_resolution: resolution of high-pass filter to be applied (units of 'pixel_size'); set to <= 0 to ignore
%  circular_mask_radius: account for pre-masking of images by a circular mask
%  reduce_boundary_artifacts: set to 1 to reduce rippling artifacts related to filament truncation near image boundaries
%%%%%%%%%%%%%%%%%%

if(nargin < 3)
  lp_resolution = 0;
end
if(nargin < 4)
  hp_resolution = 0;
end

mask_falloff_pixels = 4;

m = size(stack);

if(nargin < 5)
  circular_mask_radius = -1;
end
if(nargin < 6)
  reduce_boundary_artifacts = 1;
end

if(circular_mask_radius < 0)
  circular_mask_radius = floor(0.45 * m(1));
end

if(circular_mask_radius == 0)
  circ_mask = ones(m);
else
  circ_mask = fuzzymask(m(1),2,circular_mask_radius,mask_falloff_pixels);
end

out_stack = zeros(m);
if(prod(size(m)) < 3)
  n_images = 1;
else
  n_images = m(3)
end

for ind = 1:n_images
  c = stack(:,:,ind);
 
  if(reduce_boundary_artifacts)
    c = smear_filament_background(c);
  end
  c = real(c);

  c = mirror_pbc_2d(c);

  if(lp_resolution ~= 0)
%    c = ifftn(fftn(c.*fftshift(CTF_ACR(size(c)(1), 2.66, 100, 0, 0, -200, -1, 0))));
    c = SharpFilt(c, pixel_size / lp_resolution);
  end

  if(hp_resolution ~= 0)
    c = SharpHP(c, pixel_size / hp_resolution);
  end

  c = c(1:m(1), 1:m(2));

  val = c(m(1)/2, floor(0.85*m(2)));
  c = c .* circ_mask + val * ones(size(c)) .* (1-circ_mask);

  out_stack(:,:,ind) = c;  
end
