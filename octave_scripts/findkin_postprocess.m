chumps_round = str2num(argv(){1});
job_dir = argv(){2}
job_dir = trim_dir(job_dir);

find_single_dimers = str2num(argv(){3});

input_file = sprintf('chumps_round%d/%s/find_kinesin/sorted_map_full.spi',chumps_round,job_dir);
sorted_map = readSPIDERfile(input_file);
s = size(sorted_map);

input_file = sprintf('chumps_round%d/%s/find_kinesin/tot_decorate_map.spi',chumps_round,job_dir);
tot_decorate_map_ld = readSPIDERfile(input_file);

load(sprintf('chumps_round%d/%s/find_kinesin/sorted_indices.mat',chumps_round,job_dir), 'sorted_indices', 'box_bounds');

[seam_mask, pf_offset, seam_mask_totdec, seam_mask_multi] = find_seams(sorted_map, tot_decorate_map_ld, sorted_indices, box_bounds);

output_file = sprintf('chumps_round%d/%s/find_kinesin/sorted_map_full_seam_mask.spi',chumps_round,job_dir);
writeSPIDERfile(output_file,seam_mask);

output_file = sprintf('chumps_round%d/%s/find_kinesin/sorted_map_full_seam_mask_multi.spi',chumps_round,job_dir);
writeSPIDERfile(output_file,seam_mask_multi);

output_file = sprintf('chumps_round%d/%s/find_kinesin/tot_decorate_map_seam_mask.spi',chumps_round,job_dir);
writeSPIDERfile(output_file,seam_mask_totdec);

input_file = sprintf('chumps_round%d/%s/fmate_find/sorted_map_full.spi',chumps_round,job_dir);
if(fopen(input_file) > 0)
  hidf_exists = 1;
else
  hidf_exists = 0;
end

if(hidf_exists)
  input_file = sprintf('chumps_round%d/%s/fmate_find/tot_decorate_map.spi',chumps_round,job_dir);
  tot_decorate_map_hd = readSPIDERfile(input_file);

  input_file = sprintf('chumps_round%d/%s/fmate_find/sorted_map_full.spi',chumps_round,job_dir);
  sorted_map_hidf = readSPIDERfile(input_file);
  sorted_map_composite = sorted_map.*sorted_map_hidf;

  [seam_mask, pf_offset, seam_mask_totdec, seam_mask_multi] = ...
    find_seams(sorted_map_hidf, tot_decorate_map_hd, sorted_indices, box_bounds);

  dimer_map_hd = find_dimers_totdecmap(tot_decorate_map_hd,1, seam_mask_totdec);
  output_file = sprintf('chumps_round%d/%s/fmate_find/tot_decorate_map_dimer.spi',chumps_round,job_dir);
  writeSPIDERfile(output_file,dimer_map_hd);

  tot_010_map = find_010_totdecmap(tot_decorate_map_hd,tot_decorate_map_ld, seam_mask_totdec);
  output_file = sprintf('chumps_round%d/%s/find_kinesin/tot_decorate_map_010.spi',chumps_round,job_dir);
  writeSPIDERfile(output_file,tot_010_map);

  tot_01_map = find_01_totdecmap(tot_decorate_map_hd,tot_decorate_map_ld, seam_mask_totdec);
  output_file = sprintf('chumps_round%d/%s/find_kinesin/tot_decorate_map_01.spi',chumps_round,job_dir);
  writeSPIDERfile(output_file,tot_01_map);

  tot_10_map = find_10_totdecmap(tot_decorate_map_hd,tot_decorate_map_ld, seam_mask_totdec);
  output_file = sprintf('chumps_round%d/%s/find_kinesin/tot_decorate_map_10.spi',chumps_round,job_dir);
  writeSPIDERfile(output_file,tot_10_map);

  tot_01110_map = find_01110_totdecmap(tot_decorate_map_hd,tot_decorate_map_ld, seam_mask_totdec);
  output_file = sprintf('chumps_round%d/%s/find_kinesin/tot_decorate_map_01110.spi',chumps_round,job_dir);
  writeSPIDERfile(output_file,tot_01110_map);

  tot_011110_map = find_011110_totdecmap(tot_decorate_map_hd,tot_decorate_map_ld, seam_mask_totdec);
  output_file = sprintf('chumps_round%d/%s/find_kinesin/tot_decorate_map_011110.spi',chumps_round,job_dir);
  writeSPIDERfile(output_file,tot_011110_map);

  tot_0110_map = find_0110_totdecmap(tot_decorate_map_hd,tot_decorate_map_ld, seam_mask_totdec);
  output_file = sprintf('chumps_round%d/%s/find_kinesin/tot_decorate_map_0110.spi',chumps_round,job_dir);
  writeSPIDERfile(output_file,tot_0110_map);

  tot_012120_map = find_012120_totdecmap(tot_decorate_map_hd,tot_decorate_map_ld, seam_mask_totdec);
  output_file = sprintf('chumps_round%d/%s/find_kinesin/tot_decorate_map_012120.spi',chumps_round,job_dir);
  writeSPIDERfile(output_file,tot_012120_map);

  sorted_010_map = zeros(s);
  sorted_010_map(sorted_indices) = tot_010_map(box_bounds(1):box_bounds(2));
  output_file = sprintf('chumps_round%d/%s/find_kinesin/sorted_map_full_010.spi',chumps_round,job_dir);
  writeSPIDERfile(output_file,sorted_010_map);

  sorted_0110_map = zeros(s);
  sorted_0110_map(sorted_indices) = tot_0110_map(box_bounds(1):box_bounds(2));
  output_file = sprintf('chumps_round%d/%s/find_kinesin/sorted_map_full_0110.spi',chumps_round,job_dir);
  writeSPIDERfile(output_file,sorted_0110_map);

  sorted_01110_map = zeros(s);
  sorted_01110_map(sorted_indices) = tot_01110_map(box_bounds(1):box_bounds(2));
  output_file = sprintf('chumps_round%d/%s/find_kinesin/sorted_map_full_01110.spi',chumps_round,job_dir);
  writeSPIDERfile(output_file,sorted_01110_map);

  sorted_011110_map = zeros(s);
  sorted_011110_map(sorted_indices) = tot_011110_map(box_bounds(1):box_bounds(2));
  output_file = sprintf('chumps_round%d/%s/find_kinesin/sorted_map_full_011110.spi',chumps_round,job_dir);
  writeSPIDERfile(output_file,sorted_011110_map);

else
  [seam_mask, pf_offset, seam_mask_totdec, seam_mask_multi] = find_seams(sorted_map, tot_decorate_map_ld, sorted_indices, box_bounds);
endif

% map = find_dimers_totdecmap(tot_decorate_map,1);
% output_file = sprintf('chumps_round%d/%s/fmate_find/tot_decorate_map_special.spi',...
%                       chumps_round,job_dir);
% writeSPIDERfile(output_file,dimer_map);
if(exist(sprintf('chumps_round%d/%s/pf_offset.txt', chumps_round, job_dir)) != 2 ||
   exist(sprintf('chumps_round%d', chumps_round+1)) != 7)
  fp = fopen(sprintf('chumps_round%d/%s/pf_offset.txt', chumps_round, job_dir), 'w');
  fprintf(fp, '%d\n', pf_offset);
else
  fprintf(stdout, 'NOTE: pf_offset will not be overwritten because it may have been used for the next round of chumps\n');
end

output_file = sprintf('chumps_round%d/%s/find_kinesin/sorted_map_full_clean.spi',chumps_round,job_dir);
writeSPIDERfile(output_file,sorted_map.*seam_mask);

tmap = sorted_map;
tmap(:,2:2:s(2)) = 0;
output_file = sprintf('chumps_round%d/%s/find_kinesin/sorted_map_full_even.spi',chumps_round,job_dir);
writeSPIDERfile(output_file,tmap);

tmap = sorted_map;
tmap(:,1:2:s(2)) = 0;
output_file = sprintf('chumps_round%d/%s/find_kinesin/sorted_map_full_odd.spi',chumps_round,job_dir);
writeSPIDERfile(output_file,tmap);

dimer_map = find_dimers(sorted_map.*seam_mask, seam_mask);
lat_dimer_map = find_lateral_dimers(sorted_map.*seam_mask, seam_mask);
clean_monomer_map = find_monomer(sorted_map.*seam_mask, seam_mask);

output_file = sprintf('chumps_round%d/%s/find_kinesin/sorted_map_full_lowdf_dimer.spi',chumps_round,job_dir);
writeSPIDERfile(output_file,dimer_map);
output_file = sprintf('chumps_round%d/%s/find_kinesin/sorted_map_full_lowdf_lat_dimer.spi',chumps_round,job_dir);
writeSPIDERfile(output_file,lat_dimer_map);
output_file = sprintf('chumps_round%d/%s/find_kinesin/sorted_map_full_lowdf_monomer.spi',chumps_round,job_dir);
writeSPIDERfile(output_file,clean_monomer_map);

outmat = zeros(s(1),7);
outmat(:,1) = 1:s(1);
outmat(:,2) = sum(sorted_map(:,1:2:s(2)),2)/(s(2)/2);
outmat(:,3) = sum(sorted_map(:,2:2:s(2)),2)/(s(2)/2);

if(hidf_exists)

  sorted_map_composite = sorted_map.*sorted_map_hidf;

  [seam_mask_hidf, pf_offset_hidf, seam_mask_totdec_hidf, seam_mask_multi_hidf] = ...
    find_seams(sorted_map_hidf, tot_decorate_map_hd, sorted_indices, box_bounds);

  output_file = sprintf('chumps_round%d/%s/find_kinesin/sorted_map_full_hidf_seam_mask.spi',chumps_round,job_dir);
  writeSPIDERfile(output_file,seam_mask_hidf);

  output_file = sprintf('chumps_round%d/%s/find_kinesin/sorted_map_full_hidf_seam_mask_multi.spi',chumps_round,job_dir);
  writeSPIDERfile(output_file,seam_mask_multi_hidf);

  output_file = sprintf('chumps_round%d/%s/find_kinesin/tot_decorate_map_hidf_seam_mask.spi',chumps_round,job_dir);
  writeSPIDERfile(output_file,seam_mask_totdec_hidf);

  output_file = sprintf('chumps_round%d/%s/find_kinesin/sorted_map_full_composite.spi',chumps_round,job_dir);
  writeSPIDERfile(output_file,sorted_map_composite);

  output_file = sprintf('chumps_round%d/%s/find_kinesin/sorted_map_full_hidf_clean.spi',chumps_round,job_dir);
  writeSPIDERfile(output_file,sorted_map_hidf.*seam_mask);
  output_file = sprintf('chumps_round%d/%s/find_kinesin/sorted_map_full_composite_clean.spi',chumps_round,job_dir);
  writeSPIDERfile(output_file,sorted_map_composite.*seam_mask);

  dimer_map = find_dimers(sorted_map_hidf.*seam_mask, seam_mask);
  lat_dimer_map = find_lateral_dimers(sorted_map_hidf.*seam_mask, seam_mask);
  clean_monomer_map = find_monomer(sorted_map_hidf.*seam_mask, seam_mask);

  output_file = sprintf('chumps_round%d/%s/find_kinesin/sorted_map_full_hidf_dimer.spi',chumps_round,job_dir);
  writeSPIDERfile(output_file,dimer_map);
  output_file = sprintf('chumps_round%d/%s/find_kinesin/sorted_map_full_hidf_lat_dimer.spi',chumps_round,job_dir);
  writeSPIDERfile(output_file,lat_dimer_map);
  output_file = sprintf('chumps_round%d/%s/find_kinesin/sorted_map_full_hidf_monomer.spi',chumps_round,job_dir);
  writeSPIDERfile(output_file,clean_monomer_map);
  output_file = sprintf('chumps_round%d/%s/find_kinesin/sorted_map_full_hidf.spi',chumps_round,job_dir);
  writeSPIDERfile(output_file,sorted_map_hidf);

  tmap = sorted_map_composite;
  tmap(:,2:2:s(2)) = 0;
  output_file = sprintf('chumps_round%d/%s/find_kinesin/sorted_map_full_composite_even.spi',chumps_round,job_dir);
  writeSPIDERfile(output_file,tmap);

  tmap = sorted_map_composite;
  tmap(:,1:2:s(2)) = 0;
  output_file = sprintf('chumps_round%d/%s/find_kinesin/sorted_map_full_composite_odd.spi',chumps_round,job_dir);
  writeSPIDERfile(output_file,tmap);

  dimer_map = find_dimers(sorted_map_composite.*seam_mask, seam_mask);
  lat_dimer_map = find_lateral_dimers(sorted_map_composite.*seam_mask, seam_mask);
  clean_monomer_map = find_monomer(sorted_map_composite.*seam_mask, seam_mask);
  output_file = sprintf('chumps_round%d/%s/find_kinesin/sorted_map_full_composite_dimer.spi',chumps_round,job_dir);
  writeSPIDERfile(output_file,dimer_map);
  output_file = sprintf('chumps_round%d/%s/find_kinesin/sorted_map_full_composite_lat_dimer.spi',chumps_round,job_dir);
  writeSPIDERfile(output_file,lat_dimer_map);
  output_file = sprintf('chumps_round%d/%s/find_kinesin/sorted_map_full_composite_monomer.spi',chumps_round,job_dir);
  writeSPIDERfile(output_file,clean_monomer_map);

  outmat(:,4) = sum(sorted_map_hidf(:,1:2:s(2)),2)/(s(2)/2);
  outmat(:,5) = sum(sorted_map_hidf(:,2:2:s(2)),2)/(s(2)/2);
  outmat(:,6) = sum(sorted_map_composite(:,1:2:s(2)),2)/(s(2)/2);
  outmat(:,7) = sum(sorted_map_composite(:,2:2:s(2)),2)/(s(2)/2);
end

output_file = sprintf('chumps_round%d/%s/find_kinesin/decoration_plot.txt',chumps_round,job_dir);
dlmwrite(output_file,outmat,' ');

output_file = sprintf('chumps_round%d/%s/find_kinesin/decoration_plot.txt',chumps_round,job_dir);
dlmwrite(output_file,outmat,' ');
