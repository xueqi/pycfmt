function chumps_align_focal_pairs(micrograph,filter_resolution,n_iterations,...
                        angle_spec,scale_spec,rot_bin_factor,scale_bin_factor,supersample)

run 'cf-parameters.m'

micrograph_pixel_size = scanner_pixel_size*10000/target_magnification;

angle_offset = angle_spec(1);
if(prod(size(angle_spec)) > 1)
  angle_halfw = angle_spec(2);
else
  angle_halfw = 1;
endif

if(prod(size(angle_spec)) > 2)
  n_angle = angle_spec(3);
else
  n_angle = 11;
endif

scale_offset = scale_spec(1);
if(prod(size(scale_spec)) > 1)
  scale_halfw = scale_spec(2);
else
  scale_halfw = 0.1;
endif

if(prod(size(scale_spec)) > 2)
  n_scale = scale_spec(3);
else
  n_scale = 11;
endif

%%%%%%%%%%%%%%%%%%
% Derive related filenames
%%%%%%%%%%%%%%%%%%

if(micrograph(1:6) != 'scans/')
  micrograph = sprintf('scans/%s',micrograph);
end
focal_mate = sprintf('%s_focal_mate.mrc',micrograph(1:prod(size(micrograph))-4));
focal_mate_align = sprintf('%s_focal_mate_align.mrc',micrograph(1:prod(size(micrograph))-4))

ctf_doc_name = sprintf('%s_ctf_doc.spi',micrograph(1:prod(size(micrograph))-4));
fmate_ctf_doc_name = sprintf('%s_focal_mate_ctf_doc.spi',micrograph(1:prod(size(micrograph))-4));

%%%%%%%%%%%%%%%%%%
% Read in parameter files and extract relevant data
%%%%%%%%%%%%%%%%%%

ctf_doc = dlmread(ctf_doc_name);

index = find(ctf_doc(:,1) == 1);
if(prod(size(index)) == 0)
  fprintf(stdout,'ERROR: no CTF information found... (please run chumps_ctf first)\n');
  return
end

index = index(prod(size(index)));
defocus = ctf_doc(index,3);
astig_mag = ctf_doc(index,4);
astig_angle = ctf_doc(index,5);

ctf_doc = dlmread(fmate_ctf_doc_name);

index = find(ctf_doc(:,1) == 1);
if(prod(size(index)) == 0)
  fprintf(stdout,'ERROR: no CTF information found... (please run chumps_ctf first)\n');
  return
end

index = index(prod(size(index)));
fmate_defocus = ctf_doc(index,3);
fmate_astig_mag = ctf_doc(index,4);
fmate_astig_angle = ctf_doc(index,5);

printf('Defocus for micrograph: %7.0f ; for focal mate: %7.0f\n',defocus,fmate_defocus);

b_factor = 0;

electron_lambda = EWavelength(accelerating_voltage);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Read micrographs
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

m1 = ReadMRC(micrograph);
m2 = ReadMRC(focal_mate);
s = size(m1);

if(s(1) != s(2))
  if(s(1) < s(2))
    big_dim = s(2);
  else
    big_dim = s(1);
  end
else
  big_dim = s(1)
end

s_supersample = supersample*big_dim;
if(defocus > 0)
  ctf_flip1 = sign(CTF_ACR(big_dim, micrograph_pixel_size, ...
                     electron_lambda, defocus/10000, ...
                     spherical_aberration_constant, ...
                     b_factor, amplitude_contrast_ratio, 0));
  ctf_flip2 = sign(CTF_ACR(big_dim, micrograph_pixel_size, ...
                     electron_lambda, fmate_defocus/10000, ...
                     spherical_aberration_constant, ...
                     b_factor, amplitude_contrast_ratio, 0));
  ctf_flip1 = ifftshift(ctf_flip1);
  ctf_flip2 = ifftshift(ctf_flip2);
else
  fprintf(stdout, 'ERROR: ctf information not found...\n');
end

if(s(1) != s(2))
  m1 = pad_gently(m1, [big_dim big_dim], [1 1], [1 1], 1);
  m2 = pad_gently(m2, [big_dim big_dim], [1 1], [1 1], 1);

  m1 = real(ifftn(ctf_flip1.*fftn(m1)));
  m2_flip = real(ifftn(ctf_flip2.*fftn(m2)));  
else
  m1 = real(ifftn(ctf_flip1.*fftn(m1)));
  m2_flip = real(ifftn(ctf_flip2.*fftn(m2)));
end

[best_angle,best_scale,dx,dy] = ...
  align_focal_pairs(m1,m2_flip,micrograph_pixel_size,...
                        filter_resolution,n_iterations,angle_spec,scale_spec,
                        rot_bin_factor,scale_bin_factor,supersample)

best_m2rot = rotate2d(m2, -best_angle);
if(supersample == 1)
  m2super = best_mtrot;
else
  m2super = scale2d(best_m2rot,supersample);
end
<<<<<<< HEAD
size(m2super)
dx, dy
best_scale
m2super = TransformImage(m2super, 0, best_scale);
m2super = circshift(m2super, [supersample*dx supersample*dy]);
best_m2align = m2super(1:supersample:s_supersample,1:supersample:s_supersample);
best_m2align = real(ifftn(fftn(best_m2align).*FourierShift(big_dim,[1/supersample 1/supersample])));
if(s(1) != s(2))
  best_m2align = best_m2align(1:s(1),1:s(2));
end
mat_file = sprintf('%s_focal_align_pair.mat',micrograph(1:prod(size(micrograph))-4))
save ('-6', mat_file, 'dx','dy','best_angle', 'best_scale')
WriteMRC(best_m2align,micrograph_pixel_size,focal_mate_align);
