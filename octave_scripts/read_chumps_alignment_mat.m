function [coords, phi, theta, psi, ...
          directional_psi,est_repeat_distance] = ...
   read_chumps_alignment_mat(mat_file, helical_repeat_distance)

radon_scale=1;
%%%%%%%%%%%%%%%%%%
% Read in parameter files and extract relevant data
%%%%%%%%%%%%%%%%%%

load(mat_file);


need_to_skip = 0;
if(isempty(coords_est_smooth))
  need_to_skip = 1;
end

if(need_to_skip == 1)
% if(fopen(good_align_doc_name) <= 0)
  num_pfs = 0;
  num_starts = 0;
  coords = [];
  phi = [];
  theta = [];
  psi = [];
  directional_psi= 0;
  est_repeat_distance = 0;
  return
else
%  fclose(good_align_doc_name);
end

%good_align_doc = readSPIDERdoc_dlmlike(good_align_doc_name,1);
%n_good_boxes = size(good_align_doc);
%n_good_boxes = n_good_boxes(1);
%%%%%%%%%%%%%%%
% Read in particle coordinates
%  and estimate the true repeat spacing
%%%%%%%%%%%%%%%

tilt_scale_avg = 0;
n_tilt_scale_avg = 0;
n_good_boxes = prod(size(psi_est_smooth));
for j=1:n_good_boxes
  eulers = [psi_est_smooth(j) theta_est_smooth(j) phi_est_smooth(j)];
  coords(j,:) = coords_est_smooth(j, :);
  psi(j) = eulers(1);
  theta(j) = eulers(2);
  phi(j) = eulers(3);

  tilt_scale_avg = tilt_scale_avg + 1/sin(theta(j)*pi/180);
  n_tilt_scale_avg = n_tilt_scale_avg + 1;
end

tilt_scale_avg = tilt_scale_avg/n_tilt_scale_avg;
est_repeat_distance = helical_repeat_distance/(radon_scale*tilt_scale_avg);

%%%%%%%%%%%%%%
% The following line is correct, but commented out for backwards compatibility for now
% est_repeat_distance = helical_repeat_distance/radon_scale * tilt_scale_avg;

directional_psi = 90; % All psi values are the same; either 90 or 270

return

function out_dir = trim_dir(job_dir)

if(job_dir(prod(size(job_dir))) == '/')
  job_dir=job_dir(1:prod(size(job_dir))-1);
end
[start] = regexp(job_dir,'[^/]*$','start');
out_dir = job_dir(start:prod(size(job_dir)));

return
