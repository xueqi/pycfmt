if(nargin < 3)
  fprintf('PROGRAMMING ERROR: wrong number of arguments to octave script! (%d)\n', nargin);
%  exit(2)
end

focal_mate = str2num(argv(){1});
chumps_round = str2num(argv(){2});
bin = str2num(argv(){3});
fit_order = str2num(argv(){4});
data_redundancy = str2num(argv(){5});
max_outliers_per_window = str2num(argv(){6});
twist_tolerance = str2num(argv(){7});
coord_error_tolerance = str2num(argv(){8});
phi_error_tolerance = str2num(argv(){9});
theta_error_tolerance = str2num(argv(){10});
psi_error_tolerance = str2num(argv(){11});
min_seg_length = str2num(argv(){12});
smooth_dir = argv(){13};

num_args = 13;

num_mts = nargin - num_args;

%%%%%%%%%%%%%%%%%%%%%%%
% Read user parameter file
%%%%%%%%%%%%%%%%%%%%%%%

run 'cf-parameters.m';

micrograph_pixel_size = scanner_pixel_size*10000/target_magnification

for mt_num = 1: num_mts
%%%%%%%%%%%%%%%
% Read in filenames, etc.
%%%%%%%%%%%%%%%

  job_dir = argv(){mt_num+num_args};

  if(job_dir(prod(size(job_dir))) == '/')
    job_dir=job_dir(1:prod(size(job_dir))-1);
  end
  [start] = regexp(job_dir,'[^/]*$','start');
  job_dir = job_dir(start:prod(size(job_dir)));

  [micrograph,defocus,astig_mag,astig_angle,b_factor,find_dir] = ...
    chumps_micrograph_info(job_dir,focal_mate,chumps_round, 0);

%%%%%%%%%%%%%%%%%%
% Get alignment and micrograph info
%%%%%%%%%%%%%%%%%%

  com_doc_name = sprintf('chumps_round%d/ref_tub_subunit_bin%d/tub_cgr_doc.spi',chumps_round,bin);
  com_info = dlmread(com_doc_name);
  index = find(com_info(:,1) == 1);
  ref_com = com_info(index(1),3:5);

  est_repeat_distance = helical_repeat_distance;
  est_pix_per_repeat = est_repeat_distance/micrograph_pixel_size;

  [num_pfs,num_starts] = ...
    read_chumps_alignment(job_dir,helical_repeat_distance,chumps_round,micrograph_pixel_size);
  [coords, phi, theta, psi, directional_psi,...
    est_repeat_distance] = ...
    read_chumps_alignment_mat(sprintf('%s/%s_fil_info.mat', smooth_dir, job_dir),helical_repeat_distance);

  est_pix_per_repeat = est_repeat_distance/micrograph_pixel_size;

  load(sprintf('%s/%s_fil_info.mat', smooth_dir, job_dir));
  coords = coords_est_smooth;
  phi = phi_est_smooth;
  theta = theta_est_smooth;
  psi = psi_est_smooth;

  n_good_boxes = prod(size(phi));

 [coords_dummy,phi_dummy,theta_dummy,psi_dummy,...
   d_phi_d_repeat,d_theta_d_repeat,d_psi_d_repeat] = ...
    fixup_chumps_alignment(coords,phi,theta,psi,90,...
                           micrograph_pixel_size,helical_repeat_distance,...
                          est_pix_per_repeat, num_pfs, num_starts, ref_com);

%%%%%%%%%%%%%%%%%%
% Read in decoration map
%%%%%%%%%%%%%%%%%%

  sites_per_repeat = 2*num_pfs;
  num_repeats = 1;
  kin_lib_size = 2*num_repeats*num_pfs;

  elastic_params = [0 0 0 0];
  radius_scale_factor = 1;

  map_file = sprintf('%s/tot_decorate_map.spi',find_dir);
  tot_decorate_map = readSPIDERfile(map_file);

  pad_sort_map = 3;
  full_sorted_map = zeros([num_pfs 2*(2*pad_sort_map+n_good_boxes)]);

  [box_repeat_origin, kin_repeat_index, kin_pf_index,...
   x_tub, y_tub, phi_tub, theta_tub, psi_tub] = ...
    get_findkin_refs(1, 0, [n_good_boxes n_good_boxes 1], num_pfs,num_starts,...
                   phi, theta, psi, elastic_params,...
                   d_phi_d_repeat,d_theta_d_repeat,d_psi_d_repeat,...
                   est_repeat_distance,radius_scale_factor,ref_com);

[box_bounds, tot_bounds, sorted_indices] = ...
  map_to_helimap(tot_decorate_map, tot_decorate_map, 1, [1 n_good_boxes], 1, ...
                 num_pfs, sites_per_repeat, kin_repeat_index, kin_pf_index);

  full_sorted_map(sorted_indices) = tot_decorate_map(box_bounds(1):box_bounds(2));
  save(sprintf('chumps_round%d/%s/find_kinesin/sorted_indices.mat',chumps_round,job_dir), 'sorted_indices', 'box_bounds');

  sorted_map_file = sprintf('%s/sorted_map_full.spi',find_dir);
  writeSPIDERfile(sorted_map_file,full_sorted_map);

  fprintf(stdout,'\n%s\n', job_dir);

end % for mt_num ...

return
