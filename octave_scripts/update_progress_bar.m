function update_progress_bar(ind, n, n_initial_points, n_increments, process_id)

persistent time_points = [];
persistent initial_time_points = [];

if(nargin < 3)
  n_initial_points = 3;
end

if(nargin < 4)
  n_increments = 10;
end

if(nargin < 5)
  process_id = 1;
end

if(isempty(time_points))
  initial_time_points = zeros([process_id n_initial_points + 1]);
  time_points = zeros([process_id n_increments + 1]);
else
  sz = size(time_points);
  if(process_id > sz(1))
    if(n_increments > sz(2))
      max_sz = n_increments;
    else
      max_sz = sz(2);
    end
    temp_t_pts = time_points;
    time_points = zeros([process_id max_sz]);
    time_pts(1:sz(1), 1:sz(2)) = temp_t_pts;

    sz = size(initial_time_points);
    if(n_initial_points > sz(2))
      max_sz = n_initial_points;
    else
      max_sz = sz(2);
    end
    temp_t_pts = initial_time_points;
    initial_time_points = zeros([process_id max_sz]);
    initial_time_points(1:sz(1), 1:sz(2)) = temp_t_pts;
  end
end

which_increment = floor(ind/n * n_increments);
if(ind > 1)
  prev_increment = floor( (ind-1)/n * n_increments);
else
  prev_increment = 0;
end

%%%%%%%%%%%%%%%%%%%%%%%%%
% The "starting gun" is ind = 0:
%%%%%%%%%%%%%%%%%%%%%%%%%

if(ind == 0 || initial_time_points(process_id, 1) == 0)
  initial_time_points(process_id, 1) = time();
  time_points(process_id, 1) = time();
%  printf('Progress:\n');
end

%%%%%%%%%%%%%%%%%%%%%%%%%
% The initial period, before the first big increment:
%%%%%%%%%%%%%%%%%%%%%%%%%
if(which_increment < 1 && ind >= 1 && ind <= n_initial_points)
  initial_time_points(process_id, ind+1) = time();
  fprintf(stdout, '       %4d / %-6d %8.1f s\n', ind, n, ...
         initial_time_points(process_id, ind+1) - initial_time_points(process_id, 1));
end

if(which_increment == 1 && which_increment != prev_increment)
%  printf('\n');
end

if(which_increment >= 1 && which_increment != prev_increment)
  time_points(process_id, which_increment+1) = time();
  progressbar = which_increment / n_increments;
  fprintf(stdout, '%3.0f%% ; %4d / %-6d %8.1f s\n', 100*progressbar, ...
         ind, n, ...
         time_points(process_id, which_increment+1) - time_points(process_id, 1));
  if(which_increment == n_increments)
    printf('\n');
  end
  if(isoctave)
    fflush(stdout);
  end
end
