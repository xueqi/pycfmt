function found_kinesins(target_box_num,job_dir,filter_resolution,highpass_resolution,use_ctf,focal_mate,...
                        chumps_round,smooth_dir,bin_factor,b_factor)

find_monomer = 1;

job_dir = trim_dir(job_dir);

%mkdir(sprintf('chumps_round1/%s/find_kinesin/',job_dir));
%mkdir(sprintf('chumps_round1/%s/fmate_find/',job_dir));

stdout = 1;

ref_dir = sprintf('chumps_round%d/ref_tub_subunit_bin%d',chumps_round,bin_factor);
ref_params_name = sprintf('%s/ref_params.spi',ref_dir);
com_doc_name = sprintf('%s/tub_cgr_doc.spi',ref_dir);
tub_ref_file = sprintf('%s/tub_centered_vol.spi',ref_dir);

ref_dir = sprintf('chumps_round%d/ref_kin_subunit_bin%d',chumps_round,bin_factor);
%kin_ref_file = sprintf('%s/ref_tot.spi',ref_dir);
kin_ref_file = sprintf('%s/kin_centered_vol.spi',ref_dir);

tub_ref_vol = readSPIDERfile(tub_ref_file);
kin_ref_vol = readSPIDERfile(kin_ref_file);

num_mts = 1;

%%%%%%%%%%%%%%%%%%%%%%%
% Read user parameter file
%%%%%%%%%%%%%%%%%%%%%%%

if(isoctave)
  run 'cf-parameters.m';
else
  run 'cf-parameters';
end

elastic_params = [0 0 0 0];
radius_scale_factor = 1;

micrograph_pixel_size = scanner_pixel_size*10000/target_magnification

tot_n_particles = 1;
scale_avg = 0;
n_scale_avg = 0;
tilt_scale_avg = 0;
n_tilt_scale_avg = 0;

tot_n_particles = 1;

for mt_num = 1: num_mts

%%%%%%%%%%%%%%%%%%
% Get alignment and micrograph info
%%%%%%%%%%%%%%%%%%

  [micrograph,defocus,astig_mag,astig_angle] = ...
	       chumps_micrograph_info(job_dir,focal_mate,chumps_round, use_ctf);

  [num_pfs,num_starts] = ...
    read_chumps_alignment(job_dir,helical_repeat_distance,chumps_round,micrograph_pixel_size);

  sites_per_repeat = 2*num_pfs;
  max_overlap = 2*num_pfs;

  num_repeats = ceil( (sites_per_repeat+2*max_overlap)/num_pfs );

  tub_num_repeats = 2*num_repeats; % make the underlying MT extra long
  kin_num_repeats = tub_num_repeats;   % make the underlying MT extra long

  kin_lib_size = 2*kin_num_repeats*num_pfs;

  ref_dir = sprintf('chumps_round%d/ref_tub_subunit_bin%d',chumps_round,bin_factor);
  com_doc_name = sprintf('%s/tub_cgr_doc.spi',ref_dir);
  com_info = readSPIDERdoc_dlmlike(com_doc_name);
%  com_info = dlmread(com_doc_name);
  index = find(com_info(:,1) == 1);
  ref_com = com_info(index(1),3:5);

  ref_pixel = dlmread(sprintf('%s/info.txt',ref_dir));
  ref_pixel = ref_pixel(2);

%  ref_params_name = sprintf('%s/ref_params.spi',ref_dir);
%  ref_pixel_info = dlmread(ref_params_name);
%  ref_pixel_info = readSPIDERdoc_dlmlike(ref_params_name);
%  index = find(ref_pixel_info(:,1) == 2);
%  ref_pixel = ref_pixel_info(index(1),5);
%  index = find(ref_pixel_info(:,1) == 1);
%  min_theta = ref_pixel_info(index(1),5);
%  index = find(ref_pixel_info(:,1) == 3);
%  d_angle = ref_pixel_info(index(1),3);

  bin_factor = round(ref_pixel/micrograph_pixel_size);
  final_pixel_size = micrograph_pixel_size*bin_factor;

  box_dim = ceil(1.5*2*filament_outer_radius/final_pixel_size);
  box_dim = 16*ceil(box_dim/16);
  box_dim = box_dim*bin_factor;
  box_dim = [box_dim box_dim];

  ref_dim = floor(box_dim/bin_factor);

% TEMPORARY!!
  pad_ref_dim = 4*ref_dim;

  [start] = regexp(job_dir,'_MT[0-9]+[_][0-9]+$','start');

  if(focal_mate == 0)
    find_dir = sprintf('chumps_round%d/%s/find_kinesin',chumps_round, job_dir);
    micrograph = sprintf('scans/%s.mrc',job_dir(1:start-1));

    if(find_monomer)
      sorted_map = readSPIDERfile(sprintf('chumps_round%d/%s/find_kinesin/sorted_map_full_clean.spi', chumps_round, job_dir));
    else
      sorted_map = readSPIDERfile(sprintf('chumps_round%d/%s/find_kinesin/sorted_map_full_dimer.spi',chumps_round,job_dir));
    end
  else
    find_dir = sprintf('chumps_round%d/%s/fmate_find',chumps_round,job_dir);
%    micrograph = sprintf('scans/%s_focal_mate_align.mrc',job_dir(1:start-1));
    micrograph = sprintf('scans/%s.mrc',job_dir(1:start-1));

    if(find_monomer)
      sorted_map = readSPIDERfile(sprintf('chumps_round%d/%s/find_kinesin/sorted_map_full_hidf_clean.spi',chumps_round,job_dir));
    else
      sorted_map = readSPIDERfile(sprintf('chumps_round%d/%s/find_kinesin/sorted_map_full_hidf_dimer.spi',chumps_round, job_dir));
    end
  end

%    micrograph = sprintf('scans/%s.mrc',job_dir(1:start-1));

%%%%%%%%%%%%%%%
% Get CTF info
%%%%%%%%%%%%%%%
  if(use_ctf ~= 0)
    electron_lambda = EWavelength(accelerating_voltage);
    ctf = CTF_ACR(pad_ref_dim(1), ref_pixel, electron_lambda, defocus/10000, ...
                     spherical_aberration_constant, ...
                     b_factor, amplitude_contrast_ratio, 0);
    ctf = ifftshift(ctf);

   box_ctf = CTF_ACR(2*box_dim(1), micrograph_pixel_size, electron_lambda, defocus/10000, ...
                     spherical_aberration_constant, ...
                     b_factor, amplitude_contrast_ratio, 0);
  box_ctf = ifftshift(box_ctf);
  else
    ctf = -ones(pad_ref_dim);
  box_ctf = -ones(2*box_dim);
  end

  ctf_flip = sign(ctf);
box_ctf_flip = sign(box_ctf);

  mic_dim = ReadMRCheader(micrograph);

%%%%%%%%%%%%%%%
% Pre-compute filtering terms
%%%%%%%%%%%%%%%
  filter = ifftn(ones(pad_ref_dim));
  if(filter_resolution > 0)
    lp_freq = 0.5*(2*ref_pixel) / filter_resolution;
    delta_lp_freq = lp_freq/10;
    filter = SharpFilt(filter,lp_freq,delta_lp_freq);

    hp_freq = 0.5*(2*ref_pixel) / highpass_resolution;
    delta_hp_freq = hp_freq/10;
    filter = SharpHP(filter,hp_freq,delta_hp_freq);
  end
  filter = fftn(filter);

% highpass_resolution = 200;
% highpass_resolution = 1e9;
filter_resolution = 2*micrograph_pixel_size;

box_filter = ifftn(ones(2*box_dim));
lp_freq = 0.5*(2*micrograph_pixel_size) / filter_resolution;
delta_lp_freq = lp_freq/10;
box_filter = SharpFilt(box_filter,lp_freq,delta_lp_freq);

hp_freq = 0.5*(2*micrograph_pixel_size) / highpass_resolution;
delta_hp_freq = hp_freq/10;
box_filter = SharpHP(box_filter,hp_freq,delta_hp_freq);

box_filter = fftn(box_filter);

%  [num_pfs,num_starts,coords, phi, theta, psi, directional_psi,...
%    est_repeat_distance] = ...
%    read_chumps_alignment(job_dir,helical_repeat_distance,1,micrograph_pixel_size);
%
%  est_pix_per_repeat = est_repeat_distance/micrograph_pixel_size;
%
%  [coords,phi,theta,psi,...
%   d_phi_d_repeat,d_theta_d_repeat,d_psi_d_repeat,est_repeat_distance] = ...
%    fixup_chumps_alignment(coords,phi,theta,psi,directional_psi,...
%                         micrograph_pixel_size,helical_repeat_distance,...
%                         est_pix_per_repeat, num_pfs, num_starts, ref_com);

  load(sprintf('%s/%s_fil_info.mat', smooth_dir, job_dir));
  coords = coords_est_smooth;
  phi = phi_est_smooth;
  theta = theta_est_smooth;
  psi = psi_est_smooth;

  est_repeat_distance = helical_repeat_distance;
  est_pix_per_repeat = est_repeat_distance/micrograph_pixel_size;

  [coords_dummy,phi_dummy,theta_dummy,psi_dummy,...
   d_phi_d_repeat,d_theta_d_repeat,d_psi_d_repeat] = ...
      fixup_chumps_alignment(coords,phi,theta,psi,90,...
                             micrograph_pixel_size,helical_repeat_distance,...
                             est_pix_per_repeat, num_pfs, num_starts, ref_com);

  n_good_boxes = prod(size(phi));

  if(isoctave)
    radon_scale = dlmread(sprintf('chumps_round%d/%s/radon_scale_doc.spi',1, job_dir));
  else
    radon_scale = readSPIDERdoc_dlmlike(sprintf('chumps_round%d/%s/radon_scale_doc.spi',1, job_dir));
  end

  index = find(radon_scale(:,1) == 1);
  radon_scale = radon_scale(index(prod(size(index))),3);
  scale_avg = scale_avg + radon_scale;
  n_scale_avg = n_scale_avg + 1;

%%%%%%%%%%%%%%%%%%
% Read in decoration map, using the "cleaned" sorted maps
%%%%%%%%%%%%%%%%%%
  tot_decorate_map_temp = readSPIDERfile(sprintf('%s/tot_decorate_map.spi',find_dir));

%%%%%%%%%%%%
% The following hack allows us to make synthetic images with found dimers only
%   tot_decorate_map_temp = readSPIDERfile(sprintf('%s/tot_decorate_map_0110.spi',find_dir));
% tot_decorate_map_temp
%%%%%%%%%%%%

  [box_repeat_origin, kin_repeat_index, kin_pf_index,...
   x_tub, y_tub, phi_tub, theta_tub, psi_tub] = ...
    get_findkin_refs(1, 0, [n_good_boxes n_good_boxes 1], num_pfs,num_starts,...
                   phi, theta, psi, elastic_params,...
                   d_phi_d_repeat,d_theta_d_repeat,d_psi_d_repeat,...
                   est_repeat_distance,radius_scale_factor,ref_com);

  [box_bounds, tot_bounds, sorted_indices] = ...
    map_to_helimap(tot_decorate_map_temp, tot_decorate_map_temp, 1, [1 n_good_boxes], 1, ...
                   num_pfs, sites_per_repeat, kin_repeat_index, kin_pf_index);

  tot_decorate_map = zeros(size(tot_decorate_map_temp));
  tot_decorate_map(box_bounds(1):box_bounds(2)) = sorted_map(sorted_indices);

%  writeSPIDERfile('tot_decorate_map_regenerate.spi', tot_decorate_map);

%  map_file = sprintf('%s/tot_decorate_map.spi',find_dir);
%  map_file = 'tot_decorate_map.spi';
%  tot_decorate_map = readSPIDERfile(map_file);
%  sorted_map_file = sprintf('%s/sorted_map_composite.spi',find_dir);
%  sorted_map_file = sprintf('%s/sorted_map_full.spi',find_dir);
%  sorted_map = readSPIDERfile(sorted_map_file);

%  s1 = size(tot_decorate_map);
%  s2 = size(sorted_map);
%  sort_map_pad = (s2(2) - s1(2))/2

  tot_dec_0110_file = sprintf('chumps_round%d/%s/find_kinesin/tot_decorate_map_0110.spi', chumps_round, job_dir);
  if(exist(tot_dec_0110_file) == 2)
      tot_dec_0110 = readSPIDERfile(tot_dec_0110_file);
      tot_dec_0110(find(tot_dec_0110(:))) = 1;
  else
      tot_dec_0110 = []
  end

  fprintf(stdout,'\n%s\n', job_dir);

  real_box_num = 0;
  for box_num=1:n_good_boxes

    fprintf(stdout,'\nBox %4d/%4d : \n', box_num, n_good_boxes);
    if(isoctave)
      fflush(stdout);
    end


%if(box_num > 0 && box_num < 1000)
if(box_num == target_box_num || target_box_num == 0)
    ref_pixel = micrograph_pixel_size * bin_factor;  % not used

    [box_repeat_origin, kin_repeat_index, kin_pf_index,...
     x_tub, y_tub, phi_tub, theta_tub, psi_tub, phi_mt, theta_mt, psi_mt, ref_tub, ref_kin] = ...
      get_findkin_refs(box_num, ref_dim,[tub_num_repeats kin_num_repeats],...
                   num_pfs,num_starts,...
                   phi, theta, psi, elastic_params,...
                   d_phi_d_repeat,d_theta_d_repeat,d_psi_d_repeat,...
                   est_repeat_distance,radius_scale_factor,ref_com,...
                   tub_ref_vol,kin_ref_vol,ctf,ctf_flip,filter,...
                   ref_pixel);

    decorate = zeros([kin_lib_size 1]);
    [box_bounds, tot_bounds] = ...
        box_to_tot_findmap(decorate, [1; kin_num_repeats], ...
                        box_repeat_origin(1), ...
                        tot_decorate_map, box_num, sites_per_repeat);

    decorate(box_bounds(1):box_bounds(2)) = tot_decorate_map(tot_bounds(1):tot_bounds(2));

    final_map = ref_tub;
    for j=1:kin_lib_size
      final_map = final_map + decorate(j)*ref_kin(:,:,j);
    end

    outfile = sprintf('%s/find_%04d_5.spi',find_dir,box_num);
    writeSPIDERfile(outfile,final_map);

    if(prod(size(tot_dec_0110)) > 0)
      decorate(box_bounds(1):box_bounds(2)) = tot_dec_0110(tot_bounds(1):tot_bounds(2));

      final_map = ref_tub;
      for j=1:kin_lib_size
        final_map = final_map + decorate(j)*ref_kin(:,:,j);
      end

      outfile = sprintf('%s/find_%04d_6.spi',find_dir,box_num);
      writeSPIDERfile(outfile,final_map);
      outfile = sprintf('%s/find_%04d_7.spi',find_dir,box_num);
      writeSPIDERfile(outfile,rotate2d(final_map,-psi(box_num)));
    end


end % if(box_num == 40)

end % for box_num...

end % for mt_num ...

return

