% 

% DCTRESAMP2D Change the sampling rate of an image.

%   OUT = DCTRESAMP2D(INP, M) upsamples the columns and the rows of

%   matrix INP at times M the original sample rate using the

%   sinc-interpolation in DCT domain. The rows and the columns of the

%   output matrix OUT are times M the length of INP.

%

%   OUT = DCTRESAMP2D(INP, M, N) is the same as above except the rows of  

%   output matrix OUT are times M and the columns times N the original

%   sample rate.

%  

%   OUT = DCTRESAMP2D(INP, M, N, P) downsamples the rows of INP after

%   the times M upsampling of INP. The columns of OUT are upsampled times 

%   N.  

%

%   OUT = DCTREAMP2D(INP, M, N, P, Q) is the same as above except the

%   columns of INP are downsampled times Q after the times N upsampling. 

%   The rows of matrix INP are resampled times M/P and the columns of INP 

%   are resampled times N/Q the original sample rate.

% 

%   Downsampling is performed by the nearest neighbor interpolation. An

%   anti-aliasing filter is not applied.

%

%   The length of the rows and the columns of INP must be a power of

%   two. M, N, P and Q must be integers greater than zero. 

%

%   See also DCTRESAMP



% Copyright (c) 2002 by Antti Happonen, happonea@cs.tut.fi



function out = dctresamp2d(varargin)

  

  % Check input parameters:

  if nargin == 2 

    inp = varargin{1};

    M = varargin{2};

    N = varargin{2};

    P = 1;

    Q = 1;

  elseif nargin == 3

    inp = varargin{1};

    M = varargin{2};

    N = varargin{3};

    P = 1;

    Q = 1;

  elseif nargin == 4

    inp = varargin{1};

    M = varargin{2};

    N = varargin{3};

    P = varargin{4};

    Q = 1;

  elseif nargin == 5

    inp = varargin{1};

    M = varargin{2};

    N = varargin{3};

    P = varargin{4};

    Q = varargin{5};

  else

    error(' Invalid number of the input parameters!');

  end



  inp = double(inp);

  % Sinc interpolation (zooming) in DCT domain for the columns and rows: 

  if M > 1 | N > 1

    inp = fastinterp(inp, M, N, 1, 1, 0, 0, 0);

  end

  % Nearest neighbor downsampling for the rows:

  if P > 1

    inp = inp(:, 1:P:end);

  end

  % Nearest neighbor downsampling for the columns:

  if Q > 1

    inp = inp(1:Q:end, :);

  end

    

  out = inp;

  
